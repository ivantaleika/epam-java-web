<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
table, th, td {
	border: 1px solid black
}
</style>
<meta charset="UTF-8">
<title>Gems XML parsing page</title>
</head>
<body>
	<h2>Parse Gems XML document</h2>
	<form method="post" enctype="multipart/form-data">
		<fieldset>
			<legend>XML parsing</legend>
			Upload XML file: <br>
			<input type="file" name="xmlFile" accept="application/xml" required>
			<br> <br> Choose parser type: <br>
			<input type="radio" name="apiType" value="dom" checked>
			DOM <br>
			<input type="radio" name="apiType" value="sax">
			SAX <br>
			<input type="radio" name="apiType" value="stax">
			StAX <br> <br>
			<input type="submit" value="Parse">
		</fieldset>
	</form>
	<p style="font-size: 120%; font-weight: bold">
		<c:out value="${message}"></c:out>
	</p>
	<c:choose>
		<c:when test="${isValid == true}">
			<table style="width: 100%">
				<tr>
					<th rowspan="2">Id</th>
					<th rowspan="2">Name</th>
					<th rowspan="2">Preciousness</th>
					<th rowspan="2">Value</th>
					<th colspan="3">Visual parameters</th>
					<th rowspan="2">Origin</th>
					<th rowspan="2">Find date</th>
					<th rowspan="2">Create company</th>
					<th rowspan="2">Create date</th>
					<th rowspan="2">Create method</th>
				</tr>
				<tr>
					<th>Color</th>
					<th>Transparence</th>
					<th>Facet</th>
				</tr>
				<c:forEach items="${gems}" var="gem">
					<tr>
						<td><c:out value="${gem.id}"></c:out></td>
						<td><c:out value="${gem.name}"></c:out></td>
						<td><c:out value="${gem.preciousness.value}"></c:out></td>
						<td><c:out value="${gem.value}"></c:out></td>
						<td><c:out value="${gem.visualParameters.color}"></c:out></td>
						<td><c:out value="${gem.visualParameters.transparence}"></c:out>%</td>
						<td><c:out
								value="${gem.visualParameters.facet}(${gem.visualParameters.facetNumber})"></c:out></td>
						<c:choose>
							<c:when
								test="${gem.getClass().name == 'main.java.org.epam.webparsing.xml.gem.NaturalGem'}">
								<td><c:out value="${gem.origin}"></c:out></td>
								<td><c:out value="${gem.findDate}"></c:out></td>
							</c:when>
							<c:otherwise>
								<td>-</td>
								<td>-</td>
							</c:otherwise>
						</c:choose>
						<c:choose>
							<c:when
								test="${gem.getClass().name == 'main.java.org.epam.webparsing.xml.gem.SyntheticGem'}">
								<td><c:out value="${gem.createCompany}"></c:out></td>
								<td><c:out value="${gem.createDate}"></c:out></td>
								<td><c:out value="${gem.createMethod}"></c:out></td>
							</c:when>
							<c:otherwise>
								<td>-</td>
								<td>-</td>
								<td>-</td>
							</c:otherwise>
						</c:choose>
					</tr>
				</c:forEach>
			</table>
		</c:when>

		<c:when test="${isValid == false}">
			<table style="width: 100%">
				<tr>
					<th>Level</th>
					<th>Message</th>
				</tr>
				<c:forEach items="${errors}" var="error">
					<tr>
						<c:choose>
							<c:when test="${error.type == 'WARNING'}">
								<td style="color: yellow"><c:out value="${error.type}"></c:out></td>
							</c:when>
							<c:when test="${error.type == 'ERROR'}">
								<td style="color: red"><c:out value="${error.type}"></c:out></td>
							</c:when>
							<c:when test="${error.type == 'FATAL'}">
								<td style="color: maroon"><c:out value="${error.type}"></c:out></td>
							</c:when>
						</c:choose>

						<td><c:out value="${error.message}"></c:out></td>
				</c:forEach>
			</table>
		</c:when>
	</c:choose>
	<h2>Load XSD</h2>
	<a href="xml/gemsSchema.xsd" name=schema download="schema.xsd">Download</a>
</body>
</html>