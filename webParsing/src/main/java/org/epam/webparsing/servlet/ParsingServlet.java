package main.java.org.epam.webparsing.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.xml.sax.SAXException;

import main.java.org.epam.webparsing.exception.XMLParsingException;
import main.java.org.epam.webparsing.xml.builder.Builder;
import main.java.org.epam.webparsing.xml.builder.BuilderFactory;
import main.java.org.epam.webparsing.xml.builder.BuilderType;
import main.java.org.epam.webparsing.xml.validator.XMLError;
import main.java.org.epam.webparsing.xml.validator.XMLErrorHandler;
import main.java.org.epam.webparsing.xml.validator.XMLValidator;

/**
 * XML parsing WEB page root servlet.
 */
@WebServlet(value = "/parsing")
@MultipartConfig
public class ParsingServlet extends HttpServlet {
    /**
     * Unique ID.
     */
    private static final long serialVersionUID = 7767357210131134578L;

    /**
     * Servlet's logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();
    /**
     * XSD Path.
     */
    private static final String XSD_PATH = "xml/gemsSchema.xsd";
    /**
     * JSP parsing page path.
     */
    private static final String JSP_PATH = "/parsing.jsp";

    /**
     * {@inheritDoc}
     */
    @Override
    protected void doGet(final HttpServletRequest req,
            final HttpServletResponse resp)
            throws ServletException, IOException {
        req.getRequestDispatcher(JSP_PATH).forward(req, resp);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void doPost(final HttpServletRequest req,
            final HttpServletResponse resp)
            throws ServletException, IOException {
        if (ServletFileUpload.isMultipartContent(req)) {
            try {
                final Part filePart = req.getPart("xmlFile");
                if (processValidation(filePart, req)) {
                    processParsing(filePart, req);
                }
            } catch (final Exception e) {
                LOGGER.error(e.getMessage());
                throw e;
            }
        }
        req.getRequestDispatcher(JSP_PATH).forward(req, resp);
    }

    /**
     * Send received file on validation and process results.
     *
     * @param  filePart         received file
     * @param  req              HTTP request
     * @return                  {@code true} if document is valid agains XSD,
     *                          {@code false} otherwise
     * @throws ServletException If XSD is not well formed
     * @throws IOException      if error occurs with file resources
     */
    private boolean processValidation(final Part filePart,
            final HttpServletRequest req) throws ServletException, IOException {
        final XMLErrorHandler errorHandler = new XMLErrorHandler();
        try {
            XMLValidator.validateAgainstXSD(filePart.getInputStream(),
                    getServletContext().getResourceAsStream(XSD_PATH),
                    errorHandler);
        } catch (final XMLParsingException e) {
            throw new ServletException("Invalid XSD: " + e.getMessage());
        } catch (final SAXException e) {
            //Error is cached into the list, no need to handle the exception
        }
        final List<XMLError> errors = errorHandler.getErrors();
        if (errors.isEmpty()) {
            req.setAttribute("isValid", true);
            return true;
        } else {
            req.setAttribute("isValid", false);
            req.setAttribute("message", "Document is not valid");
            req.setAttribute("errors", errors);
            return false;
        }
    }

    /**
     * Send received file on parsing and process results.
     *
     * @param  filePart         received file
     * @param  req              HTTP request
     * @throws ServletException If XML is invalid against XSD or not well formed
     * @throws IOException      if error occurs with file resources
     */
    private void processParsing(final Part filePart,
            final HttpServletRequest req) throws ServletException, IOException {
        req.setAttribute("message", filePart.getSubmittedFileName());
        final Builder builder = BuilderFactory.createBuilder(
                BuilderType.getEnumValue(req.getParameter("apiType")));
        try {
            builder.buildGems(filePart.getInputStream());
        } catch (final XMLParsingException e) {
            throw new ServletException("XML parsing error: " + e.getMessage());
        }
        req.setAttribute("gems", builder.getGems());
    }
}
