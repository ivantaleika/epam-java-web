package main.java.org.epam.webparsing.xml.builder;

import java.io.InputStream;
import java.util.List;

import main.java.org.epam.webparsing.exception.XMLParsingException;
import main.java.org.epam.webparsing.xml.gem.Gem;

/**
 * Provides interface for {@link Gem} class builders.
 */
public interface Builder {
    /**
     * Build {@code List} of {@link Gem}s from XML file.
     *
     * @param  xml                 source file
     * @throws XMLParsingException if error occurs during XML parsing
     */
    void buildGems(InputStream xml) throws XMLParsingException;

    /**
     * @return {@code List} of {@link Gem}s received from XML file with
     *         {@link Builder#buildGems(InputStream)} invocation or {@code null}
     *         if {@link Builder#buildGems(InputStream)} wasn't invoked.
     */
    List<Gem> getGems();
}
