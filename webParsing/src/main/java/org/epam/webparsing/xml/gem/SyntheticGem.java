package main.java.org.epam.webparsing.xml.gem;

import java.beans.JavaBean;
import java.time.LocalDate;


/**
 * Represents a gemstone that was created synthetically.
 */
@JavaBean
public class SyntheticGem extends Gem {
    /**
     * The name of company that created the gem.
     */
    private String createCompany;
    /**
     * The date when gem was created.
     */
    private LocalDate createDate;
    /**
     * The name of the method by which gem was created.
     */
    private String createMethod;

    /**
     * @return the createCompany
     */
    public String getCreateCompany() {
        return createCompany;
    }

    /**
     * @param gemCreateCompany the createCompany to set
     */
    public void setCreateCompany(final String gemCreateCompany) {
        createCompany = gemCreateCompany;
    }

    /**
     * @return the createDate
     */
    public LocalDate getCreateDate() {
        return createDate;
    }

    /**
     * @param gemCreateDate the createDate to set
     */
    public void setCreateDate(final LocalDate gemCreateDate) {
        createDate = gemCreateDate;
    }

    /**
     * @return the createMethod
     */
    public String getCreateMethod() {
        return createMethod;
    }

    /**
     * @param gemCreateMethod the createMethod to set
     */
    public void setCreateMethod(final String gemCreateMethod) {
        createMethod = gemCreateMethod;
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result;
        if (createCompany != null) {
            result += createCompany.hashCode();
        }
        result = prime * result;
        if (createDate != null) {
            result += createDate.hashCode();
        }
        result = prime * result;
        if (createMethod != null) {
            result += createMethod.hashCode();
        }
        return result;
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (!(obj instanceof SyntheticGem)) {
            return false;
        }
        SyntheticGem other = (SyntheticGem) obj;
        if (createCompany == null) {
            if (other.createCompany != null) {
                return false;
            }
        } else if (!createCompany.equals(other.createCompany)) {
            return false;
        }
        if (createDate == null) {
            if (other.createDate != null) {
                return false;
            }
        } else if (!createDate.equals(other.createDate)) {
            return false;
        }
        if (createMethod == null) {
            if (other.createMethod != null) {
                return false;
            }
        } else if (!createMethod.equals(other.createMethod)) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public String toString() {
        return "SyntheticGem [name=" + getName() + ", value=" + getValue()
                + ", preciousness=" + getPreciousness() + ", visualParameters="
                + getVisualParameters() + ", createCompany=" + createCompany
                + ", createDate=" + createDate + ", createMethod="
                + createMethod + "]";
    }

}
