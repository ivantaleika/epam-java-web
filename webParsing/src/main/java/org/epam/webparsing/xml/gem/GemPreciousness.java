package main.java.org.epam.webparsing.xml.gem;

import java.util.HashMap;
import java.util.Map;

/**
 * Contains Gem's XML precious tag possible values.
 */
public enum GemPreciousness {

    /**
     * The precious value constant.
     */
    PRECIOUS("precious"),
    /**
     * The semi-precious value constant.
     */
    SEMIPRECIOUS("semi-precious");

    /**
     * String representation for the constant value.
     */
    private String value;

    /**
     * Contains String representations with corresponding enum values.
     */
    private static final Map<String, GemPreciousness> VALUES_MAP;

    static {
        VALUES_MAP = new HashMap<>();
        for (GemPreciousness enumValue : GemPreciousness.values()) {
            VALUES_MAP.put(enumValue.value, enumValue);
        }
    }

    /**
     * Constructs XML precious constant with the given string value.
     *
     * @param precious the value to set.
     */
    GemPreciousness(final String precious) {
        value = precious;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param  precious constant's value
     * @return          returns enum's constant that corresponds to given
     *                  {@code precious}, or {@code null} if it's not present.
     */
    public static GemPreciousness getEnumValue(final String precious) {
        return VALUES_MAP.get(precious);
    }
}
