package main.java.org.epam.webparsing.xml.validator;

import java.util.LinkedList;
import java.util.List;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXParseException;

/**
 * Processing XML validation errors by appending them to the error List.
 */
public class XMLErrorHandler implements ErrorHandler {

    /**
     * List of errors during the validation.
     */
    private List<XMLError> errors;

    /**
     * Constructs default handler.
     */
    public XMLErrorHandler() {
        errors = new LinkedList<>();
    }

    /**
     * Receive notification of a warning. Add {@link XMLError} with WARNING
     * level to the error list.
     */
    @Override
    public void warning(final SAXParseException exception) {
        getErrors().add(
                new XMLError(XMLErrorLevel.WARNING, exception.getMessage()));
    }

    /**
     * Receive notification of a error. Add {@link XMLError} with ERROR level to
     * the error list.
     */
    @Override
    public void error(final SAXParseException exception) {
        getErrors()
                .add(new XMLError(XMLErrorLevel.ERROR, exception.getMessage()));

    }

    /**
     * Receive notification of a fatal error. Add {@link XMLError} with FATAL
     * level to the error list and re-throw the exception.
     */
    @Override
    public void fatalError(final SAXParseException exception)
            throws SAXParseException {
        getErrors()
                .add(new XMLError(XMLErrorLevel.FATAL, exception.getMessage()));
        throw exception;
    }

    /**
     * @return the errors
     */
    public List<XMLError> getErrors() {
        return errors;
    }

}
