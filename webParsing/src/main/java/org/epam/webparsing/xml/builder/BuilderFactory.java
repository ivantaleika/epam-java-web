package main.java.org.epam.webparsing.xml.builder;

import java.security.InvalidParameterException;


/**
 * Factory for {@code Gem} {@link Builder}s.
 */
public final class BuilderFactory {

    /**
     * Prevents from utility class creation.
     */
    private BuilderFactory() {
    }

    /**
     * Constructs {@link Builder} accordingly to the {@link BuilderType}.
     *
     * @param  type the builder's type
     * @return      {@link Builder} accordingly to the {@link BuilderType}
     */
    public static Builder createBuilder(final BuilderType type) {
        switch (type) {
        case DOM:
            return new DOMBuilder();
        case SAX:
            return new SAXBuilder();
        case STAX:
            return new StAXBuilder();
        default:
            throw new InvalidParameterException(
                    "Can't create bilder for " + type + " parameter.");
        }
    }
}
