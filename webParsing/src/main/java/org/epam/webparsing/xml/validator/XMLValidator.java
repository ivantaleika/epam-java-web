package main.java.org.epam.webparsing.xml.validator;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;

import main.java.org.epam.webparsing.exception.XMLParsingException;

/**
 * Validates XML file against a {@linkplain Schema}.
 */
public final class XMLValidator {

    /**
     * Prevents from utility class creation.
     */
    private XMLValidator() {
    }

    /**
     * Validates XML file against XSD.
     *
     * @param  xml                 file InputStream to validate
     * @param  xsd                 file InputStream to validate against
     * @param  handler             Processes the validation result, if
     *                             {@code null}, {@link SAXException} will be
     *                             throws if XML file is invalid
     * @throws XMLParsingException if a SAX error occurs during XSD parsing.
     * @throws IOException         if error occurs with file resources
     * @throws SAXException        throws when validating error occurs if
     *                             {@code handler} is {@code null}, otherwise
     *                             only if error is fatal.
     */
    public static void validateAgainstXSD(final InputStream xml,
            final InputStream xsd, final XMLErrorHandler handler)
            throws SAXException, IOException, XMLParsingException {
        SchemaFactory factory = SchemaFactory
                .newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema;
        try {
            schema = factory.newSchema(new StreamSource(xsd));
        } catch (SAXException e) {
            throw new XMLParsingException(e.getMessage());
        }
        Validator validator = schema.newValidator();
        validator.setErrorHandler(handler);
        validator.validate(new StreamSource(xml));
    }
}
