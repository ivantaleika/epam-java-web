package main.java.org.epam.webparsing.xml.builder;

import java.io.InputStream;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import main.java.org.epam.webparsing.exception.XMLParsingException;
import main.java.org.epam.webparsing.xml.gem.Gem;
import main.java.org.epam.webparsing.xml.gem.GemPreciousness;
import main.java.org.epam.webparsing.xml.gem.NaturalGem;
import main.java.org.epam.webparsing.xml.gem.SyntheticGem;
import main.java.org.epam.webparsing.xml.gem.VisualParameters;

/**
 * Build {@link Gem}s values from XML using StAX parser type.
 */
public class StAXBuilder implements Builder {
    /**
     * StAX parser's factory.
     */
    private final XMLInputFactory factory;
    /**
     * Contains result of the last {@link StAXBuilder#buildGems(InputStream)}
     * invocation.
     */
    private List<Gem> gemList;

    /**
     * Constructs default StAXBuilder.
     */
    public StAXBuilder() {
        factory = XMLInputFactory.newInstance();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void buildGems(final InputStream xml) throws XMLParsingException {
        gemList = new LinkedList<>();
        XMLStreamReader reader = null;
        try {
            reader = factory.createXMLStreamReader(xml);
            while (reader.hasNext()) {
                if (reader.next() == XMLStreamConstants.START_ELEMENT) {
                    final String tagName = reader.getLocalName();
                    if (GemXML
                            .getEnumValue(tagName) == GemXML.NATURAL_GEM_TAG) {
                        gemList.add(buildNaturalGem(reader));
                    } else if (GemXML.getEnumValue(
                            tagName) == GemXML.SYNTHETIC_GEM_TAG) {
                        gemList.add(buildSyntheticGem(reader));
                    }
                }
            }
        } catch (final XMLStreamException e) {
            throw new XMLParsingException("StAX builder: " + e.getMessage(), e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (final XMLStreamException e) {
                    throw new RuntimeException(
                            "Impossible to close StAX input stream: "
                                    + e.getMessage(),
                            e);
                }
            }
        }
    }

    /**
     * Builds a {@link NaturalGem}.
     *
     * @param  reader              {@link XMLStreamReader} that is on the
     *                             naturalGem XML node.
     * @return                     the NaturalGem object
     * @throws XMLParsingException if XML file is invalid against XSD
     * @throws XMLStreamException  if fatal error occurred during the parsing.
     */
    private NaturalGem buildNaturalGem(final XMLStreamReader reader)
            throws XMLStreamException, XMLParsingException {
        final NaturalGem gem = new NaturalGem();
        fillGemInfo(gem, reader);
        while (reader.hasNext()) {
            switch (reader.next()) {
            case XMLStreamConstants.START_ELEMENT:
                switch (GemXML.getEnumValue(reader.getLocalName())) {
                case ORIGIN_TAG:
                    gem.setOrigin(getTextData(reader));
                    break;
                case FIND_DATE_TAG:
                    gem.setFindDate(LocalDate.parse(getTextData(reader)));
                    break;
                default:
                    break;
                }
                break;
            case XMLStreamConstants.END_ELEMENT:
                if (GemXML.getEnumValue(
                        reader.getLocalName()) == GemXML.NATURAL_GEM_TAG) {
                    return gem;
                }
                break;
            default:
                break;
            }
        }
        throw new XMLParsingException("XML file is invalid against XSD");
    }

    /**
     * Builds a {@link SyntheticGem}.
     *
     * @param  reader              {@link XMLStreamReader} that is on the
     *                             syntheticGem XML node.
     * @return                     the SyntheticGem object
     * @throws XMLParsingException if XML file is invalid against XSD
     * @throws XMLStreamException  if fatal error occurred during the parsing.
     */
    private SyntheticGem buildSyntheticGem(final XMLStreamReader reader)
            throws XMLStreamException, XMLParsingException {
        final SyntheticGem gem = new SyntheticGem();
        fillGemInfo(gem, reader);
        while (reader.hasNext()) {
            switch (reader.next()) {
            case XMLStreamConstants.START_ELEMENT:
                switch (GemXML.getEnumValue(reader.getLocalName())) {
                case CREATE_COMPANY_TAG:
                    gem.setCreateCompany(getTextData(reader));
                    break;
                case CREATE_DATE_TAG:
                    gem.setCreateDate(LocalDate.parse(getTextData(reader)));
                    break;
                case CREATE_METHOD_TAG:
                    gem.setCreateMethod(getTextData(reader));
                    break;
                default:
                    break;
                }
                break;
            case XMLStreamConstants.END_ELEMENT:
                if (GemXML.getEnumValue(
                        reader.getLocalName()) == GemXML.SYNTHETIC_GEM_TAG) {
                    return gem;
                }
                break;
            default:
                break;
            }
        }
        throw new XMLParsingException("XML file is invalid against XSD");
    }

    /**
     * Fills {@link Gem}'s info.
     *
     * @param  gem                 the Gem or its subclass
     * @param  reader              {@link XMLStreamReader} that is on the first
     *                             tag of syntheticGem or naturalGem XML nodes.
     * @throws XMLParsingException if XML file is invalid against XSD
     * @throws XMLStreamException  if fatal error occurred during the parsing.
     */
    private void fillGemInfo(final Gem gem, final XMLStreamReader reader)
            throws XMLStreamException, XMLParsingException {
        gem.setId(reader.getAttributeValue(null,
                GemXML.ID_ATTRIBUTE.getXmlValue()));
        while (reader.hasNext()) {
            switch (reader.next()) {
            case XMLStreamConstants.START_ELEMENT:
                switch (GemXML.getEnumValue(reader.getLocalName())) {
                case NAME_TAG:
                    gem.setName(getTextData(reader));
                    break;
                case PRECIOUSNESS_TAG:
                    gem.setPreciousness(
                            GemPreciousness.getEnumValue(getTextData(reader)));
                    break;
                case VALUE_TAG:
                    gem.setValue(Double.parseDouble(getTextData(reader)));
                    break;
                case VISUAL_PARAMETERS_TAG:
                    gem.setVisualParameters(buildVisualParameters(reader));
                    return;
                default:
                    break;
                }
                break;
            default:
                break;
            }
        }
        throw new XMLParsingException("XML file is invalid against XSD");
    }

    /**
     * Builds a {@link VisualParameters}.
     *
     * @param  reader              {@link XMLStreamReader} that is on the
     *                             visualParameters XML node.
     * @return                     the VisualParameters object
     * @throws XMLParsingException if XML file is invalid against XSD
     * @throws XMLStreamException  if fatal error occurred during the parsing.
     */
    private VisualParameters buildVisualParameters(final XMLStreamReader reader)
            throws XMLStreamException, XMLParsingException {
        final VisualParameters parameters = new VisualParameters();
        while (reader.hasNext()) {
            switch (reader.next()) {
            case XMLStreamConstants.START_ELEMENT:
                switch (GemXML.getEnumValue(reader.getLocalName())) {
                case COLOR_TAG:
                    parameters.setColor(getTextData(reader));
                    break;
                case FACET_TAG:
                    final String nFacet = reader.getAttributeValue(null,
                            "nFacet");
                    if (nFacet != null) {
                        parameters.setFacetNumber(Integer.parseInt(nFacet));
                    }
                    parameters.setFacet(getTextData(reader));
                    break;
                case TRANSPARENCE_TAG:
                    parameters.setTransparence(
                            Double.parseDouble(getTextData(reader)));
                    break;
                default:
                    break;
                }
                break;
            case XMLStreamConstants.END_ELEMENT:
                final GemXML tagValue = GemXML
                        .getEnumValue(reader.getLocalName());
                if (tagValue == GemXML.VISUAL_PARAMETERS_TAG) {
                    return parameters;
                }
                break;
            default:
                break;
            }
        }
        throw new XMLParsingException("XML file is invalid against XSD");
    }

    /**
     * Returns text data of XML Element.
     *
     * @param  reader              {@link XMLStreamReader} that is on the XML
     *                             element that contains text data.
     * @return                     element's text data
     * @throws XMLParsingException if XML file is invalid against XSD
     * @throws XMLStreamException  if fatal error occurred during the parsing.
     */
    private String getTextData(final XMLStreamReader reader)
            throws XMLStreamException, XMLParsingException {
        if (reader.hasNext()) {
            reader.next();
            return reader.getText();
        }
        throw new XMLParsingException("XML file is invalid against XSD");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Gem> getGems() {
        return gemList;
    }

}
