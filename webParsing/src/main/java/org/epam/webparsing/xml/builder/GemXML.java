package main.java.org.epam.webparsing.xml.builder;

import java.util.HashMap;
import java.util.Map;

/**
 * Contains gem's XSD tag and attribute values for parsing.
 */
public enum GemXML {
    /**
     * XML root tag.
     */
    GEMS_TAG("gems"),
    /**
     * Tag name for natural gem.
     */
    NATURAL_GEM_TAG("naturalGem"),
    /**
     * Tag name for synthetic gem.
     */
    SYNTHETIC_GEM_TAG("syntheticGem"),
    /**
     * Tag name for gem's name.
     */
    NAME_TAG("name"),
    /**
     * Tag name for gem's preciousness.
     */
    PRECIOUSNESS_TAG("preciousness"),
    /**
     * Tag name for gem's value.
     */
    VALUE_TAG("value"),

    /**
     * Tag name for gem's visual parameters.
     */
    VISUAL_PARAMETERS_TAG("visualParameters"),
    /**
     * Tag name for gem's color name.
     */
    COLOR_TAG("color"),
    /**
     * Tag name for gem's transparence value.
     */
    TRANSPARENCE_TAG("transparence"),
    /**
     * Tag name for gem's facet name.
     */
    FACET_TAG("facet"),
    /**
     * Attribute name for gem's facets amount.
     */
    N_FACET_ATTRIBUTE("nFacet"),

    /**
     * Tag name for natural gem's origin.
     */
    ORIGIN_TAG("origin"),
    /**
     * Tag name for natural gem's find date.
     */
    FIND_DATE_TAG("findDate"),
    /**
     * Tag name for synthetic gem's creating company name.
     */
    CREATE_COMPANY_TAG("createCompany"),
    /**
     * Tag name for synthetic gem's creation date.
     */
    CREATE_DATE_TAG("createDate"),
    /**
     * Tag name for synthetic gem's creation method name.
     */
    CREATE_METHOD_TAG("createMethod"),

    /**
     * Attribute name for gem's ID.
     */
    ID_ATTRIBUTE("id");

    /**
     * XML tag or attribute value.
     */
    private String xmlValue;

    /**
     * Contains xml string values with corresponding enum values.
     */
    private static final Map<String, GemXML> VALUES_MAP;

    static {
        VALUES_MAP = new HashMap<>();
        for (GemXML enumValue : GemXML.values()) {
            VALUES_MAP.put(enumValue.xmlValue, enumValue);
        }
    }

    /**
     * Constructs gem's XML parsing constant with the given string value.
     *
     * @param xmlStringValue the xmlValue to set.
     */
    GemXML(final String xmlStringValue) {
        xmlValue = xmlStringValue;
    }

    /**
     * @return the xmlValue
     */
    public String getXmlValue() {
        return xmlValue;
    }

    /**
     * @param  xmlStringValue string value from gem's xml
     * @return                returns enum value that corresponds to given
     *                        {@code xmlStringValue}
     */
    public static GemXML getEnumValue(final String xmlStringValue) {
        return VALUES_MAP.get(xmlStringValue);
    }
}
