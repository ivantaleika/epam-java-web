package main.java.org.epam.webparsing.xml.gem;

import java.beans.JavaBean;

/**
 * Contains visual gem's parameters.
 */
@JavaBean
public class VisualParameters {
    /**
     * Gem's color.
     */
    private String color;
    /**
     * Gem's transparence.
     */
    private double transparence;
    /**
     * Gem's facet name.
     */
    private String facet;
    /**
     * Gem's number of facets.
     */
    private int facetNumber;

    /**
     * Constructs empty object.
     */
    public VisualParameters() {
    }

    /**
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * @param gemColor the color to set
     */
    public void setColor(final String gemColor) {
        color = gemColor;
    }

    /**
     * @return the transparence
     */
    public double getTransparence() {
        return transparence;
    }

    /**
     * @param gemTransparence the transparence to set
     */
    public void setTransparence(final double gemTransparence) {
        transparence = gemTransparence;
    }

    /**
     * @return the facet
     */
    public String getFacet() {
        return facet;
    }

    /**
     * @param gemFacet the facet to set
     */
    public void setFacet(final String gemFacet) {
        facet = gemFacet;
    }

    /**
     * @return the facetNumber
     */
    public int getFacetNumber() {
        return facetNumber;
    }

    /**
     * @param gemFacetNumber the facetNumber to set
     */
    public void setFacetNumber(final int gemFacetNumber) {
        facetNumber = gemFacetNumber;
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        final int magicNumber = 32;
        int result = 1;
        result = prime * result;
        if (color != null) {
            result += color.hashCode();
        }
        result = prime * result;
        if (facet != null) {
            result += facet.hashCode();
        }
        result = prime * result + facetNumber;
        long temp;
        temp = Double.doubleToLongBits(transparence);
        result = prime * result + (int) (temp ^ (temp >>> magicNumber));
        return result;
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof VisualParameters)) {
            return false;
        }
        VisualParameters other = (VisualParameters) obj;
        if (color == null) {
            if (other.color != null) {
                return false;
            }
        } else if (!color.equals(other.color)) {
            return false;
        }
        if (facet == null) {
            if (other.facet != null) {
                return false;
            }
        } else if (!facet.equals(other.facet)) {
            return false;
        }
        if (facetNumber != other.facetNumber) {
            return false;
        }
        if (Double.doubleToLongBits(transparence) != Double
                .doubleToLongBits(other.transparence)) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "VisualParameters [color=" + color + ", transparence="
                + transparence + ", facet=" + facet + ", facetNumber="
                + facetNumber + "]";
    }

}
