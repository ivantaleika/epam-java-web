package main.java.org.epam.webparsing.xml.validator;

import java.beans.JavaBean;

/**
 * Represents an XML validation error.
 */
@JavaBean
public class XMLError {

    /**
     * The error type.
     */
    private XMLErrorLevel type;
    /**
     * The error message.
     */
    private String message;

    /**
     * Constructs empty error.
     */
    public XMLError() {
    }

    /**
     * Constructs an error with given {@code errorType} and
     * {@code errorMessage}.
     *
     * @param errorType    the {@code type} to set
     * @param errorMessage the {@code message} to set
     */
    public XMLError(final XMLErrorLevel errorType, final String errorMessage) {
        setType(errorType);
        message = errorMessage;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param errorMessage the message to set
     */
    public void setMessage(final String errorMessage) {
        message = errorMessage;
    }

    /**
     * @return the type
     */
    public XMLErrorLevel getType() {
        return type;
    }

    /**
     * @param errorType the type to set
     */
    public void setType(final XMLErrorLevel errorType) {
        type = errorType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result;
        if (message != null) {
            result += message.hashCode();
        }
        result = prime * result;
        if (type != null) {
            result += type.hashCode();
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof XMLError)) {
            return false;
        }
        final XMLError other = (XMLError) obj;
        if (message == null) {
            if (other.message != null) {
                return false;
            }
        } else if (!message.equals(other.message)) {
            return false;
        }
        if (type != other.type) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "XMLError [type=" + type + ", message=" + message + "]";
    }

}
