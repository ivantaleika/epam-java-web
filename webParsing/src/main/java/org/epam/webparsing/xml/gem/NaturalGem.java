package main.java.org.epam.webparsing.xml.gem;

import java.beans.JavaBean;
import java.time.LocalDate;


/**
 * Represents a gemstone that was created naturally.
 */
@JavaBean
public class NaturalGem extends Gem {
    /**
     * The place where gem was found.
     */
    private String origin;
    /**
     * The date when gem was found.
     */
    private LocalDate findDate;

    /**
     * @return the origin
     */
    public String getOrigin() {
        return origin;
    }

    /**
     * @param gemOrigin the origin to set
     */
    public void setOrigin(final String gemOrigin) {
        origin = gemOrigin;
    }

    /**
     * @return the findDate
     */
    public LocalDate getFindDate() {
        return findDate;
    }

    /**
     * @param gemFindDate the findDate to set
     */
    public void setFindDate(final LocalDate gemFindDate) {
        findDate = gemFindDate;
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result;
        if (findDate != null) {
            result += findDate.hashCode();
        }
        result = prime * result;
        if (origin != null) {
            result += origin.hashCode();
        }
        return result;
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (!(obj instanceof NaturalGem)) {
            return false;
        }
        NaturalGem other = (NaturalGem) obj;
        if (findDate == null) {
            if (other.findDate != null) {
                return false;
            }
        } else if (!findDate.equals(other.findDate)) {
            return false;
        }
        if (origin == null) {
            if (other.origin != null) {
                return false;
            }
        } else if (!origin.equals(other.origin)) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public String toString() {
        return "NaturalGem [name=" + getName() + ", value=" + getValue()
                + ", preciousness=" + getPreciousness() + ", visualParameters="
                + getVisualParameters() + ", origin=" + origin + ", findDate="
                + findDate + "]";
    }

}
