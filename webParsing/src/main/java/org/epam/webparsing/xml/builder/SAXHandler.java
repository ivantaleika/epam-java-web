package main.java.org.epam.webparsing.xml.builder;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import main.java.org.epam.webparsing.xml.gem.Gem;
import main.java.org.epam.webparsing.xml.gem.GemPreciousness;
import main.java.org.epam.webparsing.xml.gem.NaturalGem;
import main.java.org.epam.webparsing.xml.gem.SyntheticGem;
import main.java.org.epam.webparsing.xml.gem.VisualParameters;

/**
 * Handler for SAX XML parsing method.
 */
public class SAXHandler extends DefaultHandler {

    /**
     * Currently filling {@link Gem}.
     */
    private Gem currentGem;
    /**
     * Currently filling {@link VisualParameters}.
     */
    private VisualParameters currentParameters;
    /**
     * Builds element's text data.
     */
    private StringBuilder currentTextData;
    /**
     * List of {@link Gem}s from the last parsed document.
     */
    private List<Gem> gems;


    /**
     * {@inheritDoc}.
     */
    @Override
    public void startDocument() throws SAXException {
        gems = new LinkedList<>();
        currentTextData = new StringBuilder();
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public void startElement(final String uri, final String localName,
            final String qName, final Attributes attributes)
            throws SAXException {
        GemXML enumValue = GemXML.getEnumValue(localName);
        switch (enumValue) {
        case NATURAL_GEM_TAG:
            currentGem = new NaturalGem();
            break;
        case SYNTHETIC_GEM_TAG:
            currentGem = new SyntheticGem();
            break;
        case VISUAL_PARAMETERS_TAG:
            currentParameters = new VisualParameters();
            break;
        case FACET_TAG:
            if (attributes.getLength() == 1) {
                currentParameters.setFacetNumber(
                        Integer.parseInt(attributes.getValue(0)));
            }
        default:
            break;
        }
        if (enumValue == GemXML.NATURAL_GEM_TAG
                || enumValue == GemXML.SYNTHETIC_GEM_TAG) {
            currentGem.setId(attributes.getValue(0));
        }
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public void characters(final char[] ch, final int start, final int length)
            throws SAXException {
        currentTextData.append(String.valueOf(ch, start, length).trim());
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public void endElement(final String uri, final String localName,
            final String qName) throws SAXException {
        GemXML enumValue = GemXML.getEnumValue(localName);
        if (currentTextData.length() != 0) {
            switch (enumValue) {
            case COLOR_TAG:
                currentParameters.setColor(currentTextData.toString());
                break;
            case CREATE_COMPANY_TAG:
                ((SyntheticGem) currentGem)
                        .setCreateCompany(currentTextData.toString());
                break;
            case CREATE_DATE_TAG:
                ((SyntheticGem) currentGem).setCreateDate(
                        LocalDate.parse(currentTextData.toString()));
                break;
            case CREATE_METHOD_TAG:
                ((SyntheticGem) currentGem)
                        .setCreateMethod(currentTextData.toString());
                break;
            case FACET_TAG:
                currentParameters.setFacet(currentTextData.toString());
                break;
            case FIND_DATE_TAG:
                ((NaturalGem) currentGem).setFindDate(
                        LocalDate.parse(currentTextData.toString()));
                break;
            case NAME_TAG:
                currentGem.setName(currentTextData.toString());
                break;
            case ORIGIN_TAG:
                ((NaturalGem) currentGem).setOrigin(currentTextData.toString());
                break;
            case PRECIOUSNESS_TAG:
                currentGem.setPreciousness(GemPreciousness
                        .getEnumValue(currentTextData.toString()));
                break;

            case TRANSPARENCE_TAG:
                currentParameters.setTransparence(
                        Double.parseDouble(currentTextData.toString()));
                break;
            case VALUE_TAG:
                currentGem.setValue(
                        Double.parseDouble(currentTextData.toString()));
                break;
            default:
                break;
            }
            currentTextData.setLength(0);
        } else {
            if (enumValue == GemXML.NATURAL_GEM_TAG
                    || enumValue == GemXML.SYNTHETIC_GEM_TAG) {
                gems.add(currentGem);
            } else if (enumValue == GemXML.VISUAL_PARAMETERS_TAG) {
                currentGem.setVisualParameters(currentParameters);
            }
        }
    }

    /**
     * @return Parsed list of {@link Gem}s.
     */
    public List<Gem> getGems() {
        return gems;
    }
}
