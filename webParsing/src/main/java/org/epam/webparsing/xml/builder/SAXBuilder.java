package main.java.org.epam.webparsing.xml.builder;

import java.io.InputStream;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import main.java.org.epam.webparsing.exception.XMLParsingException;
import main.java.org.epam.webparsing.xml.gem.Gem;

/**
 * Build {@link Gem}s values from XML using SAX parser type.
 */
public class SAXBuilder implements Builder {
    /**
     * Parsing instrument.
     */
    private final SAXHandler handler;
    /**
     * The parser.
     */
    private SAXParser parser;
    /**
     * Contains result of the last {@link SAXBuilder#buildGems(InputStream)}
     * invocation.
     */
    private List<Gem> gemList;

    /**
     * Constructs default SAXBuilder.
     */
    public SAXBuilder() {
        handler = new SAXHandler();
        try {
            final SAXParserFactory factory = SAXParserFactory.newInstance();
            factory.setNamespaceAware(true);
            parser = factory.newSAXParser();
        } catch (ParserConfigurationException | SAXException e) {
            throw new RuntimeException(
                    "Error during SAXBuilder creation: " + e.getMessage(), e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Gem> getGems() {
        return gemList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void buildGems(final InputStream xml) throws XMLParsingException {
        try {
            parser.parse(xml, handler);
            gemList = handler.getGems();
        } catch (final Exception e) {
            throw new XMLParsingException("SAX builder: " + e.getMessage(), e);
        }
    }

}
