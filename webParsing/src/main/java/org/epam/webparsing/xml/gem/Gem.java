package main.java.org.epam.webparsing.xml.gem;

import java.beans.JavaBean;


/**
 * Represents a precious gem.
 */
@JavaBean
public class Gem {
    /**
     * Gem's ID.
     */
    private String id;
    /**
     * Gem's name.
     */
    private String name;

    /**
     * Gem's value.
     */
    private double value;

    /**
     * Gem's preciousness.
     */
    private GemPreciousness preciousness;
    /**
     * Gem's visual parameters.
     */
    private VisualParameters visualParameters;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param gemId the id to set
     */
    public void setId(final String gemId) {
        id = gemId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param gemName the name to set
     */
    public void setName(final String gemName) {
        name = gemName;
    }

    /**
     * @return the value
     */
    public double getValue() {
        return value;
    }

    /**
     * @param gemValue the value to set
     */
    public void setValue(final double gemValue) {
        value = gemValue;
    }

    /**
     * @return the visualParameters
     */
    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    /**
     * @param parameters the visualParameters to set
     */
    public void setVisualParameters(final VisualParameters parameters) {
        visualParameters = parameters;
    }

    /**
     * @return the preciousness
     */
    public GemPreciousness getPreciousness() {
        return preciousness;
    }

    /**
     * @param gemPreciousness the preciousness to set
     */
    public void setPreciousness(final GemPreciousness gemPreciousness) {
        preciousness = gemPreciousness;
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        final int magicNumber = 32;
        int result = 1;
        result = prime * result;
        if (name != null) {
            result += name.hashCode();
        }
        result = prime * result;
        if (preciousness != null) {
            result += preciousness.hashCode();
        }
        long temp;
        temp = Double.doubleToLongBits(value);
        result = prime * result + (int) (temp ^ (temp >>> magicNumber));
        result = prime * result;
        if (visualParameters != null) {
            result += visualParameters.hashCode();
        }
        return result;
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Gem)) {
            return false;
        }
        Gem other = (Gem) obj;
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (preciousness != other.preciousness) {
            return false;
        }
        if (Double.doubleToLongBits(value) != Double
                .doubleToLongBits(other.value)) {
            return false;
        }
        if (visualParameters == null) {
            if (other.visualParameters != null) {
                return false;
            }
        } else if (!visualParameters.equals(other.visualParameters)) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public String toString() {
        return "Gem [name=" + name + ", value=" + value + ", preciousness="
                + preciousness + ", visualParameters=" + visualParameters + "]";
    }

}
