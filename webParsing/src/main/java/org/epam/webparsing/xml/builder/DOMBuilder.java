package main.java.org.epam.webparsing.xml.builder;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import main.java.org.epam.webparsing.exception.XMLParsingException;
import main.java.org.epam.webparsing.xml.gem.Gem;
import main.java.org.epam.webparsing.xml.gem.GemPreciousness;
import main.java.org.epam.webparsing.xml.gem.NaturalGem;
import main.java.org.epam.webparsing.xml.gem.SyntheticGem;
import main.java.org.epam.webparsing.xml.gem.VisualParameters;

/**
 * Build {@link Gem}s values from XML using DOM parser type.
 */
public class DOMBuilder implements Builder {
    /**
     * DOM parser's factory.
     */
    private final DocumentBuilderFactory factory;
    /**
     * DOM parser's builder.
     */
    private final DocumentBuilder builder;
    /**
     * Contains result of the last {@link DOMBuilder#buildGems(InputStream)}
     * invocation.
     */
    private List<Gem> gemList;

    /**
     * Constructs default DOMBuilder.
     */
    public DOMBuilder() {
        factory = DocumentBuilderFactory.newInstance();
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(
                    "Error during SAXBuilder creation: " + e.getMessage(), e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void buildGems(final InputStream xml) throws XMLParsingException {
        gemList = new LinkedList<>();
        Document document;
        try {
            document = builder.parse(xml);
            document.getDocumentElement().normalize();
            final NodeList gems = document.getDocumentElement().getChildNodes();
            for (int i = 0; i < gems.getLength(); i++) {
                if (gems.item(i).getNodeType() == Node.ELEMENT_NODE) {
                    final Element gemElement = (Element) gems.item(i);
                    final GemXML enumValue = GemXML
                            .getEnumValue(gemElement.getNodeName());
                    if (enumValue == GemXML.NATURAL_GEM_TAG) {
                        gemList.add(buildNaturalGem(gemElement));
                    } else if (enumValue == GemXML.SYNTHETIC_GEM_TAG) {
                        gemList.add(buildSyntheticGem(gemElement));
                    }
                }
            }
        } catch (SAXException | IOException e) {
            throw new XMLParsingException("DOM builder: " + e.getMessage(), e);
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Gem> getGems() {
        return gemList;
    }

    /**
     * Builds a {@link NaturalGem}.
     *
     * @param  element XML Element for the NaturalGem
     * @return         the NaturalGem object
     */
    private NaturalGem buildNaturalGem(final Element element) {
        final NaturalGem gem = new NaturalGem();
        fillGemInfo(gem, element);
        gem.setFindDate(LocalDate.parse(
                element.getElementsByTagName(GemXML.FIND_DATE_TAG.getXmlValue())
                        .item(0).getTextContent()));
        gem.setOrigin(
                element.getElementsByTagName(GemXML.ORIGIN_TAG.getXmlValue())
                        .item(0).getTextContent());
        return gem;

    }

    /**
     * Builds a {@link SyntheticGem}.
     *
     * @param  element XML Element for the SyntheticGem
     * @return         the SyntheticGem object
     */
    private SyntheticGem buildSyntheticGem(final Element element) {
        final SyntheticGem gem = new SyntheticGem();
        fillGemInfo(gem, element);
        gem.setCreateDate(LocalDate.parse(element
                .getElementsByTagName(GemXML.CREATE_DATE_TAG.getXmlValue())
                .item(0).getTextContent()));
        gem.setCreateCompany(element
                .getElementsByTagName(GemXML.CREATE_COMPANY_TAG.getXmlValue())
                .item(0).getTextContent());
        gem.setCreateMethod(element
                .getElementsByTagName(GemXML.CREATE_METHOD_TAG.getXmlValue())
                .item(0).getTextContent());
        return gem;
    }

    /**
     * Fills {@link Gem}'s info.
     *
     * @param gem     the Gem or its subclass
     * @param element XML Element for Gem or its subclass
     */
    private void fillGemInfo(final Gem gem, final Element element) {
        gem.setId(element.getAttribute("id"));
        gem.setName(element.getElementsByTagName(GemXML.NAME_TAG.getXmlValue())
                .item(0).getTextContent());
        gem.setPreciousness(GemPreciousness.getEnumValue(element
                .getElementsByTagName(GemXML.PRECIOUSNESS_TAG.getXmlValue())
                .item(0).getTextContent()));
        gem.setValue(Double.parseDouble(
                element.getElementsByTagName(GemXML.VALUE_TAG.getXmlValue())
                        .item(0).getTextContent()));
        gem.setVisualParameters(
                buildVisualParameters((Element) element
                        .getElementsByTagName(
                                GemXML.VISUAL_PARAMETERS_TAG.getXmlValue())
                        .item(0)));

    }

    /**
     * Builds Gem's {@link VisualParameters}.
     *
     * @param  element XML Element for the VisualParameters
     * @return         the VisualParameters object
     */
    private VisualParameters buildVisualParameters(final Element element) {
        final VisualParameters parameters = new VisualParameters();
        parameters.setColor(
                element.getElementsByTagName(GemXML.COLOR_TAG.getXmlValue())
                        .item(0).getTextContent());
        final Element facetElement = (Element) element
                .getElementsByTagName(GemXML.FACET_TAG.getXmlValue()).item(0);
        parameters.setFacet(facetElement.getTextContent());
        final String nFacet = facetElement
                .getAttribute(GemXML.N_FACET_ATTRIBUTE.getXmlValue());
        if (!nFacet.isEmpty()) {
            parameters.setFacetNumber(Integer.parseInt(nFacet));
        }
        parameters.setTransparence(Double.parseDouble(element
                .getElementsByTagName(GemXML.TRANSPARENCE_TAG.getXmlValue())
                .item(0).getTextContent()));
        return parameters;
    }

}
