package main.java.org.epam.webparsing.xml.validator;

import java.util.HashMap;
import java.util.Map;

/**
 * Contains XML validation error level constants.
 */
public enum XMLErrorLevel {

    /**
     * The warning level constant.
     */
    WARNING("WARNING"),
    /**
     * The error level constant.
     */
    ERROR("ERROR"),
    /**
     * The fatal error level constant.
     */
    FATAL("FATAL");

    /**
     * String representation for the constant value.
     */
    private String level;

    /**
     * Contains String representations with corresponding enum values.
     */
    private static final Map<String, XMLErrorLevel> VALUES_MAP;

    static {
        VALUES_MAP = new HashMap<>();
        for (XMLErrorLevel enumValue : XMLErrorLevel.values()) {
            VALUES_MAP.put(enumValue.level, enumValue);
        }
    }

    /**
     * Constructs XML validation error level constant with the given string
     * error level.
     *
     * @param errorLevel the level to set.
     */
    XMLErrorLevel(final String errorLevel) {
        level = errorLevel;
    }

    /**
     * @return the level
     */
    public String getLevel() {
        return level;
    }

    /**
     * @param  errorLevel the error level
     * @return            returns enum's constant that corresponds to given
     *                    {@code errorLevel}, or {@code null} if it's not
     *                    present.
     */
    public static XMLErrorLevel getEnumValue(final String errorLevel) {
        return VALUES_MAP.get(errorLevel);
    }
}
