package main.java.org.epam.webparsing.xml.builder;

import java.util.HashMap;
import java.util.Map;

/**
 * Contains constants for {@code Gem} {@code Builder}s supported by application.
 */
public enum BuilderType {

    /**
     * Constant for DOM builder.
     */
    DOM("dom"),
    /**
     * Constant for SAX builder.
     */
    SAX("sax"),
    /**
     * Constant for StAX builder.
     */
    STAX("stax");

    /**
     * Constant's String value.
     */
    private String apiType;

    /**
     * Map for quick constant by value searching
     * {@link BuilderType#getEnumValue(String)}.
     */
    private static final Map<String, BuilderType> VALUES_MAP;

    static {
        VALUES_MAP = new HashMap<>();
        for (BuilderType enumValue : BuilderType.values()) {
            VALUES_MAP.put(enumValue.apiType, enumValue);
        }
    }

    /**
     * Constructs enum constant from it String value.
     *
     * @param builderApiType the string value to set
     */
    BuilderType(final String builderApiType) {
        apiType = builderApiType;
    }

    /**
     * @return the apiType
     */
    public String getXmlValue() {
        return apiType;
    }

    /**
     * Returns enum constant according to the string value.
     *
     * @param  builderApiType constant's string value
     * @return                the enum constant according to the string value or
     *                        null if no such constant exists
     */
    public static BuilderType getEnumValue(final String builderApiType) {
        return VALUES_MAP.get(builderApiType);
    }
}
