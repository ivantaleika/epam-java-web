package test.java.org.epam.webparsing.xml.builder;

import java.time.LocalDate;

import org.testng.annotations.DataProvider;

import main.java.org.epam.webparsing.xml.gem.GemPreciousness;
import main.java.org.epam.webparsing.xml.gem.NaturalGem;
import main.java.org.epam.webparsing.xml.gem.SyntheticGem;
import main.java.org.epam.webparsing.xml.gem.VisualParameters;

public class TestBuilderDataProvider {

    public static final String NATURAL_GEM_XML = "testData/naturalGem.xml";
    public static final String SYNTHETIC_GEM_XML = "testData/syntheticGem.xml";
    public static final String NO_N_FACET_GEM_XML = "testData/noNFacetGem.xml";
    public static final String THREE_GEMS_XML = "testData/threeGems.xml";
    public static final String INVALID_XML = "testData/invalid.xml";
    public static final String NOT_WELL_FORMED_XML = "testData/notWellFormed.xml";

    @DataProvider
    public Object[][] naturalGemProvider() {
        NaturalGem gem = new NaturalGem();
        gem.setId("id0");
        gem.setName("emerald");
        gem.setPreciousness(GemPreciousness.PRECIOUS);
        gem.setValue(113);
        VisualParameters parameters = new VisualParameters();
        parameters.setColor("green");
        parameters.setTransparence(20.5);
        parameters.setFacet("octagon");
        parameters.setFacetNumber(24);
        gem.setVisualParameters(parameters);
        gem.setFindDate(LocalDate.parse("2016-02-04"));
        gem.setOrigin("Colombia");
        return new Object[][] {{gem}};
    }

    @DataProvider
    public Object[][] syntheticGemProvider() {
        SyntheticGem gem = new SyntheticGem();
        gem.setId("id4");
        gem.setName("aquamarine");
        gem.setPreciousness(GemPreciousness.SEMIPRECIOUS);
        gem.setValue(20);
        VisualParameters parameters = new VisualParameters();
        parameters.setColor("light blue");
        parameters.setTransparence(92);
        parameters.setFacet("oval");
        parameters.setFacetNumber(58);
        gem.setVisualParameters(parameters);
        gem.setCreateCompany("BASF");
        gem.setCreateDate(LocalDate.parse("2015-08-24"));
        gem.setCreateMethod("Verneuil process");
        return new Object[][] {{gem}};
    }

    @DataProvider
    public Object[][] noNFacetGemProvider() {
        SyntheticGem gem = new SyntheticGem();
        gem.setId("id9");
        gem.setName("opal");
        gem.setPreciousness(GemPreciousness.SEMIPRECIOUS);
        gem.setValue(21);
        VisualParameters parameters = new VisualParameters();
        parameters.setColor("mixed");
        parameters.setTransparence(86);
        parameters.setFacet("drop");
        gem.setVisualParameters(parameters);
        gem.setCreateCompany("Swarovski");
        gem.setCreateDate(LocalDate.parse("2015-01-07"));
        gem.setCreateMethod("Cryochemical synthesis");
        return new Object[][] {{gem}};
    }

    @DataProvider
    public Object[][] threeGemsProvider() {

        return new Object[][] {{naturalGemProvider()[0][0],
                syntheticGemProvider()[0][0], noNFacetGemProvider()[0][0]}};
    }
}
