package test.java.org.epam.webparsing.xml.builder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import main.java.org.epam.webparsing.exception.XMLParsingException;
import main.java.org.epam.webparsing.xml.builder.Builder;
import main.java.org.epam.webparsing.xml.builder.DOMBuilder;
import main.java.org.epam.webparsing.xml.gem.Gem;
import main.java.org.epam.webparsing.xml.gem.NaturalGem;
import main.java.org.epam.webparsing.xml.gem.SyntheticGem;

public class DOMBuilderTest {

    private Builder builder;

    @BeforeClass
    public void init() {
        builder = new DOMBuilder();
    }

    @Test(dataProvider = "naturalGemProvider",
            dataProviderClass = TestBuilderDataProvider.class)
    public void buildGemsNaturalGemTest(NaturalGem expected) {
        try {
            builder.buildGems(Files.newInputStream(
                    Paths.get(TestBuilderDataProvider.NATURAL_GEM_XML)));
        } catch (XMLParsingException | IOException e) {
            Assert.fail(e.getMessage());
        }
        Assert.assertEquals(builder.getGems().get(0), expected);
    }

    @Test(dataProvider = "syntheticGemProvider",
            dataProviderClass = TestBuilderDataProvider.class)
    public void buildGemsSyntheticGemTest(SyntheticGem expected) {
        try {
            builder.buildGems(Files.newInputStream(
                    Paths.get(TestBuilderDataProvider.SYNTHETIC_GEM_XML)));
        } catch (XMLParsingException | IOException e) {
            Assert.fail(e.getMessage());
        }
        Assert.assertEquals(builder.getGems().get(0), expected);
    }

    @Test(dataProvider = "noNFacetGemProvider",
            dataProviderClass = TestBuilderDataProvider.class)
    public void buildGemsNoNFacetGemTest(Gem expected) {
        try {
            builder.buildGems(Files.newInputStream(
                    Paths.get(TestBuilderDataProvider.NO_N_FACET_GEM_XML)));
        } catch (XMLParsingException | IOException e) {
            Assert.fail(e.getMessage());
        }
        Assert.assertEquals(builder.getGems().get(0), expected);
    }

    @Test(dataProvider = "threeGemsProvider",
            dataProviderClass = TestBuilderDataProvider.class)
    public void buildGemsMultipleGemsTest(Gem[] gems) {
        try {
            builder.buildGems(Files.newInputStream(
                    Paths.get(TestBuilderDataProvider.THREE_GEMS_XML)));
        } catch (XMLParsingException | IOException e) {
            Assert.fail(e.getMessage());
        }
        Assert.assertEqualsNoOrder(builder.getGems().toArray(), gems);
    }

    @Test(expectedExceptions = Exception.class)
    public void buildGemsInvalidFileTest() throws XMLParsingException {
        try {
            builder.buildGems(Files.newInputStream(
                    Paths.get(TestBuilderDataProvider.INVALID_XML)));
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test(expectedExceptions = XMLParsingException.class)
    public void buildGemsNotWellFormedFileTest() throws XMLParsingException {
        try {
            builder.buildGems(Files.newInputStream(
                    Paths.get(TestBuilderDataProvider.NOT_WELL_FORMED_XML)));
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }
    }
}
