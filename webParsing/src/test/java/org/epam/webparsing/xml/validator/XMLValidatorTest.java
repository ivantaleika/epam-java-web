package test.java.org.epam.webparsing.xml.validator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import main.java.org.epam.webparsing.exception.XMLParsingException;
import main.java.org.epam.webparsing.xml.validator.XMLError;
import main.java.org.epam.webparsing.xml.validator.XMLErrorHandler;
import main.java.org.epam.webparsing.xml.validator.XMLErrorLevel;
import main.java.org.epam.webparsing.xml.validator.XMLValidator;

public class XMLValidatorTest {

    @Test(expectedExceptions = XMLParsingException.class)
    public void validateAgainstXSDIvalidXSDTest() throws XMLParsingException {
        try {
            XMLValidator.validateAgainstXSD(
                    Files.newInputStream(Paths.get("testData/naturalGem.xml")),
                    Files.newInputStream(Paths.get("testData/naturalGem.xml")),
                    null);
        } catch (SAXException | IOException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test(expectedExceptions = IOException.class)
    public void validateAgainstXSDIvalidXMLPathTest() throws IOException {
        try {
            XMLValidator.validateAgainstXSD(
                    Files.newInputStream(Paths.get("testData/naturalGem1.xml")),
                    Files.newInputStream(
                            Paths.get("WebContent/xml/gemsSchema.xsd")),
                    null);
        } catch (SAXException | XMLParsingException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test(expectedExceptions = IOException.class)
    public void validateAgainstXSDIvalidXSDPathTest() throws IOException {
        try {
            XMLValidator.validateAgainstXSD(
                    Files.newInputStream(Paths.get("testData/naturalGem.xml")),
                    Files.newInputStream(
                            Paths.get("WebContent/xml/gemsSchema1.xsd")),
                    null);
        } catch (SAXException | XMLParsingException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test(expectedExceptions = SAXException.class)
    public void validateAgainstXSDNoErrorHandlerInvalidXMLTest()
            throws SAXException {
        try {
            XMLValidator.validateAgainstXSD(
                    Files.newInputStream(Paths.get("testData/invalid.xml")),
                    Files.newInputStream(
                            Paths.get("WebContent/xml/gemsSchema.xsd")),
                    null);
        } catch (IOException | XMLParsingException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void validateAgainstXSDNoErrorHandlerValidXMLTest() {
        try {
            XMLValidator.validateAgainstXSD(
                    Files.newInputStream(Paths.get("testData/naturalGem.xml")),
                    Files.newInputStream(
                            Paths.get("WebContent/xml/gemsSchema.xsd")),
                    null);
        } catch (IOException | XMLParsingException | SAXException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test(expectedExceptions = SAXException.class)
    public void validateAgainstXSDFatalErrorTest() throws SAXException {
        try {
            XMLValidator.validateAgainstXSD(
                    Files.newInputStream(
                            Paths.get("testData/notWellFormed.xml")),
                    Files.newInputStream(
                            Paths.get("WebContent/xml/gemsSchema.xsd")),
                    new XMLErrorHandler());
        } catch (IOException | XMLParsingException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void validateAgainstXSDInvalidXMLTest() {
        XMLErrorHandler handler = new XMLErrorHandler();
        try {
            XMLValidator.validateAgainstXSD(
                    Files.newInputStream(Paths.get("testData/invalid.xml")),
                    Files.newInputStream(
                            Paths.get("WebContent/xml/gemsSchema.xsd")),
                    handler);
        } catch (IOException | XMLParsingException | SAXException e) {
            Assert.fail(e.getMessage());
        }
        List<XMLError> errors = new LinkedList<>();
        errors.add(new XMLError(XMLErrorLevel.ERROR,
                "cvc-id.2: There are multiple occurrences of ID value 'id0'."));
        errors.add(new XMLError(XMLErrorLevel.ERROR,
                "cvc-attribute.3: The value 'id0' of attribute 'id' on element"
                        + " 'naturalGem' is not valid with respect to"
                        + " its type, 'ID'."));
        errors.add(new XMLError(XMLErrorLevel.ERROR,
                "cvc-datatype-valid.1.2.1: 'big' is not a valid value for"
                        + " 'decimal'."));
        errors.add(new XMLError(XMLErrorLevel.ERROR,
                "cvc-type.3.1.3: The value 'big' of element 'value' is not"
                        + " valid."));
        Assert.assertEquals(handler.getErrors(), errors);
    }

    @Test
    public void validateAgainstXSDValidXMLTest() {
        XMLErrorHandler handler = new XMLErrorHandler();
        try {
            XMLValidator.validateAgainstXSD(
                    Files.newInputStream(Paths.get("testData/naturalGem.xml")),
                    Files.newInputStream(
                            Paths.get("WebContent/xml/gemsSchema.xsd")),
                    handler);
        } catch (IOException | XMLParsingException | SAXException e) {
            Assert.fail(e.getMessage());
        }
        Assert.assertTrue(handler.getErrors().isEmpty());
    }
}
