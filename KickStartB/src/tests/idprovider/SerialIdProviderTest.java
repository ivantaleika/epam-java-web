package tests.idprovider;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.epam.quadrilateral.idprovider.SerialIdProvider;

public class SerialIdProviderTest {
    private long previosId;

    @BeforeClass
    public void init() {
        previosId = SerialIdProvider.getId();
    }

    @Test(invocationCount = 10)
    public void getId() {
        Assert.assertEquals(SerialIdProvider.getId(), previosId + 1);
        previosId++;
    }
}
