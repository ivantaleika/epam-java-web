package tests.geometric.factory;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.epam.quadrilateral.exception.KSException;
import com.epam.quadrilateral.geometric.factory.QuadrilateralFactory;
import com.epam.quadrilateral.geometric.object.Point;
import com.epam.quadrilateral.geometric.object.Quadrilateral;

import tests.geometric.object.PointTest;

public class QuadrilateralFactoryTest {
    public static final String NAME = "name";

    @Test(expectedExceptions = KSException.class,
            dataProvider = "pointProvider", dataProviderClass = PointTest.class)
    public void createInvalideDataSizeTest(Point point) throws KSException {
        QuadrilateralFactory.create(new Point[] {point}, NAME);
    }

    @Test(expectedExceptions = KSException.class, invocationCount = 5,
            dataProvider = "pointProvider", dataProviderClass = PointTest.class)
    public void createCrossedDataTest(Point point) throws KSException {
        Point[] corners = new Point[] {point,
                new Point(-point.getX(), -point.getY()),
                new Point(-point.getX(), point.getY()),
                new Point(point.getX(), -point.getY())};
        QuadrilateralFactory.create(corners, NAME);
    }

    @Test(expectedExceptions = KSException.class, invocationCount = 5,
            dataProvider = "pointProvider", dataProviderClass = PointTest.class)
    public void createPointsOnOneLineTest(Point point) throws KSException {
        Point[] corners = new Point[] {point,
                new Point(point.getX(), point.getY() + 1),
                new Point(point.getX(), point.getY() - 1),
                new Point(-point.getX(), -point.getY())};
        QuadrilateralFactory.create(corners, NAME);
    }

    @Test(dataProvider = "pointProvider", dataProviderClass = PointTest.class,
            invocationCount = 5)
    public void createValidDataTest(Point point) {
        Point[] corners = new Point[] {point,
                new Point(-point.getX(), point.getY()),
                new Point(-point.getX(), -point.getY()),
                new Point(point.getX(), -point.getY())};
        try {
            Assert.assertEquals(QuadrilateralFactory.create(corners, NAME),
                    new Quadrilateral(corners, NAME, 1));
        } catch (KSException e) {
            Assert.fail("Invalid corners input");
        }
    }
}
