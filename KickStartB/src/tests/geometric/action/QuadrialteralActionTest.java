package tests.geometric.action;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.epam.quadrilateral.exception.KSException;
import com.epam.quadrilateral.geometric.action.QuadrilateralAction;
import com.epam.quadrilateral.geometric.factory.QuadrilateralFactory;
import com.epam.quadrilateral.geometric.object.Point;
import com.epam.quadrilateral.geometric.object.Quadrilateral;

public class QuadrialteralActionTest {
    private static final double DELTA = 0.001;

    @Test
    public void calcAreaSimpleSquareTest() {
        Point[] corners = {new Point(1, 1), new Point(1, -1), new Point(-1, -1),
                new Point(-1, 1)};
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = QuadrilateralFactory.create(corners, "name");
        } catch (KSException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertEquals(new QuadrilateralAction().calcArea(quadrilateral),
                2 * 2, DELTA);
    }

    @Test
    public void calcAreaSimpleParallelogramTest() {
        Point[] corners = {new Point(1, 1), new Point(0, -1), new Point(-1, -1),
                new Point(0, 1)};
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = QuadrilateralFactory.create(corners, "name");
        } catch (KSException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertEquals(new QuadrilateralAction().calcArea(quadrilateral),
                2 * 1, DELTA);
    }

    /*
     * Too big area for double number.
     */
    @Test
    public void calcAreaMaxAreaTest() {
        Point[] corners = {new Point(Double.MAX_VALUE, Double.MAX_VALUE),
                new Point(Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(-Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(-Double.MAX_VALUE, Double.MAX_VALUE)};
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = QuadrilateralFactory.create(corners, "name");
        } catch (KSException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertEquals(new QuadrilateralAction().calcArea(quadrilateral),
                Double.NaN, DELTA);
    }

    /*
     * Too small area for Double.
     */
    @Test
    public void calcAreaSmallAreaTest() {
        Point[] corners = {new Point(Double.MIN_VALUE, Double.MIN_VALUE),
                new Point(Double.MIN_VALUE, -Double.MIN_VALUE),
                new Point(-Double.MIN_VALUE, -Double.MIN_VALUE),
                new Point(-Double.MIN_VALUE, Double.MIN_VALUE)};
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = QuadrilateralFactory.create(corners, "name");
        } catch (KSException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertEquals(new QuadrilateralAction().calcArea(quadrilateral),
                Double.NaN, DELTA);
    }

    @Test
    public void calcPerimeterSimpleSquareTest() {
        Point[] corners = {new Point(1, 1), new Point(1, -1), new Point(-1, -1),
                new Point(-1, 1)};
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = QuadrilateralFactory.create(corners, "name");
        } catch (KSException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertEquals(
                new QuadrilateralAction().calcPerimeter(quadrilateral), 2 * 4,
                DELTA);
    }

    @Test
    public void calcPerimeterSimpleParallelogramTest() {
        Point[] corners = {new Point(1, 1), new Point(0, -1), new Point(-1, -1),
                new Point(0, 1)};
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = QuadrilateralFactory.create(corners, "name");
        } catch (KSException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertEquals(
                new QuadrilateralAction().calcPerimeter(quadrilateral),
                2 + 2 * Math.sqrt(5), DELTA);
    }

    @Test
    public void calcPerimeterMaxPerimeterTest() {
        Point[] corners = {new Point(Double.MAX_VALUE, Double.MAX_VALUE),
                new Point(Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(-Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(-Double.MAX_VALUE, Double.MAX_VALUE)};
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = QuadrilateralFactory.create(corners, "name");
        } catch (KSException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertEquals(
                new QuadrilateralAction().calcPerimeter(quadrilateral),
                Double.MAX_VALUE * 8, DELTA);
    }

    @Test
    public void calcPerimeterSmallAreaTest() {
        Point[] corners = {new Point(Double.MIN_VALUE, Double.MIN_VALUE),
                new Point(Double.MIN_VALUE, -Double.MIN_VALUE),
                new Point(-Double.MIN_VALUE, -Double.MIN_VALUE),
                new Point(-Double.MIN_VALUE, Double.MIN_VALUE)};
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = QuadrilateralFactory.create(corners, "name");
        } catch (KSException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertEquals(
                new QuadrilateralAction().calcPerimeter(quadrilateral),
                Double.MIN_VALUE * 8, DELTA);
    }

    @Test
    public void isSquareSimpleSquareTest() {
        Point[] corners = {new Point(1, 1), new Point(1, -1), new Point(-1, -1),
                new Point(-1, 1)};
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = QuadrilateralFactory.create(corners, "name");
        } catch (KSException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertTrue(new QuadrilateralAction().isSquare(quadrilateral));
    }

    @Test
    public void isSquareTurnedSquareTest() {
        Point[] corners = {new Point(0, 1), new Point(1, 0), new Point(0, -1),
                new Point(-1, 0)};
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = QuadrilateralFactory.create(corners, "name");
        } catch (KSException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertTrue(new QuadrilateralAction().isSquare(quadrilateral));
    }

    @Test
    public void isSquareMaxSquareTest() {
        Point[] corners = {new Point(Double.MAX_VALUE, Double.MAX_VALUE),
                new Point(Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(-Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(-Double.MAX_VALUE, Double.MAX_VALUE)};
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = QuadrilateralFactory.create(corners, "name");
        } catch (KSException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertTrue(new QuadrilateralAction().isSquare(quadrilateral));
    }

    @Test
    public void isSquareMinSquareTest() {
        Point[] corners = {new Point(Double.MIN_VALUE, Double.MIN_VALUE),
                new Point(Double.MIN_VALUE, -Double.MIN_VALUE),
                new Point(-Double.MIN_VALUE, -Double.MIN_VALUE),
                new Point(-Double.MIN_VALUE, Double.MIN_VALUE)};
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = QuadrilateralFactory.create(corners, "name");
        } catch (KSException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertTrue(new QuadrilateralAction().isSquare(quadrilateral));
    }

    @Test
    public void isRhombusSimpleRhombusTest() {
        Point[] corners = {new Point(0, 2), new Point(1, 0), new Point(0, -1),
                new Point(-2, 0)};
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = QuadrilateralFactory.create(corners, "name");
        } catch (KSException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertTrue(new QuadrilateralAction().isRhombus(quadrilateral));
    }

    @Test
    public void isRhombusSquareRhombusTest() {
        Point[] corners = {new Point(0, 1), new Point(1, 0), new Point(0, -1),
                new Point(-1, 0)};
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = QuadrilateralFactory.create(corners, "name");
        } catch (KSException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertTrue(new QuadrilateralAction().isRhombus(quadrilateral));
    }

    @Test
    public void isRhombusMaxRhombusTest() {
        Point[] corners = {new Point(Double.MAX_VALUE, Double.MAX_VALUE),
                new Point(Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(-Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(-Double.MAX_VALUE, Double.MAX_VALUE)};
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = QuadrilateralFactory.create(corners, "name");
        } catch (KSException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertTrue(new QuadrilateralAction().isRhombus(quadrilateral));
    }

    @Test
    public void isRhombusMinRhombusTest() {
        Point[] corners = {new Point(Double.MIN_VALUE, Double.MIN_VALUE),
                new Point(Double.MIN_VALUE, -Double.MIN_VALUE),
                new Point(-Double.MIN_VALUE, -Double.MIN_VALUE),
                new Point(-Double.MIN_VALUE, Double.MIN_VALUE)};
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = QuadrilateralFactory.create(corners, "name");
        } catch (KSException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertTrue(new QuadrilateralAction().isRhombus(quadrilateral));
    }

    @Test
    public void isTrapezeSimpleTrapezeTest() {
        Point[] corners = {new Point(1, -1), new Point(-1, -1),
                new Point(0, 0.5), new Point(0.5, 0.5)};
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = QuadrilateralFactory.create(corners, "name");
        } catch (KSException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertTrue(new QuadrilateralAction().isTrapeze(quadrilateral));
    }

    @Test
    public void isTrapezeTurnedTrapezeTest() {
        Point[] corners = {new Point(-1, 1), new Point(1, -1), new Point(2, 1),
                new Point(1, 2)};
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = QuadrilateralFactory.create(corners, "name");
        } catch (KSException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertTrue(new QuadrilateralAction().isTrapeze(quadrilateral));
    }

    /*
     * Too small numbers for angle.
     */
    @Test
    public void isTrapezeMinTrapezeTest() {
        Point[] corners = {new Point(Double.MIN_VALUE, Double.MIN_VALUE),
                new Point(Double.MIN_VALUE, -Double.MIN_VALUE),
                new Point(-Double.MIN_VALUE, -Double.MIN_VALUE),
                new Point(-Double.MIN_VALUE, Double.MIN_VALUE)};
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = QuadrilateralFactory.create(corners, "name");
        } catch (KSException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertFalse(new QuadrilateralAction().isTrapeze(quadrilateral));
    }

    /*
     * Too big numbers for angle.
     */
    @Test
    public void isTrapezeMaxTrapezeTest() {
        Point[] corners = {new Point(Double.MAX_VALUE, Double.MAX_VALUE),
                new Point(Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(-Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(-Double.MAX_VALUE, Double.MAX_VALUE)};
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = QuadrilateralFactory.create(corners, "name");
        } catch (KSException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertFalse(new QuadrilateralAction().isTrapeze(quadrilateral));
    }

    @Test
    public void isConvexSimpleSquareTest() {
        Point[] corners = {new Point(1, -1), new Point(-1, -1),
                new Point(-1, 1), new Point(1, 1)};
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = QuadrilateralFactory.create(corners, "name");
        } catch (KSException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertTrue(new QuadrilateralAction().isConvex(quadrilateral));
    }

    @Test
    public void isConvexConvexFigureTest() {
        Point[] corners = {new Point(1, -1), new Point(-1, -1),
                new Point(-1, 1), new Point(-0.5, -0.5)};
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = QuadrilateralFactory.create(corners, "name");
        } catch (KSException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertFalse(new QuadrilateralAction().isConvex(quadrilateral));
    }

    /*
     * Too small numbers for angle.
     */
    @Test
    public void isTrapezeMinReflexAngleTest() {
        Point[] corners = {new Point(Double.MAX_VALUE, Double.MAX_VALUE),
                new Point(Double.MAX_VALUE - Double.MIN_VALUE, 0),
                new Point(Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(-Double.MAX_VALUE, -Double.MAX_VALUE)};
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = QuadrilateralFactory.create(corners, "name");
        } catch (KSException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertTrue(new QuadrilateralAction().isConvex(quadrilateral));
    }

    /*
     * Too big numbers for angle.
     */
    @Test
    public void isTrapezeMaxReflexAngleTest() {
        Point[] corners = {new Point(-Double.MAX_VALUE, 0),
                new Point(Double.MAX_VALUE, -Double.MIN_VALUE), new Point(0, 0),
                new Point(Double.MAX_VALUE, Double.MIN_VALUE)};
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = QuadrilateralFactory.create(corners, "name");
        } catch (KSException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertTrue(new QuadrilateralAction().isConvex(quadrilateral));
    }

}
