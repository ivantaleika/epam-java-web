package tests.geometric.action;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.epam.quadrilateral.geometric.action.LineAction;
import com.epam.quadrilateral.geometric.object.Line;
import com.epam.quadrilateral.geometric.object.Point;

import tests.geometric.object.LineTest;
import tests.geometric.object.PointTest;

public class LineActionTest {

    @Test(dataProvider = "lineProvider", dataProviderClass = LineTest.class,
            invocationCount = 5)
    public void calcAngleCosParallelTest(Line line) {
        Line parallelLine = new Line(line.getA(), line.getB(), line.getC() + 1);
        Assert.assertEquals(LineAction.calcAngleCos(line, parallelLine),
                Math.cos(0), LineAction.DELTA);
    }

    @Test(dataProvider = "lineProvider", dataProviderClass = LineTest.class,
            invocationCount = 5)
    public void calcAngleCosPerpendicularTest(Line line) {
        Line perpendicularLine = new Line(1 / line.getA(), -1 / line.getB(),
                line.getC());
        Assert.assertEquals(LineAction.calcAngleCos(line, perpendicularLine),
                Math.cos(Math.PI / 2), LineAction.DELTA);
    }

    @Test(dataProvider = "pointProvider", dataProviderClass = PointTest.class,
            invocationCount = 5)
    public void calcAngleCosFirstHalfTest(Point point) {
        double x = point.getX();
        double y = point.getY();
        double cos = LineAction.calcAngleCos(
                new Line(point, new Point(x * 2, y)),
                new Line(point, new Point(x * 2, y + x)));
        Assert.assertEquals(cos, Math.cos(Math.PI / 4), LineAction.DELTA);
    }

    @Test
    public void calcAngleCosSecondHalfTest() {
        double cos = LineAction.calcAngleCos(new Line(-Math.sqrt(3), -1.0, 0.0),
                new Line(new Point(0.0, 0.0), new Point(1, 0)));
        Assert.assertEquals(cos, Math.cos(Math.PI - Math.PI * 2 / 3),
                LineAction.DELTA);
    }

    @Test(dataProvider = "lineProvider", dataProviderClass = LineTest.class)
    public void calcAngleCosEqualLinesTest(Line line) {
        Assert.assertEquals(LineAction.calcAngleCos(line, line), Math.cos(0),
                LineAction.DELTA);
    }

    @Test(dataProvider = "lineProvider", dataProviderClass = LineTest.class,
            invocationCount = 5)
    public void findIntersectPointParallelTest(Line line) {
        Line parallelLine = new Line(line.getA(), line.getB(), line.getC() + 1);
        Assert.assertEquals(LineAction.findIntersectPoint(line, parallelLine),
                null);
    }

    @Test(dataProvider = "lineProvider", dataProviderClass = LineTest.class)
    public void findIntersectPointSameLineTest(Line line) {
        Assert.assertEquals(LineAction.findIntersectPoint(line, line), null);
    }

    @Test
    public void findIntersectPointCoordinateCenterIntersectTest() {
        Assert.assertEquals(LineAction.findIntersectPoint(new Line(1, -1, 0),
                new Line(1, 1, 0)), new Point(-0., -0.));
    }

    @Test
    public void findIntersectPointMaxDoubleIntersectTest() {
        Assert.assertEquals(
                LineAction.findIntersectPoint(new Line(-1, 0, Double.MAX_VALUE),
                        new Line(0, -1, Double.MAX_VALUE)),
                new Point(Double.MAX_VALUE, Double.MAX_VALUE));
    }

    @Test
    public void findIntersectPointMinDoubleIntersectTest() {
        Assert.assertEquals(
                LineAction.findIntersectPoint(new Line(1, 0, Double.MAX_VALUE),
                        new Line(0, 1, Double.MAX_VALUE)),
                new Point(-Double.MAX_VALUE, -Double.MAX_VALUE));
    }

    @Test
    public void isLineOXLineTest() {
        Assert.assertEquals(LineAction.isPointsOnOneLine(new Point(-1, 0),
                new Point(0, 0), new Point(1, 0)), true);
    }

    @Test
    public void isLineOYLineTest() {
        Assert.assertEquals(LineAction.isPointsOnOneLine(new Point(0, -1),
                new Point(0, 0), new Point(0, 1)), true);
    }

    @Test
    public void isLineThroughStartLineTest() {
        Assert.assertEquals(LineAction.isPointsOnOneLine(new Point(-1, -1),
                new Point(0, 0), new Point(1, 1)), true);
    }

    @Test
    public void isLineRandomLineTest() {
        Assert.assertEquals(LineAction.isPointsOnOneLine(new Point(1, 1),
                new Point(2, 0), new Point(0, 2)), true);
    }

    @Test
    public void isLineEqualPointsTest() {
        Assert.assertEquals(LineAction.isPointsOnOneLine(new Point(1, 1),
                new Point(1, 1), new Point(0, 2)), true);
    }

    /*
    *Result is False value because of method can't
    *work with values close to max Double value
    */
    @Test
    public void isLineMaxDistaceLineTest() {
        Assert.assertEquals(LineAction.isPointsOnOneLine(
                new Point(-Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(Double.MAX_VALUE, Double.MAX_VALUE), new Point(0, 0)),
                false);
    }

    /*
    *Result is False value because of method can't
    *work with values close to min Double value
    */
    @Test
    public void isLineMinDistaceLineTest() {
        Assert.assertEquals(
                LineAction.isPointsOnOneLine(new Point(0, -Double.MIN_VALUE),
                        new Point(0, Double.MIN_VALUE), new Point(0, 0)),
                false);
    }

    @Test
    public void isLineSmallDistaceLineTest() {
        Assert.assertEquals(LineAction.isPointsOnOneLine(new Point(0, -0.1),
                new Point(0, 0.1), new Point(0, 0)), true);
    }

    @Test
    public void isLineNotLineTest() {
        Assert.assertEquals(LineAction.isPointsOnOneLine(new Point(-1, 1),
                new Point(1, 1), new Point(0, 0)), false);
    }

    @Test
    public void isLineNotLineMaxDistaceTest() {
        Assert.assertEquals(LineAction.isPointsOnOneLine(
                new Point(-Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(Double.MAX_VALUE, Double.MAX_VALUE),
                new Point(-Double.MAX_VALUE, Double.MAX_VALUE)), false);
    }

    @Test
    public void isLineNotLineMinDistaceTest() {
        Assert.assertEquals(LineAction.isPointsOnOneLine(
                new Point(-Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(Double.MAX_VALUE, Double.MAX_VALUE),
                new Point(Double.MIN_VALUE, 0.0)), false);
    }

}
