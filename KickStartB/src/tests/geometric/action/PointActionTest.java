package tests.geometric.action;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.epam.quadrilateral.geometric.action.PointAction;
import com.epam.quadrilateral.geometric.object.Point;

public class PointActionTest {

    @Test
    public void calcDistanceMaxLengthTest() {
        Assert.assertEquals(
                PointAction.calcDistance(
                        new Point(Double.MAX_VALUE, Double.MAX_VALUE),
                        new Point(-Double.MAX_VALUE, -Double.MAX_VALUE)),
                Math.sqrt(Math.pow(Double.MAX_VALUE, 2)
                        + Math.pow(Double.MAX_VALUE, 2)));
    }

    @Test
    public void calcDistanceZeroLengthTest() {
        Assert.assertEquals(
                PointAction.calcDistance(new Point(0, 0), new Point(0, 0)),
                0.0);
    }

    @Test
    public void calcDistancePositiveCoordinatesTest() {
        Assert.assertEquals(
                PointAction.calcDistance(new Point(-1, -3), new Point(-5, -2)),
                Math.sqrt(Math.pow(5 - 1, 2) + Math.pow(3 - 2, 2)));
    }

    @Test
    public void calcDistanceNegativeCoordinatesTest() {
        Assert.assertEquals(
                PointAction.calcDistance(new Point(-1, -3), new Point(-5, -2)),
                Math.sqrt(Math.pow(-5 + 1, 2) + Math.pow(-3 + 2, 2)));
    }

    @Test
    public void isInBoundedBoxMaxPointRangeTest() {
        Assert.assertEquals(PointAction.isInBoundedBox(
                new Point(-Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(Double.MAX_VALUE, Double.MAX_VALUE),
                new Point(1, 1)), true);
    }

    @Test
    public void isInBoundedBoxBorderRangePointTest() {
        Assert.assertEquals(PointAction.isInBoundedBox(
                new Point(-Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(Double.MAX_VALUE, Double.MAX_VALUE),
                new Point(Double.MAX_VALUE, Double.MAX_VALUE)), true);
    }

    @Test
    public void isInBoundedBoxZeroRangeTest() {
        Assert.assertEquals(PointAction.isInBoundedBox(new Point(0, 0),
                new Point(0, 0), new Point(1, 1)), false);
    }

    @Test
    public void isInBoundedBoxZeroRangeBorderPointTest() {
        Assert.assertEquals(PointAction.isInBoundedBox(new Point(0, 0),
                new Point(0, 0), new Point(0, 0)), true);
    }

    @Test
    public void isInBoundedBoxNotInRangeTest() {
        Assert.assertEquals(PointAction.isInBoundedBox(new Point(-1, -1),
                new Point(1, 1), new Point(2, 2)), false);
    }
}
