package tests.geometric.object;

import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.epam.quadrilateral.exception.KSException;
import com.epam.quadrilateral.geometric.factory.QuadrilateralFactory;
import com.epam.quadrilateral.geometric.object.Point;
import com.epam.quadrilateral.geometric.object.Quadrilateral;

public class QuadrilateralTest {
    private static final Random RANDOM = new Random();
    private static final int RANDOM_RANGE = 1000;

    @DataProvider
    public Object[][] randomRectangleProvider() throws KSException {
        Point startPoint = new Point(RANDOM.nextDouble() * RANDOM_RANGE,
                RANDOM.nextDouble() * RANDOM_RANGE);
        Point[] corners = new Point[] {startPoint,
                new Point(-startPoint.getX(), startPoint.getY()),
                new Point(-startPoint.getX(), -startPoint.getY()),
                new Point(startPoint.getX(), -startPoint.getY())};
        Quadrilateral quadrilateral = QuadrilateralFactory.create(corners,
                "name");
        return new Object[][] {{quadrilateral}};
    }

    @Test(dataProvider = "randomRectangleProvider")
    public void equalsEqualsObjectsTest(Quadrilateral quadrilateral) {
        Assert.assertTrue(quadrilateral
                .equals(new Quadrilateral(quadrilateral.getCorners(),
                        quadrilateral.getName(), quadrilateral.getId())));
    }

    @Test(dataProvider = "randomRectangleProvider", invocationCount = 5)
    public void equalsNotEqualsObjectsTest(Quadrilateral quadrilateral) {
        Point[] corners = quadrilateral.getCorners();
        corners[0] = new Point(corners[0].getX() + 1, corners[0].getY());
        Quadrilateral differentQuadrilateral = new Quadrilateral(corners,
                quadrilateral.getName(), RANDOM.nextLong());
        Assert.assertFalse(quadrilateral.equals(differentQuadrilateral));
    }

}
