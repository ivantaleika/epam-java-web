package tests.geometric.object;

import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.epam.quadrilateral.geometric.object.Point;

@Test
public class PointTest {
    private static final Random RANDOM = new Random();
    private static final int RANDOM_RANGE = 1000;

    public static Point getRandomPoint() {
        return new Point(RANDOM.nextDouble() * RANDOM_RANGE,
                RANDOM.nextDouble() * RANDOM_RANGE);
    }

    @DataProvider
    public Object[][] pointProvider() {
        return new Object[][] {{new Point(RANDOM.nextDouble() * RANDOM_RANGE,
                RANDOM.nextDouble() * RANDOM_RANGE)}};
    }

    @Test(dataProvider = "pointProvider")
    public void equalsReflexiveTest(Point point) {
        Assert.assertTrue(point.equals(point));
    }

    @Test(dataProvider = "pointProvider", invocationCount = 5)
    public void equalsDifferentPointsTest(Point point) {
        Point point1 = new Point(point);
        point1.setX(point1.getX() + 1);
        Assert.assertFalse(point.equals(point1));
    }
}
