package tests.geometric.object;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.epam.quadrilateral.geometric.object.Line;
import com.epam.quadrilateral.geometric.object.Point;

public class LineTest {

    @DataProvider
    public Object[][] lineProvider() {
        return new Object[][] {{new Line(PointTest.getRandomPoint(),
                PointTest.getRandomPoint())}};
    }

    @Test(dataProvider = "lineProvider")
    public void equalsReflexiveTest(Line line) {
        Assert.assertTrue(line.equals(line));
    }

    @Test(dataProvider = "lineProvider")
    public void equalsDifferentLinessTest(Line line) {
        Line myLine2 = new Line(line.getA(), -line.getB(), line.getC());
        Assert.assertFalse(line.equals(myLine2));
    }

    @Test(dataProvider = "lineProvider")
    public void equalsDifferentCoeffSameLineTest(Line line) {
        Line myLine2 = new Line(-line.getA(), -line.getB(), -line.getC());
        Assert.assertTrue(line.equals(myLine2));
    }

    /*
     * False because of infinite number in Line object.
     */
    @Test
    public void equalsBorderMaxValueSamePointsTest() {
        Line myLine1 = new Line(new Point(Double.MAX_VALUE, Double.MAX_VALUE),
                new Point(-Double.MAX_VALUE, -Double.MAX_VALUE));
        Line myLine2 = new Line(new Point(Double.MAX_VALUE, Double.MAX_VALUE),
                new Point(-Double.MAX_VALUE, -Double.MAX_VALUE));
        Assert.assertFalse(myLine1.equals(myLine2));
    }

    @Test
    public void equalsBorderMinValueSamePointsTest() {
        Line myLine1 = new Line(new Point(Double.MIN_VALUE, Double.MIN_VALUE),
                new Point(-Double.MIN_VALUE, -Double.MIN_VALUE));
        Line myLine2 = new Line(new Point(Double.MIN_VALUE, Double.MIN_VALUE),
                new Point(-Double.MIN_VALUE, -Double.MIN_VALUE));
        Assert.assertTrue(myLine1.equals(myLine2));
    }

    @Test
    public void equalsBorderMinValueDifferentPointsSameLineTest() {
        Line myLine1 = new Line(new Point(0, 0),
                new Point(-Double.MIN_VALUE, -Double.MIN_VALUE));
        Line myLine2 = new Line(new Point(Double.MIN_VALUE, Double.MIN_VALUE),
                new Point(0, 0));
        Assert.assertTrue(myLine1.equals(myLine2));
    }

}
