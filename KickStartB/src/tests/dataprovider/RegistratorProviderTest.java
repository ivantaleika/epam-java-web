package tests.dataprovider;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.epam.quadrilateral.dataprovider.RegistratorProvider;
import com.epam.quadrilateral.exception.KSException;
import com.epam.quadrilateral.geometric.action.QuadrilateralAction;
import com.epam.quadrilateral.geometric.object.Abstract2DFigure;
import com.epam.quadrilateral.geometric.object.Quadrilateral;
import com.epam.quadrilateral.registrator.Figure2DRegistrator;

import tests.geometric.object.QuadrilateralTest;

public class RegistratorProviderTest {

    @Test(dataProvider = "randomRectangleProvider",
            dataProviderClass = QuadrilateralTest.class, invocationCount = 5)
    public void addQuadrilateralRegistratorTest(Quadrilateral quadrilateral) {
        Assert.assertEquals(
                new Figure2DRegistrator<Quadrilateral>(quadrilateral,
                        new QuadrilateralAction()),
                RegistratorProvider.addQuadrilateralRegistrator(quadrilateral));
    }

    @Test(dataProvider = "randomRectangleProvider",
            dataProviderClass = QuadrilateralTest.class, invocationCount = 5)
    public void addRegistratorQuadrilateralTest(Quadrilateral quadrilateral) {
        try {
            Assert.assertEquals(
                    new Figure2DRegistrator<Quadrilateral>(quadrilateral,
                            new QuadrilateralAction()),
                    RegistratorProvider.addRegistrator(quadrilateral));
        } catch (KSException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test(expectedExceptions = KSException.class)
    public void addRegistratorUnknownFigureTest() throws KSException {
        Abstract2DFigure figure = new Abstract2DFigure(1, "name") {
        };
        RegistratorProvider.addRegistrator(figure);

    }
}
