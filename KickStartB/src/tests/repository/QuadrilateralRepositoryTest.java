package tests.repository;

import java.io.IOException;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.epam.quadrilateral.dataimporter.ImportManager;
import com.epam.quadrilateral.geometric.action.QuadrilateralAction;
import com.epam.quadrilateral.geometric.object.Quadrilateral;
import com.epam.quadrilateral.registrator.Figure2DRegistrator;
import com.epam.quadrilateral.repository.QuadrilateralRepository;
import com.epam.quadrilateral.repository.specification.sort.SortById;
import com.epam.quadrilateral.repository.specification.sort.SortType;

import tests.geometric.object.QuadrilateralTest;

@Test(groups = "verifyRepository")
public class QuadrilateralRepositoryTest {
    public static final String PATH = "./testData/validData.txt";
    public static final QuadrilateralRepository REPOSITORY = QuadrilateralRepository
            .getInstance();

    @BeforeClass
    public void fillRepository() throws IOException {
        new ImportManager().importData(PATH);
    }

    @AfterClass
    public void clearRepository() {
        for (Quadrilateral quadrilateral : REPOSITORY.getAll().keySet()) {
            REPOSITORY.delete(quadrilateral);
        }
    }

    /**
     * Tests that collection returned by {@code getAll} isn't backed with
     * repository.
     */
    @Test
    public void getAllBackedTest() {
        Map<Quadrilateral, Figure2DRegistrator<Quadrilateral>> map = REPOSITORY
                .getAll();
        map.forEach((k, v) -> k.setName(k.getName() + " formatted"));
        Assert.assertNotEquals(map, REPOSITORY.getAll());
    }

    @Test
    public void deleteExistedTest() {
        Quadrilateral key = REPOSITORY.getAll().keySet().iterator().next();
        Assert.assertTrue(REPOSITORY.getAll().get(key) != null);
        Assert.assertTrue(REPOSITORY.delete(key));
        Assert.assertTrue(REPOSITORY.getAll().get(key) == null);
    }

    @Test(dataProvider = "randomRectangleProvider",
            dataProviderClass = QuadrilateralTest.class)
    public void deleteNotExistedTest(Quadrilateral quadrilateral) {
        Assert.assertTrue(REPOSITORY.getAll().get(quadrilateral) == null);
        Assert.assertFalse(REPOSITORY.delete(quadrilateral));
    }

    /**
     * Tests that collection returned by {@code query} isn't backed with
     * repository.
     */
    @Test
    public void queryBackedTest() {
        Map<Quadrilateral, Figure2DRegistrator<Quadrilateral>> map = REPOSITORY
                .query(new SortById<Quadrilateral, Figure2DRegistrator<Quadrilateral>>(
                        SortType.ASCENDING));
        map.forEach((k, v) -> k.setName(k.getName() + " formatted"));
        Assert.assertNotEquals(map, REPOSITORY.query(
                new SortById<Quadrilateral, Figure2DRegistrator<Quadrilateral>>(
                        SortType.ASCENDING)));
    }

    /**
     * Tests that method save should insert new data.
     *
     * @param quadrilateral new {@code Quadrilateral}
     */
    @Test(dataProvider = "randomRectangleProvider",
            dataProviderClass = QuadrilateralTest.class)
    public void saveNewDataTest(Quadrilateral quadrilateral) {
        Assert.assertNull(REPOSITORY.getAll().get(quadrilateral));
        Figure2DRegistrator<Quadrilateral> value = new Figure2DRegistrator<>(
                quadrilateral, new QuadrilateralAction());
        REPOSITORY.save(quadrilateral, value);
        Assert.assertEquals(REPOSITORY.getAll().get(quadrilateral), value);
    }

    /**
     * Tests that method {@code save} should update existed data.
     *
     */
    public void saveExistedDataTest() {
        Quadrilateral key = REPOSITORY.getAll().keySet().iterator().next();
        QuadrilateralAction action = new QuadrilateralAction();
        action.setDelta(action.getDelta() + 1);
        Figure2DRegistrator<Quadrilateral> reg = new Figure2DRegistrator<>(key,
                action);
        Assert.assertNotEquals(REPOSITORY.getAll().get(key), reg);
        REPOSITORY.save(key, reg);
        Assert.assertEquals(REPOSITORY.getAll().get(key), reg);
    }

}
