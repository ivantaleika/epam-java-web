package tests.repository.specification.search;

import java.io.IOException;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.epam.quadrilateral.dataimporter.ImportManager;
import com.epam.quadrilateral.exception.KSException;
import com.epam.quadrilateral.geometric.Quadrant;
import com.epam.quadrilateral.geometric.factory.QuadrilateralFactory;
import com.epam.quadrilateral.geometric.object.Point;
import com.epam.quadrilateral.geometric.object.Quadrilateral;
import com.epam.quadrilateral.registrator.Figure2DRegistrator;
import com.epam.quadrilateral.repository.QuadrilateralRepository;
import com.epam.quadrilateral.repository.specification.search.SearchArea;
import com.epam.quadrilateral.repository.specification.search.SearchConexQuadrilaterals;
import com.epam.quadrilateral.repository.specification.search.SearchId;
import com.epam.quadrilateral.repository.specification.search.SearchName;
import com.epam.quadrilateral.repository.specification.search.SearchPerimeter;
import com.epam.quadrilateral.repository.specification.search.SearchQuadrant;
import com.epam.quadrilateral.repository.specification.search.SearchSquares;

@Test
public class SearchSpecificationsTest {
    public static final String PATH = "./testData/validData.txt";

    public static final QuadrilateralRepository REPOSITORY = QuadrilateralRepository
            .getInstance();

    private Quadrilateral searchedQuadrilateral;

    @BeforeClass
    public void fillRepository() throws IOException, KSException {
        new ImportManager().importData(PATH);
        searchedQuadrilateral = QuadrilateralFactory
                .create(new Point[] {new Point(1, 1), new Point(2, 1),
                        new Point(2, 2), new Point(1, 2)}, "test");
    }

    @AfterClass
    public void clearRepository() {
        for (Quadrilateral quadrilateral : REPOSITORY.getAll().keySet()) {
            REPOSITORY.delete(quadrilateral);
        }
    }

    @Test
    public void searchIdExistedIdTest() {
        Quadrilateral target = REPOSITORY.getAll().keySet().iterator().next();
        Map<Quadrilateral, Figure2DRegistrator<Quadrilateral>> map = REPOSITORY
                .query(new SearchId<Quadrilateral, Figure2DRegistrator<Quadrilateral>>(
                        target.getId()));
        Assert.assertTrue(map.size() == 1);
        Assert.assertEquals(map.entrySet().iterator().next().getKey().getId(),
                target.getId());
    }

    @Test
    public void searchIdNotExistedIdTest() {
        Map<Quadrilateral, Figure2DRegistrator<Quadrilateral>> map = REPOSITORY
                .query(new SearchId<Quadrilateral, Figure2DRegistrator<Quadrilateral>>(
                        -1));
        Assert.assertTrue(map.isEmpty());
    }

    @Test
    public void searchNameExistedNameTest() {
        Map<Quadrilateral, Figure2DRegistrator<Quadrilateral>> map = REPOSITORY
                .query(new SearchName<Quadrilateral, Figure2DRegistrator<Quadrilateral>>(
                        "test"));
        Assert.assertTrue(map.size() == 1);
        Assert.assertTrue(map.containsKey(searchedQuadrilateral));
    }

    @Test
    public void searchNameNotExistedNameTest() {
        Map<Quadrilateral, Figure2DRegistrator<Quadrilateral>> map = REPOSITORY
                .query(new SearchName<Quadrilateral, Figure2DRegistrator<Quadrilateral>>(
                        "not existed name"));
        Assert.assertTrue(map.isEmpty());
    }

    @Test
    public void searchAreaMultipleTest() {
        Map<Quadrilateral, Figure2DRegistrator<Quadrilateral>> map = REPOSITORY
                .query(new SearchArea<Quadrilateral, Figure2DRegistrator<Quadrilateral>>(
                        -Double.MAX_VALUE, Double.MAX_VALUE));
        Assert.assertEqualsNoOrder(REPOSITORY.getAll().entrySet().toArray(),
                map.entrySet().toArray());
    }

    @Test
    public void searchAreaEmptyTest() {
        Assert.assertTrue(REPOSITORY.query(
                new SearchArea<Quadrilateral, Figure2DRegistrator<Quadrilateral>>(
                        -Double.MIN_VALUE, Double.MIN_VALUE))
                .isEmpty());
    }

    @Test
    public void searchAreaTest() {
        Map<Quadrilateral, Figure2DRegistrator<Quadrilateral>> map = REPOSITORY
                .query(new SearchArea<Quadrilateral, Figure2DRegistrator<Quadrilateral>>(
                        -10, 10));
        Assert.assertTrue(map.containsKey(searchedQuadrilateral));

    }

    @Test
    public void searchPerimeterMultipleTest() {
        Map<Quadrilateral, Figure2DRegistrator<Quadrilateral>> map = REPOSITORY
                .query(new SearchPerimeter<Quadrilateral, Figure2DRegistrator<Quadrilateral>>(
                        -Double.MAX_VALUE, Double.MAX_VALUE));
        Assert.assertEqualsNoOrder(REPOSITORY.getAll().entrySet().toArray(),
                map.entrySet().toArray());
    }

    @Test
    public void searchPerimeterTest() {
        Map<Quadrilateral, Figure2DRegistrator<Quadrilateral>> map = REPOSITORY
                .query(new SearchPerimeter<Quadrilateral, Figure2DRegistrator<Quadrilateral>>(
                        -10, 10));
        Assert.assertTrue(map.containsKey(searchedQuadrilateral));
    }

    @Test
    public void searchPerimeterEmptyTest() {
        Assert.assertTrue(REPOSITORY.query(
                new SearchPerimeter<Quadrilateral, Figure2DRegistrator<Quadrilateral>>(
                        -Double.MIN_VALUE, Double.MIN_VALUE))
                .isEmpty());
    }

    @Test
    public void searchQuadrantTest() {
        Map<Quadrilateral, Figure2DRegistrator<Quadrilateral>> map = REPOSITORY
                .query(new SearchQuadrant<Quadrilateral, Figure2DRegistrator<Quadrilateral>>(
                        Quadrant.FIRST));
        Assert.assertTrue(map.containsKey(searchedQuadrilateral));
    }

    @Test
    public void searchSquaresTest() {
        Map<Quadrilateral, Figure2DRegistrator<Quadrilateral>> map = REPOSITORY
                .query(new SearchSquares());
        Assert.assertTrue(map.size() == 1);
        Assert.assertTrue(map.containsKey(searchedQuadrilateral));
    }

    @Test
    public void searchConvexQuadrilateralsTest() {
        Quadrilateral quadrilateral;
        try {
            quadrilateral = QuadrilateralFactory.create(
                    new Point[] {new Point(12, 0), new Point(13.1, 10),
                            new Point(14., 10), new Point(22.152, 15)},
                    "notConvex");
        } catch (KSException e) {
            Assert.fail(e.getMessage());
            quadrilateral = null;
        }
        Assert.assertTrue(REPOSITORY.getAll().containsKey(quadrilateral));
        Map<Quadrilateral, Figure2DRegistrator<Quadrilateral>> map = REPOSITORY
                .query(new SearchConexQuadrilaterals());
        Assert.assertTrue(map.size() == REPOSITORY.getAll().size() - 1);
        Assert.assertFalse(map.containsKey(quadrilateral));
    }

}
