package tests.repository.specification.sort;

import java.io.IOException;
import java.util.Comparator;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.epam.quadrilateral.dataimporter.ImportManager;
import com.epam.quadrilateral.exception.KSException;
import com.epam.quadrilateral.geometric.object.Quadrilateral;
import com.epam.quadrilateral.registrator.Figure2DRegistrator;
import com.epam.quadrilateral.repository.QuadrilateralRepository;
import com.epam.quadrilateral.repository.specification.sort.SortByArea;
import com.epam.quadrilateral.repository.specification.sort.SortById;
import com.epam.quadrilateral.repository.specification.sort.SortByName;
import com.epam.quadrilateral.repository.specification.sort.SortByNameLength;
import com.epam.quadrilateral.repository.specification.sort.SortByPerimeter;
import com.epam.quadrilateral.repository.specification.sort.SortType;

public class SortSpecificationsTest {
    public static final String PATH = "./testData/validData.txt";

    public static final QuadrilateralRepository REPOSITORY = QuadrilateralRepository
            .getInstance();

    @BeforeClass
    public void fillRepository() throws IOException, KSException {
        new ImportManager().importData(PATH);
    }

    @AfterClass
    public void clearRepository() {
        for (Quadrilateral quadrilateral : REPOSITORY.getAll().keySet()) {
            REPOSITORY.delete(quadrilateral);
        }
    }

    @Test
    public void sortByIdAscendingTest() {
        Map<Quadrilateral, Figure2DRegistrator<Quadrilateral>> map = REPOSITORY
                .query(new SortById<Quadrilateral, Figure2DRegistrator<Quadrilateral>>(
                        SortType.ASCENDING));
        long id = -1;
        for (Quadrilateral quadrilateral : map.keySet()) {
            if (id != -1) {
                Assert.assertTrue(id < quadrilateral.getId());
            }
            id = quadrilateral.getId();
        }
    }

    @Test
    public void sortByIdDescendingTest() {
        Map<Quadrilateral, Figure2DRegistrator<Quadrilateral>> map = REPOSITORY
                .query(new SortById<Quadrilateral, Figure2DRegistrator<Quadrilateral>>(
                        SortType.DESCENDING));
        long id = -1;
        for (Quadrilateral quadrilateral : map.keySet()) {
            if (id != -1) {
                Assert.assertTrue(id > quadrilateral.getId());
            }
            id = quadrilateral.getId();
        }
    }

    @Test
    public void sortByNameAscendingTest() {
        Map<Quadrilateral, Figure2DRegistrator<Quadrilateral>> map = REPOSITORY
                .query(new SortByName<Quadrilateral, Figure2DRegistrator<Quadrilateral>>(
                        SortType.ASCENDING));
        Comparator<String> comparator = Comparator.naturalOrder();
        String previous = null;
        for (Quadrilateral quadrilateral : map.keySet()) {
            if (previous != null) {
                Assert.assertNotEquals(
                        comparator.compare(previous, quadrilateral.getName()),
                        1);
            }
            previous = quadrilateral.getName();
        }
    }

    @Test
    public void sortByNameDescendingTest() {
        Map<Quadrilateral, Figure2DRegistrator<Quadrilateral>> map = REPOSITORY
                .query(new SortByName<Quadrilateral, Figure2DRegistrator<Quadrilateral>>(
                        SortType.DESCENDING));
        Comparator<String> comparator = Comparator.naturalOrder();
        String previous = null;
        for (Quadrilateral quadrilateral : map.keySet()) {
            if (previous != null) {
                Assert.assertNotEquals(
                        comparator.compare(previous, quadrilateral.getName()),
                        -1);
            }
            previous = quadrilateral.getName();
        }
    }

    @Test
    public void sortByNameLengthAscendingTest() {
        Map<Quadrilateral, Figure2DRegistrator<Quadrilateral>> map = REPOSITORY
                .query(new SortByNameLength<Quadrilateral, Figure2DRegistrator<Quadrilateral>>(
                        SortType.ASCENDING));
        int length = -1;
        for (Quadrilateral quadrilateral : map.keySet()) {
            if (length != -1) {
                Assert.assertTrue(length <= quadrilateral.getName().length());
            }
            length = quadrilateral.getName().length();
        }
    }

    @Test
    public void sortByNameLengthDescendingTest() {
        Map<Quadrilateral, Figure2DRegistrator<Quadrilateral>> map = REPOSITORY
                .query(new SortByNameLength<Quadrilateral, Figure2DRegistrator<Quadrilateral>>(
                        SortType.DESCENDING));
        int length = -1;
        for (Quadrilateral quadrilateral : map.keySet()) {
            if (length != -1) {
                Assert.assertTrue(length >= quadrilateral.getName().length());
            }
            length = quadrilateral.getName().length();
        }
    }

    @Test
    public void sortByAreaAscendingTest() {
        Map<Quadrilateral, Figure2DRegistrator<Quadrilateral>> map = REPOSITORY
                .query(new SortByArea<Quadrilateral, Figure2DRegistrator<Quadrilateral>>(
                        SortType.ASCENDING));
        double area = -1;
        for (Figure2DRegistrator<Quadrilateral> registrator : map.values()) {
            if (area != -1) {
                Assert.assertTrue(area <= registrator.getArea());
            }
            area = registrator.getArea();
        }
    }

    @Test
    public void sortByAreaDescendingTest() {
        Map<Quadrilateral, Figure2DRegistrator<Quadrilateral>> map = REPOSITORY
                .query(new SortByArea<Quadrilateral, Figure2DRegistrator<Quadrilateral>>(
                        SortType.DESCENDING));
        double area = -1;
        for (Figure2DRegistrator<Quadrilateral> registrator : map.values()) {
            if (area != -1) {
                Assert.assertTrue(area >= registrator.getArea());
            }
            area = registrator.getArea();
        }
    }

    @Test
    public void sortByPerimeterAscendingTest() {
        Map<Quadrilateral, Figure2DRegistrator<Quadrilateral>> map = REPOSITORY
                .query(new SortByPerimeter<Quadrilateral, Figure2DRegistrator<Quadrilateral>>(
                        SortType.ASCENDING));
        double perimeter = -1;
        for (Figure2DRegistrator<Quadrilateral> registrator : map.values()) {
            if (perimeter != -1) {
                Assert.assertTrue(perimeter <= registrator.getPerimeter());
            }
            perimeter = registrator.getPerimeter();
        }
    }

    @Test
    public void sortByPerimeterDescendingTest() {
        Map<Quadrilateral, Figure2DRegistrator<Quadrilateral>> map = REPOSITORY
                .query(new SortByPerimeter<Quadrilateral, Figure2DRegistrator<Quadrilateral>>(
                        SortType.DESCENDING));
        double perimeter = -1;
        for (Figure2DRegistrator<Quadrilateral> registrator : map.values()) {
            if (perimeter != -1) {
                Assert.assertTrue(perimeter >= registrator.getPerimeter());
            }
            perimeter = registrator.getPerimeter();
        }
    }

}
