package tests.registrator;

import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.epam.quadrilateral.exception.KSException;
import com.epam.quadrilateral.geometric.action.QuadrilateralAction;
import com.epam.quadrilateral.geometric.factory.QuadrilateralFactory;
import com.epam.quadrilateral.geometric.object.Point;
import com.epam.quadrilateral.geometric.object.Quadrilateral;
import com.epam.quadrilateral.registrator.Figure2DRegistrator;

public class Figure2DRegistratorTest {
    private Figure2DRegistrator<Quadrilateral> registrator;
    private Quadrilateral registeredFigure;
    private double previousPerimeter;
    private double previousArea;
    private Random random;
    private final int randomRange = 1000;

    @BeforeSuite
    public void init() throws KSException {
        random = new Random();
        registeredFigure = QuadrilateralFactory.create(
                new Point[] {new Point(10, 10), new Point(-10, 10),
                        new Point(-10, -10), new Point(10, -10)},
                "quadrilateral");
        registrator = new Figure2DRegistrator<Quadrilateral>(registeredFigure,
                new QuadrilateralAction());
        registeredFigure.subscribe(registrator);
    }

    @BeforeGroups(groups = "updateOneCorner")
    public void changeFigureCorner() {
        previousPerimeter = registrator.getPerimeter();
        previousArea = registrator.getArea();
        Point point = registeredFigure.getCorner(0);
        point.setX(random.nextDouble() * randomRange);
        registeredFigure.setCorner(point, 0);
    }

    @Test(groups = "updateOneCorner")
    public void oneCornerPerimeterTest() {
        Assert.assertNotEquals(registrator.getPerimeter(), previousPerimeter);
    }

    @Test(groups = "updateOneCorner")
    public void oneCornerAreaTest() {
        Assert.assertNotEquals(registrator.getArea(), previousArea);
    }

    @BeforeGroups(groups = "updateAllCorners")
    public void changeFigureCorners() {
        previousPerimeter = registrator.getPerimeter();
        previousArea = registrator.getArea();
        Point[] points = registeredFigure.getCorners();
        for (int i = 0; i < points.length; i++) {
            points[i].setX(random.nextDouble() * randomRange);
        }
        registeredFigure.setCorners(points);
    }

    @Test(groups = "updateAllCorners", invocationCount = 1)
    public void allCornerPerimeterTest() {
        Assert.assertNotEquals(registrator.getPerimeter(), previousPerimeter);
    }

    @Test(groups = "updateAllCorners", invocationCount = 1)
    public void allCornerAreaTest() {
        Assert.assertNotEquals(registrator.getArea(), previousArea);
    }
}
