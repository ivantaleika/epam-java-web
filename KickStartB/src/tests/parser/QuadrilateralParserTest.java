package tests.parser;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.epam.quadrilateral.exception.KSException;
import com.epam.quadrilateral.geometric.factory.QuadrilateralInfo;
import com.epam.quadrilateral.geometric.object.Point;
import com.epam.quadrilateral.parser.QuadrilateralParser;

public class QuadrilateralParserTest {

    @BeforeClass
    public void init() {

    }

    @DataProvider
    public Object[][] invalidStringProvider() {
        return new Object[][] {{"name+12;+0 +13.1;+10 +14.;+11 +22.152;+10"},
                {"name name +12;+0 +13.1;+10 +14.;+11 +22.152;+10 "},
                {"name 12.z;0 13.1;10 14.;10 22.152;-10"},
                {"name 12.;0 13.1;10 14.;10 22.152;-10 foo"},
                {"name 12.;0 13.1;10 foo;bar 22.152;-10 "},
                {"name 0.0; 10.;5 11;5. 12.5;5.0"},
                {"    13.;0 13.1;10 14.;10 22.152;-10"},
                {"foo13.;0 13.1;10 14.;10 22.152;-10"}};
    }

    @DataProvider
    public Object[][] validStringProvider() {
        return new Object[][] {
                {"ABCD 12;0 13.1;10 14.;10 22.152;15", new QuadrilateralInfo(
                        "ABCD",
                        new Point[] {new Point(12, 0), new Point(13.1, 10),
                                new Point(14, 10), new Point(22.152, 15)})},
                {" A1A2A3A4 12;0000 13.1;10 14.;10 22.152;-10 ",
                        new QuadrilateralInfo("A1A2A3A4",
                                new Point[] {new Point(12, 0),
                                        new Point(13.1, 10), new Point(14, 10),
                                        new Point(22.152, -10)})},
                {"  1234 -12;0 -13.1;-10 -14.;-10 -22.152;10 ",
                        new QuadrilateralInfo("1234", new Point[] {
                                new Point(-12, 0), new Point(-13.1, -10),
                                new Point(-14, -10), new Point(-22.152, 10)})},
                {" foo 13.;0 13.1;10 14.;10 22.152;-10",
                        new QuadrilateralInfo("foo",
                                new Point[] {new Point(13, 0),
                                        new Point(13.1, 10), new Point(14, 10),
                                        new Point(22.152, -10)})}};
    }

    @Test(dataProvider = "validStringProvider")
    public void isInfoStringValidDataTest(String string,
            QuadrilateralInfo info) {
        Assert.assertTrue(QuadrilateralParser.isInfoString(string));
    }

    @Test(dataProvider = "invalidStringProvider")
    public void isInfoStringInvalidDataTest(String string) {
        Assert.assertFalse(QuadrilateralParser.isInfoString(string));
    }

    @Test(dataProvider = "invalidStringProvider",
            expectedExceptions = KSException.class)
    public void parseTxtInfoInvalidDataTest(String string) throws KSException {
        QuadrilateralParser.parseTxtInfo(string);
    }

    @Test(dataProvider = "validStringProvider")
    public void parseTxtInfoValidDataTest(String string,
            QuadrilateralInfo info) {
        try {
            Assert.assertEquals(QuadrilateralParser.parseTxtInfo(string), info);
        } catch (KSException e) {
            Assert.fail(e.getMessage());
        }
    }
}
