package tests.dataimporter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.epam.quadrilateral.dataimporter.ImportManager;
import com.epam.quadrilateral.dataprovider.RegistratorProvider;
import com.epam.quadrilateral.exception.KSException;
import com.epam.quadrilateral.geometric.factory.QuadrilateralFactory;
import com.epam.quadrilateral.geometric.object.Quadrilateral;
import com.epam.quadrilateral.parser.QuadrilateralParser;
import com.epam.quadrilateral.registrator.Figure2DRegistrator;
import com.epam.quadrilateral.repository.QuadrilateralRepository;

public class ImportManagerTest {
    private static final String VALID_DATA_PATH = "./testData/validData.txt";
    private static final String INVALID_DATA_PATH = "./testData/invalidData.txt";
    private static final String[] DATA = new String[] {
            "notConvex 12;0 13.1;10 14.;10 22.152;15",
            " A1A2A3A4 12;0000 13.1;10 14.;10 22.152;-10 ",
            "  1234 -12;-0 -13.1;-10 -14.;-10 -22.152;10 ",
            " foo 13.;0 13.1;10 14.;10 22.152;-10", "test 1;1 2;1 2;2 1;2  " };
    private ImportManager manager;
    private Object[] expectedImport;

    @BeforeClass
    public void init() throws KSException {
        manager = new ImportManager();
        Map<Quadrilateral, Figure2DRegistrator<Quadrilateral>> expected = new HashMap<>();
        for (String info : DATA) {
            Quadrilateral quadrilateral = QuadrilateralFactory
                    .create(QuadrilateralParser.parseTxtInfo(info));
            expected.put(quadrilateral, RegistratorProvider
                    .addQuadrilateralRegistrator(quadrilateral));
        }
        expectedImport = expected.entrySet().toArray();
    }

    @AfterClass
    public void clearRepository() {
        QuadrilateralRepository repository = QuadrilateralRepository
                .getInstance();
        for (Quadrilateral quadrilateral : repository.getAll().keySet()) {
            repository.delete(quadrilateral);
        }
    }

    @Test
    public void importInvalidDataTest() {
        try {
            manager.importData(INVALID_DATA_PATH);
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }
        Assert.assertTrue(
                QuadrilateralRepository.getInstance().getAll().isEmpty());
    }

    @Test(dependsOnMethods = "importInvalidDataTest")
    public void importValidDataTest() {

        try {
            manager.importData(VALID_DATA_PATH);
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }
        Assert.assertEqualsNoOrder(QuadrilateralRepository.getInstance()
                .getAll().entrySet().toArray(), expectedImport);
    }

    @Test(expectedExceptions = IOException.class)
    public void importDataErrorPathTest() throws IOException {
        manager.importData(".");
    }

}
