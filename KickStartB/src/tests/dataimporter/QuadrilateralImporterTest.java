package tests.dataimporter;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.epam.quadrilateral.dataimporter.QuadrilateralImporter;
import com.epam.quadrilateral.exception.KSException;
import com.epam.quadrilateral.geometric.factory.QuadrilateralFactory;
import com.epam.quadrilateral.geometric.object.Quadrilateral;
import com.epam.quadrilateral.parser.QuadrilateralParser;

public class QuadrilateralImporterTest {
    private static final String VALID_DATA_PATH = "./testData/validData.txt";
    private static final String INVALID_DATA_PATH = "./testData/invalidData.txt";
    private Object[] expectedImport;
    private QuadrilateralImporter importer;

    @BeforeClass
    public void init() throws KSException {
        importer = new QuadrilateralImporter();
        List<Quadrilateral> expected = new LinkedList<>();
        expected.add(QuadrilateralFactory.create(QuadrilateralParser
                .parseTxtInfo("notConvex 12;0 13.1;10 14.;10 22.152;15")));
        expected.add(QuadrilateralFactory.create(QuadrilateralParser
                .parseTxtInfo(" A1A2A3A4 12;0000 13.1;10 14.;10 22.152;-10 ")));
        expected.add(QuadrilateralFactory.create(QuadrilateralParser
                .parseTxtInfo("  1234 -12;-0 -13.1;-10 -14.;-10 -22.152;10 ")));
        expected.add(QuadrilateralFactory.create(QuadrilateralParser
                .parseTxtInfo(" foo 13.;0 13.1;10 14.;10 22.152;-10")));
        expected.add(QuadrilateralFactory.create(
                QuadrilateralParser.parseTxtInfo("test 1;1 2;1 2;2 1;2  ")));
        expectedImport = expected.toArray();
    }

    @Test
    public void fromTxtInvalidDataTest() {
        Stream<Quadrilateral> result;
        try {
            result = importer.fromTxt(INVALID_DATA_PATH);
        } catch (IOException e) {
            Assert.fail(e.getMessage());
            result = null;
        }
        Assert.assertTrue(result.toArray().length == 0);
    }

    @Test(dependsOnMethods = "fromTxtInvalidDataTest")
    public void fromTxtValidDataTest() {
        Stream<Quadrilateral> result;
        try {
            result = importer.fromTxt(VALID_DATA_PATH);
        } catch (IOException e) {
            Assert.fail(e.getMessage());
            result = null;
        }
        Assert.assertEqualsNoOrder(result.toArray(), expectedImport);
    }

}
