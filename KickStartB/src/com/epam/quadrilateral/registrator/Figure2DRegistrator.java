package com.epam.quadrilateral.registrator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.quadrilateral.geometric.action.Figure2DAction;
import com.epam.quadrilateral.geometric.object.Abstract2DFigure;
import com.epam.quadrilateral.observer.Subscriber;

/**
 * Registrator for 2D figures that store its area and perimeter.
 *
 * @param <T> the figure type
 */
public class Figure2DRegistrator<T extends Abstract2DFigure>
        implements Subscriber {
    /**
     * QuadrilateralImporterTest logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();
    /**
     * Registered figure.
     */
    private T figure;
    /**
     * Action class that provides method for calculation area and perimeter.
     */
    private Figure2DAction<T> actionClass;
    /**
     * Figure's area.
     */
    private double area;
    /**
     * Figure's perimeter.
     */
    private double perimeter;

    /**
     * Construct the registrator for {@code registeredFigure} with
     * {@code figureActionClass} action class.
     *
     * @param registeredFigure  source figure.
     * @param figureActionClass class that provides methods for calculating area
     *                          and perimeter.
     */
    public Figure2DRegistrator(final T registeredFigure,
            final Figure2DAction<T> figureActionClass) {
        figure = registeredFigure;
        actionClass = figureActionClass;
        area = getActionClass().calcArea(figure);
        perimeter = getActionClass().calcPerimeter(figure);
        LOGGER.trace("{} - area = {}, perimeter = {}", registeredFigure, area,
                perimeter);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update() {
        area = getActionClass().calcArea(figure);
        perimeter = getActionClass().calcPerimeter(figure);
        LOGGER.trace("{} was changed - area = %.3f, perimeter = {}", figure,
                area, perimeter);
    }

    /**
     * @return the figure
     */
    public T getFigure() {
        return figure;
    }

    /**
     * @param registeredFigure the figure to set
     */
    public void setFigure(final T registeredFigure) {
        figure = registeredFigure;
    }

    /**
     * @return the actionClass
     */
    public Figure2DAction<T> getActionClass() {
        return actionClass;
    }

    /**
     * @param figureActionClass the actionClass to set
     */
    public void setActionClass(final Figure2DAction<T> figureActionClass) {
        actionClass = figureActionClass;
    }

    /**
     * @return figure's area
     */
    public double getArea() {
        return area;
    }

    /**
     * @return figure's perimeter
     */
    public double getPerimeter() {
        return perimeter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result;
        if (actionClass != null) {
            result = actionClass.hashCode();
        }
        long temp;
        temp = Double.doubleToLongBits(area);
        final int magicNumber = 32;
        result = prime * result + (int) (temp ^ (temp >>> magicNumber));
        result = prime * result;
        if (figure != null) {
            result += figure.hashCode();
        }
        temp = Double.doubleToLongBits(perimeter);
        result = prime * result + (int) (temp ^ (temp >>> magicNumber));
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Figure2DRegistrator<?> other = (Figure2DRegistrator<?>) obj;
        if (actionClass == null) {
            if (other.actionClass != null) {
                return false;
            }
        } else if (!actionClass.equals(other.actionClass)) {
            return false;
        }
        if (figure == null) {
            if (other.figure != null) {
                return false;
            }
        } else if (!figure.equals(other.figure)) {
            return false;
        }
        if (Double.doubleToLongBits(area) != Double
                .doubleToLongBits(other.area)) {
            return false;
        }
        if (Double.doubleToLongBits(perimeter) != Double
                .doubleToLongBits(other.perimeter)) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return String.format(
                "[Figure2DRegistrator [figure = %s, area = %.2f,"
                        + " perimeter = %.2f, actionClass = %s]",
                figure, area, perimeter, actionClass);
    }

}
