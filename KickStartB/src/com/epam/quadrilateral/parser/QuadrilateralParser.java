package com.epam.quadrilateral.parser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.epam.quadrilateral.exception.KSException;
import com.epam.quadrilateral.geometric.factory.QuadrilateralInfo;
import com.epam.quadrilateral.geometric.object.Point;
import com.epam.quadrilateral.geometric.object.Quadrilateral;

/**
 * Parses quadrilateral's data.
 */
public final class QuadrilateralParser {
    /**
     * Regex to eject strings with quadrilateral's data.
     */
    public static final String QUADRILATERAL_REGEX =
            "([ \\t]+)?\\w+([ \\t]+[+-]?\\d+\\.?(\\d+)?;"
            + "[+-]?\\d+\\.?(\\d+)?){4}([ \\t]+)?";
    /**
     * Regex to eject point's data.
     */
    public static final String POINT_REGEX = "(?<x>[+-]?\\d+\\.?(\\d+)?);"
            + "(?<y>[+-]?\\d+\\.?(\\d+)?)";
    /**
     * Regex to eject point's data.
     */
    public static final String NAME_REGEX = "^(([ \\t]+)?\\w+)";
    /**
     * {@link Pattern} object for parsing.
     */
    private static Pattern pattern;
    /**
     * {@link Matcher} object for parsing.
     */
    private static Matcher matcher;

    /**
     * Prevents from utility object creation.
     */
    private QuadrilateralParser() {
    }

    /**
     * Check if string match {@code QUADRILATERAL_REGEX} pattern.
     *
     * @param  info source {@code String}
     * @return      {@code true} if string match the
     *              {@code QUADRILATERAL_REGEX}, {@code false} otherwise
     */
    public static boolean isInfoString(final String info) {
        return info.matches(QUADRILATERAL_REGEX);
    }

    /**
     * Parse quadrilateral's txt info string.
     *
     * @param  info        quadrilateral's txt info string.
     * @return             {@link QuadrilateralInfo} object
     * @throws KSException if {@code info} parameter isn't a quadrilateral's
     *                     info string
     */
    public static QuadrilateralInfo parseTxtInfo(final String info)
            throws KSException {
        if (!isInfoString(info)) {
            throw new KSException("\"" + info + "\""
                    + " string isn't a txt quadrilateral's data string.");
        }
        return new QuadrilateralInfo(parseName(info), parseCorners(info));
    }

    /**
     * Searches for points info in the source string. Returns info as a map of
     * point's coordinates. Map will be empty if no points info is found.
     *
     * @param  info string containing figure's info
     * @return      map in witch key is a point x coordinate and value is a
     *              point y coordinate
     */
    private static Point[] parseCorners(final String info) {
        pattern = Pattern.compile(POINT_REGEX);
        matcher = pattern.matcher(info);
        Point[] points = new Point[Quadrilateral.N_CORNERS];
        for (int i = 0; i < points.length; i++) {
            matcher.find();
            points[i] = new Point(Double.parseDouble(matcher.group("x")),
                    Double.parseDouble(matcher.group("y")));
        }
        return points;
    }

    /**
     * Returns first result for {@code NAME_REGEX} regex.
     *
     * @param  info quadrilateral's info string
     * @return      quadrilateral's name
     */
    private static String parseName(final String info) {
        pattern = Pattern.compile(NAME_REGEX);
        matcher = pattern.matcher(info);
        matcher.find();
        return matcher.group(0).trim();
    }

}
