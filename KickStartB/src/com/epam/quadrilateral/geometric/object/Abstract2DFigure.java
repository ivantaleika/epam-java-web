package com.epam.quadrilateral.geometric.object;

import com.epam.quadrilateral.observer.AbstractMultiPublisher;

/**
 * Base class for 2D figures. Provides Publisher interface.
 */
public abstract class Abstract2DFigure extends AbstractMultiPublisher {
    /**
     * figure's id.
     */
    private long id;
    /**
     * figure's name.
     */
    private String name;

    /**
     * Constructs a figure with name and id.
     *
     * @param figureId   figure's id
     * @param figureName figure's name
     */
    public Abstract2DFigure(final long figureId, final String figureName) {
        super();
        id = figureId;
        name = figureName;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param newId the id to set
     */
    public void setId(final long newId) {
        id = newId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param newName the name to set
     */
    public void setName(final String newName) {
        name = newName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result;
        if (name != null) {
            result += name.hashCode();
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Abstract2DFigure)) {
            return false;
        }
        Abstract2DFigure other = (Abstract2DFigure) obj;
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }
}
