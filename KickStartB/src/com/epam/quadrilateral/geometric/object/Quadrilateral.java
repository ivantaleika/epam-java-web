package com.epam.quadrilateral.geometric.object;

import java.util.Arrays;

/**
 * The class that represents a quadrilateral.
 */
public class Quadrilateral extends Abstract2DFigure {
    /**
     * is number of quadrilateral's corners.
     */
    public static final int N_CORNERS = 4;
    /**
     * Quadrilateral's corners. Points are ordered so that figure's sides attach
     * two adjacent array's points
     */
    private Point[] corners;

    /**
     * Figure's name.
     */

    /**
     * Constructs quadrilateral from 4 corners and name.
     *
     * @param newCorners quadrilateral's corners
     * @param figureName quadrilateral's name
     * @param figureId   quadrilateral's id
     */
    public Quadrilateral(final Point[] newCorners, final String figureName,
            final long figureId) {
        super(figureId, figureName);
        corners = Arrays.copyOf(newCorners, N_CORNERS);
    }

    /**
     * Returns copy of quadrilateral's corners array.
     *
     * @return corners array
     */
    public Point[] getCorners() {
        return Arrays.copyOf(corners, N_CORNERS);
    }

    /**
     * Returns point number i in the corner's array.
     *
     * @param  i corner's index
     * @return   copy of corner's point
     */
    public Point getCorner(final int i) {
        return new Point(corners[i]);
    }

    /**
     * Set quadrilateral's corners.
     *
     * @param newCorners new quadrilateral's corners
     */
    public void setCorners(final Point[] newCorners) {
        corners = Arrays.copyOf(newCorners, N_CORNERS);
        notifySubscribers();
    }

    /**
     * Set quadrilateral's corner which index is {@code index}.
     *
     * @param newCorner new quadrilateral's corner
     * @param index     corner's index
     */
    public void setCorner(final Point newCorner, final int index) {
        corners[index] = newCorner;
        notifySubscribers();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Quadrilateral: id " + getId() + ", name \"" + getName()
                + "\" corners: " + Arrays.toString(corners);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + Arrays.hashCode(corners);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (!(obj instanceof Quadrilateral)) {
            return false;
        }
        Quadrilateral other = (Quadrilateral) obj;
        if (!Arrays.equals(corners, other.corners)) {
            return false;
        }
        return true;
    }

}
