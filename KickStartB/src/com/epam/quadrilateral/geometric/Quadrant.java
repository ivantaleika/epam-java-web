package com.epam.quadrilateral.geometric;

/**
 * Defines all two-dimention's quadrants.
 */
public enum Quadrant {
    /**
     * quadrant's indexes.
     */
    FIRST, SECOND, THIRD, FOURTH
}
