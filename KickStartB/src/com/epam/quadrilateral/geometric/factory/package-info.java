/**
 * Contains factories for geometric's figures entity-classes.
 */
package com.epam.quadrilateral.geometric.factory;
