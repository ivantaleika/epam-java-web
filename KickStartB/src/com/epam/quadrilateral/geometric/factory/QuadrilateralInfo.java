package com.epam.quadrilateral.geometric.factory;

import java.util.Arrays;

import com.epam.quadrilateral.geometric.object.Point;
import com.epam.quadrilateral.geometric.object.Quadrilateral;

/**
 * Contains info about {@link Quadrilateral} object.
 */
public class QuadrilateralInfo {
    /**
     * Quadrilateral's corners. Points are ordered so that figure's sides attach
     * two adjacent array's points
     */
    private Point[] corners;

    /**
     * figure's name.
     */
    private String name;

    /**
     * Constructs object from quadrilateral's name and corners.
     *
     * @param quadrilateralName    quadrilateral's name
     * @param quadrilateralCorners quadrilateral's corners
     */
    public QuadrilateralInfo(final String quadrilateralName,
            final Point[] quadrilateralCorners) {
        corners = Arrays.copyOf(quadrilateralCorners, Quadrilateral.N_CORNERS);
        name = quadrilateralName;

    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param newName the name to set
     */
    public void setName(final String newName) {
        name = newName;
    }

    /**
     * @return the corners
     */
    public Point[] getCorners() {
        return Arrays.copyOf(corners, Quadrilateral.N_CORNERS);
    }

    /**
     * @param newCorners the corners to set
     */
    public void setCorners(final Point[] newCorners) {
        corners = Arrays.copyOf(newCorners, Quadrilateral.N_CORNERS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Arrays.hashCode(corners);
        result = prime * result;
        if (name != null) {
            result += name.hashCode();
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        QuadrilateralInfo other = (QuadrilateralInfo) obj;
        if (!Arrays.equals(corners, other.corners)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "QuadrilateralInfo [corners=" + Arrays.toString(corners)
                + ", name=" + name + "]";
    }
}
