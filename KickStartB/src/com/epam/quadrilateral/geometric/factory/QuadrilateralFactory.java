package com.epam.quadrilateral.geometric.factory;

import com.epam.quadrilateral.exception.KSException;
import com.epam.quadrilateral.geometric.action.Validator;
import com.epam.quadrilateral.geometric.object.Point;
import com.epam.quadrilateral.geometric.object.Quadrilateral;
import com.epam.quadrilateral.idprovider.SerialIdProvider;

/**
 * Factory for {@link Quadrilateral} object that will be used with utility
 * classes.
 */
public final class QuadrilateralFactory {
    /**
     * Figure VALIDATOR.
     */
    private static final Validator VALIDATOR = new Validator();

    /**
     * Simple factory constructor.
     */
    private QuadrilateralFactory() {
    }

    /**
     * Creates quadrilateral from it's corners points and name. Points goes from
     * specific verification.
     *
     * @param  corners     quadrilateral's corners
     * @param  name        quadrilateral's name
     * @return             {@link Quadrilateral} object
     * @throws KSException if corners fail the validation
     */
    public static Quadrilateral create(final Point[] corners, final String name)
            throws KSException {
        validateQuadrilateralData(corners);
        return new Quadrilateral(corners, name, SerialIdProvider.getId());
    }

    /**
     * Creates quadrilateral from {@link QuadrilateralInfo}. Points goes from
     * specific verification.
     *
     * @param  info        quadrilateral's QuadrilateralInfo
     * @return             {@link Quadrilateral} object
     * @throws KSException if corners fail the validation
     */
    public static Quadrilateral create(final QuadrilateralInfo info)
            throws KSException {
        return create(info.getCorners(), info.getName());
    }

    /**
     * Specific validation for quadrilateral corners.
     *
     * @param  newCorners  input corners
     * @throws KSException if corners fail the validation
     */
    private static void validateQuadrilateralData(final Point[] newCorners)
            throws KSException {
        if (newCorners.length != Quadrilateral.N_CORNERS) {
            throw new KSException(
                    "Quadrilateral should have " + Quadrilateral.N_CORNERS
                            + " unique corners, found" + newCorners.length);
        }
        if (!VALIDATOR.isCorners(newCorners)) {
            throw new KSException("3 or more corners are on the same line.");
        }
        if (VALIDATOR.isCrossedFigure(newCorners)) {
            throw new KSException("Cannot create crossed quadrilateral.");
        }
    }
}
