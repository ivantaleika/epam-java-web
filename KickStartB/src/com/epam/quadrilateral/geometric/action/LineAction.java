package com.epam.quadrilateral.geometric.action;

import com.epam.quadrilateral.geometric.object.Line;
import com.epam.quadrilateral.geometric.object.Point;

/**
 * This class contains various methods for manipulating lines or check line's
 * state.
 */
public final class LineAction {

    /**
     * Max delta between angles calculations.
     */
    public static final double DELTA = 0.001;

    /**
     * Private constructor to protect from creating utility class objects.
     */
    private LineAction() {
    }

    /**
     * Returns the intersect point of two line if it exist.
     *
     * @param  l1 first line
     * @param  l2 second line
     * @return    Intersect point if lines have an intersect point, {@code null}
     *            if two objects represent the same line or parallel lines
     */
    public static Point findIntersectPoint(final Line l1, final Line l2) {
        double coeffitient = calcCramerCoeffitient(l1.getA(), l1.getB(),
                l2.getA(), l2.getB());
        if (Math.abs(coeffitient) > DELTA) {
            double x = -calcCramerCoeffitient(l1.getC(), l1.getB(), l2.getC(),
                    l2.getB()) * 1. / coeffitient;
            double y = -calcCramerCoeffitient(l1.getA(), l1.getC(), l2.getA(),
                    l2.getC()) * 1. / coeffitient;
            return new Point(x, y);
        } else {
            return null;
        }
    }

    /**
     * Calculates Cramer's coefficient for matrix [x1,y1] [x2,y2].
     *
     * @param  x1 [0][0] matrix number
     * @param  y1 [0][1] matrix number
     * @param  x2 [1][0] matrix number
     * @param  y2 [1][1] matrix number
     * @return    Cramer's coefficient
     */
    private static double calcCramerCoeffitient(final double x1,
            final double y1, final double x2, final double y2) {
        return x1 * y2 - y1 * x2;
    }

    /**
     * Check if all three points lie on the same line.
     *
     * <p>
     * Note: could return wrong result when points have coordinates close to min
     * or max Double values.
     *
     * @param  p1 1st comparable point
     * @param  p2 2st comparable point
     * @param  p3 3st comparable point
     * @return    {@code true} if all 3 points lie on the same line;
     *            {@code false} otherwise.
     */
    public static boolean isPointsOnOneLine(final Point p1, final Point p2,
            final Point p3) {
        if (p1.equals(p2) || p1.equals(p3) || p3.equals(p2)) {
            return true;
        }
        return calcDistance(new Line(p1, p2), p3).compareTo(DELTA) < 0
                && calcDistance(new Line(p1, p3), p2).compareTo(DELTA) < 0
                && calcDistance(new Line(p3, p2), p1).compareTo(DELTA) < 0;
    }

    /**
     * Return distance between line and point.
     *
     * @param  line  source line
     * @param  point source point
     * @return       distance between line and point
     */
    private static Double calcDistance(final Line line, final Point point) {
        return Math
                .abs(line.getA() * point.getX() + line.getB() * point.getY()
                        + line.getC())
                / Math.sqrt(
                        Math.pow(line.getA(), 2) + Math.pow(line.getB(), 2));
    }

    /**
     * Returns cos of acute angle between 2 lines.
     *
     * @param  l1 first line
     * @param  l2 second line
     * @return    cos of acute angle
     */
    public static double calcAngleCos(final Line l1, final Line l2) {
        return (l1.getA() * l2.getA() + l1.getB() * l2.getB()) / (Math
                .sqrt(Math.pow(l1.getA(), 2) + Math.pow(l1.getB(), 2))
                * Math.sqrt(Math.pow(l2.getA(), 2) + Math.pow(l2.getB(), 2)));
    }
}
