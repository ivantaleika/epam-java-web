package com.epam.quadrilateral.geometric.action;

import com.epam.quadrilateral.geometric.Quadrant;
import com.epam.quadrilateral.geometric.object.Line;
import com.epam.quadrilateral.geometric.object.Point;
import com.epam.quadrilateral.geometric.object.Quadrilateral;

/**
 * This class contains various methods for manipulating quadrialterals or check
 * quadrialteral's state.
 *
 * <p>
 * Note: some methods could give an unexpected result if quadrilateral is
 * crossed. To avoid this, use factories for figure creation.
 */
public class QuadrilateralAction implements Figure2DAction<Quadrilateral> {
    /**
     * Default delta for comparison methods.
     */
    private static final double DEFAULT_DELTA = 0.001;
    /**
     * Delta for comparison methods.
     */
    private double delta;

    /**
     * Constructs object with default {@code delta}.
     */
    public QuadrilateralAction() {
        delta = DEFAULT_DELTA;
    }

    /**
     * Constructs object with given {@code calculationDelta}.
     *
     * @param calculationDelta a calculation delta
     */
    public QuadrilateralAction(final double calculationDelta) {
        delta = calculationDelta;
    }

    /**
     * @return the delta
     */
    public double getDelta() {
        return delta;
    }

    /**
     * @param newDelta the delta to set
     */
    public void setDelta(final double newDelta) {
        delta = newDelta;
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public double calcPerimeter(final Quadrilateral quadrilateral) {
        Point[] corners = quadrilateral.getCorners();
        double perimeter = PointAction.calcDistance(corners[0],
                corners[corners.length - 1]);
        for (int i = 0; i < corners.length - 1; i++) {
            perimeter += PointAction.calcDistance(corners[i], corners[i + 1]);
        }
        return perimeter;
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public double calcArea(final Quadrilateral quadrilateral) {
        Point[] corners = quadrilateral.getCorners();
        final int lastCorner = 3;
        double d1 = PointAction.calcDistance(corners[0], corners[2]);
        double d2 = PointAction.calcDistance(corners[1], corners[lastCorner]);
        double sin = Math.sqrt(1 - Math
                .pow(LineAction.calcAngleCos(new Line(corners[0], corners[2]),
                        new Line(corners[1], corners[lastCorner])), 2));
        final double coefficient = 0.5;
        return coefficient * d1 * d2 * sin;
    }

    /**
     * Check if quadrilateral is a square.
     *
     * @param  quadrilateral source quadrilateral
     * @return               {@code true} if quadrilateral is a square,
     *                       {@code false} otherwise
     */
    public boolean isSquare(final Quadrilateral quadrilateral) {
        if (!isConvex(quadrilateral)) {
            return false;
        }
        final int lastCorner = 3;
        Point[] corners = quadrilateral.getCorners();
        double d1 = PointAction.calcDistance(corners[0], corners[2]);
        double d2 = PointAction.calcDistance(corners[1], corners[lastCorner]);
        double cos = LineAction.calcAngleCos(new Line(corners[0], corners[2]),
                new Line(corners[1], corners[lastCorner]));

        return d1 == d2 && (Math.abs(cos) < delta
                || Double.compare(cos, Double.NaN) == 0);
    }

    /**
     * Check if quadrilateral is a trapeze.
     *
     * @param  quadrilateral source quadrilateral
     * @return               {@code true} if quadrilateral is a trapeze,
     *                       {@code false} otherwise
     */
    public boolean isTrapeze(final Quadrilateral quadrilateral) {
        if (!isConvex(quadrilateral)) {
            return false;
        }
        final int lastCorner = 3;
        Point[] corners = quadrilateral.getCorners();
        double cos1 = LineAction.calcAngleCos(new Line(corners[0], corners[1]),
                new Line(corners[2], corners[lastCorner]));
        double cos2 = LineAction.calcAngleCos(new Line(corners[1], corners[2]),
                new Line(corners[lastCorner], corners[0]));
        return Math.abs(cos1) > 1.0 - delta && Math.abs(cos2) < 1.0 - delta
                || Math.abs(cos1) < 1.0 - delta && Math.abs(cos2) > 1.0 - delta;
    }

    /**
     * Check if quadrilateral is a rhombus.
     *
     * @param  quadrilateral source quadrilateral
     * @return               {@code true} if quadrilateral is a rhombus,
     *                       {@code false} otherwise
     */
    public boolean isRhombus(final Quadrilateral quadrilateral) {
        if (!isConvex(quadrilateral)) {
            return false;
        }
        final int lastCorner = 3;
        Point[] corners = quadrilateral.getCorners();
        double cos = LineAction.calcAngleCos(new Line(corners[0], corners[2]),
                new Line(corners[1], corners[lastCorner]));
        return Math.abs(cos) < delta || Double.compare(cos, Double.NaN) == 0;
    }

    /**
     * Check if quadrilateral is convex.
     *
     * @param  quadrilateral source quadrilateral
     * @return               {@code true} if quadrilateral is convex,
     *                       {@code false} otherwise
     */
    public boolean isConvex(final Quadrilateral quadrilateral) {
        Point[] corners = quadrilateral.getCorners();
        final int lastCorner = 3;
        Point point = LineAction.findIntersectPoint(
                new Line(corners[0], corners[1]),
                new Line(corners[2], corners[lastCorner]));
        if (point != null
                && (PointAction.isInBoundedBox(corners[0], corners[1], point)
                        || PointAction.isInBoundedBox(corners[2],
                                corners[lastCorner], point))) {
            return false;
        }
        point = LineAction.findIntersectPoint(new Line(corners[1], corners[2]),
                new Line(corners[lastCorner], corners[0]));
        return point == null
                || !PointAction.isInBoundedBox(corners[0], corners[1], point)
                        && !PointAction.isInBoundedBox(corners[2],
                                corners[lastCorner], point);
    }

    /**
     * {@inheritDoc}
     */
    public Quadrant getQuadrant(final Quadrilateral quadrilateral) {
        Point[] corners = quadrilateral.getCorners();
        if (isFirstQuadrant(corners)) {
            return Quadrant.FIRST;
        } else if (isSecondQuadrant(corners)) {
            return Quadrant.SECOND;
        } else if (isThirdQuadrant(corners)) {
            return Quadrant.THIRD;
        } else if (isFourthQuadrant(corners)) {
            return Quadrant.FOURTH;
        }
        return null;
    }

    /**
     * Checks if corners lie in fourth quadrant.
     *
     * @param  corners quandrilateral's corners
     * @return         {@code true} if all corners lie in fourth quadrant
     *                 {@code false} otherwise
     */
    private boolean isFourthQuadrant(final Point[] corners) {
        for (int i = 0; i < corners.length; i++) {
            if (corners[i].getX() <= 0 || corners[i].getY() >= 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if corners lie in third quadrant.
     *
     * @param  corners quandrilateral's corners
     * @return         {@code true} if all corners lie in third quadrant
     *                 {@code false} otherwise
     */
    private boolean isThirdQuadrant(final Point[] corners) {
        for (int i = 0; i < corners.length; i++) {
            if (corners[i].getX() >= 0 || corners[i].getY() >= 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if corners lie in second quadrant.
     *
     * @param  corners quandrilateral's corners
     * @return         {@code true} if all corners lie in second quadrant
     *                 {@code false} otherwise
     */
    private boolean isSecondQuadrant(final Point[] corners) {
        for (int i = 0; i < corners.length; i++) {
            if (corners[i].getX() >= 0 || corners[i].getY() <= 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks if corners lie in first quadrant.
     *
     * @param  corners quandrilateral's corners
     * @return         {@code true} if all corners lie in first quadrant
     *                 {@code false} otherwise
     */
    private boolean isFirstQuadrant(final Point[] corners) {
        for (int i = 0; i < corners.length; i++) {
            if (corners[i].getX() <= 0 || corners[i].getY() <= 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(delta);
        final int magicNumber = 32;
        result = prime * result + (int) (temp ^ (temp >>> magicNumber));
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        QuadrilateralAction other = (QuadrilateralAction) obj;
        return Double.doubleToLongBits(delta) == Double
                .doubleToLongBits(other.delta);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "QuadrilateralAction [delta=" + delta + "]";
    }

}
