package com.epam.quadrilateral.geometric.action;

import com.epam.quadrilateral.geometric.object.Line;
import com.epam.quadrilateral.geometric.object.Point;

/**
 * Validates inputed geometric figure's data.
 */
public class Validator {

    /**
     * Minimal number of corners for geometric figure.
     */
    public static final int MIN_CORNERS = 3;

    /**
     * Validates if inputed points could be corners of a geometric figure, e.g.
     * if there are no 3 points one line.
     *
     * @param  points array of points that should be validated
     * @return        {@code true} if points could be figure's corners,
     *                {@code false} otherwise
     */
    public boolean isCorners(final Point[] points) {
        if (points.length < MIN_CORNERS) {
            return false;
        }
        for (int i = 0; i < points.length; i++) {
            for (int j = i + 1; j < points.length; j++) {
                for (int k = j + 1; k < points.length; k++) {
                    if (LineAction.isPointsOnOneLine(points[i], points[j],
                            points[k])) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Validates if figure is crossed, i.e. it has a side(s) that crosses at
     * least one other side.
     *
     * @param  points array represents corners of a geometric figure. Points
     *                should be ordered so that figure's sides attach two
     *                adjacent array's points
     * @return        {@code true} if figure isn't crossed, {@code false}
     *                otherwise
     */
    public boolean isCrossedFigure(final Point[] points) {
        if (points.length <= MIN_CORNERS) {
            return false;
        }
        final int notAdjacentEdgeIndent = 2;
        Point point;
        for (int i = 0; i < points.length - 1; i++) {
            for (int j = i + notAdjacentEdgeIndent, k = j
                    + 1;; j++, k++) {
                if (k >= points.length) {
                    k = 0;
                }
                if (k == i) {
                    break;
                }
                if (j >= points.length) {
                    j = 0;
                }
                point = LineAction.findIntersectPoint(
                        new Line(points[i], points[i + 1]),
                        new Line(points[j], points[k]));
                if (point != null
                        && PointAction.isInBoundedBox(points[i + 1], points[i],
                                point)
                        && PointAction.isInBoundedBox(points[j], points[k],
                                point)) {
                    return true;
                }

            }
        }
        return false;
    }
}
