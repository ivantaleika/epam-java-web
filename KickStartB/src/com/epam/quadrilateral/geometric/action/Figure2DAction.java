package com.epam.quadrilateral.geometric.action;

import com.epam.quadrilateral.geometric.Quadrant;
import com.epam.quadrilateral.geometric.object.Abstract2DFigure;

/**
 *
 * Provides interface for all 2D figures utils classes.
 *
 * @param <T> specific 2D figure class
 */
public interface Figure2DAction<T extends Abstract2DFigure> {
    /**
     * Returns figure's perimeter.
     *
     * @param  figure source figure
     * @return        figure's perimeter
     */
    double calcPerimeter(T figure);

    /**
     * Returns figure's area.
     *
     * @param  figure source figure
     * @return        figure's area
     */
    double calcArea(T figure);

    /**
     * Returns figure's quadrant.
     *
     * @param  figure source figure
     * @return        figure's quadrant if all figure's points lie in the one
     *                quadrant, {@code null} otherwise
     */
    Quadrant getQuadrant(T figure);

}
