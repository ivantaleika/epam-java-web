package com.epam.quadrilateral.geometric.action;

import com.epam.quadrilateral.geometric.object.Point;

/**
 * This class contains various methods for manipulating points or check point's
 * state.
 */
public final class PointAction {
    /**
     * Private constructor to protect from creating utility class objects.
     */
    private PointAction() {
    }

    /**
     * Return distance between two points.
     *
     * @param  p1 line segment start
     * @param  p2 line segment end
     * @return    length of line segment
     */
    public static double calcDistance(final Point p1, final Point p2) {
        return Math.sqrt(Math.pow(p1.getX() - p2.getX(), 2)
                + Math.pow(p1.getY() - p2.getY(), 2));
    }

    /**
     * Check if {@code point} coordinates are in a rectangle where {@code start}
     * and {@code end} points are start and end of its diagonal.
     *
     * @param  start rectangle's diagonal first point
     * @param  end   rectangle's diagonal second point
     * @param  point point to be verified
     * @return       {@code true} if {@code point} lies in bound, {@code false}
     *               otherwise
     */
    public static boolean isInBoundedBox(final Point start, final Point end,
            final Point point) {
        double xMax = Double.max(start.getX(), end.getX());
        double xMin = Double.min(start.getX(), end.getX());
        double yMax = Double.max(start.getY(), end.getY());
        double yMin = Double.min(start.getY(), end.getY());
        return point.getX() <= xMax && point.getX() >= xMin
                && point.getY() <= yMax && point.getY() >= yMin;

    }

}
