package com.epam.quadrilateral.observer;

/**
 * Provides methods for class that should send events to its
 * {@link Subscriber}s.
 *
 */
public interface Publisher {
    /**
     * Adds the given Subscriber if possible.
     *
     * @param subscriber the subscriber
     */
    void subscribe(Subscriber subscriber);

    /**
     * Delete the given Subscriber. It no longer will receive notifications from
     * Publisher.
     *
     * @param subscriber the subscriber
     */
    void unsubscribe(Subscriber subscriber);
}
