package com.epam.quadrilateral.observer;

/**
 * A receiver of messages from {@link Publisher}.
 */
public interface Subscriber {
    /**
     * Notification that Publisher has been updated.
     */
    void update();
}
