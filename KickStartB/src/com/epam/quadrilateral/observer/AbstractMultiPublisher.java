package com.epam.quadrilateral.observer;

import java.util.HashSet;
import java.util.Set;

/**
 * Abstract class for a figure that can send notifications about changes.
 */
public abstract class AbstractMultiPublisher implements Publisher {
    /**
     * Objects that will receive notifications from a figure.
     */
    private final Set<Subscriber> subscribers;

    /**
     * Simple constructor.
     */
    public AbstractMultiPublisher() {
        subscribers = new HashSet<>();
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public void subscribe(final Subscriber subscriber) {
        subscribers.add(subscriber);
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public void unsubscribe(final Subscriber subscriber) {
        subscribers.remove(subscriber);

    }

    /**
     * Send signal that figure was updated.
     */
    public void notifySubscribers() {
        subscribers.forEach(s -> s.update());
    }
}
