package com.epam.quadrilateral.dataimporter;

import java.io.IOException;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.quadrilateral.dataprovider.RegistratorProvider;
import com.epam.quadrilateral.geometric.object.Quadrilateral;
import com.epam.quadrilateral.repository.QuadrilateralRepository;

/**
 * Organizes import from source files to repository.
 */
public class ImportManager {

    /**
     * ImportManager's logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Import data from source file to repository.
     *
     * @param  sourcePath  source file path
     * @throws IOException if file doesn't exist or cannot be read
     */
    public void importData(final String sourcePath) throws IOException {
        Stream<Quadrilateral> quadrilaterals;
        try {
            quadrilaterals = new QuadrilateralImporter().fromTxt(sourcePath);
        } catch (IOException e) {
            LOGGER.error("file {} doesn't exist or can't be read.",
                    e.getMessage());
            throw e;
        }
        QuadrilateralRepository repository = QuadrilateralRepository
                .getInstance();
        quadrilaterals.forEach(q -> repository.save(q,
                RegistratorProvider.addQuadrilateralRegistrator(q)));

    }

}
