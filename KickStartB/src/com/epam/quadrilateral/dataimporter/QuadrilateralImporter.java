package com.epam.quadrilateral.dataimporter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.quadrilateral.exception.KSException;
import com.epam.quadrilateral.geometric.factory.QuadrilateralFactory;
import com.epam.quadrilateral.geometric.factory.QuadrilateralInfo;
import com.epam.quadrilateral.geometric.object.Quadrilateral;
import com.epam.quadrilateral.parser.QuadrilateralParser;

/**
 * Imports quadrilaterals from data source to repository.
 */
public class QuadrilateralImporter {

    /**
     * QuadrilateralImporterTest's logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Imports {@link Quadrilateral}s objects from txt file.
     *
     * @param  filePath    txt file path
     * @return             {@code Stream} of {@link Quadrilateral}s
     * @throws IOException if file doesn't exist or cannot be read
     */
    public Stream<Quadrilateral> fromTxt(final String filePath)
            throws IOException {
        return Files.lines(Paths.get(filePath)).flatMap(s -> parseSource(s))
                .flatMap(i -> createQuadrilaterals(i));

    }

    /**
     * Tries to parse source string to get {@link QuadrilateralInfo} object and
     * logs parsing errors.
     *
     * @param  string source string
     * @return        QuadrilateralInfo object from source string
     */
    private Stream<QuadrilateralInfo> parseSource(final String string) {
        try {
            return Stream.of(QuadrilateralParser.parseTxtInfo(string));
        } catch (KSException e) {
            LOGGER.warn(e.getMessage());
            return Stream.empty();
        }
    }

    /**
     * Tries to create {@link Quadrilateral} from {@link QuadrilateralInfo} and
     * logs creating errors.
     *
     * @param  info {@link QuadrilateralInfo} object
     * @return      Stream from created Quadrilateral or empty Stream if error
     *              occurred.
     */
    private Stream<Quadrilateral> createQuadrilaterals(
            final QuadrilateralInfo info) {
        try {
            return Stream.of(QuadrilateralFactory.create(info));
        } catch (KSException e) {
            LOGGER.warn("can't create {} quadrilateral: {}", info,
                    e.getMessage());
            return Stream.empty();
        }
    }

}
