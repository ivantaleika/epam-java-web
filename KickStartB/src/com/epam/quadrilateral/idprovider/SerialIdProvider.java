package com.epam.quadrilateral.idprovider;
/**
 * Provides a simple unique id as a {@code long} variable.
 */
public final class SerialIdProvider {
    /**
     * Next providing id.
     */
    private static long id;

    /**
     * Returns and unique id number.
     *
     * @return unique id number
     */
    public static long getId() {
        return id++;
    }

    /**
     * Prevents from creating an utility class.
     */
    private SerialIdProvider() {
    }
}
