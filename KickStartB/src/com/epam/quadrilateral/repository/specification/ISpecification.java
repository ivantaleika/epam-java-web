package com.epam.quadrilateral.repository.specification;

import java.util.Map;

/**
 * /** Interface for specifications to {@code PairRepository}.
 *
 * @param <K> repository key's type
 * @param <V> repository value's type
 */
public interface ISpecification<K, V> {
    /**
     * Method that will be used by repository to execute specification.
     *
     * @param  source repository's data
     * @return        data that pass the specification
     */
    Map<K, V> execute(Map<K, V> source);
}
