package com.epam.quadrilateral.repository.specification.sort;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.epam.quadrilateral.geometric.object.Abstract2DFigure;
import com.epam.quadrilateral.registrator.Figure2DRegistrator;
import com.epam.quadrilateral.repository.specification.ISpecification;

/**
* Sort figures by its ID.
*
* @param <K> 2D figure's type
* @param <V> 2D figure registrator type
*/
public class SortById<K extends Abstract2DFigure,
    V extends Figure2DRegistrator<K>>
        implements ISpecification<K, V> {

    /**
     * Sort type.
     */
    private SortType type;

    /**
     * Creates specification with given {@link SortType}.
     *
     * @param sortType type of performed sorting
     */
    public SortById(final SortType sortType) {
        type = sortType;
    }

    /**
     * {@inheritDoc}. Sort figures by it ID.
     */
    @Override
    public Map<K, V> execute(final Map<K, V> source) {
        Map<K, V> sortedMap = new LinkedHashMap<>();
        List<K> list = new LinkedList<>(source.keySet());
        Comparator<K> comparator = Comparator.comparingLong(K::getId);
        if (type == SortType.DESCENDING) {
            comparator = comparator.reversed();
        }
        list.sort(comparator);
        for (K figure : list) {
            sortedMap.put(figure, source.get(figure));
        }
        return sortedMap;
    }

    /**
     * @return the type
     */
    public SortType getType() {
        return type;
    }

    /**
     * @param newType the type to set
     */
    public void setType(final SortType newType) {
        type = newType;
    }

}
