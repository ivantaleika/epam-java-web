/**
 * Contains sort specifications which perform sort of repository's data.
 */
package com.epam.quadrilateral.repository.specification.sort;
