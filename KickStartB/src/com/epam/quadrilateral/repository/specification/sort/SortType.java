package com.epam.quadrilateral.repository.specification.sort;

/**
 * Defines types for sorting.
 */
public enum SortType {
    /**
     * Types.
     */
    ASCENDING, DESCENDING
}
