/**
 * Contains search specifications which perform search in repository.
 */
package com.epam.quadrilateral.repository.specification.search;
