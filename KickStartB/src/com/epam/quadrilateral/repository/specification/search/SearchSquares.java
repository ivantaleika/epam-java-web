package com.epam.quadrilateral.repository.specification.search;

import java.util.Map;

import com.epam.quadrilateral.geometric.action.QuadrilateralAction;
import com.epam.quadrilateral.geometric.object.Quadrilateral;
import com.epam.quadrilateral.registrator.Figure2DRegistrator;
import com.epam.quadrilateral.repository.specification.ISpecification;

/**
 * Search for quadrilaterals which are sqares.
 */
public class SearchSquares implements
        ISpecification<Quadrilateral, Figure2DRegistrator<Quadrilateral>> {

    /**
     * Search for quadrilaterals which are sqares.
     */
    @Override
    public Map<Quadrilateral, Figure2DRegistrator<Quadrilateral>> execute(
            final Map<Quadrilateral, Figure2DRegistrator<Quadrilateral>>
                source) {
        QuadrilateralAction action = new QuadrilateralAction();
        source.keySet().removeIf(q -> !action.isSquare(q));
        return source;
    }

}
