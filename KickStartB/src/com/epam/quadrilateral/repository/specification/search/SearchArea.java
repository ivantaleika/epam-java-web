package com.epam.quadrilateral.repository.specification.search;

import java.util.Map;

import com.epam.quadrilateral.geometric.object.Abstract2DFigure;
import com.epam.quadrilateral.registrator.Figure2DRegistrator;
import com.epam.quadrilateral.repository.specification.ISpecification;

/**
 * Search for 2D figures with area between min and max values.
 *
 * @param <K> 2D figure's type
 * @param <V> 2D figure registrator type
 */
public class SearchArea<K extends Abstract2DFigure,
    V extends Figure2DRegistrator<K>> implements ISpecification<K, V> {

    /**
     * Wanted figure's min area.
     */
    private double min;
    /**
     * Wanted figure's max area.
     */
    private double max;

    /**
     * Constructs specification for given min and max areas.
     *
     * @param minArea wanted figure's min area
     * @param maxArea wanted figure's max area
     */
    public SearchArea(final double minArea, final double maxArea) {
        min = minArea;
        max = maxArea;
    }

    /**
     * Search for 2D figures with area between min and max values.
     */
    @Override
    public Map<K, V> execute(final Map<K, V> source) {
        source.values().removeIf(registrator -> registrator.getArea() < min
                || registrator.getArea() > max);
        return source;
    }

    /**
     * @return the min area
     */
    public double getMin() {
        return min;
    }

    /**
     * @param minArea the min area to set
     */
    public void setMin(final double minArea) {
        min = minArea;
    }

    /**
     * @return the max area
     */
    public double getMax() {
        return max;
    }

    /**
     * @param maxArea the max area to set
     */
    public void setMax(final double maxArea) {
        max = maxArea;
    }

}
