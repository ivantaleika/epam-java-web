package com.epam.quadrilateral.repository.specification.search;

import java.util.Map;

import com.epam.quadrilateral.geometric.object.Abstract2DFigure;
import com.epam.quadrilateral.registrator.Figure2DRegistrator;
import com.epam.quadrilateral.repository.specification.ISpecification;

/**
 * Search for 2D figures with perimeter between min and max values.
 *
 * @param <K> 2D figure's type
 * @param <V> 2D figure registrator type
 */
public class SearchPerimeter<K extends Abstract2DFigure,
    V extends Figure2DRegistrator<K>> implements ISpecification<K, V> {

    /**
     * Wanted figure's min perimeter.
     */
    private double min;
    /**
     * Wanted figure's max perimeter.
     */
    private double max;

    /**
     * Constructs specification for given min and max perimeters.
     *
     * @param minPerimeter wanted figure's min perimeter
     * @param maxPerimeter wanted figure's max perimeter
     */
    public SearchPerimeter(final double minPerimeter,
            final double maxPerimeter) {
        setMin(minPerimeter);
        setMax(maxPerimeter);
    }

    /**
     * Search for 2D figures with perimeter between min and max values.
     */
    @Override
    public Map<K, V> execute(final Map<K, V> source) {
        source.values().removeIf(registrator -> registrator.getPerimeter() < min
                || registrator.getPerimeter() > max);
        return source;
    }

    /**
     * @return the min perimeter
     */
    public double getMin() {
        return min;
    }

    /**
     * @param minPerimeter the min perimeter to set
     */
    public void setMin(final double minPerimeter) {
        min = minPerimeter;
    }

    /**
     * @return the max perimeter
     */
    public double getMax() {
        return max;
    }

    /**
     * @param maxPerimeter the max perimeter to set
     */
    public void setMax(final double maxPerimeter) {
        max = maxPerimeter;
    }

}
