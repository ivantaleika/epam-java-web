package com.epam.quadrilateral.repository.specification.search;

import java.util.Map;

import com.epam.quadrilateral.geometric.object.Abstract2DFigure;
import com.epam.quadrilateral.registrator.Figure2DRegistrator;
import com.epam.quadrilateral.repository.specification.ISpecification;

/**
 * Search for 2D figures with specified name.
 *
 * @param <K> 2D figure's type
 * @param <V> 2D figure registrator type
 */
public class SearchName<K extends Abstract2DFigure,
    V extends Figure2DRegistrator<K>> implements ISpecification<K, V> {

    /**
     * Wanted figure's name.
     */
    private String name;

    /**
     * Constructs specification for given name.
     *
     * @param figureName wanted figure's name
     */
    public SearchName(final String figureName) {
        name = figureName;
    }

    /**
     * Search for figures with specified name.
     */
    @Override
    public Map<K, V> execute(final Map<K, V> source) {
        source.keySet().removeIf(figure -> !figure.getName().equals(name));
        return source;
    }

    /**
     * @return the name
     */
    public String getId() {
        return name;
    }

    /**
     * @param figureName the name to set
     */
    public void setName(final String figureName) {
        name = figureName;
    }

}
