package com.epam.quadrilateral.repository.specification.search;

import java.util.Map;

import com.epam.quadrilateral.geometric.object.Abstract2DFigure;
import com.epam.quadrilateral.registrator.Figure2DRegistrator;
import com.epam.quadrilateral.repository.specification.ISpecification;

/**
 * Search for 2D figures with specified id.
 *
 * @param <K> 2D figure's type
 * @param <V> 2D figure registrator type
 */
public class SearchId<K extends Abstract2DFigure,
    V extends Figure2DRegistrator<K>> implements ISpecification<K, V> {

    /**
     * Wanted figure's id.
     */
    private long id;

    /**
     * Constructs specification for given id.
     *
     * @param figureId wanted figure's id
     */
    public SearchId(final long figureId) {
        setId(figureId);
    }

    /**
     * Search for figures with specified id.
     */
    @Override
    public Map<K, V> execute(final Map<K, V> source) {
        source.keySet().removeIf(figure -> figure.getId() != id);
        return source;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param figureId the id to set
     */
    public void setId(final long figureId) {
        id = figureId;
    }

}
