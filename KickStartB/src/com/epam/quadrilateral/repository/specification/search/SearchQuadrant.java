package com.epam.quadrilateral.repository.specification.search;

import java.util.Map;

import com.epam.quadrilateral.geometric.Quadrant;
import com.epam.quadrilateral.geometric.object.Abstract2DFigure;
import com.epam.quadrilateral.registrator.Figure2DRegistrator;
import com.epam.quadrilateral.repository.specification.ISpecification;

/**
 * Search for figures which fully lie in specified quadrant.
 *
 * @param <K> 2D figure's type
 * @param <V> 2D figure registrator type
 */
public class SearchQuadrant<K extends Abstract2DFigure,
    V extends Figure2DRegistrator<K>>
        implements ISpecification<K, V> {

    /**
     * Wanted figure's quadrant.
     */
    private Quadrant quadrant;

    /**
     * Constructs specification for quadrant.
     *
     * @param figureQuadrant wanted figure's quadrant
     */
    public SearchQuadrant(final Quadrant figureQuadrant) {
        quadrant = figureQuadrant;
    }

    /**
     * Search for figure which fully lies in specified quadrant.
     */
    @Override
    public Map<K, V> execute(final Map<K, V> source) {
        source.entrySet().removeIf(entry -> entry.getValue().getActionClass()
                .getQuadrant(entry.getKey()) != quadrant);
        return source;
    }

    /**
     * @return the quadrant
     */
    public Quadrant getQuadrant() {
        return quadrant;
    }

    /**
     * @param figureQuadrant the quadrant to set
     */
    public void setQuadrant(final Quadrant figureQuadrant) {
        quadrant = figureQuadrant;
    }

}
