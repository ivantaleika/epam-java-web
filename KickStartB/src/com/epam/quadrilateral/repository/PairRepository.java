package com.epam.quadrilateral.repository;

import java.util.Map;

import com.epam.quadrilateral.repository.specification.ISpecification;

/**
 * Provides access for storage that store elements as keys and values.
 *
 * @param <K> key's type
 * @param <V> value's type
 */
public interface PairRepository<K, V> {
    /**
     * Adds new key into the storage or updates a value if this key has already
     * been stored.
     *
     * @param  key   new key
     * @param  value associated value
     * @return       always {@code true}
     */
    boolean save(K key, V value);

    /**
     * Returns copy of keys and values from storage as a {@code Map} object.
     *
     * @return storage's objects.
     */
    Map<K, V> getAll();

    /**
     * Returns copy of keys and values specified by {@link ISpecification}
     * object.
     *
     * @param  specification an object that implements {@code ISpecification}
     *                       interface
     * @return               {@code Set} of specified keys
     */
    Map<K, V> query(ISpecification<K, V> specification);

    /**
     * Deletes an Entry specified by the {@code key}.
     *
     * @param  key the key
     * @return     {@code true} if key was found and deleted, {@code false}
     *             otherwise
     */
    boolean delete(K key);
}
