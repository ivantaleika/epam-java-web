package com.epam.quadrilateral.repository;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.quadrilateral.geometric.action.QuadrilateralAction;
import com.epam.quadrilateral.geometric.object.Quadrilateral;
import com.epam.quadrilateral.registrator.Figure2DRegistrator;
import com.epam.quadrilateral.repository.specification.ISpecification;

/**
 * Immutable {@link PairRepository} realization to store {@link Quadrilateral}
 * objects with its {@link Figure2DRegistrator} registrators.
 *
 */
public final class QuadrilateralRepository implements
        PairRepository<Quadrilateral, Figure2DRegistrator<Quadrilateral>> {
    /**
     * Registrator's storage.
     */
    private final Map<Quadrilateral, Figure2DRegistrator<Quadrilateral>>
        storage = new HashMap<>();

    /**
     * QuadrilateralImporterTest logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Allows singleton lazy implementation.
     */
    private static class SingletonHolder {
        /**
         * Single instance of QuadrilateralRepository.
         */
        private static final QuadrilateralRepository REPOSITORY =
                new QuadrilateralRepository();
    }

    /**
     * Prevents direct creation of singleton object.
     */
    private QuadrilateralRepository() {
    }

    /**
     * @return global instance of {@code QuadrilateralRepository}
     */
    public static QuadrilateralRepository getInstance() {
        return SingletonHolder.REPOSITORY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean save(final Quadrilateral key,
            final Figure2DRegistrator<Quadrilateral> value) {
        Entry<Quadrilateral, Figure2DRegistrator<Quadrilateral>> entry =
                copyData(key, value);
        storage.put(entry.getKey(), entry.getValue());
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<Quadrilateral, Figure2DRegistrator<Quadrilateral>> getAll() {
        HashMap<Quadrilateral, Figure2DRegistrator<Quadrilateral>> copy =
                new HashMap<>();
        storage.forEach((key, value) -> {
            Entry<Quadrilateral, Figure2DRegistrator<Quadrilateral>> entry =
                    copyData(key, value);
            copy.put(entry.getKey(), entry.getValue());
        });
        return copy;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean delete(final Quadrilateral key) {
        return storage.remove(key) != null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<Quadrilateral, Figure2DRegistrator<Quadrilateral>> query(
            final ISpecification<Quadrilateral,
            Figure2DRegistrator<Quadrilateral>> specification) {
        return LOGGER.traceExit(specification + ", result: {}",
                specification.execute(getAll()));
    }

    /**
     * Makes deep data copy to make repository immutable.
     *
     * @param key   quadrilateral
     * @param value quadrilateral's registrator
     * @return      deep input copy
     */
    private Entry<Quadrilateral, Figure2DRegistrator<Quadrilateral>> copyData(
            final Quadrilateral key,
            final Figure2DRegistrator<Quadrilateral> value) {
        Quadrilateral keyCopy = new Quadrilateral(key.getCorners(),
                key.getName(), key.getId());
        QuadrilateralAction action = new QuadrilateralAction(
                ((QuadrilateralAction) value.getActionClass()).getDelta());
        Figure2DRegistrator<Quadrilateral> valueCopy =
                new Figure2DRegistrator<Quadrilateral>(keyCopy, action);
        return new AbstractMap.SimpleEntry<Quadrilateral,
                Figure2DRegistrator<Quadrilateral>>(keyCopy, valueCopy);

    }

    //    /**
    //     * {@inheritDoc}
    //     */
    //    @Override
    //    public String toString() {
    //        return "QuadrilateralRepository [storage=" + storage + "]";
    //    }

}
