package com.epam.quadrilateral.dataprovider;

import com.epam.quadrilateral.exception.KSException;
import com.epam.quadrilateral.geometric.action.QuadrilateralAction;
import com.epam.quadrilateral.geometric.object.Abstract2DFigure;
import com.epam.quadrilateral.geometric.object.Quadrilateral;
import com.epam.quadrilateral.registrator.Figure2DRegistrator;

/**
 * Provides an {@code Figure2DRegistrator} objects for
 * {@code Abstract2DFigure}s.
 */
public final class RegistratorProvider {

    /**
     * Prevents from utility object creation.
     */
    private RegistratorProvider() {
    }

    /**
     * Constructs and subscribes a {@link Figure2DRegistrator} object for a
     * given {@link Abstract2DFigure}.
     *
     * @param  figure      that need to be provided with registrator
     * @return             figure's registrator
     * @throws KSException if figure is unknown for manager.
     */
    public static Figure2DRegistrator<? extends Abstract2DFigure>
        addRegistrator(final Abstract2DFigure figure) throws KSException {
        if (figure instanceof Quadrilateral) {
            return addQuadrilateralRegistrator((Quadrilateral) figure);
        }
        throw new KSException("Unknown figure.");
    }

    /**
     * Constructs and subscribes a registrator object for a given quadrilateral.
     *
     * @param  quadrilateral that need to be provided with registrator
     * @return               figure's registrator
     */
    public static Figure2DRegistrator<Quadrilateral>
        addQuadrilateralRegistrator(final Quadrilateral quadrilateral) {
        Figure2DRegistrator<Quadrilateral> registrator =
                new Figure2DRegistrator<>(
                        quadrilateral, new QuadrilateralAction());
        quadrilateral.subscribe(registrator);
        return registrator;
    }

}
