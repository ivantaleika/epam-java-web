package com.epam.infohandling.expression;

/**
 * Interface for numeric expressions.
 */
@FunctionalInterface
public interface Expression {
    /**
     * Calculates the value of an expression.
     *
     * @return the value of the expression.
     */
    int calculate();
}
