package com.epam.infohandling.expression;

/**
 * Represents integer constant.
 */
public class Constant implements Expression {
    /**
     * Constant's value.
     */
    private int value;

    /**
     * Creates constant from its value.
     * @param constantValue the value of the Constant
     */
    public Constant(final int constantValue) {
        value = constantValue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int calculate() {
        return value;
    }

    /**
     * @param constantValue the value to set
     */
    public void setValue(final int constantValue) {
        value = constantValue;
    }
}
