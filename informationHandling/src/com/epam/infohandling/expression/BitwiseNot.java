package com.epam.infohandling.expression;

/**
 * Represents "~" operation.
 */
public class BitwiseNot extends UnaryOperator {
    /**
     * Constructs operator from its operand.
     *
     * @param operand the operand to set
     */
    public BitwiseNot(final Expression operand) {
        super(operand);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int calculate() {
        return ~getOperand().calculate();
    }

}
