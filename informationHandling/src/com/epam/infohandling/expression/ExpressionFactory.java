package com.epam.infohandling.expression;

import com.epam.infohandling.exception.IHException;

/**
 * Static factory for {@linkplain Expression} classes.
 */
public final class ExpressionFactory {
    /**
     * Protects from creating an utility object.
     */
    private ExpressionFactory() {
    }

    /**
     * Creates a {@linkplain Constant} object for given {@code number}.
     *
     * @param  number Constant's value
     * @return        Constant for given {@code value}
     */
    public static Constant createConstant(final int number) {
        return new Constant(number);
    }

    /**
     * Creates Expression object that represents binary {@code operator}.
     *
     * @param  operator     binary operator constant
     * @param  leftOperand  right operand of the operator
     * @param  rightOperand right operand of the operator
     * @return              Expression object that represents {@code operator}
     * @throws IHException  if operator is unary
     */
    public static Expression createBinaryOperator(
            final SupportedOperator operator, final Expression leftOperand,
            final Expression rightOperand) throws IHException {
        switch (operator) {
        case BITWISE_AND:
            return new BitwiseAnd(leftOperand, rightOperand);
        case BITWISE_OR:
            return new BitwiseOr(leftOperand, rightOperand);
        case BITWISE_XOR:
            return new BitwiseXor(leftOperand, rightOperand);
        case LEFT_SHIFT:
            return new LeftShift(leftOperand, rightOperand);
        case RIGHT_SHIFT:
            return new RightShift(leftOperand, rightOperand);
        default:
            throw new IHException("Expression " + operator.getRepresantation()
                    + " is an unary operator.");
        }
    }

    /**
     * Creates Expression object that represents unary {@code operator}.
     *
     * @param  operator    unary operator constant
     * @param  operand     operand of the operator
     * @return             Expression object that represents {@code operator}
     * @throws IHException if operator is binary
     */
    public static Expression createUnaryOperator(
            final SupportedOperator operator, final Expression operand)
            throws IHException {
        switch (operator) {
        case BITWISE_NOT:
            return new BitwiseNot(operand);
        default:
            throw new IHException("Expression " + operator.getRepresantation()
                    + " is an binary operator.");
        }
    }

}
