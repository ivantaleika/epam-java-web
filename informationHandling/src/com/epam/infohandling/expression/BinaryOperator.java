package com.epam.infohandling.expression;

/**
 * Contains operands for binary operator.
 */
public abstract class BinaryOperator implements Expression {
    /**
     * Left expression's operand.
     */
    private Expression left;
    /**
     * Right expression's operand.
     */
    private Expression right;

    /**
     * Constructs operator from its operands.
     *
     * @param leftOperand  left operand of binary operator
     * @param rightOperand right operand of binary operator
     */
    public BinaryOperator(final Expression leftOperand,
            final Expression rightOperand) {
        left = leftOperand;
        right = rightOperand;
    }

    /**
     * @return the left
     */
    public Expression getLeft() {
        return left;
    }

    /**
     * @param leftOperand the left to set
     */
    public void setLeft(final Expression leftOperand) {
        left = leftOperand;
    }

    /**
     * @return the right
     */
    public Expression getRight() {
        return right;
    }

    /**
     * @param rightOperand the right to set
     */
    public void setRight(final Expression rightOperand) {
        right = rightOperand;
    }
}
