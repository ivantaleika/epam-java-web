package com.epam.infohandling.expression;

import com.epam.infohandling.exception.IHException;

/**
 * Contains operators constants which are supported as part of a expression.
 */
public enum SupportedOperator {
    /**
     * "~" operation.
     */
    BITWISE_NOT("~", 14, false),
    /**
     * "<<" operation.
     */
    LEFT_SHIFT("<<", 10, true),
    /**
     * ">>" operation.
     */
    RIGHT_SHIFT(">>", 10, true),
    /**
     * "&" operation.
     */
    BITWISE_AND("&", 7, true),
    /**
     * "^" operation.
     */
    BITWISE_XOR("^", 6, true),
    /**
     * "|" operation.
     */
    BITWISE_OR("|", 5, true);

    /**
     * Operator's String representation.
     */
    private String representation;
    /**
     * Operator's priority in expression.
     */
    private int priority;
    /**
     * Unary/binary operator indicator.
     */
    private boolean isBinary;

    /**
     * Creates operator constant from its string representation, priority and
     * binary/unary indicator.
     *
     * @param operatorRepresentation the representation to set
     * @param operatorPriority       the priority to set
     * @param isBinaryOperator       the isBinary to set
     */
    SupportedOperator(final String operatorRepresentation,
            final int operatorPriority, final boolean isBinaryOperator) {
        representation = operatorRepresentation;
        priority = operatorPriority;
        isBinary = isBinaryOperator;
    }

    /**
     * @return the representation
     */
    public String getRepresantation() {
        return representation;
    }

    /**
     * @return the priority
     */
    public int getPriority() {
        return priority;
    }

    /**
     * @return the isBinary
     */
    public boolean isBinary() {
        return isBinary;
    }

    /**
     * Return operator constant which string representation equals to
     * {@code representation}.
     *
     * @param  representation the representation to look for
     * @return                operator constant for the given representation
     * @throws IHException    if no constant was found
     */
    public static SupportedOperator getEnumValue(final String representation)
            throws IHException {
        for (SupportedOperator operator : SupportedOperator.values()) {
            if (operator.representation.equals(representation)) {
                return operator;
            }
        }
        throw new IHException("Unsupported operator.");
    }
}
