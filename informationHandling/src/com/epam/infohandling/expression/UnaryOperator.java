package com.epam.infohandling.expression;

/**
 * Contains an operand for unary operator.
 */
public abstract class UnaryOperator implements Expression {
    /**
     * Operator's operand.
     */
    private Expression operand;

    /**
     * Constructs operator from its operand.
     *
     * @param operatorOperand the operand to set
     */
    public UnaryOperator(final Expression operatorOperand) {
        operand = operatorOperand;
    }

    /**
     * @return the operand
     */
    public Expression getOperand() {
        return operand;
    }

    /**
     * @param operatorOperand the operand to set
     */
    public void setOperand(final Expression operatorOperand) {
        operand = operatorOperand;
    }

}
