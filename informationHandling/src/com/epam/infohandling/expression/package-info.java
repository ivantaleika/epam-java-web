/**
 * Contains classes which represent numeric expression language.
 */
package com.epam.infohandling.expression;
