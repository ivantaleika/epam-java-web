package com.epam.infohandling.expression;

/**
 * Represents ">>" operation.
 */
public class RightShift extends BinaryOperator {

    /**
     * Constructs operation from its operands.
     *
     * @param leftOperand  left operand of binary expression
     * @param rightOperand right operand of binary expression
     */
    public RightShift(final Expression leftOperand,
            final Expression rightOperand) {
        super(leftOperand, rightOperand);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int calculate() {
        return getLeft().calculate() >> getRight().calculate();
    }

}
