package com.epam.infohandling.manager;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.infohandling.exception.IHException;
import com.epam.infohandling.parser.ExpressionLexemeParser;
import com.epam.infohandling.parser.LexemeParser;
import com.epam.infohandling.parser.ParagraphParser;
import com.epam.infohandling.parser.Parser;
import com.epam.infohandling.parser.PunctuationParser;
import com.epam.infohandling.parser.SentenceParser;
import com.epam.infohandling.parser.TextDocumentParser;
import com.epam.infohandling.parser.WordParser;
import com.epam.infohandling.text.Component;
import com.epam.infohandling.text.ComponentException;
import com.epam.infohandling.text.ExpressionLexeme;
import com.epam.infohandling.text.Paragraph;
import com.epam.infohandling.text.Sentence;

/**
 * Helps to work with text composit structure.
 */
public class CompositManager {
  /**
   * Chain of parsers to parse text data.
   */
  private Parser parserChain;
  /**
   * Parser's logger.
   */
  private static final Logger LOGGER = LogManager.getLogger();

  /**
   * Constructs {@code CompositManager}.
   */
  public CompositManager() {
    parserChain = new TextDocumentParser();
    parserChain.linkParser(new ParagraphParser())
        .linkParser(new SentenceParser()).linkParser(new LexemeParser())
        .linkParser(new ExpressionLexemeParser())
        .linkParser(new WordParser())
        .linkParser(new PunctuationParser());
  }

  /**
   * Parses text data and return the {@link Component} which represents data structure.
   *
   * @param data source data.
   * @return the {@link Component} which represents data structure.
   * @throws IHException if data can't be parsed by the parsers chain
   * @throws IllegalArgumentException if data is empty
   */
  public Component parseData(final String data) throws IHException {
    try {
      return parserChain.parse(data);
    } catch (IHException e) {
      /*
       * Exception is logged here, just to show that it should be logged. Usually exception will be
       * logged where we can handle it.
       */
      LOGGER.error(e.getMessage());
      throw e;
    }
  }

  /**
   * @return {@linkplain Comparator} that comparing {@linkplain Paragraph}s by the amount of
   *         sentences in them.
   */
  public static Comparator<Component> compareParagraphBySentenceAmount() {
    return Comparator.comparingInt(CompositManager::getSentenceAmount);
  }

  /**
   * @param paragraph {@linkplain Paragraph} to get sentence amount from
   * @return amount of the sentences in the specified paragraph
   */
  private static int getSentenceAmount(final Component paragraph) {
    return ((Paragraph) paragraph).getComponents().size();
  }

  /**
   * @return {@linkplain Comparator} that comparing {@linkplain Sentence}s by the amount of words in
   *         them.
   */
  public static Comparator<Component> compareSentenceByWordAmount() {
    return Comparator.comparingInt(CompositManager::getWordAmount);
  }

  /**
   * @param sentence {@linkplain Sentence} to get word amount from
   * @return amount of the words in the specified sentence
   */
  private static int getWordAmount(final Component sentence) {
    return ((Sentence) sentence).getComponents().size();
  }

  /**
   * @param symbol char to search for in the lexeme
   * @return {@linkplain Comparator} that comparing {@code Lexeme}s first by descending of amount of
   *         specified symbol in them, then by alphabet.
   */
  public static Comparator<Component> compareLexemeBySymbolAmountDesThenByAlphabet(
      final char symbol) {
    return Comparator.comparing(Component::getText,
        Comparator
            .<String>comparingLong(string -> string.chars()
                .filter(ch -> ch == symbol).count())
            .reversed().thenComparing(Comparator.naturalOrder()));
  }

  /**
   * Sorts subcomponents of the given {@code component} which type is {@code subcomponentType} using
   * the {@code comparator}.
   *
   * @param component the {@linkplain Component} that stores subcomponents which type is
   *        {@code subcomponentType}
   * @param subcomponentType the type of sorted components
   * @param comparator the {@linkplain Comparator} used to compare sorted subcomponents
   * @throws ComponentException if {@code component} can't have subcomponents with the
   *         {@code subcomponentType} type
   */
  public void sortSubcomponents(final Component component,
      final int subcomponentType,
      final Comparator<? super Component> comparator)
      throws ComponentException {
    if (component.getType() - subcomponentType == 1) {
      component.getComponents().sort(comparator);
    } else {
      if (component.getType() - subcomponentType > 1) {
        for (Component childComponent : component.getComponents()) {
          sortSubcomponents(childComponent, subcomponentType,
              comparator);
        }
      } else {
        throw new ComponentException(component.getClass().getName()
            + " Does not support specified component"
            + " type as a subcomponent.");
      }
    }
  }

  /**
   * Return all subcomponents of the {@code component} which type is {@code subcomponentType}.
   *
   * @param component the {@linkplain Component} that stores subcomponents which type is
   *        {@code subcomponentType}
   * @param subcomponentType the type of components to search for
   * @return List of subcomponents of the {@code component} which type is {@code subcomponentType}.
   * @throws ComponentException if {@code component} can't have subcomponents with the
   *         {@code subcomponentType} type
   */
  public List<Component> getSubcomponents(final Component component,
      final int subcomponentType) throws ComponentException {
    if (component.getType() - subcomponentType == 1) {
      List<Component> list = new LinkedList<>();
      for (Component subcomponent : component.getComponents()) {
        list.add(subcomponent);
      }
      return list;
    } else {
      if (component.getType() - subcomponentType > 1) {
        List<Component> list = new LinkedList<>();
        for (Component childComponent : component.getComponents()) {
          list.addAll(
              getSubcomponents(childComponent, subcomponentType));
        }
        return list;
      } else {
        throw new ComponentException(component.getClass().getName()
            + " Does not support specified component"
            + " type as a subcomponent.");
      }
    }
  }

  /**
   * Sets the value specified in {@code isCalculate} variable to all {@linkplain ExpressionLexeme}s
   * {@code isCalculate} parameters inside the {@code component}.
   *
   * @param component the {@linkplain Component} that stores ExpressionLexeme subcomponents
   * @param isCalculate the isCalculate value from ExpressionLexeme to set
   * @throws ComponentException if {@code component} can't have ExpressionLexeme as a subcomponent
   */
  public void setExpressionIsCalculateValue(final Component component,
      final boolean isCalculate) throws ComponentException {
    List<Component> components;
    try {
      components = getSubcomponents(component, Component.LEXEME_PART);
    } catch (ComponentException e) {
      throw new ComponentException(component.getClass().getName()
          + " Does not support specified ExpressionLexeme"
          + " as a subcomponent.", e);
    }
    for (Component element : components) {
      if (element instanceof ExpressionLexeme) {
        ((ExpressionLexeme) element).setCalculateValue(isCalculate);
      }
    }

  }
}
