package com.epam.infohandling.parser;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.epam.infohandling.exception.IHException;
import com.epam.infohandling.text.Component;
import com.epam.infohandling.text.Punctuation;
import com.epam.infohandling.text.Sentence;

/**
 * Parses sentence data to {@link Sentence} object.
 */
public class SentenceParser extends ParserHelper {
    /**
     * Regex for words split.
     */
    public static final String SPLIT_REGEX = " ";
    /**
     * Regex for end punctuation symbol.
     */
    public static final String PUNCTUATION_REGEX = "\\.{3}|[.!?]$";
    /**
     * {@link Pattern} for {@code PARAGRAPH_REGEX}.
     */
    private static final Pattern PATTERN = Pattern.compile(PUNCTUATION_REGEX);

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean canParse(final String data) {
        return PATTERN.matcher(data).find();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Sentence doParse(final String sentence) throws IHException {
        List<Component> lexemes = new LinkedList<>();
        Matcher matcher = PATTERN.matcher(sentence);
        matcher.find();
        String punctuationString = matcher.group();
        Punctuation endPunctuation = new PunctuationParser()
                .doParse(punctuationString);
        if (getNextParser() != null) {
            String withoutPunctuation = sentence.substring(0,
                    sentence.length() - punctuationString.length());
            for (String lexeme : withoutPunctuation.split(SPLIT_REGEX)) {
                if (!lexeme.isEmpty()) {
                    lexemes.add(getNextParser().parse(lexeme));
                }
            }
        }
        return new Sentence(lexemes, endPunctuation);
    }
}
