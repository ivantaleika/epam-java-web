package com.epam.infohandling.parser;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import com.epam.infohandling.exception.IHException;
import com.epam.infohandling.text.Component;
import com.epam.infohandling.text.Paragraph;

/**
 * Parses paragraph data to {@link Paragraph} object.
 */
public class ParagraphParser extends ParserHelper {
    /**
     * Regex for sentences split.
     */
    public static final String SPLIT_REGEX = "(?<=(\\.{3}|[.!?])) ";
    /**
     * Regex for paragraph identification.
     */
    public static final String PARAGRAPH_REGEX = "    \\S+";
    /**
     * {@link Pattern} for parser.
     */
    private static final Pattern PATTERN = Pattern.compile(PARAGRAPH_REGEX);

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean canParse(final String data) {
        return PATTERN.matcher(data).find();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Paragraph doParse(final String paragraph) throws IHException {
        List<Component> sentences = new LinkedList<>();
        if (getNextParser() != null) {
            for (String sentence : paragraph.split(SPLIT_REGEX)) {
                if (!sentence.isEmpty()) {
                    sentences.add(getNextParser().parse(sentence));
                }
            }
        }
        return new Paragraph(sentences);
    }

}
