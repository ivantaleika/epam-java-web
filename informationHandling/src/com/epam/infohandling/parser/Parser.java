package com.epam.infohandling.parser;

import com.epam.infohandling.exception.IHException;
import com.epam.infohandling.text.Component;

/**
 * Provides chain of responsibility parse interface for text hierarchy parsing.
 */
public interface Parser {
    /**
     * Parses text data and returns an object that represents given text
     * hierarchy.
     *
     * @param  data                     the String data to parse
     * @return                          an object that represents given text
     *                                  hierarchy
     * @throws IHException              if data wasn't parsed by the parsers
     *                                  chain
     * @throws IllegalArgumentException if data is empty
     */
    Component parse(String data) throws IHException;

    /**
     * Link next parser in the chain.
     *
     * @param  nextParser the parser to link
     * @return            linked parser
     */
    Parser linkParser(Parser nextParser);

    /**
     * Unlink next parser in the chain.
     */
    void unlinkParser();

    /**
     * @return next linked parser
     */
    Parser getNextParser();
}
