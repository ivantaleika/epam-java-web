package com.epam.infohandling.parser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.epam.infohandling.exception.IHException;
import com.epam.infohandling.expression.Expression;
import com.epam.infohandling.expression.ExpressionFactory;
import com.epam.infohandling.expression.SupportedOperator;

/**
 * Parses Expression data string to the {@link Expression} object.
 */
public class ExpressionParser {

    /**
     * Regex for expression part.
     */
    public static final String REGEX = "\\d+|<<|>>|[\\^|&~()]";
    /**
     * Indicates the end of expression.
     */
    private static final String END = "$";
    /**
     * {@code Pattern} to parse the source data string.
     */
    private static final Pattern PATTERN = Pattern.compile(REGEX);
    /**
     * {@code Matcher} to parse the source data string.
     */
    private Matcher matcher;
    /**
     * Indicates if last match was consumed.
     */
    private boolean isConsumed;
    /**
     * Last String that was match by the {@code matcher}.
     */
    private String lastMatch;

    /**
     * Parses expression string data and create an {@linkplain Expression}
     * object that represents it structure.
     *
     * @param  data        the string data to parse
     * @return             An {@linkplain Expression} object that represents it
     *                     structure
     * @throws IHException if data is incorrect
     */
    public Expression parse(final String data) throws IHException {
        isConsumed = true;
        matcher = PATTERN.matcher(data);
        Expression expression = climb(0);
        if (matcher.find()) {
            throw new IHException("ExpressionLexeme wasn't parsed fully.");
        }
        return expression;
    }

    /**
     * Parse next binary operators with priority lower that {@code priority}.
     *
     * @param  priority    the priority of current operator
     * @return             An {@linkplain Expression} object that represents it
     *                     structure of parsed part of the expression data
     * @throws IHException if data is incorrect
     */
    private Expression climb(final int priority) throws IHException {
        Expression expression = getOperation();
        while (true) {
            String match = getNextMatch();
            try {
                SupportedOperator operator = SupportedOperator
                        .getEnumValue(match);
                if (!operator.isBinary()
                        || operator.getPriority() <= priority) {
                    break;
                }
                consume();
                Expression nextOperation = climb(operator.getPriority());
                expression = ExpressionFactory.createBinaryOperator(operator,
                        expression, nextOperation);
            } catch (IHException e) {
                break;
            }
        }
        return expression;
    }

    /**
     * Get first number, unary operation or start new climb session if the next
     * match is a parentheses.
     *
     * @return             An {@linkplain Expression} object that represents
     *                     structure of parsed part of the expression data
     * @throws IHException if data is incorrect
     */
    private Expression getOperation() throws IHException {
        String match = getNextMatch();
        SupportedOperator operator;
        try {
            operator = SupportedOperator.getEnumValue(match);
        } catch (IHException e) {
            if ("(".equals(match)) {
                return tryParseParentheses();
            } else {
                return tryParseNumber(match);
            }
        }
        if (operator.isBinary()) {
            throw new IHException("Binary operator "
                    + operator.getRepresantation() + " has one operand.");
        }
        consume();
        return ExpressionFactory.createUnaryOperator(operator,
                climb(operator.getPriority()));
    }

    /**
     * Trying to parse expression inside parentheses.
     *
     * @return             An {@linkplain Expression} object that represents
     *                     structure of data inside parentheses
     * @throws IHException if ")" wasn't found
     */
    private Expression tryParseParentheses() throws IHException {
        consume();
        Expression expression = climb(0);
        if (!getNextMatch().equals(")")) {
            throw new IHException("Ivalid parentheses.");
        }
        consume();
        return expression;
    }

    /**
     * Trying to parse data as a number.
     *
     * @param  number string representation of a number
     * @return        Expression that is a {@code Constant}
     */
    private Expression tryParseNumber(final String number) {
        Expression expression = ExpressionFactory
                .createConstant(Integer.parseInt(number));
        consume();
        return expression;
    }

    /**
     * @return If {@code isCosumed} is {@code true} than search for next match,
     *         return last match otherwise. If no match was found returns
     *         {@code END} string.
     */
    private String getNextMatch() {
        if (isConsumed) {
            if (matcher.find()) {
                lastMatch = matcher.group();
                isConsumed = false;
                return lastMatch;
            } else {
                return END;
            }
        }
        return lastMatch;

    }

    /**
     * Marks last match as consumed.
     */
    private void consume() {
        isConsumed = true;
    }
}
