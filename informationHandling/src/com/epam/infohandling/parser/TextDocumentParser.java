package com.epam.infohandling.parser;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import com.epam.infohandling.exception.IHException;
import com.epam.infohandling.text.Component;
import com.epam.infohandling.text.TextDocument;

/**
 * Parses text data to {@link TextDocument} object.
 */
public class TextDocumentParser extends ParserHelper {

    /**
     * Regex for paragraphs split.
     */
    public static final String SPLIT_REGEX = "\\R";
    /**
     * Regex for text identification.
     */
    public static final String TEXT_REGEX = "    \\S+";
    /**
     * {@link Pattern} for parser.
     */
    private static final Pattern PATTERN = Pattern.compile(TEXT_REGEX);
    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean canParse(final String data) {
        return PATTERN.matcher(data).find();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected TextDocument doParse(final String text) throws IHException {
        List<Component> paragraphs = new LinkedList<>();
        if (getNextParser() != null) {
            for (String paragraph : text.split(SPLIT_REGEX)) {
                if (!paragraph.isEmpty()) {
                    paragraphs.add(getNextParser().parse(paragraph));
                }
            }
        }
        return new TextDocument(paragraphs);
    }
}
