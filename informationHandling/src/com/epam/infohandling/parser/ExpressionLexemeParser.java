package com.epam.infohandling.parser;

import java.util.LinkedList;
import java.util.List;

import com.epam.infohandling.exception.IHException;
import com.epam.infohandling.text.CharacterSymbol;
import com.epam.infohandling.text.Component;
import com.epam.infohandling.text.ExpressionLexeme;

/**
 * Parses expression data to {@link ExpressionLexeme} object.
 */
public class ExpressionLexemeParser extends ParserHelper {
    /**
     * ExpressionLexeme's regex.
     */
    public static final String REGEX = "([<>&^|()~]*\\d)+[<>&^|()~]*";
    /**
     * Parser for Expression structure.
     */
    private final ExpressionParser parser;

    /**
     * Creates a parser.
     */
    public ExpressionLexemeParser() {
        super();
        parser = new ExpressionParser();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean canParse(final String data) {
        return data.matches(REGEX);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected ExpressionLexeme doParse(final String lemexe) throws IHException {
        List<Component> characters = new LinkedList<>();
        for (Character character : lemexe.toCharArray()) {
            characters.add(new CharacterSymbol(character));
        }
        return new ExpressionLexeme(characters, parser.parse(lemexe));
    }

}
