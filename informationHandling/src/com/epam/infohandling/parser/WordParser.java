package com.epam.infohandling.parser;

import java.util.LinkedList;
import java.util.List;

import com.epam.infohandling.exception.IHException;
import com.epam.infohandling.text.CharacterSymbol;
import com.epam.infohandling.text.Component;
import com.epam.infohandling.text.Word;

/**
 * Parses word data to {@link Word} object.
 */
public class WordParser extends ParserHelper {
    /**
     * WordTest's regex.
     */
    public static final String REGEX = "[a-zA-Z'-]+";

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean canParse(final String data) {
        return data.matches(REGEX);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Word doParse(final String lemexe) throws IHException {
        List<Component> characters = new LinkedList<>();
        for (Character character : lemexe.toCharArray()) {
            characters.add(new CharacterSymbol(character));
        }
        return new Word(characters);
    }
}
