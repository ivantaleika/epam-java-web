package com.epam.infohandling.parser;

import com.epam.infohandling.exception.IHException;
import com.epam.infohandling.text.Component;

/**
 * Helps parsers to organize text parsing.
 */
public abstract class ParserHelper implements Parser {
    /**
     * Next parser in the chain.
     */
    private Parser parser;

    /**
     * {@inheritDoc}
     */
    @Override
    public Parser linkParser(final Parser nextParser) {
        parser = nextParser;
        return nextParser;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unlinkParser() {
        parser = null;
    }

    /**
     * {@inheritDoc}
     */
    public Parser getNextParser() {
        return parser;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Component parse(final String data) throws IHException {
        if (data.isEmpty()) {
            throw new IllegalArgumentException("Empty source data.");
        }
        if (canParse(data)) {
            return doParse(data);
        }
        return toNext(data);
    }

    /**
     * @param  data elements to parse
     * @return      List of elements from {@code data} which can be parsed by
     *              this parser
     */
    protected abstract boolean canParse(String data);

    /**
     * Parses elements from {@code data}.
     *
     * @param  data        elements to parse
     * @return             List of {@link Component}s which represents source
     *                     data.
     * @throws IHException if data is empty
     */
    protected abstract Component doParse(String data) throws IHException;

    /**
     * Send data to be parsed by next parser in the chain if next parser exists,
     * logs data otherwise.
     *
     * @param  data        text to parse by next parser
     * @return             {@link Component} which represents source text.
     * @throws IHException if data wasn't parsed by the parsers chain
     */
    protected Component toNext(final String data) throws IHException {
        if (parser == null) {
            throw new IHException("Can't parse string: " + data);
        }
        return parser.parse(data);
    }

}
