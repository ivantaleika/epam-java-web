package com.epam.infohandling.parser;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.epam.infohandling.exception.IHException;
import com.epam.infohandling.text.Component;
import com.epam.infohandling.text.Lexeme;

/**
 * Parses lexeme data to {@link Lexeme} object.
 */
public class LexemeParser extends ParserHelper {

    /**
     * Lexeme's regex.
     */
    public static final String REGEX = "\\S+";
    /**
     * Regex for lexeme split.
     */
    public static final String SPLIT_REGEX =
            "((?<=[\"(]))|(?=\\.{3}|[\",:-;.!?)])";
    /**
     * Regex to find lexeme with punctuation.
     */
    public static final String DOES_HAVE_PUNCTUATION_REGEX =
            "(\\([a-zA-Z]+)|([a-zA-Z]+\\))|\\.{3}|[\",:;.!?]";
    /**
     * Pattern to find lexeme with punctuation.
     */
    private static final Pattern PATTERN = Pattern
            .compile(DOES_HAVE_PUNCTUATION_REGEX);

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean canParse(final String data) {
        return data.matches(REGEX);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Lexeme doParse(final String lemexe) throws IHException {
        List<Component> lexemeParts = new LinkedList<>();
        if (getNextParser() != null) {
            Matcher matcher = PATTERN.matcher(lemexe);
            if (matcher.find()) {
                for (String lexemePart : lemexe.split(SPLIT_REGEX)) {
                    if (!lexemePart.isEmpty()) {
                        lexemeParts.add(getNextParser().parse(lexemePart));
                    }
                }
            } else {
                lexemeParts.add(getNextParser().parse(lemexe));
            }
        }
        return new Lexeme(lexemeParts);
    }

}
