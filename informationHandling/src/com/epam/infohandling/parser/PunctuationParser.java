package com.epam.infohandling.parser;

import java.util.LinkedList;
import java.util.List;

import com.epam.infohandling.exception.IHException;
import com.epam.infohandling.text.CharacterSymbol;
import com.epam.infohandling.text.Component;
import com.epam.infohandling.text.Punctuation;

/**
 * Parses punctuation data to {@link Punctuation} object.
 */
public class PunctuationParser extends ParserHelper {
    /**
     * Punctuation's regex.
     */
    public static final String REGEX = "\\.{3}|[\"(),:;\\-.!?]";

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean canParse(final String data) {
        return data.matches(REGEX);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Punctuation doParse(final String lemexe) throws IHException {
        List<Component> characters = new LinkedList<>();
        for (Character character : lemexe.toCharArray()) {
            characters.add(new CharacterSymbol(character));
        }
        return new Punctuation(characters);
    }
}
