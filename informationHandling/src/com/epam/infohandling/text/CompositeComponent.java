package com.epam.infohandling.text;

import java.util.List;

/**
 * Parent class for {@link Component}s which consist from other
 * {@code Component}s.
 */
public abstract class CompositeComponent extends AbstractComponent {

    /**
     * Components from witch this component consist of.
     */
    private List<Component> components;

    /**
     * Constructs component from components from witch it consist of.
     *
     * @param  componentList            components from witch it consist of
     * @param  type                     the type of a component
     * @throws IllegalArgumentException if components type is incorrect
     */
    public CompositeComponent(final List<Component> componentList,
            final int type) {
        super(type);
        if (componentList.isEmpty()) {
            throw new IllegalArgumentException(
                    getClass().getName() + " must have children components.");
        }
        for (Component component : componentList) {
            if (!isValideComponent(component)) {
                throw new IllegalArgumentException(
                        getClass().getSimpleName() + " does not support "
                                + component + " as a component.");
            }
        }
        components = componentList;
    }

    /**
     * @return {@link ComponentList} that contains components from which this
     *         component consist of
     */
    @Override
    public ComponentList getComponents() {
        return new ComponentList(components);
    }

    /**
     * Inserts {@code newComponent} to the given {@code index} in the
     * component's list.
     *
     * @param  component                the component to add
     * @param  index                    component's index
     * @throws IllegalArgumentException if {@code component} type is invalid for
     *                                  this object.
     */
    @Override
    public void insertComponent(final Component component, final int index) {
        if (isValideComponent(component)) {
            components.add(index, component);
        } else {
            throw new IllegalArgumentException(getClass() + " does not support "
                    + component.getClass() + " as a component.");
        }
    }

    /**
     * Add component to the end of component's list.
     *
     * @param component the component to add
     */
    @Override
    public void appendComponent(final Component component) {
        insertComponent(component, components.size());
    }

    /**
     * Remove first founded component from component's list.
     *
     * @param component the component to remove
     */
    @Override
    public void removeComponent(final Component component) {
        components.remove(component);
    }

    /**
     * Remove component with given {@code index} from component's list.
     *
     * @param index the index of component to remove
     */
    @Override
    public void removeComponent(final int index) {
        components.remove(index);
    }

    /**
     * @return the components
     */
    protected List<Component> getChildren() {
        return components;
    }

    /**
     * @param componentList the components to set
     */
    protected void setChildren(final List<Component> componentList) {
        components = componentList;
    }

    /**
     * Checks {@code component} could be subcomponent of this object.
     *
     * @param  component component to add.
     * @return           {@code component} is one level lower that this object,
     *                   {@code false} otherwise.
     */
    public final boolean isValideComponent(final Component component) {
        return getType() - component.getType() == 1;
    }

}
