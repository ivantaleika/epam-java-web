package com.epam.infohandling.text;

import java.util.List;
/**
 * Represents a Punctuation object that consist of {@link CharacterSymbol}s.
 */
public class Punctuation extends LexemePart {

    /**
     * Constructs component from components from witch it consist of.
     *
     * @param  componentList            components from witch it consist of
     * @throws IllegalArgumentException if components type is incorrect
     */
    public Punctuation(final List<Component> componentList) {
        super(componentList);
    }

    /**
     * {@inheritDoc}
     */
    public String getText() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Component component : getComponents()) {
            stringBuilder.append(component.getText());
        }
        return stringBuilder.toString();
    }

}
