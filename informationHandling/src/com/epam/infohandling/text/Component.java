package com.epam.infohandling.text;

/**
 * Primary datatype for entire text data model. It represents the one of text
 * components which have hierarchical structure. It's recommended to extend
 * {@linkplain AbstractComponent} class that implements this interface.
 */
public interface Component {

    /**
     * The component is a symbol.
     */
    int SYMBOL = 0;
    /**
     * The component is a lexeme part.
     */
    int LEXEME_PART = 1;
    /**
     * The component is a lexeme.
     */
    int LEXEME = 2;
    /**
     * The component is a sentence.
     */
    int SENTENCE = 3;
    /**
     * The component is a paragraph.
     */
    int PARAGRAPH = 4;
    /**
     * The component is a text.
     */
    int TEXT = 5;

    /**
     * @return                    {@link ComponentList} that contains components
     *                            from which this component consist of
     * @throws ComponentException if component is atomic (does not consist of
     *                            anything)
     */
    ComponentList getComponents() throws ComponentException;

    /**
     * Inserts {@code newComponent} to the given {@code index} in the
     * component's list.
     *
     * @param  component                the component to add
     * @param  index                    component's index
     * @throws ComponentException       if this object is atomic (does not
     *                                  consist of anything)
     * @throws IllegalArgumentException if {@code component} type is invalid for
     *                                  this object.
     */
    void insertComponent(Component component, int index)
            throws ComponentException;

    /**
     * Add component to the end of component's list.
     *
     * @param  component                the component to add
     * @throws ComponentException       if this object is atomic (does not
     *                                  consist of anything)
     * @throws IllegalArgumentException if {@code component} type is invalid for
     *                                  this object.
     */
    void appendComponent(Component component) throws ComponentException;

    /**
     * Remove first founded component from component's list.
     *
     * @param  component          the component to remove
     * @throws ComponentException if component is atomic (does not consist of
     *                            anything)
     */
    void removeComponent(Component component) throws ComponentException;

    /**
     * Remove component with given {@code index} from component's list.
     *
     * @param  index              the index of component to remove
     * @throws ComponentException if component is atomic (does not consist of
     *                            anything)
     */
    void removeComponent(int index) throws ComponentException;

    /**
     * @return String representation of the component.
     */
    String getText();

    /**
     * @return component's type.
     */
    int getType();
}
