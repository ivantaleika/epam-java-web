package com.epam.infohandling.text;

import java.util.List;

/**
 * Represents a Paragraph object that consist of {@link Sentence}s.
 */
public class Paragraph extends CompositeComponent {
    /**
     * The separator between sentences.
     */
    public static final String SENTENCE_SEPARATOR = " ";
    /**
     * The separator between sentences.
     */
    public static final String PARAGRAPH_INDENT = "    ";

    /**
     * Constructs component from components from witch it consist of.
     *
     * @param  componentList            components from witch it consist of
     * @throws IllegalArgumentException if components type is incorrect
     */
    public Paragraph(final List<Component> componentList) {
        super(componentList, PARAGRAPH);
    }

    /**
     * {@inheritDoc}
     */
    public String getText() {
        StringBuilder stringBuilder = new StringBuilder(PARAGRAPH_INDENT);
        ComponentList components = getComponents();
        stringBuilder.append(components.get(0).getText());
        for (int i = 1; i < components.size(); i++) {
            stringBuilder.append(SENTENCE_SEPARATOR);
            stringBuilder.append(components.get(i).getText());
        }
        return stringBuilder.toString();
    }

}
