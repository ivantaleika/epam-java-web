package com.epam.infohandling.text;

import java.util.List;

/**
 * Represents a Sentence object that consist of {@link LexemePart}s.
 */
public class Sentence extends CompositeComponent {
    /**
     * The separator between words.
     */
    public static final String LEXEME_SEPARATOR = " ";

    /**
     * punctuation at the end of the sentence.
     */
    private Punctuation terminalPunctuation;

    /**
     * Constructs component from components from witch it consist of.
     *
     * @param  componentList            components from witch it consist of
     * @param  terminalSymbols          punctuation at the end of the sentence
     *                                  to set
     * @throws IllegalArgumentException if components type is incorrect
     */
    public Sentence(final List<Component> componentList,
            final Punctuation terminalSymbols) {
        super(componentList, SENTENCE);
        terminalPunctuation = terminalSymbols;
    }

    /**
     * {@inheritDoc}
     */
    public String getText() {
        StringBuilder stringBuilder = new StringBuilder();
        ComponentList components = getComponents();
        stringBuilder.append(components.get(0).getText());
        for (int i = 1; i < components.size(); i++) {
            stringBuilder.append(LEXEME_SEPARATOR);
            stringBuilder.append(components.get(i).getText());
        }
        stringBuilder.append(terminalPunctuation);
        return stringBuilder.toString();
    }


    /**
     * @return sentence terminal punctuation symbol
     */
    public Punctuation getTerminalPunctuation() {
        return terminalPunctuation;
    }

    /**
     * @param newPunctuation the terminalPunctuation to set
     */
    public void setTerminalPunctuation(final Punctuation newPunctuation) {
        terminalPunctuation = newPunctuation;
    }

}
