package com.epam.infohandling.text;

/**
 * Parent class for atomic {@link Component}s. Provides realization of methods
 * that works with components from {@link Component} interface by simply
 * throwing an {@link ComponentException}.
 */
public abstract class TerminalComponent extends AbstractComponent {
    /**
     * Message that is passed in the {@linkplain ComponentException}.
     */
    private static final String EXCEPTION_MESSAGE = "Not supported operation.";
    /**
     * Constructs teminal component from its type.
     *
     * @param componentType the type of a component
     */
    public TerminalComponent(final int componentType) {
        super(componentType);
    }

    /**
     * @throws ComponentException always
     */
    @Override
    public ComponentList getComponents() throws ComponentException {
        throw new ComponentException(EXCEPTION_MESSAGE);
    }

    /**
     * @throws ComponentException always
     */
    @Override
    public void insertComponent(final Component component, final int index)
            throws ComponentException {
        throw new ComponentException(EXCEPTION_MESSAGE);
    }

    /**
     * @throws ComponentException always
     */
    @Override
    public void appendComponent(final Component component)
            throws ComponentException {
        throw new ComponentException(EXCEPTION_MESSAGE);
    }

    /**
     * @throws ComponentException always
     */
    @Override
    public void removeComponent(final Component component)
            throws ComponentException {
        throw new ComponentException(EXCEPTION_MESSAGE);
    }

    /**
     * @throws ComponentException always
     */
    @Override
    public void removeComponent(final int index) throws ComponentException {
        throw new ComponentException("Not supported operation.");
    }
}
