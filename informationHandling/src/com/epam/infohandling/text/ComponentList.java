package com.epam.infohandling.text;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

/**
 * Provides read access and prevents from modifications for {@link Component}s.
 */
public class ComponentList implements Iterable<Component> {
    /**
     * List to store Components.
     */
    private final List<Component> components;

    /**
     * Constructs object from {@link List}.
     *
     * @param componentList the list to set
     */
    public ComponentList(final List<Component> componentList) {
        components = componentList;
    }

    /**
     * Returns the element at the specified position in this list.
     *
     * @param  index                     index of the element to return
     * @return                           the element at the specified position
     *                                   in this list
     * @throws IndexOutOfBoundsException if the index is out of range
     *                                   ({@code index < 0 || index >= size()})
     */
    public Component get(final int index) {
        return components.get(index);
    }

    /**
     * Returns the number of elements in this list. If this list contains more
     * than {@code Integer.MAX_VALUE} elements, returns
     * {@code Integer.MAX_VALUE}.
     *
     * @return the number of elements in this list
     */
    public int size() {
        return components.size();
    }

    /**
     * Returns the index of the first occurrence of the specified element in
     * this list, or -1 if this list does not contain the element. More
     * formally, returns the lowest index i such that Objects.equals(o, get(i)),
     * or -1 if there is no such index.
     *
     * @param  component            element to search for
     * @return                      the index of the first occurrence of the
     *                              specified element in this list, or -1 if
     *                              this list does not contain the element
     * @throws ClassCastException   if the type of the specified element is
     *                              incompatible with this list
     * @throws NullPointerException if the specified element is null and this
     *                              list does not permit null elements
     *                              (optional)
     */
    public int indexOf(final Component component) {
        return components.indexOf(component);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterator<Component> iterator() {
        return components.iterator();
    }

    /**
     * Sorts this list according to the order induced by the specified
     * {@link Comparator}.
     *
     * @param comparator the {@code Comparator} used to compare list elements. A
     *                   {@code null} value indicates that the elements'
     *                   {@linkplain Comparable natural ordering} should be used
     */
    public void sort(final Comparator<? super Component> comparator) {
        components.sort(comparator);
    }

}
