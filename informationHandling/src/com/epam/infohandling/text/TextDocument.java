package com.epam.infohandling.text;

import java.util.List;

/**
 * Represents a TextDocument object that consist of {@link Paragraph}s.
 */
public class TextDocument extends CompositeComponent {
    /**
     * The separator between paragraphs.
     */
    public static final String PARAGRAPH_SEPARATOR = System.lineSeparator();

    /**
     * Constructs component from components from witch it consist of.
     *
     * @param  componentList            components from witch it consist of
     * @throws IllegalArgumentException if components type is incorrect
     */
    public TextDocument(final List<Component> componentList) {
        super(componentList, TEXT);
    }

    /**
     * {@inheritDoc}
     */
    public String getText() {
        StringBuilder stringBuilder = new StringBuilder();
        ComponentList components = getComponents();
        stringBuilder.append(components.get(0).getText());
        for (int i = 1; i < components.size(); i++) {
            stringBuilder.append(PARAGRAPH_SEPARATOR);
            stringBuilder.append(components.get(i).getText());
        }
        return stringBuilder.toString();
    }

}
