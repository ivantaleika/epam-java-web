package com.epam.infohandling.text;

/**
 * Defines {@code equals}, {@code hashCode} and {@code toString} methods whick
 * are based on {@code getText} value.
 */
public abstract class AbstractComponent implements Component {
    /**
     * Component's type.
     */
    private final int type;

    /**
     * Constructs component from its type.
     *
     * @param componentType the type of a component
     */
    public AbstractComponent(final int componentType) {
        type = componentType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result;
        if (getText() != null) {
            result += getText().hashCode();
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Component)) {
            return false;
        }
        Component other = (Component) obj;
        return getText().equals(other.getText());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return getText();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getType() {
        return type;
    }
}
