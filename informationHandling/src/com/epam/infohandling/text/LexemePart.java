package com.epam.infohandling.text;

import java.util.List;

/**
 * Represents elements which are part of a Lexeme.
 */
public abstract class LexemePart extends CompositeComponent {
    /**
     * Constructs component from components from witch it consist of.
     *
     * @param  componentList            components from witch it consist of
     * @throws IllegalArgumentException if components type is incorrect
     */
    public LexemePart(final List<Component> componentList) {
        super(componentList, LEXEME_PART);
    }

}
