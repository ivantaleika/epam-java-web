package com.epam.infohandling.text;

/**
 * Represents a Unicode symbol from text.
 */
public class CharacterSymbol extends TerminalComponent {
    /**
     * The unicode symbol.
     */
    private Character symbol;

    /**
     * Constructs the object from given character.
     *
     * @param character the character to set
     */
    public CharacterSymbol(final Character character) {
        super(SYMBOL);
        symbol = character;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getText() {
        return symbol.toString();
    }

    /**
     * @return the symbol
     */
    public Character getSymbol() {
        return symbol;
    }

    /**
     * @param newSymbol the symbol to set
     */
    public void setSymbol(final Character newSymbol) {
        symbol = newSymbol;
    }

}
