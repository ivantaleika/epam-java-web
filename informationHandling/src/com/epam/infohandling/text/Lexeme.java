package com.epam.infohandling.text;

import java.util.List;

/**
 * Represents a Lexeme object - part of sentence bordered with 2 spaces.
 */
public class Lexeme extends CompositeComponent {
    /**
     * The separator between sentences.
     */
    public static final String SENTENCE_SEPARATOR = " ";

    /**
     * Constructs component from components from witch it consist of.
     *
     * @param  componentList            components from witch it consist of
     * @throws IllegalArgumentException if components type is incorrect
     */
    public Lexeme(final List<Component> componentList) {
        super(componentList, LEXEME);
    }

    /**
     * {@inheritDoc}
     */
    public String getText() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Component component : getComponents()) {
            stringBuilder.append(component.getText());
        }
        return stringBuilder.toString();
    }

}
