package com.epam.infohandling.text;

import java.util.List;

import com.epam.infohandling.expression.Expression;

/**
 * Represents expression lexeme - numeric expression in the text between 2
 * braces.
 */
public class ExpressionLexeme extends LexemePart {
    /**
     * If value if {@code true} then {@code getText} method will return
     * calculating value of the expression, raw expression otherwise.
     *
     * <p>
     * {@code true} by default.
     */
    private boolean isCalculateValue;

    /**
     * Numeric expression.
     */
    private Expression expression;

    /**
     * Constructs component from components from witch it consist of.
     *
     * @param  componentList            components from witch it consist of
     * @param  lexemeExpression         parsed sting expression
     * @throws IllegalArgumentException if components type is incorrect
     */
    public ExpressionLexeme(final List<Component> componentList,
            final Expression lexemeExpression) {
        super(componentList);
        expression = lexemeExpression;
        isCalculateValue = true;
    }

    /**
     * Calculates the expression.
     *
     * @return expression's value
     */
    public int calculate() {
        return expression.calculate();
    }

    /**
     * Set {@code isCalculateValue} to false to get raw expression
     * representation. {@inheritDoc}
     */
    @Override
    public String getText() {
        if (isCalculateValue) {
            return Integer.toString(calculate());
        } else {
            return getExpressionText();
        }
    }

    /**
     * @return string representation of the expression without calculating the
     *         result.
     */
    public String getExpressionText() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Component component : getComponents()) {
            stringBuilder.append(component.getText());
        }
        return stringBuilder.toString();
    }

    /**
     * @return the isCalculateValue
     */
    public boolean getIsCalculateValue() {
        return isCalculateValue;
    }

    /**
     * @param isCalculate the isCalculateValue to set
     */
    public void setCalculateValue(final boolean isCalculate) {
        isCalculateValue = isCalculate;
    }
}
