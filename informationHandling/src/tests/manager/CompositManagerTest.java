package tests.manager;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.epam.infohandling.exception.IHException;
import com.epam.infohandling.manager.CompositManager;
import com.epam.infohandling.text.CharacterSymbol;
import com.epam.infohandling.text.Component;
import com.epam.infohandling.text.ComponentException;
import com.epam.infohandling.text.Word;

import tests.parser.TextDocumentParserTest;

@Test(groups = "managerTestGroup", dependsOnGroups = "textTestGroup")
public class CompositManagerTest {
    private static final String PATH = "./testData/data.txt";
    public CompositManager manager;

    @BeforeClass
    public void init() throws IOException {
        manager = new CompositManager();
    }

    @Test
    public void parseDataTest() {
        try {
            String testData = new String(Files.readAllBytes(Paths.get(PATH)));
            Assert.assertEquals(manager.parseData(testData),
                    TextDocumentParserTest.parser.parse(testData));
        } catch (IHException | IOException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test(expectedExceptions = IHException.class)
    public void parseInvalidDataTest() throws IHException {
        manager.parseData("1-2+3");
    }

    @Test(expectedExceptions = ComponentException.class)
    public void sortSubcomponentsInvalidTypeTest() throws ComponentException {
        manager.sortSubcomponents(new CharacterSymbol('a'), Component.PARAGRAPH,
                CompositManager.compareParagraphBySentenceAmount());
    }

    @Test
    public void compareParagraphBySentenceNumberTest() {
        try {
            Component component = manager.parseData("    Two... Sentences!\r\n"
                    + "    Three. Sentences. In the Paragraph.\r\n"
                    + "    One Sentence.");
            manager.sortSubcomponents(component, Component.PARAGRAPH,
                    CompositManager.compareParagraphBySentenceAmount());
            Assert.assertEquals(component.getText(),
                    "    One Sentence.\r\n    Two... Sentences!\r\n"
                            + "    Three. Sentences. In the Paragraph.");
        } catch (IHException | ComponentException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void compareSentenceByWordNumberTest() {
        try {
            Component component = manager
                    .parseData("    Very very long sentence. Short sentence."
                            + " Medium length sentence.");
            manager.sortSubcomponents(component, Component.SENTENCE,
                    CompositManager.compareSentenceByWordAmount());
            Assert.assertEquals(component.getText(),
                    "    Short sentence. Medium length sentence."
                            + " Very very long sentence.");
        } catch (IHException | ComponentException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void compareSentenceByWordNumberMultipleParagraphsTest() {
        try {
            Component component = manager
                    .parseData("    Very very long sentence. Short sentence."
                            + " Medium length sentence.\r\n"
                            + "    Medium length sentence. "
                            + "Very very long sentence. Short sentence.");
            manager.sortSubcomponents(component, Component.SENTENCE,
                    CompositManager.compareSentenceByWordAmount());
            Assert.assertEquals(component.getText(),
                    "    Short sentence. Medium length sentence."
                            + " Very very long sentence.\r\n"
                            + "    Short sentence. Medium length sentence."
                            + " Very very long sentence.");
        } catch (IHException | ComponentException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void compareBySymbolAmountDesThenByAlphabetOneSenteceTest() {
        try {
            Component component = manager.parseData("abc bbc cbc ebc bce eec.");
            manager.sortSubcomponents(component, Component.LEXEME,
                    CompositManager
                            .compareLexemeBySymbolAmountDesThenByAlphabet('e'));
            Assert.assertEquals(component.getText(),
                    "eec bce ebc abc bbc cbc.");
        } catch (IHException | ComponentException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void compareBySymbolAmountDesThenByAlphabetTextTest() {
        try {
            Component component = manager
                    .parseData("    abc bbc cbc ebc bce eec.\r\n"
                            + "    abc bbc cbc ebc bce eec.");
            manager.sortSubcomponents(component, Component.LEXEME,
                    CompositManager
                            .compareLexemeBySymbolAmountDesThenByAlphabet('e'));
            Assert.assertEquals(component.getText(),
                    "    eec bce ebc abc bbc cbc.\r\n"
                            + "    eec bce ebc abc bbc cbc.");
        } catch (IHException | ComponentException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void getSubcomponentsTest() {
        try {
            Component component = manager
                    .parseData("    Sentence 1.\r\n" + "    Sentence 2.");
            Assert.assertEquals(
                    manager.getSubcomponents(component, Component.SENTENCE),
                    List.of(manager.parseData("Sentence 1."),
                            manager.parseData("Sentence 2.")));
        } catch (IHException | ComponentException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test(expectedExceptions = ComponentException.class)
    public void getSubcomponentsInvalidTypeTest() throws ComponentException {
        Component component = null;
        try {
            component = manager
                    .parseData("    Sentence 1.\r\n" + "    Sentence 2.");
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
        manager.getSubcomponents(component, Component.TEXT);
    }

    @Test
    public void setExpressionIsCalculateValueTest() {
        try {
            String testData = new String(Files.readAllBytes(Paths.get(PATH)));
            Component component = manager.parseData(testData);
            Assert.assertNotEquals(component.getText(), testData);
            manager.setExpressionIsCalculateValue(component, false);
            Assert.assertEquals(component.getText(), testData);
            manager.setExpressionIsCalculateValue(component, true);
            Assert.assertNotEquals(component.getText(), testData);
        } catch (IHException | IOException | ComponentException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test(expectedExceptions = ComponentException.class)
    public void setExpressionIsCalculateValueThrowTest()
            throws ComponentException {
        manager.setExpressionIsCalculateValue(
                new Word(List.of(new CharacterSymbol('a'))), false);
    }
}
