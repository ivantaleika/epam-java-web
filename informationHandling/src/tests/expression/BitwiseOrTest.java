package tests.expression;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.epam.infohandling.expression.BitwiseOr;
import com.epam.infohandling.expression.Constant;

public class BitwiseOrTest {
    @Test(dataProvider = "twoRandomIntProvider",
            dataProviderClass = BinaryOperatorTest.class, invocationCount = 5)
    public void calculateTest(int number1, int number2) {
        Assert.assertEquals(
                new BitwiseOr(new Constant(number1), new Constant(number2))
                        .calculate(),
                number1 | number2);
    }
}
