package tests.expression;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.epam.infohandling.expression.BitwiseNot;
import com.epam.infohandling.expression.Constant;

public class BitwiseNotTest {
    @Test(dataProvider = "randomIntProvider",
            dataProviderClass = UnaryOperatorTest.class, invocationCount = 5)
    public void calculateTest(int number) {
        Assert.assertEquals(new BitwiseNot(new Constant(number)).calculate(),
                ~number);
    }
}
