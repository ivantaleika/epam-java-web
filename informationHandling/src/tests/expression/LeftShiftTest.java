package tests.expression;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.epam.infohandling.expression.Constant;
import com.epam.infohandling.expression.LeftShift;

public class LeftShiftTest {
    @Test(dataProvider = "twoRandomIntProvider",
            dataProviderClass = BinaryOperatorTest.class, invocationCount = 5)
    public void calculateTest(int number1, int number2) {
        Assert.assertEquals(
                new LeftShift(new Constant(number1), new Constant(number2))
                        .calculate(),
                number1 << number2);
    }
}
