package tests.expression;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.epam.infohandling.expression.Constant;

public class ConstantTest {
    @Test(dataProvider = "randomIntProvider",
            dataProviderClass = UnaryOperatorTest.class, invocationCount = 5)
    public void calculateTest(int number) {
        Assert.assertEquals(new Constant(number).calculate(), number);
    }
}
