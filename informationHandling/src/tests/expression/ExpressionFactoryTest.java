package tests.expression;

import org.testng.annotations.Test;

import com.epam.infohandling.exception.IHException;
import com.epam.infohandling.expression.Constant;
import com.epam.infohandling.expression.ExpressionFactory;
import com.epam.infohandling.expression.SupportedOperator;

public final class ExpressionFactoryTest {
    @Test(expectedExceptions = IHException.class)
    public void createBinaryOperatorThrowWhenUnaryTest() throws IHException {
        ExpressionFactory.createBinaryOperator(SupportedOperator.BITWISE_NOT,
                new Constant(5), new Constant(5));
    }

    @Test(expectedExceptions = IHException.class)
    public void createUnryOperatorThrowWhenBinaryTest() throws IHException {
        ExpressionFactory.createUnaryOperator(SupportedOperator.BITWISE_AND,
                new Constant(5));
    }

}
