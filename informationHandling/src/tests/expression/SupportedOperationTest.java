package tests.expression;

import org.testng.annotations.Test;

import com.epam.infohandling.exception.IHException;
import com.epam.infohandling.expression.SupportedOperator;

public final class SupportedOperationTest {
    @Test(expectedExceptions = IHException.class)
    public void getEnumValueThrowOnUnknownRepresantationTest()
            throws IHException {
        SupportedOperator.getEnumValue("5");
    }
}
