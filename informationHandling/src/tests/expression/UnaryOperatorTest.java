package tests.expression;

import java.util.Random;

import org.testng.annotations.DataProvider;

public class UnaryOperatorTest {
    private static final Random RANDOM = new Random();

    @DataProvider
    public Object[][] randomIntProvider() {
        return new Object[][] {{RANDOM.nextInt()}};
    }
}
