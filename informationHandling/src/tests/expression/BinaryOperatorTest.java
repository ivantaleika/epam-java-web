package tests.expression;

import java.util.Random;

import org.testng.annotations.DataProvider;

public class BinaryOperatorTest {
    private static final Random RANDOM = new Random();

    @DataProvider
    public Object[][] twoRandomIntProvider() {
        return new Object[][] {{RANDOM.nextInt(), RANDOM.nextInt()}};
    }
}
