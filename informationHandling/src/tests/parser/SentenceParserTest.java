package tests.parser;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

import com.epam.infohandling.exception.IHException;
import com.epam.infohandling.parser.SentenceParser;
import com.epam.infohandling.text.Component;
import com.epam.infohandling.text.Punctuation;
import com.epam.infohandling.text.Sentence;

@Test(groups = "sentenceTestGroup", dependsOnGroups = "lexemeTestGroup")
public class SentenceParserTest {
    public static SentenceParser parser;

    @BeforeGroups(groups = "sentenceTestGroup")
    public static void init() {
        parser = new SentenceParser();
        parser.linkParser(LexemeParserTest.parser);
    }

    @Test
    public void parseIsValidTypeTest() {
        try {
            Component sentence = parser.parse("It's a sentence!");
            Assert.assertTrue(sentence instanceof Sentence);
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void parseOneWordSentenceTest() {
        String data = "Sentence.";
        try {
            List<Component> lexemes = List
                    .of(LexemeParserTest.parser.parse("Sentence"));
            Assert.assertEquals(parser.parse(data),
                    new Sentence(lexemes,
                            (Punctuation) LexemeParserTest.punctuationParser
                                    .parse(".")));
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void parseMultipleWordSentenceTest() {
        String data = "It's - my 2 sentence!";
        try {
            List<Component> lexemes = List.of(
                    LexemeParserTest.parser.parse("It's"),
                    LexemeParserTest.parser.parse("-"),
                    LexemeParserTest.parser.parse("my"),
                    LexemeParserTest.parser.parse("2"),
                    LexemeParserTest.parser.parse("sentence"));
            Assert.assertEquals(parser.parse(data),
                    new Sentence(lexemes,
                            (Punctuation) LexemeParserTest.punctuationParser
                                    .parse("!")));
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }
}
