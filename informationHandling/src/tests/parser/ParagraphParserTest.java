package tests.parser;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

import com.epam.infohandling.exception.IHException;
import com.epam.infohandling.parser.ParagraphParser;
import com.epam.infohandling.text.Component;
import com.epam.infohandling.text.Paragraph;

@Test(groups = "paragraphTestGroup", dependsOnGroups = "sentenceTestGroup")
public class ParagraphParserTest {
    public static ParagraphParser parser;

    @BeforeGroups(groups = "paragraphTestGroup")
    public static void init() {
        parser = new ParagraphParser();
        parser.linkParser(SentenceParserTest.parser);
    }

    @Test
    public void parseIsValidTypeTest() {
        try {
            Component paragraph = parser.parse("    It's a paragraph!");
            Assert.assertTrue(paragraph instanceof Paragraph);
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void parseOneSentenceParagraphTest() {
        String data = "    Paragraph's sentence.";
        try {
            List<Component> sentences = List.of(
                    SentenceParserTest.parser.parse("Paragraph's sentence."));
            Assert.assertEquals(parser.parse(data), new Paragraph(sentences));
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void parseMultipleSentenceParagraphTest() {
        String data = "    Paragraph's sentence 1. 2 - sentence again.";
        try {
            List<Component> sentences = List.of(
                    SentenceParserTest.parser.parse("Paragraph's sentence 1."),
                    SentenceParserTest.parser.parse("2 - sentence again."));
            Assert.assertEquals(parser.parse(data), new Paragraph(sentences));
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }
}
