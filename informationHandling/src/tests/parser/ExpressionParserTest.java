package tests.parser;

import java.util.LinkedList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

import com.epam.infohandling.exception.IHException;
import com.epam.infohandling.parser.ExpressionParser;
import com.epam.infohandling.text.CharacterSymbol;
import com.epam.infohandling.text.Component;

@Test(groups = "expressionParserGroup")
public class ExpressionParserTest {
    public static ExpressionParser parser;

    @BeforeGroups(groups = "expressionParserGroup")
    public static void init() {
        parser = new ExpressionParser();
    }

    @Test
    public void parseTest() {
        String data = "13<<2";
        try {
            List<Component> symbols = new LinkedList<>();
            for (Character symbol : data.toCharArray()) {
                symbols.add(new CharacterSymbol(symbol));
            }
            Assert.assertEquals(parser.parse(data).calculate(), 52);
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void parseComplexExpressionTest() {
        String data = "(1^5|1&2<<(2|5>>2&71))|1200";
        try {
            List<Component> symbols = new LinkedList<>();
            for (Character symbol : data.toCharArray()) {
                symbols.add(new CharacterSymbol(symbol));
            }
            Assert.assertEquals(parser.parse(data).calculate(), 1204);
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void parseUnnecessaryParenthesesTest() {
        String data = "(((~71&(2&3|(3|(2&1>>2|2)&2)|10&2))|78))";
        try {
            List<Component> symbols = new LinkedList<>();
            for (Character symbol : data.toCharArray()) {
                symbols.add(new CharacterSymbol(symbol));
            }
            Assert.assertEquals(parser.parse(data).calculate(), 78);
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void parseEqualPriorityOperatorsTest() {
        String data = "50>>2<<3";
        try {
            List<Component> symbols = new LinkedList<>();
            for (Character symbol : data.toCharArray()) {
                symbols.add(new CharacterSymbol(symbol));
            }
            Assert.assertEquals(parser.parse(data).calculate(), 96);
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test(expectedExceptions = IHException.class)
    public void parseIvalidParenthesesTest() throws IHException {
        String data = "(5|4&(~3)";
        parser.parse(data);
    }

    @Test(expectedExceptions = IHException.class)
    public void parseIvalidOperandNumberTest() throws IHException {
        String data = "^4";
        parser.parse(data);
    }

}
