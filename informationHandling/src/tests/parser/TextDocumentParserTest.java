package tests.parser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import com.epam.infohandling.exception.IHException;
import com.epam.infohandling.parser.TextDocumentParser;
import com.epam.infohandling.text.Component;
import com.epam.infohandling.text.TextDocument;

@Test(groups = "textTestGroup", dependsOnGroups = "paragraphTestGroup")
public class TextDocumentParserTest {
    private static final String PATH = "./testData/data.txt";
    public static TextDocumentParser parser;

    @BeforeGroups(groups = "textTestGroup")
    public static void init() {
        parser = new TextDocumentParser();
        parser.linkParser(ParagraphParserTest.parser);
    }

    @Test
    public void parseIsValidTypeTest() {
        try {
            Component text = parser.parse("    It's a text!");
            Assert.assertTrue(text instanceof TextDocument);
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void parseOneParagraphTextTest() {
        String data = "    TextDocument's paragraph.";
        try {
            List<Component> paragraphs = List.of(
                    ParagraphParserTest.parser.parse("    TextDocument's paragraph."));
            Assert.assertEquals(parser.parse(data), new TextDocument(paragraphs));
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void parseWindowsLineBreakTest() {
        String data = "    TextDocument's paragraph 1.\r\n    2 - paragraph again.";
        try {
            List<Component> paragraphs = List.of(
                    ParagraphParserTest.parser.parse("    TextDocument's paragraph 1."),
                    ParagraphParserTest.parser
                            .parse("    2 - paragraph again."));
            Assert.assertEquals(parser.parse(data), new TextDocument(paragraphs));
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Ignore //only for Unix systems
    @Test
    public void parseUnixLineBreakTest() {
        String data = "    TextDocument's paragraph 1.\n    2 - paragraph again.";
        try {
            List<Component> paragraphs = List.of(
                    ParagraphParserTest.parser.parse("    TextDocument's paragraph 1."),
                    ParagraphParserTest.parser
                            .parse("    2 - paragraph again."));
            Assert.assertEquals(parser.parse(data), new TextDocument(paragraphs));
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void parseFileDataTest() {
        try {
            String data = new String(Files.readAllBytes(Paths.get(PATH)));
            List<Component> paragraphs = List.of(ParagraphParserTest.parser
                    .parse("    It has survived - not only (five) centuries, "
                            + "but also the leap into 13<<2 electronic "
                            + "typesetting, remaining 3>>5 essentially "
                            + "~6&9|(3&4) unchanged. It was popularised in the "
                            + "5|(1&2&(3|(4&(0^5|6&47)|3)|2)|1) with the release"
                            + " of Letraset sheets containing Lorem Ipsum"
                            + " passages, and more recently with desktop"
                            + " publishing software like Aldus PageMaker"
                            + " including versions of Lorem Ipsum."),
                    ParagraphParserTest.parser.parse(
                            "    It is a long established fact that a reader "
                                    + "will be distracted by the readable content of a "
                                    + "page when looking at its layout. The point of "
                                    + "using (~71&(2&3|(3|(2&1>>2|2)&2)|10&2))|78 Ipsum"
                                    + " is that it has a more-or-less normal "
                                    + "distribution of letters, as opposed to using "
                                    + "(Content here), content here', making it look "
                                    + "like readable English."),
                    ParagraphParserTest.parser.parse(
                            "    It is a (0^5|1&2<<(2|5>>2&71))|1200 established"
                                    + " fact that a reader will be of a page when "
                                    + "looking at its layout."),
                    ParagraphParserTest.parser.parse("    Bye."));
            Assert.assertEquals(parser.parse(data), new TextDocument(paragraphs));
        } catch (IHException | IOException e) {
            Assert.fail(e.getMessage());
        }
    }
}
