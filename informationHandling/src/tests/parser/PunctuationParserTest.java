package tests.parser;

import java.util.LinkedList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

import com.epam.infohandling.exception.IHException;
import com.epam.infohandling.parser.PunctuationParser;
import com.epam.infohandling.text.CharacterSymbol;
import com.epam.infohandling.text.Component;
import com.epam.infohandling.text.Punctuation;

import tests.text.PunctuationTest;

@Test(groups = "punctuationTestGroup", dependsOnGroups = "characterTestGroup")
public class PunctuationParserTest {
    public static PunctuationParser parser;

    @BeforeGroups(groups = "punctuationTestGroup")
    public static void init() {
        parser = new PunctuationParser();
    }

    @Test(dataProvider = "randomPunctuationProvider",
            dataProviderClass = PunctuationTest.class)
    public void parseIsValidTypeTest(String data) {
        try {
            Component punctuation = parser.parse(data);
            Assert.assertTrue(punctuation instanceof Punctuation);
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }

    }

    @Test(dataProvider = "randomPunctuationProvider",
            dataProviderClass = PunctuationTest.class, invocationCount = 5)
    public void parseValidDataTest(String data) {
        List<Component> symbols = new LinkedList<>();
        for (Character characterSymbol : data.toCharArray()) {
            symbols.add(new CharacterSymbol(characterSymbol));
        }
        try {
            Assert.assertEquals(parser.parse(data),
                    new Punctuation(symbols));
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }

    }
}
