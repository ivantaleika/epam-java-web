package tests.parser;

import java.util.LinkedList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

import com.epam.infohandling.exception.IHException;
import com.epam.infohandling.parser.WordParser;
import com.epam.infohandling.text.CharacterSymbol;
import com.epam.infohandling.text.Component;
import com.epam.infohandling.text.Word;

import tests.text.WordTest;

@Test(groups = "wordTestGroup", dependsOnGroups = "characterTestGroup")
public class WordParserTest {
    public static WordParser parser;

    @BeforeGroups(groups = "wordTestGroup")
    public static void init() {
        parser = new WordParser();
    }

    @Test(dataProvider = "randomEnglishWordProvider",
            dataProviderClass = WordTest.class)
    public void parseIsValidTypeTest(String data) {
        try {
            Component word = parser.parse(data);
            Assert.assertTrue(word instanceof Word);
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test(dataProvider = "randomEnglishWordProvider",
            dataProviderClass = WordTest.class, invocationCount = 5)
    public void parseValidDataTest(String data) {
        List<Component> symbols = new LinkedList<>();
        for (Character characterSymbol : data.toCharArray()) {
            symbols.add(new CharacterSymbol(characterSymbol));
        }
        try {
            Assert.assertEquals(parser.parse(data), new Word(symbols));
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void parseWithApostropheTest() {
        String data = "It's";
        List<Component> symbols = new LinkedList<>();
        for (Character characterSymbol : data.toCharArray()) {
            symbols.add(new CharacterSymbol(characterSymbol));
        }
        try {
            Assert.assertEquals(parser.parse(data), new Word(symbols));
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test(expectedExceptions = IHException.class)
    public void parseInvalidDataTest() throws IHException {
        parser.parse(")");

    }

    @Test(expectedExceptions = IHException.class)
    public void parseWhitespeceTest() throws IHException {
        parser.parse(" word ");
    }

    @Test(expectedExceptions = IHException.class)
    public void parseNumberTest() throws IHException {
        parser.parse("14");
    }
    @Test(expectedExceptions = IllegalArgumentException.class)
    public void parseEmptyStringTest() throws IHException {
        parser.parse("");
    }

}
