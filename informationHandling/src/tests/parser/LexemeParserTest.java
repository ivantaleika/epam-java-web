package tests.parser;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;

import com.epam.infohandling.exception.IHException;
import com.epam.infohandling.parser.ExpressionLexemeParser;
import com.epam.infohandling.parser.LexemeParser;
import com.epam.infohandling.parser.PunctuationParser;
import com.epam.infohandling.parser.WordParser;
import com.epam.infohandling.text.Component;
import com.epam.infohandling.text.Lexeme;

@Test(groups = "lexemeTestGroup", dependsOnGroups = {"wordTestGroup",
        "punctuationTestGroup", "expressionTestGroup"})
public class LexemeParserTest {
    public static LexemeParser parser;
    //Unlinked parsers
    public static WordParser wordParser;
    public static PunctuationParser punctuationParser;
    public static ExpressionLexemeParser expressionLexemeParser;

    @BeforeGroups(groups = "lexemeTestGroup")
    public static void init() {
        parser = new LexemeParser();
        parser.linkParser(ExpressionLexemeParserTest.parser)
                .linkParser(WordParserTest.parser)
                .linkParser(PunctuationParserTest.parser);
        wordParser = new WordParser();
        punctuationParser = new PunctuationParser();
        expressionLexemeParser = new ExpressionLexemeParser();

    }

    public void parseIsValidTypeTest() {
        try {
            Component word = parser.parse("lexeme,");
            Assert.assertTrue(word instanceof Lexeme);
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }

    }

    @Test
    public void parseWordTest() {
        String lexeme = "testLexeme";
        try {
            Assert.assertEquals(parser.parse(lexeme),
                    new Lexeme(List.of(wordParser.parse(lexeme))));
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void parseWordWithPunctuationTest() {
        String lexeme = "(testWord),";
        try {
            Assert.assertEquals(parser.parse(lexeme),
                    new Lexeme(List.of(punctuationParser.parse("("),
                            wordParser.parse("testWord"),
                            punctuationParser.parse(")"),
                            punctuationParser.parse(","))));
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void parseMixedWithApostropheTest() {
        String lexeme = "It's!";
        try {
            Assert.assertEquals(parser.parse(lexeme),
                    new Lexeme(List.of(wordParser.parse("It's"),
                            punctuationParser.parse("!"))));
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void parseExpressionTest() {
        String lexeme = "(8)";
        try {
            Assert.assertEquals(parser.parse(lexeme),
                    new Lexeme(List.of(expressionLexemeParser.parse("(8)"))));
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }
}
