package tests.text;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.epam.infohandling.exception.IHException;
import com.epam.infohandling.text.ExpressionLexeme;

import tests.parser.ExpressionLexemeParserTest;

@Test(groups = "expressionTestGroup",
        dependsOnGroups = {"characterTestGroup", "expressionParserGroup"})
public class ExpressionLexemeTest {

    @Test
    public void getTextTest() {
        String data = "13<<2";
        try {
            Assert.assertEquals(
                    ExpressionLexemeParserTest.parser.parse(data).getText(),
                    "52");
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void setCalculateValueTest() {
        String data = "(1^5|1&2<<(2|5>>2&71))|1200";
        try {
            ExpressionLexeme expressionLexeme = (ExpressionLexeme) ExpressionLexemeParserTest.parser
                    .parse(data);
            Assert.assertEquals(expressionLexeme.getText(), "1204");
            expressionLexeme.setCalculateValue(false);
            Assert.assertEquals(expressionLexeme.getText(), data);
            expressionLexeme.setCalculateValue(true);
            Assert.assertEquals(expressionLexeme.getText(), "1204");
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void getExpressionTextTest() {
        String data = "(((~71&(2&3|(3|(2&1>>2|2)&2)|10&2))|78))";
        try {
            Assert.assertEquals(
                    ((ExpressionLexeme) ExpressionLexemeParserTest.parser
                            .parse(data)).getExpressionText(),
                    data);
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }
}
