package tests.text;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.epam.infohandling.exception.IHException;

import tests.parser.ParagraphParserTest;

@Test(groups = "paragraphTestGroup", dependsOnGroups = "sentenceTestGroup")
public class ParagraphTest {

    @Test
    public void getTextOneSentenceParagraphTest() {
        String paragraph = "    Paragraph.";
        try {
            Assert.assertEquals(
                    ParagraphParserTest.parser.parse(paragraph).getText(),
                    paragraph);
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void getTextComplexParagraphTest() {
        String paragraph = "    (An expressions) - 3. Sentence: Words:"
                + " word; word; word... Mixed: 120; word.";
        try {
            Assert.assertEquals(
                    ParagraphParserTest.parser.parse(paragraph).getText(),
                    paragraph);
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }
}
