package tests.text;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.epam.infohandling.text.CharacterSymbol;
import com.epam.infohandling.text.Component;
import com.epam.infohandling.text.Word;

@Test(groups = "wordTestGroup", dependsOnGroups = "characterTestGroup")
public class WordTest {
    private static final Random RANDOM = new Random();
    private static final int N_SYMBOLS = 26;
    private static final int MIN_INDEX = (int) 'a';
    private static final int MAX_WORD_LENGTH = 10;

    @DataProvider
    public Object[][] randomEnglishWordProvider() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < RANDOM.nextInt(MAX_WORD_LENGTH) + 1; i++) {
            builder.append((char) (RANDOM.nextInt(N_SYMBOLS) + MIN_INDEX));
        }

        return new Object[][] {{builder.toString()}};

    }

    @Test(dataProvider = "randomEnglishWordProvider", invocationCount = 5)
    public void getTextTest(String word) {
        List<Component> symbols = new LinkedList<>();
        for (Character symbol : word.toCharArray()) {
            symbols.add(new CharacterSymbol(symbol));
        }
        Word wordComponent = new Word(symbols);
        Assert.assertEquals(wordComponent.getText(), word);
    }
}
