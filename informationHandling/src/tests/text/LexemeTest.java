package tests.text;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.epam.infohandling.exception.IHException;

import tests.parser.LexemeParserTest;

@Test(groups = "lexemeTestGroup", dependsOnGroups = {"wordTestGroup",
        "punctuationTestGroup", "expressionTestGroup"})
public class LexemeTest {

    @Test
    public void getTextWordTest() {
        String lexeme = "testLexeme";
        try {
            Assert.assertEquals(LexemeParserTest.parser.parse(lexeme).getText(),
                    lexeme);
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void getTextWordWithPunctuationTest() {
        String lexeme = "(testWord),";
        try {
            Assert.assertEquals(LexemeParserTest.parser.parse(lexeme).getText(),
                    lexeme);
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void getTextExpressionTest() {
        String lexeme = "(8)";
        try {
            Assert.assertEquals(LexemeParserTest.parser.parse(lexeme).getText(),
                    "8");
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }
}
