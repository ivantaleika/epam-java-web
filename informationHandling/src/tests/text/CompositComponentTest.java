package tests.text;

import java.util.LinkedList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.epam.infohandling.text.CharacterSymbol;
import com.epam.infohandling.text.Component;
import com.epam.infohandling.text.CompositeComponent;
import com.epam.infohandling.text.Word;

public class CompositComponentTest {
    private class TestComponent extends CompositeComponent {
        public TestComponent(List<Component> componentList, int type) {
            super(componentList, type);
        }

        @Override
        public String getText() {
            return null;
        }
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void emptyComponentListConstructorTest() {
        new TestComponent(new LinkedList<>(), Component.LEXEME);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void ivalidComponentTypeConstructorTest() {
        new TestComponent(List.of(new CharacterSymbol('a')), Component.TEXT);
    }

    @Test
    public void removeComponentByIndexTest() {
        List<Component> characterSymbols = new LinkedList<>();
        characterSymbols.add(new CharacterSymbol('a'));
        TestComponent textComponent = new TestComponent(characterSymbols,
                Component.LEXEME_PART);
        textComponent.removeComponent(0);
        Assert.assertEquals(textComponent.getComponents().size(), 0);
    }

    @Test
    public void removeComponentByValueTest() {
        List<Component> characterSymbols = new LinkedList<>();
        CharacterSymbol symbol = new CharacterSymbol('a');
        characterSymbols.add(symbol);
        TestComponent textComponent = new TestComponent(characterSymbols,
                Component.LEXEME_PART);
        textComponent.removeComponent(symbol);
        Assert.assertEquals(textComponent.getComponents().size(), 0);
    }

    @Test
    public void insertComponentValidComponentTest() {
        List<Component> characterSymbols = new LinkedList<>();
        CharacterSymbol symbol = new CharacterSymbol('a');
        characterSymbols.add(symbol);
        TestComponent textComponent = new TestComponent(characterSymbols,
                Component.LEXEME_PART);
        textComponent.insertComponent(new CharacterSymbol('b'), 0);
        Assert.assertEquals(textComponent.getComponents().size(), 2);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void insertComponentInvalidComponentTest() {
        List<Component> characterSymbols = new LinkedList<>();
        CharacterSymbol symbol = new CharacterSymbol('a');
        characterSymbols.add(symbol);
        TestComponent textComponent = new TestComponent(characterSymbols,
                Component.LEXEME_PART);
        textComponent.insertComponent(
                new Word(List.of(new CharacterSymbol('b'))), 0);
        Assert.assertEquals(textComponent.getComponents().size(), 2);
    }

    @Test
    public void appendComponentValidComponentTest() {
        List<Component> characterSymbols = new LinkedList<>();
        CharacterSymbol symbol = new CharacterSymbol('a');
        characterSymbols.add(symbol);
        TestComponent textComponent = new TestComponent(characterSymbols,
                Component.LEXEME_PART);
        textComponent.appendComponent(new CharacterSymbol('b'));
        Assert.assertEquals(textComponent.getComponents().size(), 2);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void appendComponentInvalidComponentTest() {
        List<Component> characterSymbols = new LinkedList<>();
        CharacterSymbol symbol = new CharacterSymbol('a');
        characterSymbols.add(symbol);
        TestComponent textComponent = new TestComponent(characterSymbols,
                Component.LEXEME_PART);
        textComponent
                .appendComponent(new Word(List.of(new CharacterSymbol('b'))));
        Assert.assertEquals(textComponent.getComponents().size(), 2);
    }
}
