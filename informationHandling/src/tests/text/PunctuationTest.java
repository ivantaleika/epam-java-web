package tests.text;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.epam.infohandling.text.CharacterSymbol;
import com.epam.infohandling.text.Component;
import com.epam.infohandling.text.Punctuation;

@Test(groups = "punctuationTestGroup", dependsOnGroups = "characterTestGroup")
public class PunctuationTest {
    private static final Random RANDOM = new Random();
    private static final String[] SYMBOLS = {".", ",", ":", ";", "-", "(", ")",
            "\"", "...", "!", "?"};

    @DataProvider
    public Object[][] randomPunctuationProvider() {
        return new Object[][] {{SYMBOLS[RANDOM.nextInt(SYMBOLS.length)]}};
    }

    @Test(dataProvider = "randomPunctuationProvider", invocationCount = 5)
    public void getTextTest(String punctuation) {
        List<Component> symbols = new LinkedList<>();
        for (Character symbol : punctuation.toCharArray()) {
            symbols.add(new CharacterSymbol(symbol));
        }
        Punctuation wordComponent = new Punctuation(symbols);
        Assert.assertEquals(wordComponent.getText(), punctuation);
    }
}
