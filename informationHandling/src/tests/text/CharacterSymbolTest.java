package tests.text;

import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.epam.infohandling.text.CharacterSymbol;

@Test(groups = "characterTestGroup")
public class CharacterSymbolTest {
    private static final Random RANDOM = new Random();
    private static final int N_SYMBOLS = 92;
    private static final int MIN_INDEX = 33;

    @DataProvider
    public Object[][] randomStandartSymbolProvider() {
        return new Object[][] {{Character
                .valueOf((char) (RANDOM.nextInt(N_SYMBOLS) + MIN_INDEX))}};

    }

    @Test(dataProvider = "randomStandartSymbolProvider", invocationCount = 5)
    public void getTextTest(Character symbol) {
        CharacterSymbol symbol2 = new CharacterSymbol(symbol);
        Assert.assertEquals(symbol2.getText(), symbol.toString());
    }
}
