package tests.text;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.epam.infohandling.exception.IHException;

import tests.parser.SentenceParserTest;

@Test(groups = "sentenceTestGroup", dependsOnGroups = "lexemeTestGroup")
public class SentenceTest {
    @Test
    public void getTextSimpleSentenceTest() {
        String sentence = "Sentence for testing...";
        try {
            Assert.assertEquals(
                    SentenceParserTest.parser.parse(sentence).getText(),
                    sentence);
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void getTextOneLexemeSentenceTest() {
        String sentence = "Sentence.";
        try {
            Assert.assertEquals(
                    SentenceParserTest.parser.parse(sentence).getText(),
                    sentence);
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void getTextExpressionSentenceTest() {
        String sentence = "(5<<3) - 4&1?";
        try {
            Assert.assertEquals(
                    SentenceParserTest.parser.parse(sentence).getText(),
                    "40 - 0?");
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void getTextComplexSentenceTest() {
        String sentence = "(An expressions) - (5<<3) and 4&1 - mixed: word 1;"
                + " word 2.";
        try {
            Assert.assertEquals(
                    SentenceParserTest.parser.parse(sentence).getText(),
                    "(An expressions) - 40 and 0 - mixed: word 1; word 2.");
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }
}
