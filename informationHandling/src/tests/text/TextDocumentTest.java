package tests.text;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import com.epam.infohandling.exception.IHException;

import tests.parser.TextDocumentParserTest;

@Test(groups = "textTestGroup", dependsOnGroups = "paragraphTestGroup")
public class TextDocumentTest {

    @Test
    public void getTextOneParagraphTextTest() {
        String text = "    TextDocument.";
        try {
            Assert.assertEquals(TextDocumentParserTest.parser.parse(text).getText(),
                    text);
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void parseWindowsLineBreakTest() {
        if (!System.getProperty("os.name").contains("Windows")) {
            throw new SkipException("Only for Windows system");
        }
        String data = "    Test.\r\n    For windows platform.";
        try {
            Assert.assertEquals(TextDocumentParserTest.parser.parse(data).getText(),
                    data);
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Ignore //only for Unix systems
    @Test
    public void parseLinuxLineBreakTest() {
        String data = "    Test.\n    For unix platform.";
        try {
            Assert.assertEquals(TextDocumentParserTest.parser.parse(data).getText(),
                    data);
        } catch (IHException e) {
            Assert.fail(e.getMessage());
        }
        System.getProperty("os.name");
    }
}
