package com.epam.multithreading.idprovider;

/**
 * Provides a simple unique id as a {@code long} variable.
 */
public final class SerialIdProvider {
    /**
     * Next providing id.
     */
    private static long id = 0;

    /**
     * Prevents from creating an utility class.
     */
    private SerialIdProvider() {
    }

    /**
     * Returns and unique id number.
     *
     * @return unique id number
     */
    public static long getId() {
        return id++;
    }

}
