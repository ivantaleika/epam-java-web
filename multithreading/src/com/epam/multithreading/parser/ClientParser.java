package com.epam.multithreading.parser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.multithreading.client.ClientInfo;
import com.epam.multithreading.exception.MultithreadingException;
import com.epam.multithreading.navigation.GeoPoint;

/**
 * Parse source data for {@code Client} class.
 */
public final class ClientParser {
    /**
     * Regex group for {@code GEO_POINT_REGEX} to get latitude.
     */
    private static final int LATITUDE_GROUP = 1;
    /**
     * Regex group for {@code GEO_POINT_REGEX} to get longitude.
     */
    private static final int LONGITUDE_GROUP = 3;
    /**
     * Parser's logger.
     */
    private static final Logger LOGGER;
    /**
     * Regex for {@code GeoPoint} data.
     */
    private static final String GEO_POINT_REGEX;
    /**
     * Regex for {@code GeoPoint} data.
     */
    private static final String WITESPACES_REGEX;
    /**
     * Regex for {@code Client}'s name data.
     */
    private static final String NAME_REGEX;
    /**
     * Regex for {@code Client}'s departure location data.
     */
    private static final String DEPARTURE_REGEX;
    /**
     * Regex for {@code Client}'s destination location data.
     */
    private static final String DESTINATION_REGEX;
    /**
     * Regex for {@code Client}'s departure time data.
     */
    private static final String TIME_REGEX;
    /**
     * Regex for {@code Client}'s data.
     */
    private static final String CLIENT_REGEX;
    /**
     * A regex {@code Pattern}.
     */
    private static Pattern pattern;
    /**
     * A regex {@code Matcher}.
     */
    private static Matcher matcher;

    static {
        LOGGER = LogManager.getLogger();
        GEO_POINT_REGEX = "([+-]?\\d+\\.?(\\d+)?);([+-]?\\d+\\.?(\\d+)?)";
        WITESPACES_REGEX = "[ \\t]+";
        NAME_REGEX = "name=\"(.+)\"" + WITESPACES_REGEX;
        DEPARTURE_REGEX = "departure=" + GEO_POINT_REGEX + WITESPACES_REGEX;
        DESTINATION_REGEX = "destination=" + GEO_POINT_REGEX + WITESPACES_REGEX;
        TIME_REGEX = "orderTime=(\\d+)(" + WITESPACES_REGEX + ")?";
        CLIENT_REGEX = NAME_REGEX + DEPARTURE_REGEX + DESTINATION_REGEX
                + TIME_REGEX;
    }

    /**
     * Prevents from utility object creation.
     */
    private ClientParser() {
    }

    /**
     * Parse source {@code Stream<String>} data accordingly to client's regex.
     *
     * @param  source String stream
     * @return        {@code Stream<String>} with client's data.
     */
    public static Stream<String> findInfoStrings(final Stream<String> source) {
        return source.filter(str -> {
            if (!str.matches(CLIENT_REGEX)) {
                LOGGER.warn("\"{}\" doesn't match {}", str, CLIENT_REGEX);
                return false;
            }
            return true;
        });
    }

    /**
     * Returns {@link ClientInfo} object constructed from client's info string.
     *
     * @param  infoString              string that contains client's info
     * @return                         client's info
     * @throws MultithreadingException if string does not contain client's info
     */
    public static ClientInfo parseImportInfo(final String infoString)
            throws MultithreadingException {
        try {
            ClientInfo info = new ClientInfo();
            info.setName(parseName(infoString));
            info.setDepartureLocation(parseDeparture(infoString));
            info.setDestinationLocation(parseDestination(infoString));
            info.setTime(parseTime(infoString));
            return info;
        } catch (IllegalStateException e) {
            throw new MultithreadingException(
                    "\"" + infoString + "\" must contain info to match "
                            + pattern.pattern() + " pattern.",
                    e);
        }
    }

    /**
     * Parse string for Client's name.
     *
     * @param  infoString string that contains client's info
     * @return            Client's name
     */
    private static String parseName(final String infoString) {
        pattern = Pattern.compile(NAME_REGEX);
        matcher = pattern.matcher(infoString);
        matcher.find();
        return matcher.group(1);
    }

    /**
     * Parse string for Client's departure.
     *
     * @param  infoString string that contains client's info
     * @return            Client's departure
     */
    private static GeoPoint parseDeparture(final String infoString) {
        pattern = Pattern.compile(DEPARTURE_REGEX);
        matcher = pattern.matcher(infoString);
        matcher.find();
        return new GeoPoint(Double.parseDouble(matcher.group(LATITUDE_GROUP)),
                Double.parseDouble(matcher.group(LONGITUDE_GROUP)));
    }

    /**
     * Parse string for Client's destination.
     *
     * @param  infoString string that contains client's info
     * @return            Client's destination
     */
    private static GeoPoint parseDestination(final String infoString) {
        pattern = Pattern.compile(DESTINATION_REGEX);
        matcher = pattern.matcher(infoString);
        matcher.find();
        return new GeoPoint(Double.parseDouble(matcher.group(LATITUDE_GROUP)),
                Double.parseDouble(matcher.group(LONGITUDE_GROUP)));
    }

    /**
     * Parse string for Client's order time.
     *
     * @param  infoString string that contains client's info
     * @return            Client's order time
     */
    private static Integer parseTime(final String infoString) {
        pattern = Pattern.compile(TIME_REGEX);
        matcher = pattern.matcher(infoString);
        matcher.find();
        return Integer.parseInt(matcher.group(1));
    }

}
