package com.epam.multithreading.parser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.multithreading.exception.MultithreadingException;
import com.epam.multithreading.navigation.GeoPoint;

/**
 * Parse source data for {@code Taxi} class.
 */
public final class TaxiParser {
    /**
     * Regex group for {@code GEO_POINT_REGEX} to get latitude.
     */
    private static final int LATITUDE_GROUP = 1;
    /**
     * Regex group for {@code GEO_POINT_REGEX} to get longitude.
     */
    private static final int LONGITUDE_GROUP = 3;
    /**
     * Parser's logger.
     */
    private static final Logger LOGGER;
    /**
     * Regex for {@code GeoPoint} data.
     */
    private static final String GEO_POINT_REGEX;
    /**
     * Regex for {@code Taxi}'s data.
     */
    private static final String TAXI_REGEX;
    /**
     * A regex {@code Pattern}.
     */
    private static Pattern pattern;
    /**
     * A regex {@code Matcher}.
     */
    private static Matcher matcher;

    static {
        LOGGER = LogManager.getLogger();
        GEO_POINT_REGEX = "([+-]?\\d+\\.?(\\d+)?);([+-]?\\d+\\.?(\\d+)?)";
        TAXI_REGEX = "startLocation=" + GEO_POINT_REGEX + "([ \\t]+)?";
    }

    /**
     * Prevents from utility object creation.
     */
    private TaxiParser() {
    }

    /**
     * Parse source {@code Stream<String>} data accordingly to taxi's regex.
     *
     * @param  source String stream
     * @return        {@code Stream<String>} with taxi's data.
     */
    public static Stream<String> findInfoStrings(final Stream<String> source) {
        return source.filter(str -> {
            if (!str.matches(TAXI_REGEX)) {
                LOGGER.warn("\"{}\" doesn't match {}", str, TAXI_REGEX);
                return false;
            }
            return true;
        });
    }

    /**
     * Returns {@code Taxi}'s start location.
     *
     * @param  infoString              string that contains taxi's info
     * @return                         {@code Taxi}'s start location
     * @throws MultithreadingException if string does not contain taxi's info
     */
    public static GeoPoint findTaxiStartLocation(final String infoString)
            throws MultithreadingException {
        try {
            pattern = Pattern.compile(GEO_POINT_REGEX);
            matcher = pattern.matcher(infoString);
            matcher.find();
            return new GeoPoint(
                    Double.parseDouble(matcher.group(LATITUDE_GROUP)),
                    Double.parseDouble(matcher.group(LONGITUDE_GROUP)));
        } catch (IllegalStateException e) {
            throw new MultithreadingException(
                    "\"" + infoString + "\" must contain info to match "
                            + pattern.pattern() + " pattern.",
                    e);
        }
    }

}
