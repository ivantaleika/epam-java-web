package com.epam.multithreading.navigation;

/**
 * The class that represents a geographic coordinate point.
 */
public class GeoPoint {
    /**
     * point's latitude.
     */
    private double latitude;
    /**
     * point's longitude.
     */
    private double longitude;

    /**
     * Constructs point from it's latitude and longitude coordinates.
     *
     * @param latitudeCoordinate  point's latitude
     * @param longitudeCoordinate point's latitude coordinate
     */
    public GeoPoint(final double latitudeCoordinate,
            final double longitudeCoordinate) {
        latitude = latitudeCoordinate;
        longitude = longitudeCoordinate;
    }

    /**
     * Constructs geo point from another geo point.
     *
     * @param other source point
     */
    public GeoPoint(final GeoPoint other) {
        latitude = other.latitude;
        longitude = other.longitude;
    }

    /**
     * Returns latitude coordinate.
     *
     * @return latitude coordinate
     */
    public double getLattitude() {
        return latitude;
    }

    /**
     * Set latitude coordinate.
     *
     * @param newLattitude new coordinate
     */
    public void setLattitude(final double newLattitude) {
        latitude = newLattitude;
    }

    /**
     * Returns latitude coordinate.
     *
     * @return longitude coordinate
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * Set longitude coordinate.
     *
     * @param newLongitude new coordinate
     */
    public void setLongitude(final double newLongitude) {
        longitude = newLongitude;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        String output = String.format("(latitude: %.2f, longitude: %.2f)",
                latitude, longitude);
        return output;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int magicNumber = 32;
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(latitude);
        result = prime * result + (int) (temp ^ (temp >>> magicNumber));
        temp = Double.doubleToLongBits(longitude);
        result = prime * result + (int) (temp ^ (temp >>> magicNumber));
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        GeoPoint other = (GeoPoint) obj;
        if (Double.doubleToLongBits(latitude) != Double
                .doubleToLongBits(other.latitude)) {
            return false;
        }
        if (Double.doubleToLongBits(longitude) != Double
                .doubleToLongBits(other.longitude)) {
            return false;
        }
        return true;
    }

}
