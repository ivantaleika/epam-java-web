package com.epam.multithreading.navigation;

/**
 * Utility class that to work with geolocation.
 */
public final class Navigator {

    /**
     * Prevents from constructing ulitity class objects.
     */
    private Navigator() {
    }

    /**
     * Calculates distance between two {@link GeoPoint}. It is always
     * non-negative.
     *
     * @param  departure   start point
     * @param  destination end point
     * @return             distance between two {@link GeoPoint}
     */
    public static double getDistance(final GeoPoint departure,
            final GeoPoint destination) {
        return Math.sqrt(Math
                .pow(departure.getLattitude() - destination.getLattitude(), 2)
                + Math.pow(
                        departure.getLongitude() - destination.getLongitude(),
                        2));
    }

    /**
     * Calculates time that needed to transfer the distance between two
     * {@link GeoPoint} with the given speed.
     *
     * @param  departure   start point
     * @param  destination end point
     * @param  speed       movement speed
     * @return             time that needed to reach the destination point from
     *                     given departure point
     */
    public static double getTransferTime(final GeoPoint departure,
            final GeoPoint destination, final double speed) {
        return getDistance(departure, destination) / speed;
    }
}
