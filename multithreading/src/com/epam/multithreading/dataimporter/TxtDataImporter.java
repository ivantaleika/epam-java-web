package com.epam.multithreading.dataimporter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.multithreading.client.Client;
import com.epam.multithreading.client.ClientFactory;
import com.epam.multithreading.client.ClientInfo;
import com.epam.multithreading.exception.MultithreadingException;
import com.epam.multithreading.parser.ClientParser;

/**
 * Imports data from txt files.
 */
public final class TxtDataImporter {
    /**
     * TxtDataImporter's logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Prevents utility class creation.
     */
    private TxtDataImporter() {
    }

    /**
     * Imports {@link Client}s objects from txt source file.
     *
     * @param  sourcePath  source file path
     * @return             list of {@link Client}s specified in source file
     * @throws IOException if file isn't exist or isn't accessible
     */
    public static List<Client> importClients(final String sourcePath)
            throws IOException {
        Stream<String> clientStrings = ClientParser
                .findInfoStrings(Files.lines(Paths.get(sourcePath)));
        Stream<ClientInfo> clientInformations = clientStrings.flatMap(str -> {
            try {
                return Stream.of(ClientParser.parseImportInfo(str));
            } catch (MultithreadingException e) {
                LOGGER.error("ClientParser.findInfoStrings method leaved"
                        + " inappropriate string: {}", str);
                return Stream.empty();
            }
        });
        return clientInformations
                .map(info -> ClientFactory.createUberClient(info))
                .collect(Collectors.toList());
    }

}
