package com.epam.multithreading.taxi;

import com.epam.multithreading.idprovider.SerialIdProvider;
import com.epam.multithreading.navigation.GeoPoint;

/**
 * Factory for {@link Taxi} class. Provides unique ID for every created
 * {@code Taxi}.
 */
public final class TaxiFactory {

    /**
     * Prevents from utility object creation.
     */
    private TaxiFactory() {
    }

    /**
     * Creates {@link Taxi} object for company from position where it is at the
     * start of the work.
     *
     * @param  startLocation position where the taxi is at the start of the work
     * @return               the {@link Taxi} object
     */
    public static Taxi createUberTaxi(final GeoPoint startLocation) {
        return new Taxi(startLocation, SerialIdProvider.getId());
    }
}
