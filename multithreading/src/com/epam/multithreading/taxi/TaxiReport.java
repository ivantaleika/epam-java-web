package com.epam.multithreading.taxi;

import java.util.LinkedList;
import java.util.List;

import com.epam.multithreading.client.Client;

/**
 * Stores information about {@link Taxi}'s work session.
 */
public class TaxiReport {
    /**
     * Id of reported taxi.
     */
    private long taxiId;
    /**
     * Reported taxi work start time.
     */
    private double workStartTime;
    /**
     * Reported taxi work end time.
     */
    private double workEndTime;
    /**
     * Clients transfered by reported taxi.
     */
    private List<Client> transferredClients;

    /**
     * Constructs report from taxi's id and it's work start time.
     *
     * @param id        report taxi's id
     * @param startTime taxi's work start time
     */
    public TaxiReport(final long id, final double startTime) {
        setTaxiId(id);
        setWorkStartTime(startTime);
        transferredClients = new LinkedList<>();
    }

    /**
     * @param id the taxiId to set
     */
    public void setTaxiId(final long id) {
        taxiId = id;
    }

    /**
     * @return the taxiId
     */
    public long getTaxiId() {
        return taxiId;
    }

    /**
     * @return the workStartTime
     */
    public double getWorkStartTime() {
        return workStartTime;
    }

    /**
     * @param time the workStartTime to set
     */
    public void setWorkStartTime(final double time) {
        workStartTime = time;
    }

    /**
     * @return the workEndTime
     */
    public double getWorkEndTime() {
        return workEndTime;
    }

    /**
     * @param endTime the workEndTime to set
     */
    public void setWorkEndTime(final double endTime) {
        workEndTime = endTime;
    }

    /**
     * @return the transferredClients
     */
    public List<Client> getTransferredClients() {
        return transferredClients;
    }

    /**
     * @param client the {@link Client} who was transferred
     */
    public void addTransferredClient(final Client client) {
        transferredClients.add(client);
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer(String.format(
                "TaxiReport [taxi #%d: work started at %.2f s,"
                        + " work ended at %.2f s, transfered clients:\n",
                taxiId, workStartTime, workEndTime));
        for (Client client : transferredClients) {
            buffer.append(client).append(";\n");
        }
        buffer.append(']');
        return buffer.toString();
    }
}
