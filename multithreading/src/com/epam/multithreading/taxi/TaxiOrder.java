package com.epam.multithreading.taxi;

/**
 * Provides information about taxi order.
 */
public class TaxiOrder {
    /**
     * The period for which the {@code Client} will travel from departure to
     * destination.
     */
    private double travelTime;
    /**
     * The period for which the {@code Taxi} arrives to the {@code Client}.
     */
    private double waitingTime;
    /**
     * The {@code Client}'s name.
     */
    private String clientName;
    /**
     * Client's taxi ID.
     */
    private long taxiId;
    /**
     * The time from the start of Java virtual machine until the order was
     * taken.
     */
    private double invocationStartTime;

    /**
     * Constructs an empty order.
     */
    public TaxiOrder() {
    }

    /**
     * @return the travelTime
     */
    public double getTravelTime() {
        return travelTime;
    }

    /**
     * @param newTravelTime the travelTime to set
     */
    public void setTravelTime(final double newTravelTime) {
        travelTime = newTravelTime;
    }

    /**
     * @return the waitingTime
     */
    public double getWaitingTime() {
        return waitingTime;
    }

    /**
     * @param newWaitingTime the waitingTime to set
     */
    public void setWaitingTime(final double newWaitingTime) {
        waitingTime = newWaitingTime;
    }

    /**
     * @return the clientName
     */
    public String getClientName() {
        return clientName;
    }

    /**
     * @param newClientName the clientName to set
     */
    public void setClientName(final String newClientName) {
        clientName = newClientName;
    }

    /**
     * @return the taxiId
     */
    public long getTaxiId() {
        return taxiId;
    }

    /**
     * @param newTaxiId the taxiId to set
     */
    public void setTaxiId(final long newTaxiId) {
        taxiId = newTaxiId;
    }

    /**
     * @return the invocationStartTime
     */
    public double getInvocationStartTime() {
        return invocationStartTime;
    }

    /**
     * @param time the invocationStartTime to set
     */
    public void setInvocationStartTime(final double time) {
        invocationStartTime = time;
    }

    /**
     * {@inheritDoc}.
     */
    @Override
    public String toString() {
        String output = String.format(
                "TaxiOrder [client = %s, taxi ID = %d,"
                        + " taken at = %.2f s, waitingTime = %.2f s,"
                        + " traveling time = %.2f s]",
                clientName, taxiId, invocationStartTime, waitingTime,
                travelTime);
        return output;
    }
}
