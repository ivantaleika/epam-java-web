package com.epam.multithreading.taxi;

import com.epam.multithreading.client.Client;

/**
 * Facade for {@link Taxi} objects to communicate with a taxi company.
 */
public interface TaxiManager {

    /**
     * Calls when the {@code Taxi} finished it's order.
     *
     * @param taxi company's {@code Taxi}
     */
    void onOrderCompleted(Taxi taxi);

    /**
     * Calls when the {@code Taxi} finished it's work session.
     *
     * @param taxi   company's {@code Taxi}
     * @param report about work that has been done during the session
     */
    void onWorkCompleted(Taxi taxi, TaxiReport report);

    /**
     * Returns a client for the {@code Taxi} if it exist, {@code null}
     * otherwise.
     *
     * @param  taxi company's {@code Taxi}
     * @return      client for the taxi if it exist, {@code null} otherwise
     */
    Client findClient(Taxi taxi);
}
