package com.epam.multithreading.taxi;

import java.lang.management.ManagementFactory;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.multithreading.client.Client;
import com.epam.multithreading.navigation.GeoPoint;
import com.epam.multithreading.navigation.Navigator;

/**
 * Represents a taxi that performs {@link Client} transferring.
 */
public class Taxi implements Callable<TaxiReport> {
    /**
     * Taix's logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();
    /**
     * Speed (per second) for all Taxis.
     */
    public static final double SPEED = 2;
    /**
     * Taxi's location after all orders will be completed. If taxi hasn't any
     * orders {@code goalLocation} is equal to {@code currentLocation}.
     */
    private GeoPoint goalLocation;
    /**
     * Current taxi's geo location.
     */
    private GeoPoint currentLocation;
    /**
     * Current client if it exists, {@code null} otherwise.
     */
    private Client client;
    /**
     * Taxi's unique id.
     */
    private long id;
    /**
     * The managerCompany company that set taxi's orders.
     */
    private TaxiManager managerCompany;
    /**
     * Provides taxi transferring synchronization.
     */
    private CountDownLatch transferLatch;

    /**
     * Constructs the taxi from its start location and id.
     *
     * @param startLocation location where the taxi is at the start of its work
     * @param taxiId        unique taxi's ID
     */
    public Taxi(final GeoPoint startLocation, final long taxiId) {
        currentLocation = startLocation;
        goalLocation = startLocation;
        id = taxiId;
    }

    /**
     * Performs clients transferring.
     *
     * @return {@link TaxiReport} about work that has been done
     */
    @Override
    public TaxiReport call() {
        final int second = 1000;
        TaxiReport report = new TaxiReport(this.id,
                ManagementFactory.getRuntimeMXBean().getUptime() / second);
        do {
            GeoPoint destination = client.getDepartureLocation();
            double distance = Navigator.getDistance(currentLocation,
                    destination);
            try {
                TimeUnit.SECONDS.sleep((long) (distance / SPEED));
                currentLocation = destination;
                destination = client.getDestinationLocation();
                distance = Navigator.getDistance(currentLocation, destination);
                TimeUnit.SECONDS.sleep((long) (distance / SPEED));
                currentLocation = destination;
                client.setLocation(destination);
                report.addTransferredClient(client);
                transferLatch.countDown();
            } catch (InterruptedException e) {
                LOGGER.error("{} tranfering was ended: {}", client, e);
            }
            managerCompany.onOrderCompleted(this);
            client = managerCompany.findClient(this);
        } while (client != null);
        report.setWorkEndTime(
                ManagementFactory.getRuntimeMXBean().getUptime() / second);
        managerCompany.onWorkCompleted(this, report);
        return report;
    }

    /**
     * @return the currentLocation
     */
    public GeoPoint getCurrentLocation() {
        return currentLocation;
    }

    /**
     * @param newCurrentLocation the currentLocation to set
     */
    public void setCurrentLocation(final GeoPoint newCurrentLocation) {
        currentLocation = newCurrentLocation;
    }

    /**
     * @return the goalLocation
     */
    public GeoPoint getGoalLocation() {
        return goalLocation;
    }

    /**
     * @return the client
     */
    public Client getClient() {
        return client;
    }

    /**
     * @param newClient the client to set
     */
    public void setClient(final Client newClient) {
        client = newClient;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param newId the id to set
     */
    public void setId(final long newId) {
        id = newId;
    }

    /**
     * @return the managerCompany
     */
    public TaxiManager getManagerCompany() {
        return managerCompany;
    }

    /**
     * @param newManagerCompany the managerCompany to set
     */
    public void setManagerCompany(final TaxiManager newManagerCompany) {
        managerCompany = newManagerCompany;
    }

    /**
     * @return the transferLatch
     */
    public CountDownLatch getTransferLatch() {
        return transferLatch;
    }

    /**
     * @param clientTransferLatch the transferLatch to set
     */
    public void setTransferLatch(final CountDownLatch clientTransferLatch) {
        transferLatch = clientTransferLatch;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Taxi [ id=" + id + ", managerCompany=" + managerCompany + "]";
    }

}
