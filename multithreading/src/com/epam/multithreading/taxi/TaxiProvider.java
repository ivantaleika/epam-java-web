package com.epam.multithreading.taxi;

import java.util.concurrent.TimeoutException;

import com.epam.multithreading.client.Client;

/**
 * Facade for clients to communicate with a taxi company.
 */
public interface TaxiProvider {

    /**
     * Tries to orders a taxi for given client by certain amount of time, throws
     * exception if it cannot be performed.
     *
     * @param  client           who is ordering a taxi
     * @param  time             waiting taxi max time in seconds
     * @return                  an order with information about transfer
     * @throws TimeoutException if taxi's waiting time is too long.
     */
    TaxiOrder orderTaxi(Client client, long time) throws TimeoutException;

}
