package com.epam.multithreading.taxi;

import com.epam.multithreading.client.Client;
import com.epam.multithreading.navigation.Navigator;

/**
 * Factory for {@link TaxiOrder} class.
 */
public final class TaxiOrderFactory {

    /**
     * Prevents from utility object creation.
     */
    private TaxiOrderFactory() {
    }

    /**
     * Construct {@link TaxiOrder} from {@link Client}, {@link Taxi} and order
     * invocation start time.
     *
     * @param  client              who ordered the taxi
     * @param  taxi                that took the order
     * @param  invocationStartTime time in which order was taken
     * @return                     {@link TaxiOrder} for specified order
     */
    public static TaxiOrder createOrder(final Client client, final Taxi taxi,
            final double invocationStartTime) {
        TaxiOrder order = new TaxiOrder();
        order.setClientName(client.getName());
        order.setTaxiId(taxi.getId());
        order.setInvocationStartTime(invocationStartTime);
        order.setWaitingTime(
                Navigator.getTransferTime(taxi.getCurrentLocation(),
                        client.getDepartureLocation(), Taxi.SPEED));
        order.setTravelTime(
                Navigator.getTransferTime(client.getDepartureLocation(),
                        client.getDestinationLocation(), Taxi.SPEED));
        return order;
    }
}
