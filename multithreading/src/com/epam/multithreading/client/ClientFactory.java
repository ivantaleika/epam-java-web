package com.epam.multithreading.client;

/**
 * Factory for {@link Client} class.
 */
public final class ClientFactory {

    /**
     * Prevents from utility object creation.
     */
    private ClientFactory() {
    }

    /**
     * Creates {@link Client} object company from {@link ClientInfo}.
     *
     * @param  info client's info
     * @return      the {@link Client} object
     */
    public static Client createUberClient(final ClientInfo info) {
        return new Client(info.getDepartureLocation(),
                info.getDestinationLocation(), info.getTime(), info.getName());
    }

}
