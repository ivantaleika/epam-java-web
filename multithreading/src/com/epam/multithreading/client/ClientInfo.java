package com.epam.multithreading.client;

import com.epam.multithreading.navigation.GeoPoint;

/**
 * Contains all imported info data about {@code Client}.
 */
public class ClientInfo {
    /**
     * time in seconds from start of a program until order will be performed.
     */
    private long time;
    /**
     * Departure person's geolocation.
     */
    private GeoPoint departureLocation;
    /**
     * Client's geolocation after after transfer. If transfer has already been
     * performed it is equal to {@code departureLocation}.
     */
    private GeoPoint destinationLocation;
    /**
     * Client's name.
     */
    private String name;

    /**
     * @return the departureLocation
     */
    public GeoPoint getDepartureLocation() {
        return departureLocation;
    }

    /**
     * @param location the departureLocation to set
     */
    public void setDepartureLocation(final GeoPoint location) {
        departureLocation = location;
    }

    /**
     * @return the destinationLocation
     */
    public GeoPoint getDestinationLocation() {
        return destinationLocation;
    }

    /**
     * @param lcoation the destinationLocation to set
     */
    public void setDestinationLocation(final GeoPoint lcoation) {
        destinationLocation = lcoation;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param newName the name to set
     */
    public void setName(final String newName) {
        name = newName;
    }

    /**
     * @return the time
     */
    public long getTime() {
        return time;
    }

    /**
     * @param newTime the time to set
     */
    public void setTime(final long newTime) {
        time = newTime;
    }

}
