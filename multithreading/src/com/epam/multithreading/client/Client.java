package com.epam.multithreading.client;

import java.lang.management.ManagementFactory;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.epam.multithreading.exception.MultithreadingException;
import com.epam.multithreading.navigation.GeoPoint;
import com.epam.multithreading.taxi.TaxiOrder;
import com.epam.multithreading.taxi.TaxiProvider;

/**
 * A taxi's client. Taxi ordering performs in separate {@code Thread}.
 */
public class Client implements Callable<TaxiOrder> {
    /**
     * Waiting orderTime in seconds in which order should be performed.
     */
    public static final long WAITING_TIME = 15;
    /**
     * Departure person's geolocation.
     */
    private GeoPoint departureLocation;
    /**
     * Client's geolocation after after transfer.
     */
    private GeoPoint destinationLocation;
    /**
     * orderTime in seconds from start of a program until order will be
     * performed.
     */
    private long orderTime;
    /**
     * A company that provides Taxis.
     */
    private TaxiProvider taxiCompany;
    /**
     * Client's name.
     */
    private String name;
    /**
     * Provides taxi transferring synchronization.
     */
    private CountDownLatch transferLatch;

    /**
     * Current client's geolocation.
     */
    private GeoPoint location;

    /**
     * Constructs client with name {@code clientName}, road from
     * {@code departure} to {@code destination} and {@code orderTime} in which
     * order will be performed.
     *
     * @param departure   road's start {@code GeoPoint}
     * @param destiantion road's end {@code GeoPoint}
     * @param time        orderTime in seconds from start of a program until
     *                    order will be performed
     * @param clientName  client's name
     */
    public Client(final GeoPoint departure, final GeoPoint destiantion,
            final long time, final String clientName) {
        departureLocation = departure;
        destinationLocation = destiantion;
        location = departure;
        orderTime = time;
        name = clientName;
    }

    /**
     * Tries to order a taxi.
     *
     * @return                         TaxiOrder an object that provides
     *                                 information about order
     * @throws TimeoutException        if taxi wasn't found by the given in
     * @throws MultithreadingException if transfer was canceled in processing
     */
    @Override
    public TaxiOrder call() throws TimeoutException, MultithreadingException {
        final int second = 1000;
        try {
            TimeUnit.SECONDS.sleep(
                    orderTime - ManagementFactory.getRuntimeMXBean().getUptime()
                            / second);
        } catch (InterruptedException exception) {
            throw new MultithreadingException(
                    "Transfer is canseled before infocation.");
        }
        TaxiOrder order = taxiCompany.orderTaxi(this, WAITING_TIME);
        try {
            getTransferLatch().await();
        } catch (InterruptedException e) {
            throw new MultithreadingException("Transfer is canseled by Uber.");
        }
        return order;
    }

    /**
     * @return the departureLocation
     */
    public GeoPoint getDepartureLocation() {
        return departureLocation;
    }

    /**
     * @param newDepartureLocation the departureLocation to set
     */
    public void setDepartureLocation(final GeoPoint newDepartureLocation) {
        departureLocation = newDepartureLocation;
    }

    /**
     * @return the destinationLocation
     */
    public GeoPoint getDestinationLocation() {
        return destinationLocation;
    }

    /**
     * @param newDestinationLocation the destinationLocation to set
     */
    public void setDestinationLocation(final GeoPoint newDestinationLocation) {
        destinationLocation = newDestinationLocation;
    }

    /**
     * @return the taxiCompany
     */
    public TaxiProvider getTaxiCompany() {
        return taxiCompany;
    }

    /**
     * @param newTaxiCompany the taxiCompany to set
     */
    public void setTaxiCompany(final TaxiProvider newTaxiCompany) {
        taxiCompany = newTaxiCompany;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param newName the name to set
     */
    public void setName(final String newName) {
        name = newName;
    }

    /**
     * @return the orderTime
     */
    public long getOrderTime() {
        return orderTime;
    }

    /**
     * @param time the orderTime to set
     */
    public void setOrderTime(final long time) {
        orderTime = time;
    }

    /**
     * @return the transferLatch
     */
    public CountDownLatch getTransferLatch() {
        return transferLatch;
    }

    /**
     * @param taxiTransferLatch the transferLatch to set
     */
    public void setTransferLatch(final CountDownLatch taxiTransferLatch) {
        transferLatch = taxiTransferLatch;
    }

    /**
     * @return the location
     */
    public GeoPoint getLocation() {
        return location;
    }

    /**
     * @param newLocation the location to set
     */
    public void setLocation(final GeoPoint newLocation) {
        location = newLocation;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        String output = String.format(
                "Client [name = %s, departure = %s,"
                        + " destination = %s, departure orderTime = %d]",
                name, departureLocation, destinationLocation, orderTime);
        return output;
    }

}
