package com.epam.multithreading.uber;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.multithreading.exception.MultithreadingException;
import com.epam.multithreading.navigation.GeoPoint;
import com.epam.multithreading.parser.TaxiParser;
import com.epam.multithreading.taxi.Taxi;
import com.epam.multithreading.taxi.TaxiFactory;

/**
 * Performs data import from configuration file for {@link Uber} class.
 */
public final class UberConfigReader {
    /**
     * UberConfigReader's logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Prevents utility class creation.
     */
    private UberConfigReader() {
    }

    /**
     * Imports {@link Taxi} objects from txt source file.
     *
     * @param  sourcePath  source file path
     * @return             list of {@link Taxi}s specified in source file
     * @throws IOException if file isn't exist or isn't accessible
     */
    public static List<Taxi> importTaxies(final String sourcePath)
            throws IOException {
        Stream<String> taxisStrings = TaxiParser
                .findInfoStrings(Files.lines(Paths.get(sourcePath)));
        Stream<GeoPoint> startLocations = taxisStrings.flatMap(str -> {
            try {
                return Stream.of(TaxiParser.findTaxiStartLocation(str));
            } catch (MultithreadingException e) {
                LOGGER.error("TaxiParser.findInfoStrings method leaved"
                        + " inappropriate string: {}", str);
                return Stream.empty();
            }
        });
        return startLocations
                .map(location -> TaxiFactory.createUberTaxi(location))
                .collect(Collectors.toList());
    }
}
