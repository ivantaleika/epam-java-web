package com.epam.multithreading.uber;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.multithreading.client.Client;
import com.epam.multithreading.exception.MultithreadingException;
import com.epam.multithreading.navigation.Navigator;
import com.epam.multithreading.taxi.Taxi;
import com.epam.multithreading.taxi.TaxiManager;
import com.epam.multithreading.taxi.TaxiOrder;
import com.epam.multithreading.taxi.TaxiOrderFactory;
import com.epam.multithreading.taxi.TaxiProvider;
import com.epam.multithreading.taxi.TaxiReport;

/**
 * Represents a taxi company that acts as a {@link TaxiManager} for
 * {@link Taxi}s and as a {@link TaxiProvider} for {@link Client}s.
 */
public final class Uber implements TaxiManager, TaxiProvider {
    /**
     * Uber's logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();
    /**
     * Max radius in witch taxis are searching.
     */
    public static final double SEARCH_RADIUS = 20;
    /**
     * {@code Taxi}s which are busy by performing orders.
     */
    private Map<Taxi, Future<TaxiReport>> busyTaxis;
    /**
     * {@code Client}s which are transfered by {@code Taxi}s.
     */
    private Map<Client, Taxi> processingClients;
    /**
     * {@code Client}s which are waiting for a {@code Taxi}s.
     */
    private Map<Client, Thread> waitingClients;
    /**
     * Reports about {@code Taxi}'s last work session.
     */
    private List<TaxiReport> reports;
    /**
     * {@code Taxi}s which are ready to take a {@code Client}.
     */
    private List<Taxi> freeTaxis;
    /**
     * Contains thread for each taxi.
     */
    private ExecutorService taxisPool;

    /*
     * Volatile keyword allows to use double checked locking which is much
     * faster that a simple INSTACE_LOCK.
     */
    /**
     * Singleton holder.
     */
    private static volatile Uber instance;
    /**
     * Path to txt file with data for creating {@code Uber} object.
     */
    private static String configPath;
    /**
     * Singleton creation locker.
     */
    private static final Lock INSTACE_LOCK = new ReentrantLock();
    /**
     * Client Collections locker.
     */
    private final Lock clientLock;
    /**
     * Taxi Collections locker.
     */
    private final Lock taxiLock;

    /**
     * Constructs object from configurations file. It is private cause of
     * {@code Uber} is a Singleton.
     *
     * @throws MultithreadingException if data file is unaccessible or
     *                                 corrupted.
     */
    private Uber() throws MultithreadingException {
        waitingClients = new HashMap<>();
        processingClients = new HashMap<>();
        busyTaxis = new HashMap<>();
        reports = new LinkedList<>();
        clientLock = new ReentrantLock();
        taxiLock = new ReentrantLock();
        importTaxis();
        taxisPool = Executors.newFixedThreadPool(freeTaxis.size());
    }

    /**
     * Import taxis data from configurations file specified by
     * {@code configPath}.
     *
     * @throws MultithreadingException if data file is unaccessible or
     *                                 corrupted.
     */
    private void importTaxis() throws MultithreadingException {
        try {
            freeTaxis = UberConfigReader.importTaxies(configPath);
        } catch (IOException e) {
            throw new MultithreadingException(
                    "Source file: " + configPath + " does not exist.", e);
        }
        if (freeTaxis.isEmpty()) {
            throw new MultithreadingException("Source file: " + configPath
                    + " does not contains taxi's info.");
        }
        freeTaxis.forEach(taxi -> taxi.setManagerCompany(this));
    }

    /**
     * Creates an {@code Uber} object or returns existed instance if it was
     * already created.
     *
     * <p>
     * User should provide configurations file with company taxi's and set the
     * {@code configPath} for it.
     *
     * @return                         creates an {@code Uber} object or returns
     *                                 existed instance if it was already
     *                                 created
     * @throws MultithreadingException if data file is unaccessible or
     *                                 corrupted.
     */
    public static Uber getInstance() throws MultithreadingException {
        Uber localInstance = instance;
        if (localInstance == null) {
            INSTACE_LOCK.lock();
            try {
                if (instance == null) {
                    instance = new Uber();
                }
            } finally {
                INSTACE_LOCK.unlock();
            }
            localInstance = instance;
        }
        return localInstance;
    }

    /**
     * Set path to txt configurations file with data for creating {@code Uber}
     * object.
     *
     * @param path the configPath to set.
     */
    public static void setConfigPath(final String path) {
        INSTACE_LOCK.lock();
        try {
            configPath = path;
        } finally {
            INSTACE_LOCK.unlock();
        }
    }

    /**
     * Removes client from processing list. {@inheritDoc}
     */
    @Override
    public void onOrderCompleted(final Taxi taxi) {
        clientLock.lock();
        try {
            processingClients.remove(taxi.getClient());
        } finally {
            clientLock.unlock();
        }
    }

    /**
     * {@inheritDoc}. Move {@code Taxi} to free taxis list and save its report
     * about current work session.
     */
    @Override
    public void onWorkCompleted(final Taxi taxi, final TaxiReport report) {
        taxiLock.lock();
        try {
            reports.add(report);
            busyTaxis.remove(taxi);
            freeTaxis.add(taxi);
        } finally {
            taxiLock.unlock();
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Client findClient(final Taxi taxi) {
        clientLock.lock();
        try {
            Client client = getNearestClient(taxi);
            if (client != null) {
                processingClients.put(client, taxi);
                setTransferLatch(taxi, client);
                waitingClients.remove(client).interrupt();
            }
            return client;
        } finally {
            clientLock.unlock();
        }

    }

    /**
     * Find the nearest {@code Client} in the {@code SEARCH_RADIUS} for the
     * given {@code Taxi}.
     *
     * @param  taxi searching {@code Taxi}
     * @return      nearest {@code Client} if it exists, {@code null} otherwise
     */
    private Client getNearestClient(final Taxi taxi) {
        Client nearestClient = null;
        double distance = 0;
        for (Client client : waitingClients.keySet()) {
            double clientDistance = Navigator.getDistance(
                    taxi.getCurrentLocation(), client.getDepartureLocation());
            if (clientDistance < SEARCH_RADIUS
                    && (nearestClient == null || clientDistance < distance)) {
                nearestClient = client;
                distance = clientDistance;
            }
        }
        return nearestClient;
    }

    /**
     * Set common {@link CountDownLatch} for both {@code Taxi} and
     * {@code Client} for transferring synchronization.
     *
     * @param taxi   ordered taxi
     * @param client who orders the taxi
     */
    private void setTransferLatch(final Taxi taxi, final Client client) {
        CountDownLatch latch = new CountDownLatch(1);
        taxi.setTransferLatch(latch);
        client.setTransferLatch(latch);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TaxiOrder orderTaxi(final Client client, final long time)
            throws TimeoutException {
        try {
            return orderFreeTaxi(client);
        } catch (MultithreadingException e) {
            return waitFreeTaxi(client, time);
        }
    }

    /**
     * Tries to order a free Taxi.
     *
     * @param  client                  who orders taxi
     * @return                         client's order
     * @throws MultithreadingException if there are no free taxis
     */
    private TaxiOrder orderFreeTaxi(final Client client)
            throws MultithreadingException {
        taxiLock.lock();
        try {
            if (!freeTaxis.isEmpty()) {

                Taxi taxi = getNearestTaxi(client);
                if (taxi != null) {
                    double invocationStartTime = getOrderInvocationStartTime();
                    taxi.setClient(client);
                    setTransferLatch(taxi, client);
                    freeTaxis.remove(taxi);
                    Future<TaxiReport> transer = taxisPool.submit(taxi);
                    busyTaxis.put(taxi, transer);
                    return TaxiOrderFactory.createOrder(client, taxi,
                            invocationStartTime);
                }
            }
        } finally {
            taxiLock.unlock();
        }
        throw new MultithreadingException("No free taxis found.");
    }

    /**
     * Find the nearest free {@code Taxi} in the {@code SEARCH_RADIUS} for the
     * given {@code Client}.
     *
     * @param  client searching {@code Client}
     * @return        nearest {@code Taxi} if it exists, {@code null} otherwise
     */
    private Taxi getNearestTaxi(final Client client) {
        Taxi nearestTaxi = null;
        double distance = 0;
        for (Taxi taxi : freeTaxis) {
            double taxiDistance = Navigator.getDistance(
                    client.getDepartureLocation(), taxi.getCurrentLocation());
            if (taxiDistance < SEARCH_RADIUS
                    && (nearestTaxi == null || taxiDistance < distance)) {
                distance = taxiDistance;
                nearestTaxi = taxi;
            }
        }
        return nearestTaxi;
    }

    /**
     * Tries to wait for free taxi.
     *
     * @param  client           who orders a taxi
     * @param  time             wait time
     * @return                  client's order
     * @throws TimeoutException if no free taxis was found in provided time
     */
    private TaxiOrder waitFreeTaxi(final Client client, final long time)
            throws TimeoutException {
        clientLock.lock();
        try {
            waitingClients.put(client, Thread.currentThread());
        } finally {
            clientLock.unlock();
        }
        try {
            TimeUnit.SECONDS.sleep(time);
            clientLock.lock();
            try {
                waitingClients.remove(client);
            } finally {
                clientLock.unlock();
            }
            throw new TimeoutException(
                    "No taxis was found in " + time + " s, for " + client);
        } catch (InterruptedException e) {
            double invocationStartTime = getOrderInvocationStartTime();
            Taxi taxi;
            clientLock.lock();
            try {
                taxi = processingClients.get(client);
            } finally {
                clientLock.unlock();
            }
            return TaxiOrderFactory.createOrder(client, taxi,
                    invocationStartTime);
        }
    }

    /**
     * @return time when order invocation starts.
     */
    private double getOrderInvocationStartTime() {
        final int second = 1000;
        return ManagementFactory.getRuntimeMXBean().getUptime() / second;
    }

    /**
     * Free all resources and try to stop all processing orders. After method
     * invocation old {@code Uber} references are corrupted.
     */
    public void close() {
        INSTACE_LOCK.lock();
        try {
            clientLock.lock();
            try {
                taxiLock.lock();
                try {
                    if (instance != null) {
                        busyTaxis.clear();
                        busyTaxis = null;
                        processingClients.clear();
                        processingClients = null;
                        waitingClients.clear();
                        waitingClients = null;
                        logReports();
                        reports.clear();
                        reports = null;
                        freeTaxis.clear();
                        freeTaxis = null;
                        taxisPool.shutdownNow();
                        taxisPool = null;
                        instance = null;
                    }
                } finally {
                    taxiLock.unlock();
                }
            } finally {
                clientLock.unlock();
            }
        } finally {
            INSTACE_LOCK.unlock();
        }
    }

    /**
     * Logs all existed reports.
     */
    private void logReports() {
        for (TaxiReport report : reports) {
            LOGGER.info("{}", report);
        }
    }
}
