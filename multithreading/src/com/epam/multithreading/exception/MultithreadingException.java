package com.epam.multithreading.exception;

/**
 * Parent class for Multithreading project exceptions hierarchy.
 */
public class MultithreadingException extends Exception {

    /**
     * Auto-generated ID.
     */
    private static final long serialVersionUID = 787617164731343845L;

    /**
     * Constructs an {@code MultithreadingException} with {@code null} as its
     * error detail message.
     */
    public MultithreadingException() {
        super();
    }

    /**
     * Constructs an {@code MultithreadingException} with the specified detail
     * message.
     *
     * @param message The detail message (which is saved for later retrieval by
     *                the {@link #getMessage()} method)
     */
    public MultithreadingException(final String message) {
        super(message);
    }

    /**
     * Constructs an {@code MultithreadingException} with the specified detail
     * message and cause.
     *
     * <p>
     * Note that the detail message associated with <code>cause</code> is
     * <i>not</i> automatically incorporated in this exception's detail message.
     *
     * @param message the detail message (which is saved for later retrieval by
     *                the {@link Throwable#getMessage()} method).
     * @param cause   the cause (which is saved for later retrieval by the
     *                {@link Throwable#getCause()} method). (A {@code null}
     *                value is permitted, and indicates that the cause is
     *                nonexistent or unknown.)
     */
    public MultithreadingException(final String message,
            final Throwable cause) {
        super(message, cause);
    }
}
