package com.epam.multithreading.runner;

import java.io.IOException;
import java.util.List;

import com.epam.multithreading.client.Client;
import com.epam.multithreading.dataimporter.TxtDataImporter;
import com.epam.multithreading.exception.MultithreadingException;
import com.epam.multithreading.uber.Uber;

/**
 * Application entry class.
 */
public final class Runner {

    /**
     * Clients source file path.
     */
    public static final String CLIENTS_INFO_PATH = "./data/clients.txt";
    /**
     * Taxis source file path.
     */
    public static final String TAXIS_INFO_PATH = "./data/uber.txt";

    /**
     * Prevents from constructing utility class objects.
     */
    private Runner() {
    }

    /**
     * Application entry method.
     *
     * @param args console arguments
     */
    public static void main(final String[] args) {
        Uber.setConfigPath(TAXIS_INFO_PATH);
        Uber uber;
        try {
            uber = Uber.getInstance();
        } catch (MultithreadingException exception) {
            System.err.println(exception.getMessage());
            return;
        }
        List<Client> clients;
        try {
            clients = TxtDataImporter.importClients(CLIENTS_INFO_PATH);
        } catch (IOException e) {
            System.err.println(
                    "Source file: " + CLIENTS_INFO_PATH + " does not exist.");
            return;
        }
        clients.forEach(client -> client.setTaxiCompany(uber));
        OrderExecutor.executeOrders(clients);
        uber.close();
    }

}
