package com.epam.multithreading.runner;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.multithreading.client.Client;
import com.epam.multithreading.taxi.TaxiOrder;

/**
 * Execute {@link Client}s orders.
 */
public final class OrderExecutor {
    /**
     * OrderExecutor's logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Prevents from creation of utility object.
     */
    private OrderExecutor() {
    }

    /**
     * Execute transfers for all clients in the List.
     *
     * @param clients taxi's clients
     */
    public static void executeOrders(final List<Client> clients) {
        ExecutorService clientExecutor = Executors
                .newFixedThreadPool(clients.size());
        try {
            List<Future<TaxiOrder>> clientOrders = clientExecutor
                    .invokeAll(clients);
            clientOrders.forEach(future -> {
                try {
                    System.out.println(future.get());
                } catch (InterruptedException e) {
                    System.err.println(e.getMessage());
                } catch (ExecutionException e) {
                    System.out.println(e.getCause().getMessage());
                }
            });
        } catch (InterruptedException e) {
            LOGGER.error("Client's orders execution was ended.");
        }
        clientExecutor.shutdown();
    }

}
