CREATE DATABASE  IF NOT EXISTS `db_races` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `db_races`;
-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: db_races
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bet_type`
--

DROP TABLE IF EXISTS `bet_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `bet_type` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id типа ставки.',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Название типа ставки',
  `n_horses` enum('zero','one','two','three') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Количество лошадей на который распространяется тип ставки ("победитель" - 1 лошадь, "первая тройка" - 3 лошади)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Содержит все доступные типы ставок на скачки.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bet_type`
--

LOCK TABLES `bet_type` WRITE;
/*!40000 ALTER TABLE `bet_type` DISABLE KEYS */;
INSERT INTO `bet_type` VALUES (1,'Win or E/W','one'),(2,' Tricast','three'),(3,' Distance','one'),(4,' Forecast','two'),(5,' Insurance','one'),(6,' Place','one'),(7,' Betting W/O','two');
/*!40000 ALTER TABLE `bet_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `color`
--

DROP TABLE IF EXISTS `color`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `color` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id цвета',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Название (описание) цвета',
  `image` blob COMMENT 'Картинка для отображения амуниции (может отсутствовать)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Содержит достыпные цвета амуниции лошади во время забега';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `color`
--

LOCK TABLES `color` WRITE;
/*!40000 ALTER TABLE `color` DISABLE KEYS */;
INSERT INTO `color` VALUES (1,'blue_yellow',_binary '�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0.\0\0\0:\0\0\0\��k�\0\0\0	pHYs\0\0\0\0\0��\0\0\nIDATh�Z	|Se�?��\�\�$M\�tI�\�\�J+m�\"PAv\\PA������y\�2\�C�Ou|3\�\�\�8��\�.P���\�RJik[��I��ٗ�d\�wKiڦ\�\"s~���˷�\�|\��\�rˁ\�L�x�͝��NLߘ�`\�\"�K\��z\�\�}�SKϻ9+׵4�}��{�yu�6\�\�k<0\����Bt\�\�\'�l}m�<Z\r^\'@�\�g�q(\nh@\�zs[\�\�5\�Ǿ>\�p��\�)�tM\'\�^4�uf\�n\�d�\���g��8M\�5�3\�\�θ)\�N��)���G���/O�t�\���߶Om\�z��\�_��Q`7�F3Mư��\�\�Mj$?�`�V���U$�\n\�\�\�~Esi:����}�1#�\�\�P�\�P\��>x]<\�Zz\��ގtc\�\�F?Ä\Zq\�gt�\�MW\�0ٗ�ӑ��M�\�B@]\�\�%)�<��sX\�:-n��b��/N�Ɩɮ܏�HH��\�k�\�}\�\��\�N�{3�bA�b�~4Z�Q�\�\Zh\�\�Ȣc!�\��h�{:ʉ�N���\\ϾsQ�\�\�R@�Y���h�\n\��� ���X2t\n�\�S4D\n�W�\�\�}Z�/���\���\�C�2�7.�\��\��O�0*	|}\�@c?s �M�.�Մ\�\�ȡ�\�C27tAwME�s\�4�?�\�P\�ϙ=�u�z�v�\�	ܾ6 �+�U�30u��\�j�\0�?((D|\��/��\�\��\�ȸ\�L4w�\��;\�\r���3�\��\Z_,��,ԅ\�\�-\�\�H3�gh\�V#J�˃��\�*e\�e|\�GC%zI4�˥@�V\�.��>�\�10�X}\�xC\��]z\�H\�7��\n4��n��W\�/�\��\��o��\�\��b4<	�\������ED�\nD*PGk�ȤC�s�\�G���)\'d\�B�m\�a\�\���zH�s\"�x\��Ώ�\��\\v�A\���\Z�&\�\�À\�v�\�/�\�}<v++tӐ0\�&�u՜F��\�q<5�:��z3\0�\�D$��\�u.���[8k�nW|~UƉ�&ގl�Bis\��sVރhP��l��D/�r����H��s%�qq@��H:��{\�\��\�\��-�_\�ic.�&\�ʄcH�FTt\"��͸V��\r\�˄SGd-�*>z1\�KF8B\�[\�5K:{zA���\�9\�<\�\�{�<�f\�ou㖡\�\0\"ȴ	`\�\�\�}(�\�CÉ�#j\�\�pZL�y\�@�ɉw��\��:c�%\Z�3\�\�\�Qj�P�\�w>qy\Z�>zԲkϾ���\�\�0\�Ƶ�	m\�\�A$\�N��F)p��)3<G�\�\�<qdC\�Wn��\��\�~_\�kUv\�HM�v�\0\��z6:��`9�\�᧟\�̓�\�(���\r\�p$-��o��}�N;\�\�!\�MH�*��\Z5,u�e\�;�\��@\"���\�J\�LN\�\�1��e��\�q�áY\�c5�\��\�۾?Uq,\\)sAƢۀB�!*\�ua?I��\�)\�W;\Z���],��gDg\��}̪���>����\�\�Ǡb\�\�\�\�rHX<�\�S�����\'Aȣ��I�\�\��=ê\�>\�p\�uM�\'�+V���Ф\'h\�A\�\ZZc2�\�qwxl\�\�\�\��TI7�9��\�\�\�BW�	h>~\�1��7s���i�\�k;@�M\�X���\�y�\�(�}U\'\�H�\�Z\"��\�\�U�h�@dz�M\�\��륪\�E9+\�$���V�p�Q�Ʌ`ՙ���8��k�\�\�P�H��͌*\�B0\�\�w\0��^ho0;�\�\�v{\�ѿ�T��8�[��?�P\�!A(\�p(\�\�\�f\�}j\�\�\�\�EfdE2\�6\�:!�H�\�I�8\�P0��4U\nHBA�T�ƍ90Z(:Z���\��q;ZO����\�\�5�\�\�\�C�\Z+�.;�p\��~D�\0bm�\�\�w\�߹q�U�G#t#$ Hh�\�D\�1���.	贆���AY \��/3M���Y���&bu$\�4�Ӝ�-VE<\�0$\�QűDq\�;�mda\�IF�ߡL�eW\�;<!\�\�;a�=�ǋ��F�\�\�t\rA\\p\'r��%e\�\�n\�`\�Ƈ&V��=+���T0�q�VY��G5yT?�ԧ\�Es�\\V\�\n\�\Z\�\��X�\�	�k�7E�\�fY�� N>\�g\�qɨ^\�Q��8\�h\"�\�ެe+J|\��\�\n�ȰR=~��\�x<^r����zm�h�ʨc�R|˝��\�\�x \�cK\�G$Wmx�9\Z�\�{)�:�\�\�b����\rC[��`���#�*�Q|e|\�\�ɬx�Qm�\'.ؐ�da�\�8�$R\�`&��\�\�\�	ɉ��׃͉\��1@^!�x�\�]~\�\�\�P;d^C\'=Z\���x\�S�x�Ȏ���\�⇨i3Xo7ZX~]Q\�$�%pLj�\�\�HZ���$&\n�Q����uq3�\�\����\�\�FI��C��p6a\"	j�ȩOz�\�\�s:�f\�|\�\"c��T\Z\�mڲI��&\�\�1:M\�\r\"Wt��=B\�_�\"\�DD\�h�C��~\��\'\\��ئ\��2����\�C]OT \�=;�\���E&:&+�5IxĂ��\�LT�qL_\���8ml|�o\�՗\"�L֑�D|\��DM@/��0L\�\"�OwTg�*y�ō\�q*p�G\�\�e�\�2�\�#��=�\�}~\�B8\�\�t� 8�rٓ\�@DJVT\�Go�d\�l}�^@�\�[\n�ys\�	�\�7�uIr��$;p%*\��cJ�Q\�4�|��\�\�F��\�z�4�B�3\�@&��+Ť=�$tw\���9�$�\�\�x�c�s�	��2�}W\raaP�\Z	�rB�M��	~�\�O暜&cw��AZ\�R6�\�� <6��\�K\�6\�.�ӧ��b9J\�<�\��1	0��\'��\�\�\�zP�O�\�E�a�\�ta0�g@�ꏁ\���)0Y��]��\0�\"3l�a\�;3YJ�BPƧ\�k9�\�\��\�?*N)^�\0�Q���QK����}e��M���\�Ը�����1Dp�,6u�ىD4�\�k�\�\�],�\�� \�\�b�#(\�\�S\'N/:3�`\�\��QT\�#\�ꤌ\�\�C�PƇΚƖ\�]\�W�=6�f�uo�:\"Jvt�\���j�\��UCy$\�\�\��}\�3]���F\�]\r�!\�\�ؙk8�+\�j�\�\�&H�\�Y]\��\�\�]�|�\�\�\�	w\�b\�)\�p�澶K.�J�\��o8~���SRlN���\�z\�|$*�\�h�\�\�\�tj暟rq` �hCz�@\�58U�\�\�L��ϔ:\�;���:KHQOF\�9j�)\��\�]k�e�j0���\�1\�\��L>d\\��ޔRʄ�<���\�ˑndw|�\0�	)7ǧ%@]v��\�\�\�\���-�1]7B!�-n\�\�[l��V�\�\�p$K4柆���sc%�\�sCu\"A\�\'ع�7��(7s\�<\�R|�u\�_�\�7��\���Ǯ5�{\ZsY��\�L��x��$�\��P岘�\�1\�3I�\�\�>&͔\�\�\���x�T	� ��}R��4iل\��\��4z t�\�I_\�ZL��7y$�3�_4��~\��}\�	Z�\��I\�S{��\"I�\�\�\�ᲚS\�g�P\r����\����\���3�\�0TT���\�tbȜ\\8ﱨi\�\Z�}$��0�!yz\�j\�+���\��\�]+\���K\r�2XV�\�$\n%J\�/\�\���\�琖��\�7�>p���\�O\\\��D`w�\�]&7DN˗%\�,\���0\'�N�\�U��\�	�Ǭ(�QW\�S[�\��\�a,���H&&�\\����Z*\�૷�\�[��h@\������\�N\�J�S\�K���C	���Q*)���\�l�Ԭ\�\�M[�\"���/���ʏ\�+�\�A\"�\���$��o�\�\�^�U}\���{s9\�\�\�M�\"B�\�n���h��6;�\��Z6m���l��d�\�Z�I��\�\�\�\��ݎW���=2N\��\�m��\��\�O��\�A�-�X2MaE\�\��\�`w��P\�Bu\�\�tX;W\�\�V[���	\�\n���\��\�}��ݟ3P\�3$��\�}^}m�\Zl�\�g\��\�3�P�m^7&\�\�;BF��e��ˣS\�\'\�\�3�ڱz\�s\nݝ�0Z$���;;L*\�\�ť9R��J��K\����\�K.O3\�2�\�\�fhj\��6K��U�j\�\�鯕\�J�6�@?%\�z�\Z�\�!\�lJ**1%ⷨq\Z\Z��\�Ç\�|`\�,_\�P��\"�\Z\�\'\\	?q�@���\Z�\�Z\�JL��JW��T��Gޏ%�d�*�K��H�a��\�\\\0^*\r~�#\�\�Ŝ�|7\�i\\v\�_��\�V}��\�\�$�p\�p���\�^��!\�rHd:�R,\�\Z��J\�\r`1\"\��\�|�������z\�Y\��qA&\�\0@T>2�\�T(J9*)2j3y��\�0��\rO\�\�\�a\�\��z����\�0��\�p\�|#� �0\�sǤ�T\���W�-�ez\Zc�͂�\�4��G\�	�\�\�<psnAT-�\�\�iYw\�\�r�Y,X6\��\�fӠ\�\�V��ں.\�\�@\�}+L\�a\�\n�Hax̴�8tV\�\�pr2de\�t�\�A�k\��\�t���\��\�~������8#�a�����\��\�\�\�0���\�\\�XSa�\�+�?\�t�\�\0jF�\�`�1�H\�H�\�[�N�I�p�\�\�:;�p֖��\\Y\�դ\�\Z\��\�\� =#K��G�|\\_\�t�dN4es�h�\�\�ykWK�\�p\�D\n\�K�N��R�b!>���v�\�~ȝ�h\�O�\�\��\�R3:J�6:\Z�{�@�R}�\�>P�\��O�}q&��\�k<\�DJ@�6��w��j݆�gN�j\�\�\�m\�]m\�I��g�\�U8�y��ǟ+?=\�R�9F#lP*x-�\�\"�ׁ:�\���\n\�m8��/?����B\"�fn߶\�/z��g��y�ٗO\�DF\�_\��\�W��6=-%$\�dn��Є���\0\rq\�Xhm慨\��_>\�Xy|l�\�\�w\�DG)��k~rR��\n\�O�8\�\�QiTLL�lN\�\�f�~\�\�ʳ\�ĞY��|��׷h##\������]\�/^\�\�<\�D2.\�O{]\�=��\��8p8]_:��Q\"g��\�{+V/��@ \�7\�\�FRa%��\�YB,\�Cc �I��\�\�8Pvh^nN6l{v\�[;\��\�`wOo�\�\�!F=��q\Z�\�b��!ZE�\�\�\�\�|�B!7\�l�\�g�h��\r\�%5��\�/�J\�a,\�r�/���7Mf\�\0��S�R\��\0\0\0\0IEND�B`�'),(2,'blue_white',_binary '�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0.\0\0\0:\0\0\0\��k�\0\0\0	pHYs\0\0\0\0\0��\0\01IDATh�Z	t\\\�u�o�7��٤\�.!\�2^���\r6�@H\�\�\�\�$\'!	IӜfi\�H�C\�)9\'9i���	=��Pcl\�ݒlKB��u�ͼ�y3\�\�[��#\�\�c������\����\��\��{\�\�ߥ?YD4\rR�26�\�FU����z}���\�-#�>��K%�\��\"ǽj���$��s6׻\�X�W\�(\�\r};�\�\�[�Z�Y&yp��|��U��c�\�\�麗\���+�q�\��h�s�A�\���|\�\�}\��9b�p�4�q<-\�b�\�x\�s�ww�\�\\\�\�|�\�?�\��\�\�[\\\�WN\�ĉ�?ù}D1\�\�Ȳ,\��y\�\"\r���|\�?x\�5\�\�ԯp����y�\\.�������5w0}�,�&\���\rKo�Zc\�\�\�e\�_\�X�4^\�\�\�^�F���%K\�3mD%\�f˰\�ҋ�	�V�\�\��\�%���\��\���3�~����l\�\�u�r�\�\�7�]\�w\�I4��a�.�\�\�50Σ�U\�_\nFHYy���\�߽\�Ts�2D,v$��?^D�\�6�y\�\�\�\�v/u�]�1F�\�O.K\'[��x\�p	�3މE\"���\�\�~d�1\�&aeWN\�#>p\�/\�2����\�_S���\�\�1�Q�L�\�Z�6�\rҴf�P6ϓ�a7\�䝙\�+hy\�*.\�ŏ�n6\���Ӝ!8m|�xA����\�d�ވ���G)K \'oQP0Ix�\n��O|c \��}Qr\�<\�\��r\"\r_�)\�V	�X�HJQ�\�\�W�E]\�loi��\�\��\��\�FWE\�(RHbB\�L\��H\\�\��Aer@ss\�\n#^��x,yi\�1�d.GB.M�j�$Hq0��~\�n��ы\�yeg\�\��\�ڶ\�\r\�\�ᐲ\"\���5ĺ\���\�9)�\�]�\�\�y�$.N\�C7$\�*��\�T�@Z<C�7L����T\0Jؘ��ۍt&\�\�\�X�}��7QF��H\�tf.kJ<ޛ�)�Bj�\�`h�8\��Ce�8\�\Z0~E�\�xJ&%\�L!_����sҦ�:�q���/n�m+\�J���@vn&�&9�{����$&Y$	k�\�q\�.LU�y�t%Gn�Tb\�Rl\�&2!�\�H��\��\�\�Lf\�\�[ol�\n�K\�R��=�]`\�y�hlxd���ϓ� �\�F\�=��\�NN�@,\�)U��T&A-+�U\�H\��\�C��I��\�zZ)d�\�&�j!\�	��\�\�q\n�a`Rיfϒ\0\�\�\0�=�?N�3g�J�\�\0�2�I\'\�S�\�trF\�\��7`+�hQ��8�,|\�(d��d[����,5��(�z�iGܾ!\n4pv���\0����$��\��\��\��\�|v`7\�\�sE �@G�nJ\�\�\�Tr�`{��T9[4\�\�g�d��Φ\�~\���\�\�;6\�z��y������)�L��En��R\'O+O~��ψ5-=>\�HFtҙ\�J\"/0�\�\����|;0\�\�@vV�o\�l\�)O\�rD��̧\�X��\�(K\�˻\�jڵt��İ\�|e\�!�W�\�\n�d�&\'\�\��z\�W^\�OґD�)\"q�Np\�\�\�Jiq\�BA!n&^S�9;�X\�\�\�Y�w�mu|��@�\�>���\�rMߴM�\��1�����<Vp{\\ڸ+\��\�7k���|\�vt�\�zr\�;\r�7\�v�te�c`��Z��]�	[S�G(\�ڨ�� ��!`�,��\�t+\�\��\��k/�=\���E�?\���V_C\"Զ0<NY��\�q\�}/�\n�.}r\�Y�;C��見�B_�\�\�Jke]\�\�3!�\�\�z%�����\�e\�}Vu�\�p�&�?<�[A�\r[x{��\�=\���/\�ɳd��ba|���\0P\r/_G�%\����\�>8�.w\��h�w �P�|�K��\�n\�7�_��\�V5�H=%����k\�{\\V��$$�٥]ܕ%�\�E���\�@�E\�4y��E�{��\�Z���%\��z�p\�t�\�\�	��\�o\�\�R\�w���\�K��\�_\��3����\���\�sš~\�\�caW+\�\�\�\"8�#��ǅp����\'s�s+�0j\�\�\�\�\Z`x�\�\�\�\Z\�\�\�L<\�#\�\��v\�e\�D\�Is�N�\�7\�\�>+\�7�=43݇LDCX\�<����	<.-BłKW��\�]\�sZ,\�@N|ky=ݸ\��\�}3\�T�͹\��b��qx8�va�e\����&�� \�\�\�Ƨ�\�2#$a�-0��F\�Ѵ�D�V\�\�!\�\�ԙU/ �\�J\�]h/b!�\��i�m\�z\�=J�\0\�V�N���ʀh�<��\�\�mc��\�V\�\�\�\�v��\�\�@M\�.&m\����5_!�N2U�\�\�%l|nxٹcQ�,�1\�\�7&�A\�!~\�x\\dU�\�<�\�~���[���|�P�$j�n͇���`�B��\�\�4)\���E��r���Fa8ҁH%�1y\�!\� �|}۟(vnrs{udU�$�B�\�3k�I��r\��?�A,f\��\�9d\�-Ԙ\Z�f;\���z2:\�[�j\\�a� R�m@\�ȅ�C��4D4^e�#Ѯ�;R�5����\'�{�\�F⛯��� ��\�\�\�l^\Z`\�1���W�{���\�Kw{\�K]\�L\�H6\0�耪rJ��\�`L\�S�M�gL7;8z�\�^��\n��$\��AV��\�2\�m\�U�\ZI(c�\Z��J�\��^ÚP<��\��q%\�ǿ��\�\�@�u	\"\�?��p�Xܔ��]\�(����\�\�݅P\0m\�{����s�\�\0&�\�\�9��-�I,��=,\�\�\r�\�\�\�\�*P��\Z2�G)��\�I\�\�\�g�\�a\�\�A\��=�Ox@����(״\�X\�EL��\���E\��jD��v��^\�\�詳\Z\�Q%\�\�b�֢\��A,��d\�F!ܟ\�W)��U��fǚ��Ԝd\�Z6��\�D�n��@\�G$�\�\���ᝣn\' P�ѦUw./\�f�36��\�¶·\�\�C3?sY�\�\�*Ѯ����)2+\�7G�u��_\�h\r��l\���Ay��s�\�1��ʫ:\�\�9\�`\�óƚ�ޣ�/�);t��F �\�\�V5\�p;���H�q���4\�9xLv޺�Ո�\�&��\�U)�Y�\�r\'\r@\�=�6jt�\�\�N����C\Z��H��\�)��XHgS1\�\rIB%?��&ۼ\���amH�v\���(�ϵw�O`\nX�؊=J���[��H\0Ɨ\�\�H�U��\���P��\�I\��\�S5����\�	���:��4�r�^@\0�W{\�\�\�XÛ�\�\�\��b\�M�\�J���6ËS䒕�y�8\�ik�,ݰ��R\�is\�F?\�\�\�HdP(\�:���f�NB\��\�\�\�K�\�\��y}4�Jb��_\�s\0\�\�;Q��	O,��\�n�P8[��כGaH<u�>�<L�h��\�\��c\�J�!UV��� C�\�\"�\�X��z��6\r\�Yt�L\�ǇFe\�\��\'�\�5$\'\�.g��7_pU<P[\�[\�\���/\\ϣ�v.\�\�a)��1\����H���\�SyK\�\�_\�\��3\�Y4U�\�\��}�d5C��P�yZ\rc���%:14�\��ު\�CO�3q��͕I�\�ˮ�>��v�&��v6.)k\� �l\��XϛE��荧��PK���\�^+\�F�\n+��8�Q����u8\�O!\��םg��ot�\�9�S�\�#��~y\"��YU�)r,şv��6\��#3��7��oN@��\�1IC��=�>볊�%=G��v=��\�\�ԅ�U��\�\�\�jmP	`=v�����S��Eq���3}���AN\�iD(\�s�[1\�\�T�r\�#M�\�j\�JҾp\�R#��T_�����)Y���\�\�ﾚ}��T�\����\�m&\�p�D�������\����X^\�l\�f^�\��r�\�\�o{�h�s�Zm#�4��]D$�\�g�(�/��P9F5�$8]$7t��=�{\�P��\n-53�\�\��\�l\�VQ\�)��9\�]�\�@��`B�c\�\�y̘\����a;\�\�n\��\�Y\�����ME\"��\�\ZZd�\�}\���Wr�J���t8���<&7��B���\�_\rJ\�\�\\�\�gzZ�\�z:u�\�/ڀ\�: %?h4Q?�9]z�\�ʳ\�\�\'f��vlؔ��C}�/sgEEּ�$���ʚ�\�N\�\���\�\�v��\�\��4\�\�$N(�W$ȐE�6\�ʛ\�Z{���P�|Cǟ�yc\�\�R%\0B.��]�2��\�ȇ�\�\�/�΍��a\�Y_�6!�\�\n驤���б�˅Xk�G�:O\�k9H\�{\"zF\�}�\�\�$��{��u\�\�\rT�i^z؍S4\�\�M�V�\�3}]\�(g�HXP\���h1w�TgDNg6\�	�r�\�	�32�9G\�d�bT\�\�T��N\�;\'~b0�	忈��]Wb����њ\�\�G\�VS\�RD\�+�\�E�\\�E)��:{`�����A����)9\\\�\�\�6X�QPB�ch\�W��ft��Q\�\�{8g�6\'y[0�.B%TW 8!g�c�1z�:Iu��\�����U�̽#F\�޻\�\�:�5�@6��\�\�̏̓\�\\m1/�FO��\�N\0�O��\�\�}Tw��\�d��mc\�H�xTit��#�{�Qj\�*.C�|�����w���h�.�*\�4�\�\�u7�\��\�w}\�+�\�ȏ\�\�f>�#UD$�b���\�s��\�Q\�\�e�f�R��k�t\�\�\�_z�~͆�\�\�tH3�(tw\���4\�m�BG}�NyDD9!\�\�ß0\�\�XoG,?�C�\�9N�\08��\���G�\�um��5\�F�\�\�Z�kCv�\�&�Ӧ�\�!\0��s|$�{Ԭ�M�78��Tg0\"g)��#\�箹y�	�o\�-�\�b<�ZpJ��Q粹5SK�4\�y\�\��):\�\��Y�XnHs+`uo����`\0hcVbS�5�Qz�9F��\Z\�\�x5;B\�Κ�N\���\�Z�O?=w\�D�SN<TmC��p�����!ӧP\�\Zع�TѸE�\ZI~�ɨ��#�K5\�\"im81\������ٖ#���.G\rp\�N���֘8\�RqR\�+�\����]6q\n�	ij��a\�\�\�hd:�1Y)�c]G}mla��\��ڷ\�}�\�\�\�\�S���\�#�b\�A5<�ٝ���\'��[��c\r��K���q��G��\�/s@\r\���\�R<��\�33�\��5k\�Y\��T\0wE�}Ɔ\�\�#�\\b\�\�\�T�\�L�R#�\'X\�\�\�[\�u\�\���\�ׯFLc\�\�Fq�N8���\r�\��\�N?��:\���cO�\�\��!�P9�����\�\�I>\�\�\��\�\�&��1���ڭ\�V\�dJ��헿\�\�~�?S�L\���3y���\�Д\���E�-a��3ɺP�^�i\��\�g7n\\Oow�Rd\�pEBU45=���Ӂc=;���=�Hv���\�1��%�aD\��9������ݷ�K$�\���\�D,\Z�72TUܱ}\�=�[\"{Zk���.\�k�\�\�I�\�\�\�*B�J�aL\�]�,r�\'KkZk\�\�7��s\�%fҩ5+�\��\�!7j\��ձS�w�}\�N�	�f\�3\�8�\�8k�Vl.�\�M����7�\��jKS�402�\��\�ǣ\�\�\�\�=��\�>�-\nl�v�� Pq^�\�\��M*\n��T*�\�\�\�\�ECOv\�9�w�\�0;\�>=��rl��^�l�p\���-�\���R\�HNg\�\��\�\�\�\��\��z\�[GN\���u�\\���PfJ0�\�\�T��B\��d�!��/\�;ڽ��:�j���p\�\�ɣZ������\�\�V�K2΂+�+P�\�\Z91#wb\�F`�g\0YY�a�\�s[Yi�+x��,���\�:)g�#�\Z�f\�\�R���\�P�\��2\0\0\0\0IEND�B`�'),(3,'purple_black',_binary '�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0.\0\0\0<\0\0\0���\0\0\0	pHYs\0\0\�\0\0\��+\0\0\�IDATh\�Z	tT\��\�͛\�d�\�{BBI �HY\"�A��1\�\n�K�Z����h\����W�s,(��XT��D�L��,d!\�$$3�d�y3���\�L0��?\�ef\��߽\��\�ދLE�$\��4a���9\�R�u9K�S����0Z)㕵���3��:j�z\�-�[�\�\�<N>��\�K�\�&$p2�E�2�2H-��\��к\��N�i\��?��2>c\�\�\�OM\�\�D��-�/�0�\Z\�X/X�׿R`�\�O��_\�o��2^	߂ȩk���\r�nk@v�\�T\�C\�1��\��t�qr��O\�\'\�F�\�E�hJF0\�@�\�	<,���\�P�كv\��?\�\�HYI���\rM�;�`\�é*\�r\'�N�\�\�N\'�9\��@\�Dt��\�\�\�\�%pW\\��h\�L-\�,�x\������\'���\�sk������v9�z�\���h�\"�\�B$��\�X�V�A�\\\�!q����q�\�\�\��7\�o~�Ϋ鋋��\�ٺbb��%sÉ\�_C�ՐhԀZ�#l8�D�\�f�L*3|k�x;�\n4\r4l�U!!mJ�\\\�n7L-̇�\�P��\�J� \\P��\�\�:*\"rba\�\�w�=Xs��lE�\�?���Y4��}͵챶7�\�E���N4�W����V4w7��\� �xp\�\�׊w8��\�\�g�}���x��\��\�\"(\�\�U�;e Z\0�t\ZM\��p����\��w6�J:�{\�a��\�j�\�1^a\�pJ��<�H�\�A�a���GD���g�kn\�u(�\�\���8�Z����v\�/ S��\��Xg�\�\�\�s5>d�\�9�C�\�\�_�b�\��#�g����3\�G��\�\�Dix\�P(l�>j�N��i9v�l�\�Z>���SL[%\��ͮ��1\�{ -#\�**���X\Z�z}��^Z�:��\�Z\0K\�W\0\"�N�+rƫ�\��]MF8���Y1��y`5zS��z\�\�ϊ\�`�\�M�k�l��G�IVFO\�\rM-��\�.\�\�\�\�\�\�xf�XH�\Z�F�Ά��8^#\�be��\�\��\�ҍ��\�p��\�\��\�Z\�\�DE\�-7�\���\��#MV\\�\\\�\nW�(`:\"�o�\r\�\�\�d	T�j`\�%�o!_���p(\�p\�r�\�0�Zjw1�\�\�\�4~�^�\�?D��W����N_t[��?/�\�\�\�X-I)�o᠔)0�u\�Is5�`�FDUgFH\"�3t\n&�\�\�#�\�=�*m\r\�=����6\�q��\�Aw\"(pL�oz:�h\�O�\n\�@&�̓�v�*�\�E\��\��,�\�e�&g\'\��(ܝ��i�┘�c\�G²B��\n\r+f�\�\�\�\�)KM\�۝_��<��1��3$p� �o�+�c�\�i��˭����\0\�Cyp��RT��#Zp(�ꄋ,��\�\�z��|\�6T!�Z�P�\�ѳ2IZ\��VH\�ğ��j�\�D�j���K\�w���t��\�\�\�h�7_@\�s\�\'��M\�݅}n;�\\Z]]Lr:y\�UK\�\�ā\�\��\rB�c����\"O[\��\�����\�>FҢ$)<R��6\�\"�����\�H]Ȇ�^�\"�\�G\��7�_K�g?=Z�̑q\�@X�C7J\�\�\�\�\�`\�\�F#]\�V�p\"�=\�o\�A��m�\��P��A�_nv^b*#uJ\�\�o\�O:��H¾���*jf\��D\�Z�Nb�/\� \�K�o^���73TB�\�m\�㲁\��\�걣�t�\�ru$=\��G�M\�}����p\n\�zǫ\�\�\�a\�=\�����yݜy�o! �D\0\�y4Nx\�\�&�)\�/\�;�\�\Z�j�+$i�O6o�Z{�``�&:�hsvßڶ��\�\rނA\"�G��1��P糥6�\0�\��뗦*\�\�a�7���$�!h0�x8\�wvK`�\�3�5\�DZ�>t\�W�\��jǿ�#�� �7:w\�G���\�*�A��3@7\�\�a\��3-e�6w5c�\�O?p�@\�\�\�\�%��f��ӄ�h\�\�P_l\�κ(��\��1\�	\�#�\�\��f\�g��p_%lG��+L���\�~f:\��\�@��Ȏ\�\�(mJ\rBq�+zh�\�.X�_\�\��\��o�K�cU�*Za �AH�H:\����\�\��.\�c}���\�w\0nJD��|\�\�\�\�sm�GRS�W;>e�\�\�A�\"|�b(\0N\�d%\�\�\�=\'�\�/��\�S\�\�L\�-�$]\�[\���m@\�\���\�pN`�\�MF���\�\�\n2\����0S\�.P獃\���@c\'�lYh8�O$>�QQ(,�\np	��\�}x�9���w�F]\'\0$%_�d��\���r�\�\r\�2\'\�&̿P\�H�\0KD��\�H(�\�p��L\�\�\�\�S�1J:A\rD\�\�Q5�hG\�4̭�yt]\��\�A���W��\�.\�\�\�.�\���[&�T���+�\��闍\�?+���\�l\���\"\�Zx8n\�Ŕ\�\��9;l<�|/<g�������v�/\�&]�w3���*\�	�d\�\�SV��~\�\�(�\�\�f��kL \�	Y��)����\� \�FG��p\�\�\�N[뾤��ï3/ �Cw;O$\�I�\r`���.�9\n��boEr���\�fI�7\�ݸ(4\�bl�GYs\�|\�N\�ɛH#y�\n�\\.^Auc�Ɨhըw��M�-�$�|���d��\�;I�	+\�o\�e%\�\�7�x�\"\���,\�J�qQQx>nA�A�X��P\�h�\�+\�nG����F\�mS�\�f�J��\�H@I��q\��\�}�\Z2F�9{�\�5\�ާ���\�sSiELruD\��h\�M\'?\�<�\�\�F�\�#�E���o	����#.}Eވ*3������Edt��Ԁ^%}�:#\�\�5�ޓ01F�X̤���\�\0�|nvhJ���vn\��\�\�wDL\�B�%�g\�&/e���*���Cnmn@OA�\�\r�\0P��\ZF���RGr�4\0	�_�S @n�\�\�r\0if\ZZ�\�\��9\�Qט\�>�c��XMH.�\�#�T\�\'p�}��z<~\��\�ڕ4��h\��G�ta[́Ev�	Ku\']py	�<�B\�I�\�c��\�f��dHX\�\�\��ɫ�웻�\�\�e�H\�gz��A��/P�筞�z�8�tAj\�z�|5��\�c:�\�\�6\�~�\" QB�$�`�wv\�\�\�ڌ*SL�!�f�\�\�H%\�X\�\��\��\�3|�T9·ކ\�\�a�\�^n����G��cT)\��c����\�\�4b$%#��D��\�-��\�\'Q3���\�0\�e����̍\�m>�^�\�\r\n��@�Ĩ\�D\"%�D\��\�V��\�?\�\'�\�aJ��գR\����U���jg\�*1W�yY��A�������\�L�$,E\Z�ԬpJ\�$\� �t\�\�\��Wx�ᑋ��<�8QM\�h8i��\�u�\�\����\�\�$Rҍ�\r��Kg�p,@P\�R\�1Ds����ٿ����\��vԂ\�P��6\�����9��\�^ak܊�\����	Y^9L\�˜�y�\0��	\�\�\�\�r��n\�ho�\�\'\�5\�V\�x�\�B�M�pz�\�2z�f,�Q�ǧ�\\;�g�F��\�\�M&=\��\Z\r{{\�/�o\0m��\�A\"C�<�Ӭ?H\�b\���	ʆY�x;L�.�&9Y^n8��Ĕ��\�Ȱŀ<��0M��K�w\�K�K\"mE\�Џsey���Z5\�\�x\�\�\\�)\�\�\����������\�л\�\r�ho��1\�(�1�_n��h\��\�+\�8�fp:��8@.��J�\�\�ˌQ�͝z\�5\�!�ktWT�(1	�\�A\�2l\�\\\nZ\�(y�8\�бP	����\�m�\���>\"8\�LN\�c���Q��\�\�n@5e*\"-|�ĩ%��\�tׂ\�\�o\��֣<σ\�l%\�\�t�`E�o۲��[.\�%\�z�?��\�A/b\�I��\�\�<�\�(X\Z�.C&�\�\�\�ue�F\�)\�\"��T9=.�\\\�b�J\�\�\��3\�O�+D\�R��p4j5/-��\��u\�\�/>����>\�M�\�}\�;\�_>#��\�\�\�v�\�7��wMi���;o�\�DG\�`)hD�*�����\\�1\�\�\�&,�:uȱo\�6\���u�?yĵ\�wnC��\�c�\�\�q�~/6\�0CvE��\�\�3\��\�v���ߖ�.V��\�\��ծ-�j�\�\�mÝH�m����xem\�ӫ��\�sn����Y��\�/�|�~\�\�_|\��20Q�?F�\�Ԕ\�٭U�\�	\�r�xm\��G\���\�ʜ2i\��0�63�9�S�׈Ժj\�C_-��\�ͼܜ�ͭ\�1Y�\�\��~�w\�0>�|\�8��:\�F�\��p��镂�7�ض�\�v�����!�x\�\�\�\�\�\�ʤb�!\�G\�Z����o�\�\���\�\0\0\0\0IEND�B`�'),(4,'red_white',_binary '�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0.\0\0\0:\0\0\0\��k�\0\0\0	pHYs\0\0\0\0\0��\0\0\�IDATh�Z	xSe����4m�$ݡm\�R��E��(��,W�U�\�+\�#^ԫ\�6��\�w`d-B-{[JAZ\�ֽiiK�%\�\�4�\�wh0�M�ܙ\�yNs\��\�����\�S	��\�3�(�O<�B��\��\�$��\�]\�VN�)?���{Ҍ/$m\ZQ\��G^]B�1�<���6\"��\�/\�O4��\r\�@d1٬w�I�D27�����3�\�/�Pi\��St\�I��q\�\�\�E\�60\���E��\� 2hj\�ع͌ӈQ��\�Q*˜�\�;=\\�@\�<\\χ\�5�H\�Idjt\�\�\�B�w�\�+�*g&)TD�\��.\�$��x\�\�fk[�\�ڡ\�j8C�%1f2\�c��2��m�z�l���/��\�b�\"�`�)��ƻض�a$n����\��JrW�D7��ݭu\�;���(\�:����%��;&RCI)$\�	3t\�Z	(�HPd%�O����p�Z�Ou.�T_ё�Gh{��\�]G\�1Z��`\��\n\�]�fR\��\�aƥ\�yaԲ\�XJR9\�C��\�\���Н�\�h0��>�p)�#�\�KhӁ+��X%U�|���\�\0�\�\�!H\�\n85W\�\�\'��\�	\�G�\�Fw\�][[-%z����\�@���ϊ�����\�9�\�T^=�6��\�C ����r\���Ӗ͟��r�|=�\0�\�8	[��\�57x\�\�\�BVXo/\�.����\�߷w�oߠ�\�z\�dу�\��i\�D�u)̞���\0KH����d���q��$MN\�{Xx/�Pq�\�.�\�\�\��\�m\��햏�\��,mm�k1C\�$@� \�%\�\��\�L6�$o��\r҈�v�icWsq|g\�B\�\'&ҢE�\�\�͍d2\�v\�\��;�\�\'wp\�N60e\�\�f\�/_��HyY\�\�\�܏FԈ\�\�f,h%JHH @�\�\�\�\�h�ru���ɿЋ�.ELR\��m{�\�MN�\�U\�Re�1\�e��^<�q>\�6{SS�(m~^�`eddPMM\rUVV���r9;\'�$+kb��p2��\��2��\�=PX�\��b�S�\�B<\�\�\09W\���\�^�%\�\'ЫW/qyyy�\�s\�\�g�}&.�!z�\\�\�\�j�XV^A�~�$�Z���\�r�F8��I?}~4�d��\��\�\�$��\'EFFR�VKb�K�߇f\�>F\n|2L�\Z\Z\Zh\�ܹ�d>�\�#�\�c�C(�\�)SP@\0��\�ڰqy`]9}�iǞ�\�\�s�T*���K�\�ɓ\'�\�\��\�\�\�\'�w8�N�ی;\��\�#U(\��9sf\�ڏ6\�*,̃m�\�h\rU\�\�ҝj8\Z��L\�g_-[�ꃥЙ��={R8 B\�\�O<A��\��`3l6ߵJ�k\�\�hU\�ٲ�\�E;\�{�~�lٲ\��\�i\\\�D����_\�.�\�d\�)G��uu�\�w\�t\�\�3\�DDDД)Sh�\�\�\"�,釥n1\�߹s�����~~~�C�}\r?�dɒ�7nl���U\�ݻ������FS\�\�d�\�Ĺ:�k՚m��\��\��~���\�ثW��\�\��\�Ww衠\��\�\�\�\�̛7\�? �g!���@��w���Jgϝ��� \Z6l�(퐰0�\�\��5\�\�\�\�h�~�\�g/\\�p\����L@%�����r��\���9r\�H���\�.\0&\'O�>=�\�,N\r�\�\�ׯN�X���\�l��=z4\r8PU�dgg7\0n\�0\�\�~x\�O5j\�E�q�^|��\�ZV6�Ə�\�?���\�\�\�\�ջ�Y\�\�***D]`�\"���2�F:#\�Ds�N��q���͛瘍\�\\O�\�\�ޙ3�\"l�!\�ى��\�\�\�h�\�jt��7�Ki\��U[S�\�\�L�UG��v#.��3g\�\�=z��F��\�7l�a\�ud��\"���$�$A@#I\�h�a�\�\�<\�g0a��5�edCh`ý��\�\�rغ\�\�\�K��ɮ��s�\�x\�!9���c�iժU�L�<y\"`7��\�*�^��\"��dD�!�\�F`\rf-$��p`\�<�\0�ހ�q;�����dmi&2S�җ\�2\�QrY\�?O8��3��c�\��v��\�\�^˖RN\�\ZP^EF��6\�\0����\��\���<�+���\0i�\"�	Hp��C2__L&\�\�Y�MMIH������8åC�\�\�\����\��%�\�\�yPy�P�AO��OP ��M\�F\�\�pZ.2�Lt}��\�a,C�7юc9N��W)%RO__ܫ��l?K�Ͳ�\�\0�I�&%Bac�ePP\��ǩ\�V\�!�pǋ:HO\���t\��(\�,���-���/�cP\� Ӧ���d�昞|�\��\��\nӘ\�h]\�c��\0ۯ�#:�38>��g<E�Wo��\�ڀLJeӡ:��̊g���\�\��3\�Q\�Ml؆\�\��%jIH��%j5I ��q�\�>  @�)^�����c�x\�\n�|Kv\�\���8\�şc\�\�\�Ш\n�x,�\�\�\�m�u������bp@u�(��H�Iʖ�\�nX i\�$�E\�_ړ$�J_t��B��\�\�34\�ڵk0��\�n\'������Ki���_5l��A(��IpH\�\����EE\�\\UM>#�ߛJ�Ѩf�^�]�i�q\�\�&<���\�.n\�\nY�@\����X���\�%>>~1t�Z�62օ�gϊ\�]��yoo\��\0��濋,�\���:p\�\�/�Xؼ\��0m\�4\�f\�;wv~6��3���\�p$�V~Q.���9w\�\�3\�_\�\�<*��I�O?\�a<c{ƌܶm\�7n\n��;v\"L`���\Z�n_���5\r\�\��e\��n�`\�$��RMJh��p��m?+rR;�\�Ab4Qr\�։\�i\��\�:����KANMJJ�)pވ�1��th�l�v\�N\n\�+$��Y\�yJX�\�Ll�ؠ\\�\�O����[\Z�?0�mR�q~�\�8\�\��\�\�ݻ��\�\�\�Od+V�P\�V�yԆ]S\�ɓVëo�����6�wd\�\�1{M`N��]�\�..�{<{�n_��]���zJ︸a�p�\�:!0\�\r۾S\n\���\�\�I�{MZm\���W?�\�\\�nkh\"	<��Kd��fJj&�b��(\�6\�sd�����T{\��\�w�[;�X[�xq/�\nC`\n;\�#�ݼyS�iӦg�2�E-{f\�X/�`�=%��UU��ߊ`\�f@�\�N~9,1�[��\�.�brS�S+x\�T���Τ�%11:$\�(\�]�o�_�\�O�8+��\��\�r\n�u\�֥Z�6��D�:�綋o��\�\�1}\��\�\ZxLd�ֲr1F�\�#�\�\�[�紡,a�kÁ{�\�\�˄�G��\�\�\��/\�F\��t����\�\�\�\��\�9BCC	ɍ�\0�\�ߛ\�\�c��T__DC�/_֭^�\����\�HPđ\�b��\�\�4���������ئ\�;T��UC(ݔ\�\rq|\�x\�\�Y(\�r\��N�\�t�С��O�~�\\\�qr�����\�sg\�ԝ<}�.]�t\�qv$T	����󥨋Tv|�[\rb�\�8\�\�5H�������*}�.==]�(����\��v�\�nݺ�cǎr!33�8R���ZG3�cƌ�N\�\���\��\�\�E�}��\�C��pF\�I	\�a*,�,\nU��\0\�qn����q�iE�׽HK\�?�P�Z��g\�5k\�}��	rg�?t\�P��S��\r�G\��e\�0\\�F�qG]\��~\�\�_��7Z��\�^y\�\�H/\�m\�e����/*b>\n\�\�@0}d\�\�J4�fr�\�54V���\�#F��\Z92Q��sMv�j�M@I-��n	/:���\��\��x˖5QA��]�\�bq�L� ���g1�\0\�U*�Ʀf�gW\�DlqD����J��IwH�\�<X�BiDD���O�҆u\�\r0 Y\Z	��w_\��\�\�ϟ��\�2�}TjeKcS���\�\�|\Z.s��iU�\r�\�ƒ\�%\�\',d14e�q�p�/\�rl�V\�\rŉ(�\"#j\��(0w?U�^�:����UW\�s:�\�t\�\�9vo\�\�\\:u2��z��U�E%�؊\�\�-\�g\�`1��ދ)��\�\�t͐5\�+\�\�$`�\�L�$<\�L��f!؏|�+#�r�~*\�(��?�s\�N�5�\�Du��\�yf\�Ν;�c��	a\�͜,�\�\Zɠoi�]Q��r&���)\�\�\�W�\��a	\�\�S+\�g.\�X�\��Z�q\�\�U\�,�mH\�\�##\�R�\�@�e��V�\Z?���:@M\�~I\�\�Ci��\�%%MG\�P\�\�\�\n\�\�\�ng\�3Ο�� _�\�\�_̤l1P\0JOKk0ZlƐ\�~=bcb�\�#|,U�Pk}\r\�\�\�y\0�\���99j7:�\\iU=����O�\�\rWU�o�G\�\�[\�d2%��<�\�\�$3\�cBR�ɟ����Qߠ\�\�\�R2U�Q�LB!�\��\�\����;��[.\�~=\��[dn�i\"�\�\�c~O�P|C����OD?�\�N\�}��W�\�=6��\��;\��\�	\�\�\�^xa\�8���J�m\���s}\�1u��P:J�h��\\�\�Y\�5u�\�c~�D\�q\�ƺCR8<��\n�\�a�jഴǎ�\�b�x2�\�\�\�.M\����oľ�!=�XX\0��7)��[\��6n~\�\�\�rd�r:��\�*\�ܚRϟ%ow�R�\��\�\�\�\�?\�\���+�	\�$`\�\�ߑ&J\�\�\�w8r<e\�\�?+�\�R�f�\�\�\�\�\�v>̶�\�%G\�_,�I�\��Ů�\�;+f�:z��\�ա�\�\�F\�#\0�֞�w�t�Ć\��e:�g�\�\�C������j�JJ\�N��8!3-����ZWRZV������\�\��\���X�����ӻ	GU\�L��A���\�cl�/�MR\�Ѝk9jԾ�oX��\�\��	\�h_ґ\�|\0�ˆOv�\��M\�\���\�i���J7؉\�E\�Cè�D{\�_~3�\���s�_�R\�\�2\��<c\�(MD�ܷo��_���\�*n\��fs�V�4TUUϼ�k�\�<l\�\�k7~�vhppo`{�7\�v|�\nx\��d\�\��$L����p,��\�:O�r��\���\�S�E\�E̞\�\�\�r�a�3v�e���\�\"�OǓ\�\r4�ֽ��˯���\�\��\�<\�?v\�u9�K\��?&�Pd�GV\�\�W��_��V�\��\�R���ȴWN\��3�K6�Y&6F�\�Zŝ;y3\��\�t|t\\E\0\0\0\0IEND�B`�'),(5,'green_white',_binary '�PNG\r\n\Z\n\0\0\0\rIHDR\0\0\0.\0\0\0:\0\0\0\��k�\0\0\0	pHYs\0\0\0\0\0��\0\0IDATh�Z	xTU�>�ޫ}O*����QdGDE�]F\\?��]f\�\�QGm\�V�\�Q�uF�?m\�Q\��\��i�\�\�\"��1�@B�T�*�\�\�WU�*PA\�ߜ�^\�ջ�\�{\��YoT��L�2=Y\Z\�\���\�\�/T��j�\"���\�MG\�N��^v\�u���\�&\�\�\��\�*�8\�\����`NΑy��\�\�\�<\�	�CG\�T���^*�HЈi���.\��ڿٷ\�3d����\�#\�?l\�|&G�A�r��\rOZ�2�̐�X\�\�9�4\�lj�z}\�6\�<<j\�i?���\�G\���]oo�G2J���\�(G3d�5klM�����WhlZ\"հ݇m���aGҐ\�\�+L�-cY���|.O9@\�9�raʗlHxb�x�t�m�\\�Fm\�C\�^Wa�j�T�c_�:d��N92h욛\��o�\�H���]#\�?l\�|&\�\�Gu��J\�\\a%�\�F\�H\�l�2�N:@\��\�ŚXy+/��\��e\���/�7\�7^��S\�23��2cA_�r�����/\�{ImҐ�����u�~\�Ġ�JЮ�\�;&��\�KUa�\�\�\'\�s\�\�F\�ؾ\�D*��\�\"mE���^�z\r�t�OAK�T\Z���C�\n��\��I,�Hkp[ �\�HH�7s\�H�\�7��;\�8	�#`P�eg_Es]Sh�{/\�\�m%� �h�\n��Z-UK\�\�\�?w��c-��T�*>`\���4Hϼ�r�\�\'�aE�҉�E\�2�̆	�\���\�rG$\�G���ѮZ\Z娢+兔�6\n\0���$�$��\�/}$m�r�:\�\0����~\�\�\�����Mf\���\����\'W��\�Sw�%�d(e4��Y\�\�քG���L^&�FC�r\�he�\��T�X�q\�T�p,U.�VJ5`N�^_��\�\�֐\�jl\�#H\�)W\�0���6E�\�@t�\�oN��,�쇊dV���X\�S��>\�}��m���\�?��p��\Z5e|^�[�{fM�:\\�\�>�^\��m�vل�m�s�r�\�\�C�	�)�9=�՛w\�ݷ�cϞ�55�H$ir�\�\rzo�\��Ý�\�T��y���\�i�Dޙ�v\�\�b|�l���\�\��89\�Ƚ\�w\�q�\��\�T:������\�PČ|\�D�\�jlc�V��\�\����x\�.�A�BN\�\�\�p��q�\�\�(�w@�e\�1��QQ\�vej٥Zb6�إ\���v���(s�*mY8 5~\��\��\�֮T\�dT#\�\�uq-��*��y��b\��@�\�\�3^�2o-KZ�d\�R:͹4}�\�ʛ?|�>�6:c\�:\�\� �߯�f\�\�)��\����|趜1\�7��ׯ)ёڢ&�\�R�7�\�\�l�s��[�D#���\�Yy\�l��ҍ՗\��wuC5\�[v.�\�\�>O*8L�֨8�x(A�?�\�g\�[?՗��\�\�2r�WA�\�\�p\��CiDg�Y޸qF#��\��l\�2\�E5�\�W޷\�O�\�S�t\'e�\�\�\�C[hc�6��9H�\�\�1���\�ԍ���[�[{n�u�?G��Cy\�\��\��D\�Ǣ���\�\�+\�\�\'9\�3V)���ബ|N%�[�Ի\�G:\'\�{���b\�3t�)��E�\�G��q\�?M�,�m\�{p�kB\��\�P\�4�xň�Bο����F\�Q\�\�\�%j�va\�\\W=3��UO�V�I�B�\�L\�\�\"CQ�2 �Βmb	Y\�\�`ZUs\�(q,\�Dҟg���#o�m\0���pS�ht\�\�K\�\�|\n�e����\��$��\�-:\�~�\n;��aW\�1�ːZ��X�\�\�5\��n1+q.�%]����Z�G�\�\�iI2� dJc�;\�\�o\���s1���]̔K��\�\\\�K\�-�\�uQ\�T_R��p���\�C$��SL�S�\�F�T�4��,���L\�(���؁e\�y�=d\�Z(�MR$%{\Z\�L8�Ԗ\�\�H\�L�Us\�\�[\�r�<\'���4(\r\�8p\�R\�T���y\�\\o6 7�$.C�a��yb���\��s�f\�4\�5�zS\�t�N\�*RfI+\�	\�H�i1/\�M�\�\��7\��\��1\'\�w�6R�Ԋ�\�\�\�]5\�Y&Ŏ�פ�i���\�.��\'-ւj����lf\����\�\�#S�E��\��YB�M�9{�]v\�\�\�1\��\�\�\�#�d|\�p�f�tߔ�\�ڱW\r>\�\��\�~��{+�\�i�\�(Iz5���\�X\�R+�6���\�\�\�K2k~R>۵@�f���h����`�vRNҔ���0\�\�+���L6�{�\�S���.���wy~\�<Ja��\�e\�\�	���\�\�\�R�d.yHH�S4V�VsY\�ì`�LL�m\�6y�#\�E\�\ZL��*]��ъZEa�<��\�yH�d\�a�Xə<CX9\�3�\n�F\�\�\�\��w.�N�㶦ҥ\�*ҡQ\�NSf0@ҟ C���)5{v\�n�74\�\�4\�Ce���k\�\��ƨ6R[��6��\��\�l\��\�Ӯ/H#pdU@��a\�z��u�\�\�\�}�`��B�3\�<\�eh���=`=��aû���4�\�E���ӷ�\�\Z3�:%;HO\�~�Z�l&0ZH�-\n\"\��\�\�O\��\��\���H[ܟ��fEe��*\�ڪ!�M+v���6)&�����s��\�M���b{�\�\��-\�\�/=d��� �T.��DV>\�ff@�#����-*\��C]-�+%�1c�oIo\\)i�nq�=	��{ \�9��\�xe�`)�\�KG\��+.\Z\��ٴ.ʚ�$\�\��I�=�⋞\�.��\�Ԋy\�ň�D\�P1b��\�+\��ϋ .\�NKoC��LT,t@0���\�Y\�1\�9\�4\�:A�8���y�/\�t\�\��À��\Z\�\rd��	�§{��\r\r\�29\��8�g�\�\�,\�QVb\�W\�@n8�\��\�P�CyJ9a+��\��\r\�\�X$�h~�l8��\�%�t��\�wo\�ƣ[�ɶ�q�\�s��?m;�g\�\Zc�{]\�#�\��ݼ�dJ�ZG\�G\�Y�u\0Op\�\'r\�\���l�jL\��Ԝ\�i�eI�_��r�Q1�>\�����(~v\n)\�\�Q��\�=\�P�\�!u��\0�\�	O\�-�M\�\"Hz\�f\�\�D{�\�0(F`PB\�=V�	q�➋u���4\�y.\�4CqoTh6S4+���\�2\"\�,�2;�Gv@��\�\�a׍B6%/\��\�18��gᔠ��Z\���=��\\\Z\�uN�9�[\�K�/\������sP*JR�(�x\�\0\����c�*�sx���+\�\r�\�\���C+�tQ+R\�7�9QR\�\�\Z�\�I-�g�\�\�ٮ8�\�\�۽;\�3@�ۋ��ı\�\�\�M\�9�\�\�ϑ\n*�Yؗ�7��LsΫ�aE\�W�x\�r	ɁR\�(\�{N��z�e�֡��O,�E�R�?\�\�p\�ܳ��|	}ر�^\��g\n�\�p\�X�\���\�w��\n�!�U�aE��*%�xBĩ��dr\�*	;7Y�྘\�C�`K_\'�I�\�fw}\�%��\�@Q[�	\�bpTȑ\�\�mk�䂝�;�x^̢�ɢ\�\�a\�m�.s4\�\�\��5<�7/��#��\rB\�5\�S\'K�M��ɳ�}[�7�#�zv�=�\�I�\�YI\�\���i\��\�\�NԑA�W�{S��S�3YUix\��&\�+@�8M+�\�\�]s\�h\�\�\\Q/j��s�A\�\�z����C�\�b\�fw\�\�]�tvGj�\�\�\�\�\��\�H�\�\�2Ŏ[	\�.ä\�j(n�\�&��\��\"M�	�\��Q!7U��\�\�K\';�ιUk+��@:���\�\�%�ͶH�B���.ư{-�Rv\�\�W�ݰ�Ի\��?�ݾ;\�fT:b���\�Ҳ\�%4��3�7ZW\�\�\�]dњ����O\"�h,�d��t^\�4�c��-}�Q&�9l	��\\}z��\��%ؘAB�@dDz��M\�\�y\�\�u��z��4\�=��l��\�:�Si\�\n˧?@?�[D��::�j.=>\�7Tc�R��q\��\���_&ݪ,t�m�\�D�q\�CtQ���Eí�8j-\\�	@�\�Z8��\�ď\�v�|\�\�%D�6��o͐~�?�e��֫`\�\�rIAcՒ[ϋ��\�\�<c\�t�`o�;�j\�G4ԖsD8\�6�\�!/=�\�\�Ϧ@{�5���\n��\�\�;QU(\�Lh��п�g/\�9\�\���Ӂ�\��1\�J��\�\�L㈏զ\\\�{5ћw\�7G�\nD�r\�\�\�X�2� Y_�\r\�N�u�})[�B\�d˓B���\r\nR$ \�\�n��W\��b%�-���M�\�Ӥ_�Kճjik\�6\��f\�+ބ�6v~�hhك]�\���\�\��\�\�HB��\�	�TS-\�\��(`�-O�\'5֚�\�^3�$�h\���\�a��]\\Q�*|)/ \�5MieC:�rĶF�����lb�P�t4��\�t��+\�@\�h\�M\�z�:#GZ�䝬�f��/xE	y\'�M\��ѳ���l�l4\�q\�ys(b\�}�@�B\�D8��8\�Jz���\�\�8n��Ԁ|.\���=��0c�\�\�Q�\�d��y\�vu�z#A2\'\r��.\�^�z�;Pk(�	�5���7�?|���H\�rX�sΈN&	�X�\\��\��\ZY\�\�W����@��⿐MOmh�JgM\���F�R\�{Q�)�\�\Z\���\�$\�F\���\�9�8��ҳ�i��\�Q˸�\�(dfj\"-�_F׍�\�4{\�\�V����lt|G���\�+\�LѥP�Ǉ�3�\�ٜ�\�\�sF\�N+�$2)\�y$�\�1\�8{Ҵ\�\�N��\�\r~d?#S\'����uGx\�f\������U\�g�Xz\�3\�r\�\�\Z\�М`�Xx/I%0�)@P�8C\�Ҿ��;}�\�羨\��ɨE\�g�\0�gP�\�\�\�VZ�nV�����\�f\���i4�qQ��2���[Z�ʈ�\�֕�\�{x-ѓ�So\�{\�\��6\�]��](w�/Q���(=_�PǗ7��\�\�PGa_\�g8:I&!\�<�]��\�4C&�L��j7�q�\�\'\�\�\�+Ō�qw���X��|&\�e�\��\�\�yE(������\�HӨ;f�i*\\T\�F�j墰�i�e�\�9���t)�8�gO۳�/8\�F\�n\��Ҹ\���-�Y4\�\�\\z4�L�\�g\��._�wo������\�m^\�D\�rʟ�\�8R0?�Vg`�w;\�\�I\�lW�T��\"i6\�l8P\�r�\�7�#��@�|\"mɒ\�b&-�]�ۻ-�\�G}�h�۟o��?�r*��pN�\�hN\r}�m\�m�Tz\�\�\�S\�x\�dˁ�w�W_#,^t	Mh����;~���\���\�n4\�ߛ�\��@:�\�\��K�\�ص\���T%�Q*T�\�\"b$A�6�+\�]�\�{o��\�u\�rV�U�*\�X���%�jͻo�[��7\�yw\� �7\�\��B�<��9��;�>z\��K\�\�����\�\�vu3��^g\�N�(\�?p�oqH\�����m\��,�c|�\��F,��fh�\��N��\ZlI13\�GV��\�\�\�?����_��N�\�O��\�\��뺆qc�2�Q�;E\�(���D5U8�\�\�\�}\�O��\�\�\�_�^�\�\����\�5U��%/�z\�\�\�r!+z�{\�j�RW䁇%�Cǐ5\�A����\���\�S�|�\�O\�W\�t��ey\�+���\��o[�-\�P�t���^�#��.������\�\�_7}�Y�h0L|\�O��\��EM\�j5��x���ȎbH�2̄�8�\�?\�7��8=_�aӜ\�MgҊ�\�\��W^��XW�\���o�毢4,\�\�\�H��Ba*\��om;�\�f�|��Q�w\���}\� 1FHli�Ԉ���\���?d��aE{��\�\�M\0\0\0\0IEND�B`�');
/*!40000 ALTER TABLE `color` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `horse`
--

DROP TABLE IF EXISTS `horse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `horse` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id лошади',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Имя лошади',
  `age` tinyint(3) unsigned NOT NULL COMMENT 'Возраст лошади',
  `sex` enum('male','female','gelding') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Пол лошади',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Содержит лошадей, учавствующих в скачках';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `horse`
--

LOCK TABLES `horse` WRITE;
/*!40000 ALTER TABLE `horse` DISABLE KEYS */;
INSERT INTO `horse` VALUES (1,'Boris Ferry',10,'male'),(2,'Lou Barton',12,'female'),(3,'Maybelle Goodwin IV',8,'gelding'),(4,'Amely Price',6,'gelding'),(5,'Dr. Nicholas Smitham',6,'female'),(6,'Ms. Josie Will PhD',13,'gelding'),(7,'Niko Kuvalis',12,'gelding'),(8,'Prof. Sven Kovacek Jr.',7,'gelding'),(9,'Sister Upton',10,'male'),(10,'Doyle Walker',3,'male'),(11,'Eloisa Rodriguez PhD',5,'female'),(12,'Ms. Berenice Kuhic V',8,'female'),(13,'Columbus Legros',12,'male'),(14,'Lysanne Yost',11,'gelding'),(15,'Aidan Kovacek',5,'male');
/*!40000 ALTER TABLE `horse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `race`
--

DROP TABLE IF EXISTS `race`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `race` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id заезда',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Название заезда',
  `location` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Место проведения заезда',
  `distance` smallint(5) unsigned NOT NULL COMMENT 'Расстояние заезда в метрах',
  `date_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Содержит текущие и будущие заезды';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `race`
--

LOCK TABLES `race` WRITE;
/*!40000 ALTER TABLE `race` DISABLE KEYS */;
INSERT INTO `race` VALUES (1,'Connellyberg','Vermont',2000,'2017-10-31 01:14:00'),(2,'Brauntown','Nebraska',3000,'2017-09-01 12:30:00'),(3,'New Lavonnemouth','Louisiana',1000,'2017-09-01 12:40:00'),(4,'Lake Gabrielmouth','Louisiana',4000,'2017-10-30 10:00:00'),(5,'West Eryn','RhodeIsland',1000,'2017-09-02 19:10:00');
/*!40000 ALTER TABLE `race` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `race_archive`
--

DROP TABLE IF EXISTS `race_archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `race_archive` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID гонки',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Название заезда',
  `location` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Место проведение заезда',
  `distance` smallint(5) unsigned NOT NULL COMMENT 'Дистанция забега ',
  `date_time` datetime NOT NULL COMMENT 'Дата и время заезда',
  `is_held` tinyint(1) NOT NULL COMMENT 'Состоялась ли гонка',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Архив прошедших скачек для просмотра статистики';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `race_archive`
--

LOCK TABLES `race_archive` WRITE;
/*!40000 ALTER TABLE `race_archive` DISABLE KEYS */;
INSERT INTO `race_archive` VALUES (21,'North Fanniefurt','Arizona',2000,'1993-08-27 00:46:51',1),(22,'Franeckiport','Minnesota',1000,'1995-03-13 10:52:29',1),(23,'Thielborough','Kansas',500,'2004-07-14 09:24:54',1),(24,'New Alysonmouth','Virginia',3000,'1994-09-22 09:00:47',1),(25,'Stiedemannberg','Minnesota',500,'1997-01-16 16:30:00',0),(26,'East Cloyd','Kansas',2000,'1987-05-05 16:15:45',1),(27,'Millsburgh','Nebraska',3000,'1982-03-14 11:32:57',1),(28,'West Melody','Louisiana',3000,'2012-11-24 09:12:23',1),(29,'Leopoldomouth','Louisiana',500,'1972-03-23 19:57:36',1),(30,'Stiedemannberg','Minnesota',2000,'1997-01-16 16:10:40',0);
/*!40000 ALTER TABLE `race_archive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `race_bet`
--

DROP TABLE IF EXISTS `race_bet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `race_bet` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id ставки',
  `race_id` int(10) unsigned NOT NULL COMMENT 'Id заезда',
  `bet_type_id` tinyint(3) unsigned NOT NULL COMMENT 'Id типа ставки',
  `first_horse_id` int(10) unsigned DEFAULT NULL COMMENT 'Id лошади, фигурирующей в ставке. NULL - ставка, которая не зависит от такого количества лошадей',
  `second_horse_id` int(10) unsigned DEFAULT NULL COMMENT 'Id лошади, фигурирующей в ставке. NULL - ставка, которая не зависит от такого количества лошадей',
  `third_horse_id` int(10) unsigned DEFAULT NULL COMMENT 'Id лошади, фигурирующей в ставке. NULL - ставка, которая не зависит от такого количества лошадей',
  `odds` decimal(4,3) unsigned DEFAULT NULL COMMENT 'Коэффициет ставки. Значение NULL - SP ставка (коэффициет становится известен только перед началом забега, после принятия всех ставок)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_bet_idx` (`race_id`,`bet_type_id`,`first_horse_id`,`second_horse_id`,`third_horse_id`),
  KEY `bet_type_id_idx` (`bet_type_id`),
  KEY `horse_id_idx` (`first_horse_id`) /*!80000 INVISIBLE */,
  KEY `third_horse_id_idx` (`third_horse_id`) /*!80000 INVISIBLE */,
  KEY `second_horse_id_idx` (`second_horse_id`) /*!80000 INVISIBLE */,
  KEY `race_id_idx` (`race_id`),
  CONSTRAINT `FK_RaceBet_BetType` FOREIGN KEY (`bet_type_id`) REFERENCES `bet_type` (`id`),
  CONSTRAINT `FK_RaceBet_FirstHorseId_Horse` FOREIGN KEY (`first_horse_id`) REFERENCES `horse` (`id`),
  CONSTRAINT `FK_RaceBet_Race` FOREIGN KEY (`race_id`) REFERENCES `race` (`id`),
  CONSTRAINT `FK_RaceBet_SecondHorseId_Horse` FOREIGN KEY (`second_horse_id`) REFERENCES `horse` (`id`),
  CONSTRAINT `FK_RaceBet_ThirdHorseId_Horse` FOREIGN KEY (`third_horse_id`) REFERENCES `horse` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Содержит заезды и назначенные для них ставки';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `race_bet`
--

LOCK TABLES `race_bet` WRITE;
/*!40000 ALTER TABLE `race_bet` DISABLE KEYS */;
INSERT INTO `race_bet` VALUES (1,1,1,1,NULL,NULL,4.650),(3,3,3,4,NULL,NULL,3.688),(4,4,4,2,4,NULL,4.553),(5,5,5,5,NULL,NULL,3.847),(6,1,6,2,NULL,NULL,8.272),(8,3,7,4,5,NULL,2.150),(9,4,1,8,NULL,NULL,9.662),(10,5,2,5,6,10,1.152),(11,1,3,8,NULL,NULL,4.625),(13,3,5,11,NULL,NULL,9.851),(14,4,6,14,NULL,NULL,8.328),(16,1,7,1,4,NULL,7.095),(18,3,2,4,5,11,1.550),(19,4,3,4,NULL,NULL,3.217),(20,5,4,5,6,NULL,3.020),(21,1,5,3,NULL,NULL,1.893),(24,4,7,8,2,NULL,3.834),(25,5,1,10,NULL,NULL,8.975),(26,1,2,1,2,3,5.178),(28,3,4,9,7,NULL,3.651),(29,4,5,2,NULL,NULL,8.970),(30,5,6,5,NULL,NULL,5.295),(31,1,1,2,NULL,NULL,1.400),(32,1,1,3,NULL,NULL,3.200);
/*!40000 ALTER TABLE `race_bet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `race_bet_archive`
--

DROP TABLE IF EXISTS `race_bet_archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `race_bet_archive` (
  `id` bigint(20) unsigned NOT NULL COMMENT 'Id ставки',
  `race_id` bigint(20) unsigned NOT NULL COMMENT 'Id заезда',
  `bet_type_id` tinyint(3) unsigned NOT NULL COMMENT 'Id типа ставки',
  `first_horse_id` int(10) unsigned DEFAULT NULL COMMENT 'Id лошади, фигурирующей в ставке. NULL - ставка, которая не зависит от такого количества лошадей',
  `second_horse_id` int(10) unsigned DEFAULT NULL COMMENT 'Id лошади, фигурирующей в ставке. NULL - ставка, которая не зависит от такого количества лошадей',
  `third_horse_id` int(10) unsigned DEFAULT NULL COMMENT 'Id лошади, фигурирующей в ставке. NULL - ставка, которая не зависит от такого количества лошадей',
  `final_odds` decimal(4,3) unsigned NOT NULL COMMENT 'Итоговый коэффициет ставки.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_bet_idx` (`race_id`,`bet_type_id`,`first_horse_id`,`second_horse_id`,`third_horse_id`),
  KEY `race_id_idx` (`race_id`),
  KEY `bet_type_id_idx` (`bet_type_id`),
  KEY `first_horse_id_idx` (`first_horse_id`),
  KEY `second_horse_id_idx` (`second_horse_id`),
  KEY `third_horse_id_idx` (`third_horse_id`),
  CONSTRAINT `FK_RaceBetArchive_BetType` FOREIGN KEY (`bet_type_id`) REFERENCES `bet_type` (`id`),
  CONSTRAINT `FK_RaceBetArchive_FirstHorseId_Horse` FOREIGN KEY (`first_horse_id`) REFERENCES `horse` (`id`),
  CONSTRAINT `FK_RaceBetArchive_RaceArchive` FOREIGN KEY (`race_id`) REFERENCES `race_archive` (`id`),
  CONSTRAINT `FK_RaceBetArchive_SecondHorseId_Horse` FOREIGN KEY (`second_horse_id`) REFERENCES `horse` (`id`),
  CONSTRAINT `FK_RaceBetArchive_ThirdHorseId_Horse` FOREIGN KEY (`third_horse_id`) REFERENCES `horse` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Содержит закончившиеся заезды и назначенные для них ставки';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `race_bet_archive`
--

LOCK TABLES `race_bet_archive` WRITE;
/*!40000 ALTER TABLE `race_bet_archive` DISABLE KEYS */;
INSERT INTO `race_bet_archive` VALUES (33,21,7,1,2,NULL,7.534),(35,21,4,4,3,NULL,3.184),(36,21,3,1,NULL,NULL,2.135),(38,22,6,7,NULL,NULL,6.730),(39,24,7,13,14,NULL,4.679),(40,22,5,7,NULL,NULL,4.468),(41,23,7,8,9,NULL,2.925),(44,23,1,10,NULL,NULL,2.311),(45,23,4,11,12,NULL,2.050),(46,22,1,7,NULL,NULL,7.843),(47,22,4,7,6,NULL,2.339),(48,23,2,8,9,10,1.141),(50,24,4,13,14,NULL,3.326),(51,23,6,8,NULL,NULL,2.697),(52,24,4,14,13,NULL,5.689),(53,22,3,7,NULL,NULL,8.577),(54,21,1,1,NULL,NULL,5.386),(55,24,5,14,NULL,NULL,7.220),(56,22,7,7,6,NULL,4.085),(57,22,2,7,6,5,9.900),(58,23,3,8,NULL,NULL,1.241),(59,21,5,2,NULL,NULL,1.858),(60,22,2,6,7,5,2.235),(61,21,2,1,2,3,2.407),(62,23,5,8,NULL,NULL,2.277),(63,23,7,8,9,NULL,1.587),(64,24,1,13,NULL,NULL,4.407),(65,21,6,1,NULL,NULL,3.722),(66,24,3,14,NULL,NULL,3.416),(67,24,6,14,NULL,NULL,9.038),(68,21,6,2,NULL,NULL,3.353),(69,21,1,3,NULL,NULL,2.381),(70,24,1,14,NULL,NULL,1.479),(71,23,3,10,NULL,NULL,2.863);
/*!40000 ALTER TABLE `race_bet_archive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `race_bet_archive_m2m_user_bet_archive`
--

DROP TABLE IF EXISTS `race_bet_archive_m2m_user_bet_archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `race_bet_archive_m2m_user_bet_archive` (
  `user_bet_id` bigint(20) unsigned NOT NULL COMMENT 'Id ставки пользователя',
  `race_bet_id` bigint(20) unsigned NOT NULL COMMENT 'Id предоставляемой букмейкерами ставки',
  `odds` decimal(4,3) unsigned NOT NULL COMMENT 'Коэффициет по которому израла ставка пользователя',
  PRIMARY KEY (`user_bet_id`,`race_bet_id`),
  KEY `FK_RaceBetArchiveM2MUserBetArchive_RaceBetArchive_idx` (`race_bet_id`),
  CONSTRAINT `FK_RaceBetArchiveM2MUserBetArchive_RaceBetArchive` FOREIGN KEY (`race_bet_id`) REFERENCES `race_bet_archive` (`id`),
  CONSTRAINT `FK_RaceBetArchiveM2MUserBetArchive_UserBetArchive` FOREIGN KEY (`user_bet_id`) REFERENCES `user_bet_archive` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Содержит архивные ставки клиента и ставки предоставляемые букмейкерами, входящие в них';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `race_bet_archive_m2m_user_bet_archive`
--

LOCK TABLES `race_bet_archive_m2m_user_bet_archive` WRITE;
/*!40000 ALTER TABLE `race_bet_archive_m2m_user_bet_archive` DISABLE KEYS */;
INSERT INTO `race_bet_archive_m2m_user_bet_archive` VALUES (10,35,3.138),(10,36,2.135),(11,54,5.700),(12,52,5.000),(12,70,1.400),(13,41,3.500),(14,60,2.200),(15,57,9.900);
/*!40000 ALTER TABLE `race_bet_archive_m2m_user_bet_archive` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `race_bet_m2m_user_bet`
--

DROP TABLE IF EXISTS `race_bet_m2m_user_bet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `race_bet_m2m_user_bet` (
  `user_bet_id` int(10) unsigned NOT NULL COMMENT 'Id ставки пользователя',
  `race_bet_id` bigint(20) unsigned NOT NULL COMMENT 'Id предоставляемой букмейкерами ставки',
  `odds` decimal(4,3) unsigned DEFAULT NULL COMMENT 'Коэффициет предлагаемой букмейкерами на момент ставки пользователем. NULL - значение будет определено перед началом заезда.',
  PRIMARY KEY (`user_bet_id`,`race_bet_id`),
  KEY `race_bet_id_idx` (`race_bet_id`) /*!80000 INVISIBLE */,
  CONSTRAINT `FK_RaceBetM2MUserBet_ActiveUserBet` FOREIGN KEY (`user_bet_id`) REFERENCES `user_bet` (`id`),
  CONSTRAINT `FK_RaceBetM2MUserBet_RaceBet` FOREIGN KEY (`race_bet_id`) REFERENCES `race_bet` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Содержит ставки клиента и ставки предоставляемые букмейкерами, входящие в них';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `race_bet_m2m_user_bet`
--

LOCK TABLES `race_bet_m2m_user_bet` WRITE;
/*!40000 ALTER TABLE `race_bet_m2m_user_bet` DISABLE KEYS */;
INSERT INTO `race_bet_m2m_user_bet` VALUES (1,30,5.295),(2,21,1.750),(3,10,1.373),(4,4,4.553),(4,21,1.893),(5,9,6.501),(8,1,3.485),(8,18,1.324),(8,21,1.893),(11,20,2.273),(12,18,1.550),(13,18,1.984),(14,28,3.901),(15,8,2.150),(18,20,3.030);
/*!40000 ALTER TABLE `race_bet_m2m_user_bet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `race_m2m_horse`
--

DROP TABLE IF EXISTS `race_m2m_horse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `race_m2m_horse` (
  `race_id` int(10) unsigned NOT NULL COMMENT 'Id забега',
  `horse_id` int(10) unsigned NOT NULL COMMENT 'Id учавствующей лошади',
  `color_id` smallint(5) unsigned NOT NULL COMMENT 'Id цвета амуниции',
  `number` tinyint(3) unsigned NOT NULL COMMENT 'Стартовый номер лошади',
  PRIMARY KEY (`race_id`,`horse_id`),
  UNIQUE KEY `unique_race_color_idx` (`race_id`,`color_id`) /*!80000 INVISIBLE */,
  UNIQUE KEY `unique_race_number_idx` (`race_id`,`number`),
  KEY `horse_id_idx` (`horse_id`),
  KEY `color_id_idx` (`color_id`),
  CONSTRAINT `FK_RaceM2MHorse_Color` FOREIGN KEY (`color_id`) REFERENCES `color` (`id`),
  CONSTRAINT `FK_RaceM2MHorse_Horse` FOREIGN KEY (`horse_id`) REFERENCES `horse` (`id`),
  CONSTRAINT `FK_RaceM2MHorse_Race` FOREIGN KEY (`race_id`) REFERENCES `race` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Содержит предстоящие и текущие забеги и их участников';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `race_m2m_horse`
--

LOCK TABLES `race_m2m_horse` WRITE;
/*!40000 ALTER TABLE `race_m2m_horse` DISABLE KEYS */;
INSERT INTO `race_m2m_horse` VALUES (1,1,1,1),(1,2,2,2),(1,3,5,5),(1,4,3,3),(1,8,4,4),(2,3,5,3),(2,12,4,2),(2,15,3,1),(3,4,1,1),(3,5,2,2),(3,7,4,3),(3,9,5,4),(3,11,3,5),(4,2,4,2),(4,4,3,4),(4,8,1,3),(4,14,2,1),(5,5,4,2),(5,6,5,1),(5,10,3,3);
/*!40000 ALTER TABLE `race_m2m_horse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `result`
--

DROP TABLE IF EXISTS `result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `result` (
  `race_id` bigint(20) unsigned NOT NULL,
  `horse_id` int(10) unsigned NOT NULL COMMENT 'Id лошади',
  `color_id` smallint(5) unsigned NOT NULL COMMENT 'Id цвета амуниции',
  `place` tinyint(3) unsigned DEFAULT NULL COMMENT 'Место лошади на финише. Null - лошадь снялась с забега',
  `number` tinyint(3) unsigned DEFAULT NULL COMMENT 'Стартовый номер лошади',
  PRIMARY KEY (`race_id`,`horse_id`),
  UNIQUE KEY `unique_color_idx` (`race_id`,`color_id`) /*!80000 INVISIBLE */,
  UNIQUE KEY `unique_number_idx` (`race_id`,`number`) /*!80000 INVISIBLE */,
  UNIQUE KEY `unique_place_idx` (`race_id`,`place`),
  KEY `horse_id_idx` (`horse_id`),
  KEY `color_id_idx` (`color_id`),
  CONSTRAINT `FK_Result_Color` FOREIGN KEY (`color_id`) REFERENCES `color` (`id`),
  CONSTRAINT `FK_Result_Horse` FOREIGN KEY (`horse_id`) REFERENCES `horse` (`id`),
  CONSTRAINT `FK_Result_RaceArchive` FOREIGN KEY (`race_id`) REFERENCES `race_archive` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Результаты состоявшихся забегов';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `result`
--

LOCK TABLES `result` WRITE;
/*!40000 ALTER TABLE `result` DISABLE KEYS */;
INSERT INTO `result` VALUES (21,1,1,3,1),(21,2,2,2,2),(21,3,3,4,3),(21,4,4,1,4),(22,5,1,1,1),(22,6,2,2,2),(22,7,3,3,3),(23,8,1,5,2),(23,9,5,2,5),(23,10,2,1,4),(23,11,3,4,1),(23,12,4,3,3),(24,13,1,2,1),(24,14,2,1,2),(26,4,2,2,3),(26,5,3,1,1),(26,15,1,3,2),(27,2,1,1,1),(27,3,2,2,2),(28,1,4,1,5),(28,4,3,4,4),(28,7,1,2,2),(28,11,5,5,1),(28,13,2,3,3),(29,6,2,3,3),(29,8,1,2,2),(29,14,3,1,1);
/*!40000 ALTER TABLE `result` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id пользователя',
  `email` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Email пользователя',
  `password` binary(41) NOT NULL COMMENT 'Hash значение пароля',
  `first_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Имя пользователя',
  `second_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Фамилия пользователя',
  `role` enum('client','admin','bookmaker') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Роль пользователя',
  `balance` decimal(13,4) NOT NULL COMMENT 'Баланс пользователя',
  `date_of_birth` date NOT NULL COMMENT 'Дата рождения пользователя',
  `passport_series` char(2) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Серия паспорта',
  `passport_id` char(7) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Норер паспорта',
  `is_banned` tinyint(4) NOT NULL COMMENT 'Забанен ли пользователь (высставляется администратором)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Содержит зарегестророванных пользователей';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'mara32@example.net',_binary 'd6eb26f22e5e0d14f3a8b271bba57b8055061ede\0','Tate','Schinner','client',999.0000,'1978-07-04','MC','5574968',1),(2,'keanu.schoen@example.org',_binary 'a74396f67dec3394286ab8ece0e362810eef5b43\0','Ayana','Kuphal','client',956.0000,'1973-07-11','MC','5027969',0),(3,'fhansen@example.net',_binary '9c8e1202e1466dd3c831685d40df99cb44ea87d6\0','Conner','Feest','client',936.0000,'1977-05-30','MC','4940305',0),(4,'angeline85@example.org',_binary 'aa5ed75adf2a08a28f3281873801e710fb87ed3a\0','Jamison','Sanford','client',749.0000,'1979-12-12','MC','1811383',1),(5,'reymundo00@example.org',_binary '63c88324ac9d72dbc58c1799f8403d00953224bc\0','Clementine','Conroy','client',127.0000,'1997-03-27','MC','1104658',0),(6,'sanford.lavina@example.com',_binary '8cc8e2b05e207d0a81bedc62d1ea0beaf3bebf35\0','Krystel','Streich','bookmaker',111.0000,'1995-05-23','MC','3778601',0),(7,'iemard@example.net',_binary 'af6decb2b50087bbd944122ff6838dc05410c90c\0','Janice','Fisher','bookmaker',274.0000,'1984-11-09','MC','5601606',0),(8,'albert.rogahn@example.net',_binary 'd3e587650c852407aa895751af90fa12892f21ef\0','Adelbert','Gleason','client',260.0000,'1977-03-06','MC','3854212',0),(9,'xbernier@example.org',_binary 'f041e4d092cc15ed06666c9f5d1f3587605e509e\0','Kelsie','Huel','admin',955.0000,'1980-06-16','MC','2269633',0),(10,'aanderson@example.net',_binary 'a4238cd5ae267af2015c217edcb7aa3f5f461ac4\0','Samanta','Walter','bookmaker',827.0000,'1991-08-22','MC','6855362',0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_bet`
--

DROP TABLE IF EXISTS `user_bet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_bet` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id пользовательской ставки',
  `user_id` int(10) unsigned NOT NULL COMMENT 'Id пользователя, сделавшего ставку',
  `stake` decimal(13,4) unsigned NOT NULL COMMENT 'Поставленные деньги',
  `odds` decimal(4,3) unsigned DEFAULT NULL COMMENT 'Коэффициет ставки. NULL - значение ещё не известно (ставка определяется перед началом забега).',
  `date_time` datetime NOT NULL COMMENT 'Дата и время ставки',
  PRIMARY KEY (`id`),
  KEY `account_id_idx` (`user_id`),
  CONSTRAINT `FK_ActiveUserBet_User` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Содержит ставки пользователя (возможна экспресс ставка)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_bet`
--

LOCK TABLES `user_bet` WRITE;
/*!40000 ALTER TABLE `user_bet` DISABLE KEYS */;
INSERT INTO `user_bet` VALUES (1,1,818.7600,5.295,'1974-01-12 13:09:10'),(2,8,923.3800,1.750,'1972-09-01 10:10:48'),(3,3,837.1500,1.373,'2010-09-27 17:01:53'),(4,4,831.1700,8.619,'1971-12-02 13:37:38'),(5,5,409.1900,6.501,'2004-10-09 00:30:08'),(8,8,166.9600,8.736,'1985-06-19 06:49:43'),(11,1,337.7400,2.273,'1977-03-15 02:01:02'),(12,2,700.1100,1.550,'2016-11-21 13:56:29'),(13,2,913.5100,1.984,'1990-07-10 12:54:47'),(14,4,996.0900,3.901,'1992-01-27 21:52:46'),(15,5,430.3500,2.150,'2016-04-05 10:38:56'),(18,8,486.9600,3.030,'2008-06-25 02:22:24');
/*!40000 ALTER TABLE `user_bet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_bet_archive`
--

DROP TABLE IF EXISTS `user_bet_archive`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_bet_archive` (
  `id` bigint(20) unsigned NOT NULL COMMENT 'Id ставки',
  `user_id` int(10) unsigned NOT NULL COMMENT 'Id пользователя',
  `status` enum('loss','win','refund') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'Статус ставки',
  `stake` decimal(13,4) unsigned NOT NULL COMMENT 'Поставленные деньги',
  `odds` decimal(4,3) unsigned NOT NULL COMMENT 'Коэффициет ставки',
  `date_time` datetime NOT NULL COMMENT 'Дата и время ставки',
  PRIMARY KEY (`id`),
  KEY `account_id_idx` (`user_id`),
  CONSTRAINT `FK_UserBetArchive_User` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Содержит архив пользовательских ставок (возможна экспресс ставка)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_bet_archive`
--

LOCK TABLES `user_bet_archive` WRITE;
/*!40000 ALTER TABLE `user_bet_archive` DISABLE KEYS */;
INSERT INTO `user_bet_archive` VALUES (10,1,'win',100.5000,6.700,'1993-06-25 02:22:24'),(11,1,'refund',250.0000,5.700,'1993-06-25 02:25:24'),(12,2,'loss',130.0000,7.000,'1994-03-11 01:10:32'),(13,5,'loss',800.0000,3.500,'2004-07-14 05:26:55'),(14,8,'win',1400.0000,2.100,'1995-01-12 15:10:42'),(15,8,'win',50.0000,9.900,'1995-02-11 17:11:32');
/*!40000 ALTER TABLE `user_bet_archive` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-29  2:15:19
