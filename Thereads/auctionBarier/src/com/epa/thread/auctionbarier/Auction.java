package com.epa.thread.auctionbarier;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.CyclicBarrier;

public class Auction {
    private ArrayList<Bid> bids;
    private CyclicBarrier barrier;
    public final int BIDS_NUMBER = 5;

    public Auction() {
        bids = new ArrayList<>();
        barrier = new CyclicBarrier(BIDS_NUMBER, new Runnable() {

            @Override
            public void run() {
                Bid winner = definaWinner();
                System.out.println("Bid a " + winner.getBidId() + ", price:"
                        + winner.getPrice() + " win!");
            }
        });
    }

    public CyclicBarrier getBarier() {
        return barrier;
    }

    public boolean add(Bid e) {
        return bids.add(e);
    }

    public Bid definaWinner() {
        return Collections.max(bids, (b1, b2) -> b1.getPrice() - b2.getPrice());
    }
}
