package com.epam.thread.priority;

public class PriorThread extends Thread {
    public PriorThread(String name) {
        super(name);
    }

    public void run() {
        for (int i = 0; i < 50; i++) {
            System.out.println(getName() + " " + i);
            try {
//                Thread.sleep(0);
                Thread.sleep(1);
//                Thread.sleep(10);
            } catch (InterruptedException exception) {
                System.err.println(exception.getMessage());
            }
        }
    }
}
