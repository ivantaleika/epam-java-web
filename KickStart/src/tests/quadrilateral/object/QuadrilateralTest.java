package tests.quadrilateral.object;

import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.epam.quadrilateral.object.Point;
import com.epam.quadrilateral.object.Quadrilateral;

public class QuadrilateralTest {
    private Random random;
    private static final int RANDOM_RANGE = 1000;

    @BeforeClass
    public void init() {
        random = new Random();
    }

    @DataProvider
    public Object randomQuadrilateralProvider() {
        Point startPoint = new Point(random.nextDouble() * RANDOM_RANGE,
                random.nextDouble() * RANDOM_RANGE);
        Point[] corners = new Point[] { startPoint,
                new Point(-startPoint.getX(), startPoint.getY()),
                new Point(-startPoint.getX(), -startPoint.getY()),
                new Point(startPoint.getX(), -startPoint.getY()) };
        Quadrilateral quadrilateral = new Quadrilateral(corners);
        return new Object[][] { { quadrilateral } };
    }

    @Test(dataProvider = "randomQuadrilateralProvider")
    public void equalsEqualsObjectsTest(Quadrilateral[] quadrilaterals) {
        Assert.assertTrue(quadrilaterals[0]
                .equals(new Quadrilateral(quadrilaterals[0].getCorners())));
    }

    @Test(dataProvider = "randomQuadrilateralProvider")
    public void equalsNotEqualsObjectsTest(Quadrilateral[] quadrilaterals) {
        Point[] corners = quadrilaterals[0].getCorners();
        corners[0].setX(corners[0].getX() + 1);
        Quadrilateral differentQuadrilateral = new Quadrilateral(corners);
        Assert.assertTrue(quadrilaterals[0].equals(differentQuadrilateral));
    }

}
