package tests.quadrilateral.object;

import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.epam.quadrilateral.exception.KSIllegalArgumentException;
import com.epam.quadrilateral.object.Point;
import com.epam.quadrilateral.object.Quadrilateral;
import com.epam.quadrilateral.object.QuadrilateralFactory;

public class QuadrilateralFactoryTest {
    private Random random;
    private QuadrilateralFactory factory;
    private static final int RANDOM_RANGE = 1000;

    @BeforeClass
    public void init() {
        random = new Random();
        factory = new QuadrilateralFactory();
    }

    @DataProvider
    public Object randomPointProvider() {
        return new Object[][] { { new Point(random.nextDouble() * RANDOM_RANGE,
                random.nextDouble() * RANDOM_RANGE) } };
    }

    @Test(expectedExceptions = KSIllegalArgumentException.class,
            dataProvider = "randomPointProvider")
    public void createFigureInvalideDataSizeTest(Point[] points)
            throws KSIllegalArgumentException {
        factory.createFigure(new Point[] { points[0], points[0], points[0] });
    }

    @Test(expectedExceptions = KSIllegalArgumentException.class,
            dataProvider = "randomPointProvider")
    public void createFigureCrossedDataTest(Point[] points)
            throws KSIllegalArgumentException {
        Point[] corners = new Point[] { points[0],
                new Point(-points[0].getX(), -points[0].getY()),
                new Point(-points[0].getX(), points[0].getY()),
                new Point(points[0].getX(), -points[0].getY()) };
        factory.createFigure(corners);
    }

    @Test(expectedExceptions = KSIllegalArgumentException.class,
            dataProvider = "randomPointProvider")
    public void createFigurePointsOnOneLineTest(Point[] points)
            throws KSIllegalArgumentException {
        Point[] corners = new Point[] { points[0],
                new Point(points[0].getX(), points[0].getY() + 1),
                new Point(points[0].getX(), points[0].getY() - 1),
                new Point(-points[0].getX(), -points[0].getY()) };
        factory.createFigure(corners);
    }

    @Test(dataProvider = "randomPointProvider")
    public void createFigureValidDataTest(Point[] points) {
        Point[] corners = new Point[] { points[0],
                new Point(-points[0].getX(), points[0].getY()),
                new Point(-points[0].getX(), -points[0].getY()),
                new Point(points[0].getX(), -points[0].getY()) };
        try {
            Assert.assertEquals(factory.createFigure(corners),
                    new Quadrilateral(corners));
        } catch (KSIllegalArgumentException e) {
            Assert.fail("Invalid corners input");
        }
    }
}
