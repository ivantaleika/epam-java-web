package tests.quadrilateral.object;

import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.epam.quadrilateral.object.Point;

public class PointTest {
    private Random random;
    private static final int RANDOM_RANGE = 1000;

    @BeforeClass
    public void init() {
        random = new Random();
    }

    @DataProvider
    public Object randomPointProvider() {
        return new Object[][] { { new Point(random.nextDouble() * RANDOM_RANGE,
                random.nextDouble() * RANDOM_RANGE) } };
    }

    @Test(dataProvider = "randomPointProvider")
    public void equalsSamePointsTest(Point[] points) {
        Point point1 = new Point(points[0]);
        Assert.assertTrue(points[0].equals(point1));
    }

    @Test(dataProvider = "randomPointProvider")
    public void equalsDifferentPointsTest(Point[] points) {
        Point point1 = new Point(points[0]);
        point1.setX(point1.getX() + 1);
        Assert.assertFalse(points[0].equals(point1));
    }
}
