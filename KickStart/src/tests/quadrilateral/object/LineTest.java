package tests.quadrilateral.object;

import java.util.Random;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.epam.quadrilateral.object.Line;
import com.epam.quadrilateral.object.Point;

public class LineTest {
    private Random random;
    private static final int RANDOM_RANGE = 1000;

    @BeforeClass
    public void init() {
        random = new Random();
    }

    @DataProvider
    public Object randomPointsProvider() {
        return new Object[][] { {
                new Point(random.nextDouble() * RANDOM_RANGE,
                        random.nextDouble() * RANDOM_RANGE),
                new Point(random.nextDouble() * RANDOM_RANGE,
                        random.nextDouble() * RANDOM_RANGE) } };
    }

    @Test(dataProvider = "randomPointsProvider")
    public void equalsSamePointsTest(Point[] points) {
        Line myLine1 = new Line(points[0], points[1]);
        Line myLine2 = new Line(points[0], points[1]);
        Assert.assertTrue(myLine1.equals(myLine2));
    }

    @Test(dataProvider = "randomPointsProvider")
    public void equalsDifferentPointsTest(Point[] points) {
        Line myLine1 = new Line(points[0], points[1]);
        Line myLine2 = new Line(points[0],
                new Point(-points[1].getX(), points[1].getY()));
        Assert.assertFalse(myLine1.equals(myLine2));
    }

    @Test
    public void equalsDifferentPointsSameLineTest() {
        Line myLine1 = new Line(new Point(0, 0), new Point(-1, -1));
        Line myLine2 = new Line(new Point(0, 0), new Point(1, 1));
        Assert.assertTrue(myLine1.equals(myLine2));
    }

    /*
     * False because of infinite number in Line object. 
     */
    @Test
    public void equalsBorderMaxValueSamePointsTest() {
        Line myLine1 = new Line(new Point(Double.MAX_VALUE, Double.MAX_VALUE),
                new Point(-Double.MAX_VALUE, -Double.MAX_VALUE));
        Line myLine2 = new Line(new Point(Double.MAX_VALUE, Double.MAX_VALUE),
                new Point(-Double.MAX_VALUE, -Double.MAX_VALUE));
        Assert.assertFalse(myLine1.equals(myLine2));
    }

    @Test
    public void equalsBorderMinValueSamePointsTest() {
        Line myLine1 = new Line(new Point(Double.MIN_VALUE, Double.MIN_VALUE),
                new Point(-Double.MIN_VALUE, -Double.MIN_VALUE));
        Line myLine2 = new Line(new Point(Double.MIN_VALUE, Double.MIN_VALUE),
                new Point(-Double.MIN_VALUE, -Double.MIN_VALUE));
        Assert.assertTrue(myLine1.equals(myLine2));
    }

    @Test
    public void equalsBorderMinValueDifferentPointsSameLineTest() {
        Line myLine1 = new Line(new Point(0, 0),
                new Point(-Double.MIN_VALUE, -Double.MIN_VALUE));
        Line myLine2 = new Line(new Point(Double.MIN_VALUE, Double.MIN_VALUE),
                new Point(0, 0));
        Assert.assertTrue(myLine1.equals(myLine2));
    }

}
