package tests.quadrilateral.dataprovider;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.epam.quadrilateral.dataprovider.StreamParser;

public class StreamParserTest {
    private StreamParser parser;

    @BeforeClass
    public void init() {
        parser = new StreamParser();
    }

    @Test
    public void parseFiguresInfoTest() {

        List<String> expected = new LinkedList<>();
        expected.add("12;0 13.1;10 14.;10 22.152;15");
        expected.add("12;0000 13.1;10 14.;10 22.152;-10 ");
        expected.add("-12;-0 -13.1;-10 -14.;-10 -22.152;-10 ");
        expected.add("+12;+0 +13.1;+10 +14.;+10 +22.152;+10 ");
        expected.add("0.0;0.0  0.0;0.0  0.0;0.0  0.0;0.0");
        expected.add("     13.;0 13.1;10 14.;10 22.152;-10");
        Stream<String> stream = null;
        try {
            stream = Files.lines(
                    Paths.get("./data/testData/kickStartReaderData.txt"));
        } catch (IOException e) {
            Assert.fail("File opening exception: " + e.getMessage());
        }
        List<String> result = parser.parseFiguresInfo(stream);
        Assert.assertEquals(result, expected);
    }
}
