package tests.quadrilateral.dataprovider;

import static org.testng.Assert.fail;

import java.util.LinkedList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.epam.quadrilateral.dataprovider.QuadrilateralDataImporter;
import com.epam.quadrilateral.exception.KSIOException;
import com.epam.quadrilateral.exception.KSIllegalArgumentException;
import com.epam.quadrilateral.object.Point;
import com.epam.quadrilateral.object.Quadrilateral;
import com.epam.quadrilateral.object.QuadrilateralFactory;

public class QuadrilateralDataImporterTest {
    private QuadrilateralDataImporter quadrilateralDataImporter;
    private String testFilePath;

    @BeforeClass
    public void init() {
        quadrilateralDataImporter = new QuadrilateralDataImporter();
        testFilePath = "./data/testData/kickStartReaderData.txt";
    }

    @Test(expectedExceptions = KSIOException.class)
    public void readElementsNoSuchFileTest() throws KSIOException {
        quadrilateralDataImporter.setFilePath("fileName");
        quadrilateralDataImporter.readElements();
    }

    @Test
    public void readElementsFileTest() {
        quadrilateralDataImporter.setFilePath(testFilePath);
        List<Quadrilateral> expected = new LinkedList<>();
        QuadrilateralFactory quadrilateralFactory = new QuadrilateralFactory();
        try {
            expected.add(quadrilateralFactory.createFigure(
                    new Point[] { new Point(12, 0), new Point(13.1, 10),
                            new Point(14., 10), new Point(22.152, 15) }));
            expected.add(quadrilateralFactory.createFigure(
                    new Point[] { new Point(12, 0), new Point(13.1, 10),
                            new Point(14., 10), new Point(22.152, -10) }));
            expected.add(quadrilateralFactory.createFigure(
                    new Point[] { new Point(13, 0), new Point(13.1, 10),
                            new Point(14., 10), new Point(22.152, -10) }));
        } catch (KSIllegalArgumentException e) {
            fail("Invalide test data  " + e.getMessage());
        }
        List<Quadrilateral> result = null;
        try {
            result = quadrilateralDataImporter.readElements();
        } catch (KSIOException e) {
            fail("Ivalide file " + e.getMessage());
        }
        Assert.assertEquals(result, expected);
    }
}
