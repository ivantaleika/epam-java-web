package tests.quadrilateral.dataprovider;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.epam.quadrilateral.dataprovider.StringParser;

public class StringParserTest {
    private StringParser parser;

    @BeforeClass
    public void init() {
        parser = new StringParser();
    }

    @Test
    public void parsePointsInfoNumbersTest() {
        Map<String, String> result = new HashMap<String, String>();
        result.put("12", "0");
        result.put("13.1", "10");
        result.put("14.", "10");
        result.put("22.152", "10");
        Assert.assertEquals(
                parser.parsePointsInfo("12;0 13.1;10 14.;10 22.152;10\\n"),
                result);
    }

    @Test
    public void parsePointsInfoSymbolsTest() {
        Map<String, String> result = new HashMap<String, String>();
        result.put("12", "0.");
        result.put("1", "10");
        result.put("14.", "10");
        result.put("22.152", "10");
        result.put("22.152", "10");
        result.put("22.152", "10");
        Assert.assertEquals(
                parser.parsePointsInfo(
                        "12;0.foo13.foobar1;10 foo14.;10 22.152;10bar \\n"),
                result);
    }

    @Test
    public void parsePointsInfoInvalideDataTest() {
        Assert.assertEquals(
                parser.parsePointsInfo("12z;0 13.110 14.o;10 22.152;z10\\n"),
                new HashMap<String, String>());
    }
}
