package tests.quadrilateral.dataprovider;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.epam.quadrilateral.dataprovider.KickStartReader;
import com.epam.quadrilateral.exception.KSIOException;

public class KickStartReaderTest {
    class KickStartReaderThrowingKSIOException extends KickStartReader
            implements Assert.ThrowingRunnable {
        String filePath;

        public KickStartReaderThrowingKSIOException(String newFilePath) {
            filePath = newFilePath;
        }

        @Override
        public void run() throws Throwable {
            new KickStartReader().readFileContent(filePath);
        }
    }

    @Test
    public void readFileContentNotExistedFileTest() {
        Assert.assertThrows(KSIOException.class,
                new KickStartReaderThrowingKSIOException(""));
    }

}
