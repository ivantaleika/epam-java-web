package tests.quadrilateral.action;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.epam.quadrilateral.action.QuadrilateralAction;
import com.epam.quadrilateral.exception.KSIllegalArgumentException;
import com.epam.quadrilateral.object.Point;
import com.epam.quadrilateral.object.Quadrilateral;
import com.epam.quadrilateral.object.QuadrilateralFactory;

public class QuadrialteralActionTest {
    private static final double DELTA = 0.001;

    @Test
    public void findAreaSimpleSquareTest() {
        Point[] corners = { new Point(1, 1), new Point(1, -1),
                new Point(-1, -1), new Point(-1, 1) };
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = new QuadrilateralFactory().createFigure(corners);
        } catch (KSIllegalArgumentException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertEquals(QuadrilateralAction.calcArea(quadrilateral), 2 * 2,
                DELTA);
    }

    @Test
    public void findAreaSimpleParallelogramTest() {
        Point[] corners = { new Point(1, 1), new Point(0, -1),
                new Point(-1, -1), new Point(0, 1) };
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = new QuadrilateralFactory().createFigure(corners);
        } catch (KSIllegalArgumentException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertEquals(QuadrilateralAction.calcArea(quadrilateral), 2 * 1,
                DELTA);
    }

    /*
     * Too big area for double number.
     */
    @Test
    public void findAreaMaxAreaTest() {
        Point[] corners = { new Point(Double.MAX_VALUE, Double.MAX_VALUE),
                new Point(Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(-Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(-Double.MAX_VALUE, Double.MAX_VALUE) };
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = new QuadrilateralFactory().createFigure(corners);
        } catch (KSIllegalArgumentException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertEquals(QuadrilateralAction.calcArea(quadrilateral),
                Double.NaN, DELTA);
    }

    /*
     * Too small area for Double.
     */
    @Test
    public void findAreaSmallAreaTest() {
        Point[] corners = { new Point(Double.MIN_VALUE, Double.MIN_VALUE),
                new Point(Double.MIN_VALUE, -Double.MIN_VALUE),
                new Point(-Double.MIN_VALUE, -Double.MIN_VALUE),
                new Point(-Double.MIN_VALUE, Double.MIN_VALUE) };
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = new QuadrilateralFactory().createFigure(corners);
        } catch (KSIllegalArgumentException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertEquals(QuadrilateralAction.calcArea(quadrilateral),
                Double.NaN, DELTA);
    }

    @Test
    public void findPerimeterSimpleSquareTest() {
        Point[] corners = { new Point(1, 1), new Point(1, -1),
                new Point(-1, -1), new Point(-1, 1) };
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = new QuadrilateralFactory().createFigure(corners);
        } catch (KSIllegalArgumentException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertEquals(QuadrilateralAction.calcPerimeter(quadrilateral),
                2 * 4, DELTA);
    }

    @Test
    public void findPerimeterSimpleParallelogramTest() {
        Point[] corners = { new Point(1, 1), new Point(0, -1),
                new Point(-1, -1), new Point(0, 1) };
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = new QuadrilateralFactory().createFigure(corners);
        } catch (KSIllegalArgumentException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertEquals(QuadrilateralAction.calcPerimeter(quadrilateral),
                2 + 2 * Math.sqrt(5), DELTA);
    }

    @Test
    public void findPerimeterMaxPerimeterTest() {
        Point[] corners = { new Point(Double.MAX_VALUE, Double.MAX_VALUE),
                new Point(Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(-Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(-Double.MAX_VALUE, Double.MAX_VALUE) };
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = new QuadrilateralFactory().createFigure(corners);
        } catch (KSIllegalArgumentException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertEquals(QuadrilateralAction.calcPerimeter(quadrilateral),
                Double.MAX_VALUE * 8, DELTA);
    }

    @Test
    public void findPerimeterSmallAreaTest() {
        Point[] corners = { new Point(Double.MIN_VALUE, Double.MIN_VALUE),
                new Point(Double.MIN_VALUE, -Double.MIN_VALUE),
                new Point(-Double.MIN_VALUE, -Double.MIN_VALUE),
                new Point(-Double.MIN_VALUE, Double.MIN_VALUE) };
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = new QuadrilateralFactory().createFigure(corners);
        } catch (KSIllegalArgumentException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertEquals(QuadrilateralAction.calcPerimeter(quadrilateral),
                Double.MIN_VALUE * 8, DELTA);
    }

    @Test
    public void isSquareSimpleSquareTest() {
        Point[] corners = { new Point(1, 1), new Point(1, -1),
                new Point(-1, -1), new Point(-1, 1) };
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = new QuadrilateralFactory().createFigure(corners);
        } catch (KSIllegalArgumentException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertTrue(QuadrilateralAction.isSquare(quadrilateral));
    }

    @Test
    public void isSquareTurnedSquareTest() {
        Point[] corners = { new Point(0, 1), new Point(1, 0),
                new Point(0, -1), new Point(-1, 0) };
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = new QuadrilateralFactory().createFigure(corners);
        } catch (KSIllegalArgumentException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertTrue(QuadrilateralAction.isSquare(quadrilateral));
    }

    @Test
    public void isSquareMaxSquareTest() {
        Point[] corners = { new Point(Double.MAX_VALUE, Double.MAX_VALUE),
                new Point(Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(-Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(-Double.MAX_VALUE, Double.MAX_VALUE) };
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = new QuadrilateralFactory().createFigure(corners);
        } catch (KSIllegalArgumentException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertTrue(QuadrilateralAction.isSquare(quadrilateral));
    }

    @Test
    public void isSquareMinSquareTest() {
        Point[] corners = { new Point(Double.MIN_VALUE, Double.MIN_VALUE),
                new Point(Double.MIN_VALUE, -Double.MIN_VALUE),
                new Point(-Double.MIN_VALUE, -Double.MIN_VALUE),
                new Point(-Double.MIN_VALUE, Double.MIN_VALUE) };
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = new QuadrilateralFactory().createFigure(corners);
        } catch (KSIllegalArgumentException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertTrue(QuadrilateralAction.isSquare(quadrilateral));
    }

    @Test
    public void isRhombusSimpleRhombusTest() {
        Point[] corners = { new Point(0, 2), new Point(1, 0),
                new Point(0, -1), new Point(-2, 0) };
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = new QuadrilateralFactory().createFigure(corners);
        } catch (KSIllegalArgumentException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertTrue(QuadrilateralAction.isRhombus(quadrilateral));
    }

    @Test
    public void isRhombusSquareRhombusTest() {
        Point[] corners = { new Point(0, 1), new Point(1, 0),
                new Point(0, -1), new Point(-1, 0) };
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = new QuadrilateralFactory().createFigure(corners);
        } catch (KSIllegalArgumentException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertTrue(QuadrilateralAction.isRhombus(quadrilateral));
    }

    @Test
    public void isRhombusMaxRhombusTest() {
        Point[] corners = { new Point(Double.MAX_VALUE, Double.MAX_VALUE),
                new Point(Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(-Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(-Double.MAX_VALUE, Double.MAX_VALUE) };
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = new QuadrilateralFactory().createFigure(corners);
        } catch (KSIllegalArgumentException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertTrue(QuadrilateralAction.isRhombus(quadrilateral));
    }

    @Test
    public void isRhombusMinRhombusTest() {
        Point[] corners = { new Point(Double.MIN_VALUE, Double.MIN_VALUE),
                new Point(Double.MIN_VALUE, -Double.MIN_VALUE),
                new Point(-Double.MIN_VALUE, -Double.MIN_VALUE),
                new Point(-Double.MIN_VALUE, Double.MIN_VALUE) };
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = new QuadrilateralFactory().createFigure(corners);
        } catch (KSIllegalArgumentException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertTrue(QuadrilateralAction.isRhombus(quadrilateral));
    }

    @Test
    public void isTrapezeSimpleTrapezeTest() {
        Point[] corners = { new Point(1, -1), new Point(-1, -1),
                new Point(0, 0.5), new Point(0.5, 0.5) };
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = new QuadrilateralFactory().createFigure(corners);
        } catch (KSIllegalArgumentException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertTrue(QuadrilateralAction.isTrapeze(quadrilateral));
    }

    @Test
    public void isTrapezeTurnedTrapezeTest() {
        Point[] corners = { new Point(-1,1), new Point(1, -1),
                new Point(2, 1), new Point(1, 2) };
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = new QuadrilateralFactory().createFigure(corners);
        } catch (KSIllegalArgumentException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertTrue(QuadrilateralAction.isTrapeze(quadrilateral));
    }

    /*
     * Too small numbers for angle.
     */
    @Test
    public void isTrapezeMinTrapezeTest() {
        Point[] corners = { new Point(Double.MIN_VALUE, Double.MIN_VALUE),
                new Point(Double.MIN_VALUE, -Double.MIN_VALUE),
                new Point(-Double.MIN_VALUE, -Double.MIN_VALUE),
                new Point(-Double.MIN_VALUE, Double.MIN_VALUE) };
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = new QuadrilateralFactory().createFigure(corners);
        } catch (KSIllegalArgumentException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertFalse(QuadrilateralAction.isTrapeze(quadrilateral));
    }

    /*
     * Too big numbers for angle.
     */
    @Test
    public void isTrapezeMaxTrapezeTest() {
        Point[] corners = { new Point(Double.MAX_VALUE, Double.MAX_VALUE),
                new Point(Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(-Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(-Double.MAX_VALUE, Double.MAX_VALUE) };
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = new QuadrilateralFactory().createFigure(corners);
        } catch (KSIllegalArgumentException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertFalse(QuadrilateralAction.isTrapeze(quadrilateral));
    }
    @Test
    public void isConvexSimpleSquareTest() {
        Point[] corners = { new Point(1, -1), new Point(-1, -1),
                new Point(-1, 1), new Point(1, 1) };
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = new QuadrilateralFactory().createFigure(corners);
        } catch (KSIllegalArgumentException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertTrue(QuadrilateralAction.isConvex(quadrilateral));
    }
    
    @Test
    public void isConvexConvexFigureTest() {
        Point[] corners = { new Point(1, -1), new Point(-1, -1),
                new Point(-1, 1), new Point(-0.5, -0.5) };
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = new QuadrilateralFactory().createFigure(corners);
        } catch (KSIllegalArgumentException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertFalse(QuadrilateralAction.isConvex(quadrilateral));
    }
    
    /*
     * Too small numbers for angle.
     */
    @Test
    public void isTrapezeMinReflexAngleTest() {
        Point[] corners = { new Point(Double.MAX_VALUE, Double.MAX_VALUE),
                new Point(Double.MAX_VALUE -Double.MIN_VALUE, 0),
                new Point(Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(-Double.MAX_VALUE, -Double.MAX_VALUE) };
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = new QuadrilateralFactory().createFigure(corners);
        } catch (KSIllegalArgumentException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertTrue(QuadrilateralAction.isConvex(quadrilateral));
    }
    
    /*
     * Too big numbers for angle.
     */
    @Test
    public void isTrapezeMaxReflexAngleTest() {
        Point[] corners = { new Point(-Double.MAX_VALUE, 0),
                new Point(Double.MAX_VALUE, -Double.MIN_VALUE),
                new Point(0, 0),
                new Point(Double.MAX_VALUE, Double.MIN_VALUE) };
        Quadrilateral quadrilateral = null;
        try {
            quadrilateral = new QuadrilateralFactory().createFigure(corners);
        } catch (KSIllegalArgumentException e) {
            Assert.fail("Invalide quadrilateral parameters " + e.getMessage());
        }
        Assert.assertTrue(QuadrilateralAction.isConvex(quadrilateral));
    }

}
