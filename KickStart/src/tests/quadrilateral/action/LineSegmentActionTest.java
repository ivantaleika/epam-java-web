package tests.quadrilateral.action;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.epam.quadrilateral.action.LineSegmentAction;
import com.epam.quadrilateral.object.Point;

public class LineSegmentActionTest {

    @Test
    public void findLengthMaxLengthTest() {
        Assert.assertEquals(
                LineSegmentAction.calcLength(
                        new Point(Double.MAX_VALUE, Double.MAX_VALUE),
                        new Point(-Double.MAX_VALUE, -Double.MAX_VALUE)),
                Math.sqrt(Math.pow(Double.MAX_VALUE, 2)
                        + Math.pow(Double.MAX_VALUE, 2)));
    }

    @Test
    public void findLengthZeroLengthTest() {
        Assert.assertEquals(
                LineSegmentAction.calcLength(new Point(0, 0), new Point(0, 0)),
                0.0);
    }

    @Test
    public void findLengthPositiveCoordinatesTest() {
        Assert.assertEquals(
                LineSegmentAction.calcLength(new Point(-1, -3), new Point(-5, -2)),
                Math.sqrt(Math.pow(5 - 1, 2) + Math.pow(3 - 2, 2)));
    }

    @Test
    public void findLengthNegativeCoordinatesTest() {
        Assert.assertEquals(
                LineSegmentAction.calcLength(new Point(-1, -3), new Point(-5, -2)),
                Math.sqrt(Math.pow(-5 + 1, 2) + Math.pow(-3 + 2, 2)));
    }

    @Test
    public void isBetweenMaxPointRangeTest() {
        Assert.assertEquals(LineSegmentAction.isBetween(
                new Point(-Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(Double.MAX_VALUE, Double.MAX_VALUE),
                new Point(1, 1)), true);
    }

    @Test
    public void isBetweenBorderRangePointTest() {
        Assert.assertEquals(LineSegmentAction.isBetween(
                new Point(-Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(Double.MAX_VALUE, Double.MAX_VALUE),
                new Point(Double.MAX_VALUE, Double.MAX_VALUE)), true);
    }

    @Test
    public void isBetweenZeroRangeTest() {
        Assert.assertEquals(LineSegmentAction.isBetween(new Point(0, 0),
                new Point(0, 0), new Point(1, 1)), false);
    }

    @Test
    public void isBetweenZeroRangeBorderPointTest() {
        Assert.assertEquals(LineSegmentAction.isBetween(new Point(0, 0),
                new Point(0, 0), new Point(0, 0)), true);
    }

    @Test
    public void isBetweenNotInRangeTest() {
        Assert.assertEquals(LineSegmentAction.isBetween(new Point(-1, -1),
                new Point(1, 1), new Point(2, 2)), false);
    }
}
