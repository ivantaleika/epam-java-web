package tests.quadrilateral.action;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.epam.quadrilateral.action.Validator;
import com.epam.quadrilateral.object.Point;

public class ValidatorTest {
    private Validator validator = new Validator();

    @BeforeClass
    public void init() {
        validator = new Validator();
    }

    @Test
    public void isCornersFewArgumentsTest() {
        Assert.assertEquals(
                validator.isCorners(new Point[] { new Point(1, 0) }), false);
    }

    @Test
    public void isCornersEqualCornersTest() {
        Assert.assertEquals(
                validator.isCorners(new Point[] { new Point(1, 0),
                        new Point(1, 0), new Point(1, 0), new Point(1, 0) }),
                false);
    }

    @Test
    public void isCornersSimpleFigureTest() {
        Point[] points = new Point[] { new Point(1, 1), new Point(2, 0),
                new Point(-1, -1), new Point(-2, 0) };
        Assert.assertEquals(validator.isCorners(points), true);
    }

    @Test
    public void isCornersMinCornersValueTest() {
        Point[] points = new Point[] {
                new Point(-Double.MIN_VALUE, -Double.MIN_VALUE),
                new Point(-Double.MIN_VALUE, Double.MIN_VALUE),
                new Point(Double.MIN_VALUE, -Double.MIN_VALUE),
                new Point(Double.MIN_VALUE, Double.MIN_VALUE) };
        Assert.assertEquals(validator.isCorners(points), true);
    }

    @Test
    public void isCornersMaxCornersValueTest() {
        Point[] points = new Point[] {
                new Point(-Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(-Double.MAX_VALUE, Double.MAX_VALUE),
                new Point(Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(Double.MAX_VALUE, Double.MAX_VALUE) };
        Assert.assertEquals(validator.isCorners(points), true);
    }

    @Test
    public void isCornersMinFigureAngleTest() {
        Point[] points = new Point[] { new Point(-Double.MAX_VALUE, 0),
                new Point(0, -Double.MIN_VALUE), new Point(Double.MAX_VALUE, 0),
                new Point(0, Double.MIN_VALUE) };
        Assert.assertEquals(validator.isCorners(points), true);
    }

    @Test
    public void isCornersSimpleTriangleTest() {
        Point[] points = new Point[] { new Point(0, 0), new Point(0, -1),
                new Point(0, 2), new Point(1, 1) };
        Assert.assertEquals(validator.isCorners(points), false);
    }

    /*
    * Result is true value because of method can't 
    * work with values close to max Double value.
    */
    @Test
    public void isCornersBigTriangleTest() {
        Point[] points = new Point[] {
                new Point(-Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(0, -Double.MAX_VALUE),
                new Point(Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(0, Double.MAX_VALUE) };
        Assert.assertEquals(validator.isCorners(points), true);
    }

    /*
    * Result is true value because of method can't 
    * work with values close to min Double value.
    */
    @Test
    public void isCornersSmallTriangleTest() {
        Point[] points = new Point[] {
                new Point(-Double.MIN_VALUE, -Double.MIN_VALUE),
                new Point(0, -Double.MIN_VALUE),
                new Point(Double.MIN_VALUE, -Double.MIN_VALUE),
                new Point(0, Double.MIN_VALUE) };
        Assert.assertEquals(validator.isCorners(points), true);
    }

    /*
    * Result is true value because of method can't 
    * work with values close to min Double value.
    */
    @Test
    public void isCornersNarrowTriangleTest() {
        Point[] points = new Point[] { new Point(-Double.MAX_VALUE, 0),
                new Point(0, 0), new Point(Double.MAX_VALUE, 0),
                new Point(0, Double.MIN_VALUE) };
        Assert.assertEquals(validator.isCorners(points), true);
    }

    @Test
    public void isCrossedFigureSamePointsTest() {
        Point[] points = { new Point(1, 0), new Point(1, 0), new Point(1, 0),
                new Point(1, 0) };
        Assert.assertEquals(validator.isCrossedFigure(points), false);
    }

    @Test
    public void isCrossedFigureTriangleTest() {
        Point[] points = { new Point(1, 0), new Point(0, 0), new Point(0, 1) };
        Assert.assertEquals(validator.isCrossedFigure(points), false);
    }

    @Test
    public void isCrossedFigureSquareTest() {
        Point[] points = { new Point(1, 1), new Point(1, -1), new Point(-1, -1),
                new Point(-1, 1) };
        Assert.assertEquals(validator.isCrossedFigure(points), false);
    }

    @DataProvider
    public Object[][] notCrossedPolygons() {
        final int maxPolygonCorners = 10;
        Object[][] data = new Object[maxPolygonCorners][];
        Point[] oldPoints = new Point[0];
        Point[] points;
        for (int i = 0; i < data.length; i++) {
            points = new Point[i + 1];
            points[i] = new Point(i, 2 * i + 1);
            System.arraycopy(oldPoints, 0, points, 0, oldPoints.length);
            data[i] = points;
            oldPoints = points;
        }
        return data;
    }

    @Test(dataProvider = "notCrossedPolygons")
    public void isCrossedFigureNotCrossedPolygonsTest(Point[] points) {
        Assert.assertEquals(validator.isCrossedFigure(points), false);
    }

    @DataProvider
    public Object[][] crossedPolygons() {
        final int maxPolygonCorners = 10;
        final int minPolygonCorners = 3;
        Object[][] data = new Object[(maxPolygonCorners - minPolygonCorners)
                / 2][];
        Point[] oldPoints = new Point[] { new Point(1, 0), new Point(2, 3),
                new Point(2, -3) };
        Point[] points;
        for (int i = minPolygonCorners, j = 0; j < data.length; i += 2, j++) {
            points = new Point[oldPoints.length + 2];
            points[i] = new Point(i, 2 * i + 1);
            points[i + 1] = new Point(i, -2 * i + 1);
            System.arraycopy(oldPoints, 0, points, 0, oldPoints.length);
            data[j] = points;
            oldPoints = points;
        }
        return data;
    }

    @Test(dataProvider = "crossedPolygons")
    public void isCrossedFigureCrossedPolygonsTest(Point[] points) {
        Assert.assertEquals(validator.isCrossedFigure(points), true);
    }

}
