package tests.quadrilateral.action;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.epam.quadrilateral.action.LineAction;
import com.epam.quadrilateral.object.Line;
import com.epam.quadrilateral.object.Point;

public class LineActionTest {

    @Test
    public void findAngleCosParallelTest() {
        double cos = LineAction.calcAngleCos(
                new Line(new Point(0.0, 0.0), new Point(0, 1)),
                new Line(new Point(1.0, 0.0), new Point(1, 1)));
        Assert.assertEquals(cos, Math.cos(0), LineAction.DELTA);
    }

    @Test
    public void findAngleCosPerpendicularTest() {
        double cos = LineAction.calcAngleCos(
                new Line(new Point(0.0, 0.0), new Point(0, 1)),
                new Line(new Point(0.0, 0.0), new Point(1, 0)));
        Assert.assertEquals(cos, Math.cos(Math.PI / 2), LineAction.DELTA);
    }

    @Test
    public void findAngleCosFirstHalfTest() {
        double cos = LineAction.calcAngleCos(
                new Line(new Point(0.0, 0.0), new Point(1, 1)),
                new Line(new Point(0.0, 0.0), new Point(1, 0)));
        Assert.assertEquals(cos, Math.cos(Math.PI / 4), LineAction.DELTA);
    }

    @Test
    public void findAngleCosSecondHalfTest() {
        double cos = LineAction.calcAngleCos(new Line(-Math.sqrt(3), -1.0, 0.0),
                new Line(new Point(0.0, 0.0), new Point(1, 0)));
        Assert.assertEquals(cos, Math.cos(Math.PI - Math.PI * 2 / 3),
                LineAction.DELTA);
    }

    @Test
    public void findAngleCosEqualLinesTest() {
        double cos = LineAction.calcAngleCos(
                new Line(new Point(0.0, 0.0), new Point(1, 0)),
                new Line(new Point(0.0, 0.0), new Point(1, 0)));
        Assert.assertEquals(cos, Math.cos(0), LineAction.DELTA);
    }

    @Test
    public void findIntersectPointParallelTest() {
        Assert.assertEquals(LineAction.findIntersectPoint(new Line(1, -1, 0),
                new Line(1, -1, 2)), null);
    }

    @Test
    public void findIntersectPointSameLineTest() {
        Assert.assertEquals(LineAction.findIntersectPoint(new Line(1, -1, 0),
                new Line(1, -1, 0)), null);
    }

    @Test
    public void findIntersectPointCoordinateCenterIntersectTest() {
        Assert.assertEquals(LineAction.findIntersectPoint(new Line(1, -1, 0),
                new Line(1, 1, 0)), new Point(-0., -0.));
    }

    @Test
    public void findIntersectMaxDoubleIntersectTest() {
        Assert.assertEquals(
                LineAction.findIntersectPoint(new Line(-1, 0, Double.MAX_VALUE),
                        new Line(0, -1, Double.MAX_VALUE)),
                new Point(Double.MAX_VALUE, Double.MAX_VALUE));
    }

    @Test
    public void findIntersectMinDoubleIntersectTest() {
        Assert.assertEquals(
                LineAction.findIntersectPoint(new Line(1, 0, Double.MAX_VALUE),
                        new Line(0, 1, Double.MAX_VALUE)),
                new Point(-Double.MAX_VALUE, -Double.MAX_VALUE));
    }

    @Test
    public void isLineOXLineTest() {
        Assert.assertEquals(LineAction.isPointsOnOneLine(new Point(-1, 0),
                new Point(0, 0), new Point(1, 0)), true);
    }

    @Test
    public void isLineOYLineTest() {
        Assert.assertEquals(LineAction.isPointsOnOneLine(new Point(0, -1),
                new Point(0, 0), new Point(0, 1)), true);
    }

    @Test
    public void isLineThroughStartLineTest() {
        Assert.assertEquals(LineAction.isPointsOnOneLine(new Point(-1, -1),
                new Point(0, 0), new Point(1, 1)), true);
    }

    @Test
    public void isLineRandomLineTest() {
        Assert.assertEquals(LineAction.isPointsOnOneLine(new Point(1, 1),
                new Point(2, 0), new Point(0, 2)), true);
    }

    @Test
    public void isLineEqualPointsTest() {
        Assert.assertEquals(LineAction.isPointsOnOneLine(new Point(1, 1),
                new Point(1, 1), new Point(0, 2)), true);
    }

    /*
    *Result is False value because of method can't 
    *work with values close to max Double value
    */
    @Test
    public void isLineMaxDistaceLineTest() {
        Assert.assertEquals(LineAction.isPointsOnOneLine(
                new Point(-Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(Double.MAX_VALUE, Double.MAX_VALUE), new Point(0, 0)),
                false);
    }

    /*
    *Result is False value because of method can't 
    *work with values close to min Double value
    */
    @Test
    public void isLineMinDistaceLineTest() {
        Assert.assertEquals(
                LineAction.isPointsOnOneLine(new Point(0, -Double.MIN_VALUE),
                        new Point(0, Double.MIN_VALUE), new Point(0, 0)),
                false);
    }

    @Test
    public void isLineSmallDistaceLineTest() {
        Assert.assertEquals(LineAction.isPointsOnOneLine(new Point(0, -0.1),
                new Point(0, 0.1), new Point(0, 0)), true);
    }

    @Test
    public void isLineNotLineTest() {
        Assert.assertEquals(LineAction.isPointsOnOneLine(new Point(-1, 1),
                new Point(1, 1), new Point(0, 0)), false);
    }

    @Test
    public void isLineNotLineMaxDistaceTest() {
        Assert.assertEquals(LineAction.isPointsOnOneLine(
                new Point(-Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(Double.MAX_VALUE, Double.MAX_VALUE),
                new Point(-Double.MAX_VALUE, Double.MAX_VALUE)), false);
    }

    @Test
    public void isLineNotLineMinDistaceTest() {
        Assert.assertEquals(LineAction.isPointsOnOneLine(
                new Point(-Double.MAX_VALUE, -Double.MAX_VALUE),
                new Point(Double.MAX_VALUE, Double.MAX_VALUE),
                new Point(Double.MIN_VALUE, 0.0)), false);
    }

}
