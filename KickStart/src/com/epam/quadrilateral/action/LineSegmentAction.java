package com.epam.quadrilateral.action;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.quadrilateral.object.Point;

/**
 * This class contains various methods for manipulating points or check point's
 * state.
 */
public final class LineSegmentAction {

    /**
     * LineSegmentAction logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Private constructor to protect from creating utility class objects.
     */
    private LineSegmentAction() {
    }

    /**
     * Return length of line segment that is limited my two point.
     *
     * @param  p1 line segment start
     * @param  p2 line segment end
     * @return    length of line segment
     */
    public static double calcLength(final Point p1, final Point p2) {
        double lineSegmentLength = Math.sqrt(Math.pow(p1.getX() - p2.getX(), 2)
                + Math.pow(p1.getY() - p2.getY(), 2));
        return LOGGER.traceExit("length between " + p1 + " " + p2 + " is {}",
                lineSegmentLength);
    }

    /**
     * Check if {@code point} coordinates are in a square where {@code start}
     * and {@code end} points are start and end of diagonal.
     *
     * @param  start square's diagonal first point
     * @param  end   square's diagonal second point
     * @param  point point to be verified
     * @return       {@code true} if point lies between {@code start} and
     *               {@code finish} points, {@code false} otherwise
     */
    public static boolean isBetween(final Point start, final Point end,
            final Point point) {
        double xMax = Double.max(start.getX(), end.getX());
        double xMin = Double.min(start.getX(), end.getX());
        double yMax = Double.max(start.getY(), end.getY());
        double yMin = Double.min(start.getY(), end.getY());
        return LOGGER.traceExit(
                point + " is between {" + start + ", " + end + "} - {}",
                point.getX() <= xMax && point.getX() >= xMin
                        && point.getY() <= yMax && point.getY() >= yMin);

    }

}
