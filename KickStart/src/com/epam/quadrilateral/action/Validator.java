package com.epam.quadrilateral.action;

import com.epam.quadrilateral.object.Line;
import com.epam.quadrilateral.object.Point;

/**
 * Validates inputed geometric figure's data.
 */
public class Validator {

    /**
     * Minimal number of corners for geometric figure.
     */
    public static final int MIN_CORNERS = 3;

    /**
     * Validates if inputed points could be corners of a geometric figure, e.g.
     * if there are no 3 points one line.
     *
     * @param  points array of points that should be validated
     * @return        {@code true} if points could be figure's corners,
     *                {@code false} otherwise
     */
    public boolean isCorners(final Point[] points) {
        if (points.length < MIN_CORNERS) {
            return false;
        }
        for (int i = 0; i < points.length; i++) {
            for (int j = i + 1; j < points.length; j++) {
                for (int k = j + 1; k < points.length; k++) {
                    if (LineAction.isPointsOnOneLine(points[i], points[j],
                            points[k])) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Validates if figure is crossed, i.e. it has a side(s) that crosses at
     * least one other side.
     *
     * @param  points array represents corners of a geometric figure. Points
     *                should be ordered so that figure's sides attach two
     *                adjacent array's points
     * @return        {@code true} if figure isn't crossed, {@code false}
     *                otherwise
     */
    public boolean isCrossedFigure(final Point[] points) {
        if (points.length <= MIN_CORNERS) {
            return false;
        }
        final int notAdjacentLineStartCornerIndent = 2;
        final int notAdjacentLineEndCornerIndent = 3;
        for (int i = 0; i < points.length - 1; i++) {
            for (int j = i + notAdjacentLineStartCornerIndent, k = i
                    + notAdjacentLineEndCornerIndent;; j++, k++) {
                if (k >= points.length) {
                    k = 0;
                }
                if (k == i) {
                    break;
                }
                if (j >= points.length) {
                    j = 0;
                }
                Point point = LineAction.findIntersectPoint(
                        new Line(points[i], points[i + 1]),
                        new Line(points[j], points[k]));
                if (point != null
                        && LineSegmentAction.isBetween(points[i + 1], points[i],
                                point)
                        && LineSegmentAction.isBetween(points[j], points[k],
                                point)) {
                    return true;
                }

            }
        }
        return false;
    }
}
