package com.epam.quadrilateral.action;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.quadrilateral.object.Line;
import com.epam.quadrilateral.object.Point;
import com.epam.quadrilateral.object.Quadrilateral;

/**
 * This class contains various methods for manipulating quadrialterals or check
 * quadrialteral's state.
 */
public final class QuadrilateralAction {
    /**
     * QuadrilateralAction logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();
    /**
     * Max delta between angles calculations.
     */
    public static final double DELTA = 0.001;

    /**
     * Private constructor to protect from creating utility class objects.
     */
    private QuadrilateralAction() {
    }

    /**
     * Returns quadrilateral's perimeter.
     *
     * @param  quadrilateral source quadrilateral
     * @return               quadrilateral's perimeter.
     */
    public static double calcPerimeter(final Quadrilateral quadrilateral) {
        Point[] corners = quadrilateral.getCorners();
        double perimeter = LineSegmentAction.calcLength(corners[0],
                corners[corners.length - 1]);
        for (int i = 0; i < corners.length - 1; i++) {
            perimeter += LineSegmentAction.calcLength(corners[i],
                    corners[i + 1]);
        }
        return LOGGER.traceExit(quadrilateral + " perimeter = {}", perimeter);
    }

    /**
     * Returns quadrilateral's area.
     *
     * @param  quadrilateral source quadrilateral
     * @return               quadrilateral's area.
     */
    public static double calcArea(final Quadrilateral quadrilateral) {
        Point[] corners = quadrilateral.getCorners();
        final int lastCorner = 3;
        double d1 = LineSegmentAction.calcLength(corners[0], corners[2]);
        double d2 = LineSegmentAction.calcLength(corners[1],
                corners[lastCorner]);
        double sin = Math.sqrt(1 - Math
                .pow(LineAction.calcAngleCos(new Line(corners[0], corners[2]),
                        new Line(corners[1], corners[lastCorner])), 2));
        final double coefficient = 0.5;
        return LOGGER.traceExit(quadrilateral + " area = {}",
                coefficient * d1 * d2 * sin);
    }

    /**
     * Check if quadrilateral is a square.
     *
     * @param  quadrilateral source quadrilateral
     * @return               {@code true} if quadrilateral is a square,
     *                       {@code false} otherwise
     */
    public static boolean isSquare(final Quadrilateral quadrilateral) {
        if (!isConvex(quadrilateral)) {
            return false;
        }
        final int lastCorner = 3;
        Point[] corners = quadrilateral.getCorners();
        double d1 = LineSegmentAction.calcLength(corners[0], corners[2]);
        double d2 = LineSegmentAction.calcLength(corners[1],
                corners[lastCorner]);
        double cos = LineAction.calcAngleCos(new Line(corners[0], corners[2]),
                new Line(corners[1], corners[lastCorner]));

        return LOGGER.traceExit(quadrilateral + " is a square = {}",
                d1 == d2 && (Math.abs(cos) < DELTA
                        || Double.compare(cos, Double.NaN) == 0));
    }

    /**
     * Check if quadrilateral is a trapeze.
     *
     * @param  quadrilateral source quadrilateral
     * @return               {@code true} if quadrilateral is a trapeze,
     *                       {@code false} otherwise
     */
    public static boolean isTrapeze(final Quadrilateral quadrilateral) {
        if (!isConvex(quadrilateral)) {
            return false;
        }
        final int lastCorner = 3;
        Point[] corners = quadrilateral.getCorners();
        double cos1 = LineAction.calcAngleCos(new Line(corners[0], corners[1]),
                new Line(corners[2], corners[lastCorner]));
        double cos2 = LineAction.calcAngleCos(new Line(corners[1], corners[2]),
                new Line(corners[lastCorner], corners[0]));
        return LOGGER.traceExit(quadrilateral + " is a trapeze = {}",
                (Math.abs(cos1) > 1.0 - DELTA && Math.abs(cos2) > DELTA)
                        || (Math.abs(cos1) > DELTA
                                && Math.abs(cos2) > 1.0 - DELTA));
    }

    /**
     * Check if quadrilateral is a rhombus.
     *
     * @param  quadrilateral source quadrilateral
     * @return               {@code true} if quadrilateral is a rhombus,
     *                       {@code false} otherwise
     */
    public static boolean isRhombus(final Quadrilateral quadrilateral) {
        if (!isConvex(quadrilateral)) {
            return false;
        }
        final int lastCorner = 3;
        Point[] corners = quadrilateral.getCorners();
        double cos = LineAction.calcAngleCos(new Line(corners[0], corners[2]),
                new Line(corners[1], corners[lastCorner]));
        return LOGGER.traceExit(quadrilateral + " is a rhombus = {}",
                Math.abs(cos) < DELTA || Double.compare(cos, Double.NaN) == 0);
    }

    /**
     * Check if quadrilateral is convex.
     *
     * @param  quadrilateral source quadrilateral
     * @return               {@code true} if quadrilateral is convex,
     *                       {@code false} otherwise
     */
    public static boolean isConvex(final Quadrilateral quadrilateral) {
        Point[] corners = quadrilateral.getCorners();
        final int lastCorner = 3;
        Point point = LineAction.findIntersectPoint(
                new Line(corners[0], corners[1]),
                new Line(corners[2], corners[lastCorner]));
        if (point != null
                && (LineSegmentAction.isBetween(corners[0], corners[1], point)
                        || LineSegmentAction.isBetween(corners[2],
                                corners[lastCorner], point))) {
            return LOGGER.traceExit(quadrilateral + " is convex = {}", false);
        }
        point = LineAction.findIntersectPoint(new Line(corners[1], corners[2]),
                new Line(corners[lastCorner], corners[0]));
        if (point != null
                && (LineSegmentAction.isBetween(corners[0], corners[1], point)
                        || LineSegmentAction.isBetween(corners[2],
                                corners[lastCorner], point))) {
            return LOGGER.traceExit(quadrilateral + " is convex = {}", false);
        }
        return LOGGER.traceExit(quadrilateral + " is convex = {}", true);
    }

}
