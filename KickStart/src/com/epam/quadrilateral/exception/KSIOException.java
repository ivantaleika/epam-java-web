package com.epam.quadrilateral.exception;

/**
 * Represents Input/Output exception class for KickStart project.
 */
public class KSIOException extends KSException {

    /**
     * Auto-generated ID.
     */
    private static final long serialVersionUID = -657943966799502084L;

    /**
     * Constructs an {@code KSIOException} with {@code null} as its error
     * detail message.
     */
    public KSIOException() {
        super();
    }

    /**
     * Constructs an {@code KSIOException} with the specified detail
     * message.
     *
     * @param message The detail message (which is saved for later retrieval by
     *                the {@link #getMessage()} method)
     */
    public KSIOException(final String message) {
        super(message);
    }
}
