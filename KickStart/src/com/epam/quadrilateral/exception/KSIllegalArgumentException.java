package com.epam.quadrilateral.exception;

/**
 * Represents {@code IllegalArgumentException} class for KickStart project.
 */
public class KSIllegalArgumentException extends KSException {

    /**
     * Auto-generated ID.
     */
    private static final long serialVersionUID = -5237273399720671921L;

    /**
     * Constructs an {@code KSIllegalArgumentException} with {@code null} as its
     * error detail message.
     */
    public KSIllegalArgumentException() {
        super();

    }

    /**
     * Constructs an {@code KSIllegalArgumentException} with the specified
     * detail message.
     *
     * @param message The detail message (which is saved for later retrieval by
     *                the {@link #getMessage()} method)
     */
    public KSIllegalArgumentException(final String message) {
        super(message);
    }
}
