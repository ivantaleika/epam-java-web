package com.epam.quadrilateral.exception;

/**
 * Parent class for KickStart exceptions hierarchy.
 */
public class KSException extends Exception {

    /**
     * Auto-generated ID.
     */
    private static final long serialVersionUID = 787617164731343845L;

    /**
     * Constructs an {@code KSException} with {@code null} as its error
     * detail message.
     */
    public KSException() {
        super();
    }

    /**
     * Constructs an {@code KSException} with the specified detail
     * message.
     *
     * @param message The detail message (which is saved for later retrieval by
     *                the {@link #getMessage()} method)
     */
    public KSException(final String message) {
        super(message);
    }
}
