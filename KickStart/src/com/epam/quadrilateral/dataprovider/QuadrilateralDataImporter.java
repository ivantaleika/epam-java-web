package com.epam.quadrilateral.dataprovider;

import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.quadrilateral.exception.KSIOException;
import com.epam.quadrilateral.exception.KSIllegalArgumentException;
import com.epam.quadrilateral.object.Point;
import com.epam.quadrilateral.object.Quadrilateral;
import com.epam.quadrilateral.object.QuadrilateralFactory;

/**
 * The class to perform data import from .txt file.
 */
public class QuadrilateralDataImporter {
    /**
     * .txt file path.
     */
    private String filePath;

    /**
     * Class event logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Simple constructor.
     */
    public QuadrilateralDataImporter() {
    }

    /**
     * Construct Quadrilateral objects with data from file with path
     * {@code filePath}.
     *
     * @return               list of {@link Quadrilateral} objects
     * @throws KSIOException if source file cannot be opened
     */
    public List<Quadrilateral> readElements() throws KSIOException {
        Stream<String> sourceLines = null;
        try {
            sourceLines = new KickStartReader().readFileContent(filePath);
        } catch (KSIOException e) {
            LOGGER.error("Unable to read file: " + e.getMessage());
            /*
             * This just an example of using log4j.
             * It's better to log exception where you can handle it.
             * We can't handle exception here so just rethrow it.
             */
            throw e;
        }
        List<Quadrilateral> quadrilaterals = new LinkedList<>();
        QuadrilateralFactory factory = new QuadrilateralFactory();
        for (String quadrilateralInfo : new StreamParser()
                .parseFiguresInfo(sourceLines)) {
            Set<Entry<String, String>> cornersInfo = new StringParser()
                    .parsePointsInfo(quadrilateralInfo).entrySet();
            if (cornersInfo.size() == Quadrilateral.N_CORNERS) {
                Point[] corners = new Point[Quadrilateral.N_CORNERS];
                int i = 0;
                for (Entry<String, String> pointInfo : cornersInfo) {
                    corners[i] = new Point(
                            Double.parseDouble(pointInfo.getKey()),
                            Double.parseDouble(pointInfo.getValue()));
                    i++;
                }
                try {
                    quadrilaterals.add(factory.createFigure(corners));
                } catch (KSIllegalArgumentException e) {
                    LOGGER.warn("String " + quadrilateralInfo
                            + " doesn't have valid quadrilateral source data:"
                            + e.getMessage());
                }
            } else {
                LOGGER.warn("String " + quadrilateralInfo
                        + " doesn't have valid quadrilateral source data:"
                        + "Quadrilateral should have " + Quadrilateral.N_CORNERS
                        + " unique corners, found " + cornersInfo.size());

            }
        }
        return LOGGER.traceExit("Following quadrilaterals was created: {}",
                quadrilaterals);
    }

    /**
     * Returns input file path.
     *
     * @return input file path
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * Set input file path.
     *
     * @param newFilePath new input file path
     */
    public void setFilePath(final String newFilePath) {
        filePath = newFilePath;
    }

}
