package com.epam.quadrilateral.dataprovider;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class performs source data stream parsing.
 */
public class StreamParser {

    /**
     * Regex to eject strings with quadrilateral's data.
     */
    public static final String QUADRILATERAL_REGEX =
            "(((([ \\t]+)?(([+-]?\\d+)\\.?(\\d+)?);"
            + "(([+-]?\\d+)\\.?(\\d+)?))){4}([ \\t]+)?\\n?)";

    /**
     * StreamParser logger.
     */
    private static final Logger LOGGER = LogManager.getLogger();

    /**
     * Collect and returns list of strings that represents quadrilateral's
     * parameters from source {@code String} stream.
     *
     * @param  stream source stream
     * @return        list of strings that represents quadrilateral's parameters
     */
    public List<String> parseFiguresInfo(final Stream<String> stream) {
        return stream.filter(s -> filterFunction(s))
                .collect(Collectors.toList());
    }

    /**
     * Check if string match {@code QUADRILATERAL_REGEX} pattern, warn if it's
     * not.
     *
     * @param  s source {@code String}
     * @return   {@code true} if string match the regex, {@code false} otherwise
     */
    private boolean filterFunction(final String s) {
        if (s.matches(QUADRILATERAL_REGEX)) {
            return true;
        }
        LOGGER.warn("{} isn't a quadrilateral's parameters string", s);
        return false;
    }
}
