/**
 * Contains classes that perform reading and validation of source data.
 */
package com.epam.quadrilateral.dataprovider;
