package com.epam.quadrilateral.dataprovider;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import com.epam.quadrilateral.exception.KSIOException;
/**
 * Simple file reader.
 */
public class KickStartReader {
    /**
     * Returns {@code Stream<String>} from file's lines.
     *
     * @param  filePath      file path
     * @return               stream of file's data
     * @throws KSIOException if file cannot be opened
     */
    public Stream<String> readFileContent(final String filePath)
            throws KSIOException {
        try {
            return Files.lines(Paths.get(filePath));
        } catch (IOException e) {
            throw new KSIOException(e.getMessage());
        }
    }
}
