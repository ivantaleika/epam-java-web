package com.epam.quadrilateral.dataprovider;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class performs parsing of strings with data about geometric objects.
 */
public class StringParser {
    /**
     * Regex to eject point's data.
     */
    public static final String POINT_REGEX = "((([\\+-]?[0-9]+)\\.?([0-9]+)?);"
            + "(([\\+-]?[0-9]+)\\.?([0-9]+)?))";
    /**
     * Regex for divide x and y coordinates.
     */
    public static final String SEPARATOR_REGEX = ";";

    /**
     * Searches for points info in the source string. Returns this info as a map
     * of point's coordinates.
     *
     * @param  figureInfo string containing figure's info
     * @return            map in witch key is a point x coordinate and value is
     *                    a point y coordinate
     */
    public Map<String, String> parsePointsInfo(final String figureInfo) {
        Pattern pattern = Pattern.compile(POINT_REGEX);
        Matcher matcher = pattern.matcher(figureInfo);
        Map<String, String> points = new LinkedHashMap<>();
        while (matcher.find()) {
            String[] coordinates = matcher.group(1).split(SEPARATOR_REGEX);
            points.put(coordinates[0], coordinates[1]);
        }
        return points;
    }

}
