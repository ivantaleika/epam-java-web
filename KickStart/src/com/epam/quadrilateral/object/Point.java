package com.epam.quadrilateral.object;

/**
 * The class that represents a point.
 */
public class Point {
    /**
     * x point's coordinate.
     */
    private double x;
    /**
     * y point's coordinate.
     */
    private double y;

    /**
     * Constructs point from it's x and y coordinates.
     *
     * @param xCoordinate point's x coordinate
     * @param yCoordinate point's x coordinate
     */
    public Point(final double xCoordinate, final double yCoordinate) {
        x = xCoordinate;
        y = yCoordinate;
    }

    /**
     * Constructs point from another point.
     *
     * @param other source point
     */
    public Point(final Point other) {
        x = other.x;
        y = other.y;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "(x: " + x + ", y: " + y + ")";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int magicNumber = 32;
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(x);
        result = prime * result + (int) (temp ^ (temp >>> magicNumber));
        temp = Double.doubleToLongBits(y);
        result = prime * result + (int) (temp ^ (temp >>> magicNumber));
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Point other = (Point) obj;
        if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x)) {
            return false;
        }
        if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y)) {
            return false;
        }
        return true;
    }

    /**
     * Returns x coordinate.
     *
     * @return x coordinate
     */
    public double getX() {
        return x;
    }

    /**
     * Set x coordinate.
     *
     * @param newX new coordinate
     */
    public void setX(final double newX) {
        x = newX;
    }

    /**
     * Returns x coordinate.
     *
     * @return y coordinate
     */
    public double getY() {
        return y;
    }

    /**
     * Set y coordinate.
     *
     * @param newY new coordinate
     */
    public void setY(final double newY) {
        y = newY;
    }

}
