package com.epam.quadrilateral.object;

import java.util.Arrays;

/**
 * The class that represents a quadrilateral.
 */
public class Quadrilateral {
    /**
     * is number of quadrilateral's corners.
     */
    public static final int N_CORNERS = 4;
    /**
     * Quadrilateral's corners. Points are ordered so that figure's sides attach
     * two adjacent array's points
     */
    private Point[] corners;

    /**
     * Constructs quadrilateral from it's 4 corners.
     *
     * @param newCorners quadrilateral's corners
     */
    public Quadrilateral(final Point[] newCorners) {
        setCorners(newCorners);
    }

    /**
     * Returns copy of quadrilateral's corners array.
     *
     * @return corners array
     */
    public Point[] getCorners() {
        return Arrays.copyOf(corners, N_CORNERS);
    }

    /**
     * Returns point number i in the corner's array.
     *
     * @param  i corner's number
     * @return   copy of corner's point
     */
    public Point getCorner(final int i) {
        return new Point(corners[i]);
    }

    /**
     * Set quadrilateral's corners.
     *
     * @param newCorners new quadrilateral's corners
     */
    public void setCorners(final Point[] newCorners) {
        this.corners = Arrays.copyOf(newCorners, N_CORNERS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Quadrilateral: " + Arrays.toString(corners);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Arrays.hashCode(corners);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Quadrilateral other = (Quadrilateral) obj;
        if (!Arrays.equals(corners, other.corners)) {
            return false;
        }
        return true;
    }
}
