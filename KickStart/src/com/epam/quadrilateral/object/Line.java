package com.epam.quadrilateral.object;

/**
 * The class that represents a line as a equation a*x + b*y + c = 0.
 */
public class Line {

    /**
     * coefficient for x.
     */
    private double a;

    /**
     * coefficient for y.
     */
    private double b;

    /**
     * free coefficient.
     */
    private double c;

    /**
     * Construct line from coefficients.
     *
     * @param aCoefficient coefficient for x
     * @param bCoefficient coefficient for y
     * @param cCoefficient free coefficient
     */
    public Line(final double aCoefficient, final double bCoefficient,
            final double cCoefficient) {
        setA(aCoefficient);
        setB(bCoefficient);
        setC(cCoefficient);
    }

    /**
     * Construct line from 2 points.
     *
     * @param p1 first point
     * @param p2 second point
     */
    public Line(final Point p1, final Point p2) {
        a = p2.getY() - p1.getY();
        b = p1.getX() - p2.getX();
        c = p2.getX() * p1.getY() - p2.getY() * p1.getX();
    }

    /**
     * Returns coefficient for x.
     *
     * @return coefficient for x
     */
    public double getA() {
        return a;
    }

    /**
     * Sets coefficient for x.
     *
     * @param aCoefficient new x coefficient
     */
    public void setA(final double aCoefficient) {
        a = aCoefficient;
    }

    /**
     * Returns coefficient for y.
     *
     * @return coefficient for y
     */
    public double getB() {
        return b;
    }

    /**
     * Set coefficient for y.
     *
     * @param bCoefficient new y coefficient
     */
    public void setB(final double bCoefficient) {
        b = bCoefficient;
    }

    /**
     * Returns free coefficient.
     *
     * @return free coefficient
     */
    public double getC() {
        return c;
    }

    /**
     * Set free coefficient.
     *
     * @param cCoefficient new free coefficient
     */
    public void setC(final double cCoefficient) {
        c = cCoefficient;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int magicNumber = 32;
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(a);
        result = prime * result + (int) (temp ^ (temp >>> magicNumber));
        temp = Double.doubleToLongBits(b);
        result = prime * result + (int) (temp ^ (temp >>> magicNumber));
        temp = Double.doubleToLongBits(c);
        result = prime * result + (int) (temp ^ (temp >>> magicNumber));
        return result;
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     *
     * <p>
     * Note that two different equations could represent the same line.
     *
     * @return {@code true} if objects represent the same line, {@code false}
     *         otherwise
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Line other = (Line) obj;
        Double fisrtCoeffitientAbs = Math
                .abs(getCramerCoeffitient(a, b, other.a, other.b));
        Double secondCoeffitientAbs = Math
                .abs(getCramerCoeffitient(a, c, other.a, other.c));
        Double thirdCoeffitientAbs = Math
                .abs(getCramerCoeffitient(b, c, other.b, other.c));
        return fisrtCoeffitientAbs.compareTo(0.) == 0
                && secondCoeffitientAbs.compareTo(0.) == 0
                && thirdCoeffitientAbs.compareTo(0.) == 0;
    }

    /**
     * Calculates Cramer's coefficient for matrix [x1,y1] [x2,y2].
     *
     * @param  x1 [0][0] matrix number
     * @param  y1 [0][1] matrix number
     * @param  x2 [1][0] matrix number
     * @param  y2 [1][1] matrix number
     * @return    Cramer's coefficient
     */
    private Double getCramerCoeffitient(final double x1, final double y1,
            final double x2, final double y2) {
        return x1 * y2 - y1 * x2;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        String string = a + " * x";
        if (b > 0) {
            string += " + ";
        } else {
            string += " - ";
        }
        string += Math.abs(b) + " * y";
        if (c > 0) {
            string += " + ";
        } else {
            string += " - ";
        }
        string += Math.abs(c) + " = 0";
        return string;
    }
}
