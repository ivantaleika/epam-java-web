package com.epam.quadrilateral.object;

import com.epam.quadrilateral.action.Validator;
import com.epam.quadrilateral.exception.KSIllegalArgumentException;

/**
 * Factory for {@link Quadrilateral} class.
 */
public class QuadrilateralFactory {
    /**
     * Figure validator.
     */
    private Validator validator;

    /**
     * Simple factory constructor.
     */
    public QuadrilateralFactory() {
        validator = new Validator();
    }

    /**
     * Creates quadrilateral from it's corners points.
     *
     * @param  corners                    quadrilateral's corners
     * @return                            {@link Quadrilateral} object
     * @throws KSIllegalArgumentException if corners fail the validation
     */
    public Quadrilateral createFigure(final Point[] corners)
            throws KSIllegalArgumentException {
        checkQuadrilateralData(corners);
        return new Quadrilateral(corners);
    }

    /**
     * Specific validation for quadrilateral corners.
     *
     * @param  newCorners                 input corners
     * @throws KSIllegalArgumentException if corners fail the validation
     */
    private void checkQuadrilateralData(final Point[] newCorners)
            throws KSIllegalArgumentException {
        if (newCorners.length != Quadrilateral.N_CORNERS) {
            throw new KSIllegalArgumentException(
                    "Quadrilateral should have " + Quadrilateral.N_CORNERS
                            + " unique corners, found" + newCorners.length);
        }
        if (!validator.isCorners(newCorners)) {
            throw new KSIllegalArgumentException(
                    "3 or more corners are on the same line.");
        }
        if (validator.isCrossedFigure(newCorners)) {
            throw new KSIllegalArgumentException(
                    "Cannot create crossed quadrilateral.");
        }
    }
}
